import * as bcrypt from 'bcryptjs';

const generateHash = async (input) => {
  const SALT_ROUNDS = 10;
  return await bcrypt.hash(input, SALT_ROUNDS);
};

const compareHash = async (plain, hash) => {
  return await bcrypt.compare(plain, hash);
};

export {
  compareHash,
  generateHash,
};
