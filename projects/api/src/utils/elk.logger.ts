export enum LogLevel {
    ERROR = 'ERROR',
    WARN = 'WARN',
    INFO = 'INFO',
    DEBUG = 'DEBUG',
  }
  export interface FieldArguments {
    level: LogLevel;
    domain: string;
    action: string;
    user: string;
    data?: any;
  }
  declare interface LogFields extends FieldArguments {
    readonly timestamp: number;
  }
  
  const DELIMITER: string = ':';
  
  export const log = (args: FieldArguments): void => {
  
    const timestamp: number = new Date().valueOf();
  
    args.data = JSON.stringify(args.data || {});
  
    const fields: LogFields = {
        timestamp,
        ...args,
    };
  
    //  ensures order without args being a Map (order is important for field extraction)
    const logString = 
        `${fields.timestamp}${DELIMITER}` +
        `${fields.level}${DELIMITER}` +
        `${fields.domain}${DELIMITER}` +
        `${fields.action}${DELIMITER}` +
        `${fields.user}${DELIMITER}` +
        `${fields.data}`
    ;
  
    switch (args.level) {
        case LogLevel.ERROR: console.error(logString); break;
        case LogLevel.WARN: console.warn(logString); break;
        case LogLevel.INFO: console.info(logString); break;
        case LogLevel.DEBUG: console.debug(logString);
    }
  }