import { LOG_ENABLED } from '@utils/env';

export default {
  error: LOG_ENABLED ? console.error : () => null,
  // tslint:disable-next-line:no-console
  log: LOG_ENABLED ? console.log : () => null,
  warn: LOG_ENABLED ? console.warn : () => null,
};
