export const API_ENV = process.env.API_ENV;
export const API_URL = process.env.API_URL;
export const APP_URL = process.env.APP_URL;
export const AWS_COGNITO_USER_POOL_CLIENT_ID = process.env.AWS_COGNITO_USER_POOL_CLIENT_ID;
export const AWS_COGNITO_USER_POOL_ID = process.env.AWS_COGNITO_USER_POOL_ID;
export const DB_HOST = process.env.DB_HOST;
export const DB_NAME = process.env.DB_NAME;
export const DB_PASS = process.env.DB_PASS;
export const DB_USER = process.env.DB_USER;
export const LOG_ENABLED = process.env.LOG_ENABLED;
export const POSTCODE_API_HOST = process.env.POSTCODE_API_HOST;
export const POSTCODE_API_KEY = process.env.POSTCODE_API_KEY;
export const SEQUELIZE_LOG_ENABLED = process.env.SEQUELIZE_LOG_ENABLED;
export const AWS_SES_ACCESS_KEY = process.env.AWS_SES_ACCESS_KEY;
export const AWS_SES_SECRET_KEY = process.env.AWS_SES_SECRET_KEY;
export const MAIL_TRANSPORT = process.env.MAIL_TRANSPORT;
export const MAIL_FROM = process.env.MAIL_FROM;
export const MAIL_ADMIN = process.env.MAIL_ADMIN;
export const AWS_S3_PROFILE_PICTURE_BUCKET = process.env.AWS_S3_PROFILE_PICTURE_BUCKET;
export const AWS_COGNITO_IDENTITY_POOL_ID = process.env.AWS_COGNITO_IDENTITY_POOL_ID;
export const AWS_REGION = process.env.AWS_REGION;
function validateEnv(): void {
  const envParams = {
    API_ENV,
    API_URL,
    APP_URL,
    AWS_SES_ACCESS_KEY,
    AWS_SES_SECRET_KEY,

    AWS_COGNITO_USER_POOL_CLIENT_ID,
    AWS_COGNITO_USER_POOL_ID,
    AWS_COGNITO_IDENTITY_POOL_ID,
    AWS_S3_PROFILE_PICTURE_BUCKET,
    AWS_REGION,
    DB_HOST,
    DB_NAME,
    DB_PASS,
    DB_USER,
    LOG_ENABLED,
    POSTCODE_API_HOST,
    POSTCODE_API_KEY,
    SEQUELIZE_LOG_ENABLED,

    MAIL_FROM,

    MAIL_ADMIN,

    MAIL_TRANSPORT,
  };
  const missingParams: string[] = [];
  const envKeys = Object.keys(envParams);
  envKeys.forEach(param => {
    if (!envParams[param] && envParams[param] !== '' && envParams[param] !== false) {
      missingParams.push(param);
    }
  });
  if (missingParams.length) {
    const errorMsg = `Missing environment parameters: ${missingParams.join(', ')}`;
    console.error(errorMsg);
    throw Error(errorMsg);
  }
}

validateEnv();
