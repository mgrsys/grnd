import * as handlebars from 'handlebars';
import * as nodemailer from 'nodemailer';
import * as mockTransport from 'nodemailer-mock-transport';
import * as sesTransport from 'nodemailer-ses-transport';

import {
  AWS_SES_ACCESS_KEY,
  AWS_SES_SECRET_KEY,
  MAIL_ADMIN,
  MAIL_FROM,
  MAIL_TRANSPORT,
} from '@utils/env';

export interface IMailPacketInterface {
  from?: string;
  html?: string;
  subject: string;
  text?: string;
  to?: string;
}

const fromDefault: string = MAIL_FROM;
const toDefault: string = MAIL_ADMIN;

function compileTransporter() {
  let transporter;
  let transport;
  // Set Mailer
  switch (String(MAIL_TRANSPORT).toLowerCase()) {
    case 'ses': {
      transport = sesTransport({
        accessKeyId: AWS_SES_ACCESS_KEY,
        region: 'us-east-1',
        secretAccessKey: AWS_SES_SECRET_KEY,
      });
      transporter = nodemailer.createTransport(transport);
      break;
    }
    default: {
      transport = mockTransport();
      transporter = nodemailer.createTransport(transport);
      break;
    }
  }
  return { transporter, transport };
}

export function compileTemplate(source: string, data: any): string {
  const template = handlebars.compile(source);
  const result = template(data);
  return result;
}

export async function sendMail(mailPacket: IMailPacketInterface): Promise<any> {
  if (!mailPacket.from) mailPacket.from = fromDefault;
  if (!mailPacket.to) mailPacket.to = toDefault;
  if (!mailPacket.html && !mailPacket.text) {
    throw Error('Mailer service: must provide either text or html in mailPacket');
  }
  const { transporter } = compileTransporter();
  const result = await transporter.sendMail(mailPacket);
  return result;
}
