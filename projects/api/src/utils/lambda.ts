import * as jwtd from 'jwt-decode';
import * as _ from 'lodash';

export const getBody = (event) => {
  if (event && typeof event.body === 'string') {
    let parsed = {};
    try {
      parsed = JSON.parse(event.body);
      return parsed;
    } catch (e) {
      return {};
    }
  }
  if (event && typeof event.body === 'object') {
    return event.body;
  }
  return {};
};

export const getQuery = (event) => {
  if (event && _.isObject(event.queryStringParameters) && !_.isEmpty(event.queryStringParameters)) {
    return event.queryStringParameters || {};
  }
  return {};
};

export const getParams = (event) => {
  if (event && _.isObject(event.pathParameters) && !_.isEmpty(event.pathParameters)) {
    return event.pathParameters;
  }
  return {};
};

export const response = (code, data) => {
  let responseData = data;
  let errorMsg = {};
  if (code < 200 || code > 300) {
    errorMsg = data;
    responseData = {};
  }
  const body = {
    code,
    data: responseData,
    error: errorMsg,
  };
  return {
    body: JSON.stringify(body),
    headers: {
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Access-Control-Allow-Origin': '*',
    },
    statusCode: code,
  };
};

export const error = (code, err) => {
  const rspCode = code ? code : 500;
  const rspError = code ? err : 'Internal Server Error';
  return response(rspCode, rspError);
};

export const callback = cb => rsp => cb(null, rsp);

export const init = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
};

export const getAuthorization = (event) => {
  const headers = event && event.headers;
  if (headers) {
    return event.headers.authorization || event.headers.Authorization;
  }
  return null;
};

export const getCognitoUser = (event) => {
  const authorization = getAuthorization(event);
  try {
    const token = authorization.split('Bearer')[1];
    const data = jwtd(token);
    return data || null;
  } catch (e) {
    const msg = 'Unable to parse token from header';
    console.warn(msg, e, event);
    throw Error(msg);
  }
};

export const getCognitoId = (event) => {
  const authorization = getAuthorization(event);
  try {
    const token = authorization.split('Bearer')[1];
    const data = jwtd(token);
    return data.sub || null;
  } catch (e) {
    return null;
  }
};
