import 'isomorphic-fetch';

import { POSTCODE_API_HOST, POSTCODE_API_KEY } from '@utils/env';
import logger from '@utils/logger';

const API_METHOD_GET = 'GET';

function apiLog(title = 'apiLog', request?) {
  logger.log(...arguments);
}

function getUrl(endpoint, params?) {
  return POSTCODE_API_HOST + '/' + endpoint; // TODO: Add params here
}

function apiCall(method, endpoint, params = {}, data = {}) {
  if (POSTCODE_API_KEY) {
    const url = getUrl(endpoint, params);
    const headers = { 'auth-key': POSTCODE_API_KEY };
    const request = {
      data,
      headers,
      method,
      url,
    };
    apiLog('Fetch Request', request);
    return fetch(url, {
      body: JSON.stringify(data),
      headers,
      method,
    }).then((response: any) => {
      apiLog('Fetch Response', response);
      return response.json();
    });
  }
  throw { code: 400, message: 'An Auspost API Key is required' };
}

export const suburbSearch = name => {
  return apiCall(API_METHOD_GET, `postcode/search.json?q=${name}`, null, null);
};
