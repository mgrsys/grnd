export const asyncHandler =
  fn =>
    (req, res, next) =>
      Promise.resolve(fn(req, res, next)).catch(next);

export const asyncForEach = async (array, callback) => {
  // tslint:disable-next-line:no-increment-decrement
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};
