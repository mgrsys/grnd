export enum LogLevel {
    ERROR = 'ERROR',
    WARN = 'WARN',
    INFO = 'INFO',
    DEBUG = 'DEBUG',
    SUCCESS = 'SUCCESS',

}
const DELIMITER: string = ':';

class Logger {
    
    private lastLogTime: number;
    private mt: number;
    private currentLevel: LogLevel;

    private domain: string;
    private action: string;
    private userData: string;
    private messageData: string;
    private data: any;


    constructor(domain: string, action: string) {
        this.domain = domain;
        this.action = action;
        this.lastLogTime = 0;
        this.mt = null;
        this.currentLevel = LogLevel.INFO;
        this.userData = null;
        this.messageData = null;
        this.data = {};
    }

    public withMicrotime(): Logger {
        
        //  Set the current time
        const rightNow: number = new Date().valueOf();
        //  Find the difference between now and the last log time
        const microtime: number = rightNow - this.lastLogTime;
        //  Set the difference to be the microtime
        this.mt = microtime;
        if(this.lastLogTime === 0) this.mt = 0;
        this.lastLogTime = rightNow;        
        return this;
    }
    public withoutMicrotime(): Logger {
        this.mt = null;
        return this;
    }
    public user(user: string): Logger {
        this.userData = user;
        return this;
    }
    public message(message: string): Logger {
        this.messageData = message;
        return this;
    }
    public withData(data: any): Logger {
        this.data = data;
        return this;
    }
    public error(): Logger {
        this.currentLevel = LogLevel.ERROR;
        return this;
    }
    public warn(): Logger {
        this.currentLevel = LogLevel.WARN;
        return this;
    }
    public info(): Logger {
        this.currentLevel = LogLevel.INFO;
        return this;
    }
    public debug(): Logger {
        this.currentLevel = LogLevel.DEBUG;
        return this;
    }
    public success(): Logger {
        this.currentLevel = LogLevel.SUCCESS;
        return this;
    }
    private reset() {
        this.mt = null;
        this.lastLogTime = new Date().valueOf();
    }
    public send(): void {

        const timestamp: number = new Date().valueOf();
        
        const data: any = { ...this.data };
        if(this.mt !== null) data.microtime = this.mt;
        if(this.messageData) data.message = this.messageData;

        //  ensures order without args being a Map (order is important for field extraction)
        const logString: string =
            `${timestamp}${DELIMITER}` +
            `${this.currentLevel}${DELIMITER}` +
            `${this.domain}${DELIMITER}` +
            `${this.action}${DELIMITER}` +
            `${this.userData}${DELIMITER}` +
            `${JSON.stringify(data)}`
        ;
    
        switch (this.currentLevel) {
            case LogLevel.ERROR: console.error(logString); break;
            case LogLevel.WARN: console.warn(logString); break;
            case LogLevel.INFO: console.info(logString); break;
            case LogLevel.DEBUG: console.debug(logString); break;
            case LogLevel.SUCCESS: console.info(logString);
        }
        this.reset();
    }
}

export default Logger;
