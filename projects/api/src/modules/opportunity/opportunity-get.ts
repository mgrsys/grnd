import { APIGatewayEvent, Callback, Context } from 'aws-lambda';

import * as _ from 'lodash';
import * as Sequelize from 'sequelize';

import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import { StudentSchool } from '@modules/student/model/student-school.schema';

import * as opportunity from '@modules/opportunity/opportunity.service';
import * as user from '@modules/user/user.service';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

const Op = Sequelize.Op;

function ensureQueryData(query) {
  const errors: any = {};
  if (query.title && query.title.length < 3) {
    errors.title = 'You must enter at least 3 characters';
  }
  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }
  return true;
}

function getStudentSchools(studentModel) {
  const studentId = studentModel.get('id');
  return StudentSchool.findOne({
    where: {
      studentId,
    },
  })
    .then((results) => {
      return results.get('schoolId');
    });
}

function findStudentOpportunities(query, userModel) {
  let where = null;
  if (query.title) {
    where = { title: { [Op.iLike]: `%${query.title}%` } };
  }

  logger.log('Getting student opportunities');
  return user.isStudent(userModel)
    .then((student) => getStudentSchools(student))
    .then((schoolId) => {
      return Opportunity.findAll(opportunity.query(where, [], schoolId));
    }).catch(e => {
      logger.log('findStudentOpportunities', e);
      throw { code: 500, message: 'Internal Server Error' };
    });
}

function findOrganisationOpportunities(query, userModel) {
  let where = null;
  if (query.title) {
    where = { title: { [Op.iLike]: `%${query.title}%` } };
  }

  logger.log('Getting organisation opportunities');
  return user.isOrganisation(userModel)
    .then(organisation => {
      const orgWheres = {
        organisationId: organisation.get('id'),
      };
      const wheres = Object.assign({}, where, orgWheres);
      return Opportunity.findAll(opportunity.query(wheres, null));
    })
    .catch(e => {
      logger.log('findOrganisationOpportunities', e);
      throw { code: 500, message: 'Internal Server Error' };
    });
}

function findSchoolOpportunities(query, userModel) {
  let where = null;
  if (query.title) {
    where = { title: { [Op.iLike]: `%${query.title}%` } };
  }

  logger.log('Getting school opportunities');
  return user.isSchool(userModel)
    .then(schoolModel => {
      const wheres = Object.assign({}, where);
      return Opportunity.findAll(opportunity.query(wheres, null, schoolModel.get('id')));
    })
    .catch(e => {
      logger.log('findSchoolOpportunities', e);
      throw { code: 500, message: 'Internal Server Error' };
    });
}

function findOpportunities(role, query, userModel) {
  switch (role) {
    case 'student':
      return findStudentOpportunities(query, userModel);
    case 'organisation':
      return findOrganisationOpportunities(query, userModel);
    case 'school':
      return findSchoolOpportunities(query, userModel);
    default: throw { code: 403, message: 'Invalid User Role' };
  }
}

function getOpportunities(query, cognitoId, role) {
  let userModel = null;
  return new Promise((resolve) => {
    Promise.resolve()
      .then(() => {
        return user.ensureValidUser(cognitoId);
      })
      .then((result) => {
        userModel = result;
        return ensureQueryData(query);
      })
      .then(() => {
        return findOpportunities(role, query, userModel);
      })
      .then((results: any) => {
        return resolve(lambda.response(200, results));
      })
      .catch(e => {
        logger.log('getOpportunities-e', e);
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event: APIGatewayEvent, context: Context, callback: Callback) => {
  lambda.init(event, context);
  const query = lambda.getQuery(event);
  const cognitoUser = lambda.getCognitoUser(event);
  if (!cognitoUser) {
    const msg = 'Unable to parse user from event';
    console.warn(msg, event);
    callback(msg);
  }

  const cognitoId = _.get(cognitoUser, 'sub', null);
  const role = _.get(cognitoUser, 'custom:role', null);
  getOpportunities(query, cognitoId, role).then(lambda.callback(callback));
};
