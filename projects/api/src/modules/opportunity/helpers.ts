import * as _ from 'lodash';

import { sequelize as dbService } from '@modules/database/database.service';
import { OpportunitySchool } from '@modules/opportunity/model/opportunity-school.schema';
import { Location } from '@modules/location/model/location.schema';

export function ensureOpportunitySchool(opportunityModel, opportunityData) {
  if (opportunityData.schoolIds && opportunityData.schoolIds.length > 0) {
    const schoolData = _.map(opportunityData.schoolIds, schoolId => {
      return {
        opportunityId: opportunityModel.id,
        schoolId,
      };
    });

    return dbService.bulkUpsert(OpportunitySchool, schoolData);
  }
}

export function ensureOpportunityLocation(opportunityModel, opportunityData) {
  if (_.isEmpty(opportunityData.location)) {
    return;
  }
  const location = opportunityData.location;
  const locationData = Object.assign({}, {
    postcode: location.postcode,
    state: location.state,
    suburb: location.location,
  });

  if (opportunityModel.location && opportunityModel.location.id) {
    return opportunityModel.location.update(locationData);
  }
  return Location.create(locationData).then(locationModel => {
    return opportunityModel.setLocation(locationModel);
  });
}
