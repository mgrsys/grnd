import * as _ from 'lodash';
import * as Sequelize from 'sequelize';

import { Contact } from '@modules/contact/model/contact.relations';
import { sequelize as dbService } from '@modules/database/database.service';
import { Location } from '@modules/location/model/location.schema';
import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
// tslint:disable-next-line:max-line-length
import { OpportunityPrerequisites } from '@modules/opportunity/model/opportunity-prerequisites.schema';
import { OpportunitySkill } from '@modules/opportunity/model/opportunity-skill.schema';
import { OpportunityTimePeriod } from '@modules/opportunity/model/opportunity-time-period.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';
import { TimePeriod } from '@modules/opportunity/model/time-period.schema';
import { OrganisationContact } from '@modules/organisation/model/organisation-contact.schema';
import { School } from '@modules/school/model/school.relations';
import { Skill } from '@modules/skill/model/skill.relations';
import { ensureOpportunitySchool, ensureOpportunityLocation } from './helpers';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';
import { OpportunityTypes } from './model/opportunity-types.schema';

const Op = Sequelize.Op;

function ensureAuthorization(id: any, authorization: any) {
  return new Promise((resolve, reject) => {
    resolve(true);
  });
}

function getOpportunity(id) {
  return Opportunity.findOne({
    include: [{
      attributes: ['id', 'name'],
      model: Skill,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: School,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: TimePeriod,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: Prerequisites,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: Location,
    },
    {
      model: Contact,
    },
    {
      attributes: ['id', 'name'],
      as: 'opportunityTypes',
      model: OpportunityType,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      as: 'opportunityDuration',
      model: OpportunityDuration,
    }],
    where: {
      id: {
        [Op.eq]: id,
      },
    },
  });
}

function ensureOpportunityContact(opportunityModel, opportunityData) {

  let contactPromise;
  let contactModel;

  if (_.isNumber(opportunityData.contactId)) {
    contactPromise = Contact.findById(opportunityData.contactId);
  } else if (opportunityData.contact && opportunityData.contact.name) {
    const contactData = opportunityData.contact;
    contactData.firstName = contactData.name.split(' ')[0] || '';
    contactData.lastName = contactData.name.split(' ')[1] || '';
    contactPromise = Contact.create(contactData, { returning: true });
  } else {
    return;
  }

  return contactPromise
    .then(result => {
      contactModel = result;
      return opportunityModel.update({
        contactId: contactModel.id,
      });
    })
    .then(() => {
      return OrganisationContact.upsert({
        contactId: contactModel.id,
        organisationId: opportunityModel.get('organisationId'),
        // Todo: Make sure this works once opportunities only being created by organisation users.
      });
    });

}

function ensureOpportunityTimePeriods(opportunityModel, opportunityData) {
  if (opportunityData.timePeriodIds && opportunityData.timePeriodIds.length > 0) {
    const timePeriodData = _.map(opportunityData.timePeriodIds, timePeriodId => {
      return {
        opportunityId: opportunityModel.id,
        timePeriodId: timePeriodId,
      };
    });

    return dbService.bulkReplace(OpportunityTimePeriod, timePeriodData, opportunityData.id);
  }
}

function ensureOpportunityTypes(opportunityModel, opportunityData) {
  if (opportunityData.opportunityTypeIds && opportunityData.opportunityTypeIds.length > 0) {
    const opportunityTypeData = _.map(opportunityData.opportunityTypeIds, typeId => {
      return {
        opportunityId: opportunityModel.id,
        opportunityTypeId: typeId,
      };
    });

    return dbService.bulkReplace(OpportunityTypes, opportunityTypeData, opportunityData.id);
  }

  return;
}

function ensureOpportunityPrerequisites(opportunityModel, opportunityData) {
  if (opportunityData.prerequisitesIds) {
    const prerequisitesData = _.map(opportunityData.prerequisitesIds, prerequisitesId => {
      return {
        opportunityId: opportunityModel.id,
        prerequisitesId,
      };
    });

    if (!prerequisitesData.length) {
      return OpportunityPrerequisites.destroy({ where: { opportunityId: opportunityData.id } });
    }

    return dbService.bulkReplace(OpportunityPrerequisites, prerequisitesData, opportunityData.id);
  }

  return;
}

function ensureOpportunitySkills(opportunityModel, opportunityData) {
  if (opportunityData.skillIds) {
    const skillData = _.map(opportunityData.skillIds, id => {
      return {
        opportunityId: opportunityModel.id,
        skillId: id,
      };
    });

    if (!skillData.length) {
      return OpportunitySkill.destroy({ where: { opportunityId: opportunityData.id } });
    }

    return dbService.bulkReplace(OpportunitySkill, skillData, opportunityData.id);
  }

  return;
}

function updateOpportunity(id, opportunityData, authorization) {
  return new Promise((resolve) => {
    let opportunityModel;

    Promise.resolve()
      .then(() => {
        return ensureAuthorization(id, authorization);
      })
      .then((bool) => {
        if (bool !== true) {
          throw { code: 401, message: 'Unauthorized' };
        }
        return getOpportunity(id);
      })
      .then(model => {
        return opportunityModel = model;
      })
      .then(() => {
        return ensureOpportunityContact(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunityTypes(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunitySchool(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunitySkills(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunityTimePeriods(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunityPrerequisites(opportunityModel, opportunityData);
      })
      .then(() => {
        return ensureOpportunityLocation(opportunityModel, opportunityData);
      })
      .then(() => {
        if (!opportunityModel) {
          throw { code: 404, message: 'Opportunity Not Found' };
        }
        opportunityData.status = 'approved';
        return opportunityModel.update(opportunityData, { returning: true })
          .catch(e => {
            logger.log('opportunityModelUpdate', e);
            throw { code: 400, message: 'Invalid Opportunity Data' };
          });
      })
      .then(() => {
        return getOpportunity(id);
      })
      .then(oppModel => {
        const data = oppModel.get();
        return resolve(lambda.response(200, data));
      })
      .catch(e => {
        logger.log('updateOpportunity', e);
        return resolve(lambda.error(e.code, e.message));
      });

  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  const authorization = event && event.headers && event.headers.Authorization;
  const body = lambda.getBody(event);
  updateOpportunity(id, body, authorization).then(lambda.callback(callback));
};
