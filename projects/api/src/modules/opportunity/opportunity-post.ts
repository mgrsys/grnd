import * as user from '@modules/user/user.service';

import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import logger from '@utils/logger';
import * as lambda from '@utils/lambda';
import { ensureOpportunitySchool } from './helpers';

function ensurePostData(body) {
  const errors: any = {};
  if (!body.title) {
    errors.title = 'Title is required';
  }

  if (!body.description) {
    errors.description = 'Description is required';
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

function ensureOrganisation(organisation) {
  if (!organisation || !organisation.get('id')) {
    throw { code: 403, message: 'Invalid Organisation' };
  }
}

async function opportunityPost(body, cognitoId) {
  try {
    await ensurePostData(body);

    const organisationModel = await user.cognitoIdGetOrganisation(cognitoId);
    if (organisationModel) {
      await ensureOrganisation(organisationModel);
      body.organisationId = organisationModel.get('id');
    }

    const opportunityModel = await Opportunity.create({
      ...body, status: 'pending',
    });

    // to help school users to create opportunities without organisation
    await ensureOpportunitySchool(opportunityModel, body);

    return lambda.response(201, opportunityModel.get());
  } catch (e) {
    logger.log('opportunityPost', e);
    return lambda.error(e.code, e.message);
  }
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  const cognitoId = lambda.getCognitoId(event);
  opportunityPost(body, cognitoId).then(lambda.callback(callback));
};
