import * as Sequelize from 'sequelize';

import { Application } from '@modules/application/model/application.relations';
import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import { Student } from '@modules/student/model/student.relations';

import * as opportunity from '@modules/opportunity/opportunity.service';
import * as lambda from '@utils/lambda';
import { Profile } from '@modules/profile/schema/profile.schema';

import { getQuery } from '@modules/profile/schema/profile.query';
import { rejects } from 'assert';
import { School } from '@modules/school/model/school.schema';

const Op = Sequelize.Op;

function findOpportunity(id) {
  const where = { id: { [Op.eq]: id } };
  const includes = [{
    include: {
      model: Student,
      include: {
        model: School,
        as: 'schools',
        through: {
            attributes: [],
            where: {
                deletedAt: {
                    [Op.eq]: null,
                },
            },
        },
      },
    },
    model: Application,
    required: false,
    where: {
      deletedAt: {
        [Op.eq]: null,
      },
    },
  }];

  return Opportunity.findOne(opportunity.query(where, includes));
}

function getOpportunity(id) {
  return new Promise((resolve) => {
    findOpportunity(id)
      .then(oppResult => {
        if (!oppResult) {
          throw { code: 404, message: 'Opportunity Not Found' };
        }
        return oppResult;
      })
      .then(async opportunityModel => {
        try {
          const opportunityData: any = opportunityModel.get();
          const profiles: any[] = [];

          opportunityData.applications.forEach((application: any, index: number): void => {
            const profileInstance = Profile.findOne(getQuery(application.studentId));
            profiles.push(profileInstance);
          });
          opportunityData.profiles = await Promise.all(profiles);
          return resolve(lambda.response(200, opportunityData));
        } catch(e) {
          console.error(e);
          return resolve(lambda.error(500, e));
        }
      })
      .catch(e => {
        console.error(e);
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  getOpportunity(id).then(lambda.callback(callback));
};
