import { Contact } from '@modules/contact/model/contact.relations';
import { sequelize as dbService } from '@modules/database/database.service';
import { Location } from '@modules/location/model/location.schema';
import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';
import { TimePeriod } from '@modules/opportunity/model/time-period.schema';
import { School } from '@modules/school/model/school.relations';
import { Skill } from '@modules/skill/model/skill.relations';

import * as _ from 'lodash';
import * as Sequelize from 'sequelize';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

const Op = Sequelize.Op;

//  TODO fix this ffs.
function ensureAuthorization(id: any, authorization: any) {
  return new Promise((resolve, reject) => {
    resolve(true);
  });
}

function getOpportunity(id) {
  return Opportunity.findOne({
    include: [{
      attributes: ['id', 'name'],
      model: Skill,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: School,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: TimePeriod,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: Prerequisites,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: Location,
    },
    {
      model: Contact,
    },
    {
      attributes: ['id', 'name'],
      as: 'opportunityTypes',
      model: OpportunityType,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      as: 'opportunityDuration',
      model: OpportunityDuration,
    }],
    where: {
      id: {
        [Op.eq]: id,
      },
    },
  });
}
function deleteOpportunity(id, authorization) {
  return new Promise(async (resolve) => {
    try {
      const authSuccess = await ensureAuthorization(id, authorization);
      if (authSuccess !== true) { throw { code: 401, message: 'Unauthorized' }; }
      const model = await getOpportunity(id);
      if (!model) { throw { code: 404, message: 'Opportunity not found' }; }
      await model.update({
        deletedAt: new Date().toDateString(),
      });
      resolve(lambda.response(200, { success: true }));
    } catch (e) {
      logger.log('deleteOpportunity', e);
      return resolve(lambda.error(e.code, e.message));
    }

  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  const authorization = event && event.headers && event.headers.Authorization;
  deleteOpportunity(id, authorization).then(lambda.callback(callback));
};
