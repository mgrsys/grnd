/* tslint:disable:object-literal-sort-keys */
import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const OpportunityPrerequisites = sequelize.define<any>('opportunity_prerequisites', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  opportunityId: {
    field: 'opportunity_id',
    type: Sequelize.INTEGER,
  },
  prerequisitesId: {
    field: 'prerequisites_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
