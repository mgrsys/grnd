import { Opportunity } from '@modules/opportunity/model/opportunity.schema';

import { Application } from '@modules/application/model/application.schema';
import { Contact } from '@modules/contact/model/contact.schema';
import { Location } from '@modules/location/model/location.schema';
import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
// tslint:disable-next-line:max-line-length
import { OpportunityPrerequisites } from '@modules/opportunity/model/opportunity-prerequisites.schema';
import { OpportunitySchool } from '@modules/opportunity/model/opportunity-school.schema';
import { OpportunitySkill } from '@modules/opportunity/model/opportunity-skill.schema';
import { OpportunityTimePeriod } from '@modules/opportunity/model/opportunity-time-period.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { OpportunityTypes } from '@modules/opportunity/model/opportunity-types.schema';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';
import { TimePeriod } from '@modules/opportunity/model/time-period.schema';
import { Organisation } from '@modules/organisation/model/organisation.schema';
import { School } from '@modules/school/model/school.schema';
import { Skill } from '@modules/skill/model/skill.schema';

Opportunity.belongsTo(Location, {
  foreignKey: 'location_id',
  foreignKeyConstraint: true,
});

Opportunity.belongsToMany(OpportunityType, {
  as: 'opportunityTypes',
  foreignKey: 'opportunity_id',
  otherKey: 'opportunity_type_id',
  through: OpportunityTypes,
  foreignKeyConstraint: true,
});

Opportunity.belongsToMany(TimePeriod, {
  foreignKey: 'opportunity_id',
  otherKey: 'time_period_id',
  through: OpportunityTimePeriod,
  foreignKeyConstraint: true,
});

Opportunity.belongsToMany(Prerequisites, {
  foreignKey: 'opportunity_id',
  otherKey: 'prerequisites_id',
  through: OpportunityPrerequisites,
  foreignKeyConstraint: true,
});

Opportunity.belongsTo(OpportunityDuration, {
  as: 'opportunityDuration',
  foreignKey: 'opportunity_duration_id',
  foreignKeyConstraint: true,
});

Opportunity.belongsToMany(Skill, {
  foreignKey: 'opportunity_id',
  otherKey: 'skill_id',
  through: OpportunitySkill,
  foreignKeyConstraint: true,
});

Opportunity.belongsToMany(School, {
  foreignKey: 'opportunity_id',
  otherKey: 'school_id',
  through: OpportunitySchool,
  foreignKeyConstraint: true,
});

Opportunity.belongsTo(Contact, {
  foreignKey: 'contact_id'
});

Opportunity.belongsTo(Organisation, {
  foreignKey: 'organisation_id',
  foreignKeyConstraint: true,
});

Opportunity.hasMany(Application);

export { Opportunity };
