/* tslint:disable:object-literal-sort-keys */
import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Opportunity = sequelize.define<any>('opportunity', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  title: {
    field: 'title',
    type: Sequelize.STRING,
  },
  description: {
    field: 'description',
    type: Sequelize.TEXT,
  },
  status: {
    field: 'status',
    type: Sequelize.STRING,
  },
  extraInformation: {
    field: 'extra_information',
    type: Sequelize.TEXT,
  },
  startDate: {
    allowNull: false,
    defaultValue: Sequelize.NOW,
    field: 'start_date',
    type: Sequelize.DATE,
  },
  endDate: {
    field: 'end_date',
    type: Sequelize.DATE,
  },
  openDate: {
    allowNull: false,
    defaultValue: Sequelize.NOW,
    field: 'open_date',
    type: Sequelize.DATE,
  },
  closeDate: {
    field: 'close_date',
    type: Sequelize.DATE,
  },
  ongoing: {
    field: 'ongoing',
    type: Sequelize.BOOLEAN,
  },
  locationId: {
    field: 'location_id',
    type: Sequelize.INTEGER,
  },
  workingHours: {
    field: 'working_hours',
    type: Sequelize.INTEGER,
  },
  opportunityDurationId: {
    field: 'opportunity_duration_id',
    type: Sequelize.INTEGER,
  },
  duration: {
    field: 'duration',
    type: Sequelize.TEXT,
  },
  contactId: {
    field: 'contact_id',
    type: Sequelize.INTEGER,
  },
  organisationId: {
    field: 'organisation_id',
    type: Sequelize.INTEGER,
  },
  tasks: {
    field: 'tasks',
    type: Sequelize.JSONB,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
