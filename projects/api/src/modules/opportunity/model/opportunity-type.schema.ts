/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const OpportunityType = sequelize.define<any>('opportunity_type', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  name: {
    type: Sequelize.STRING,
    field: 'name',
  },

  tooltip: {
    type: Sequelize.STRING,
    field: 'tooltip',
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
