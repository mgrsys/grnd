/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';
import { Opportunity } from './opportunity.schema';
import { Skill } from '@modules/skill/model/skill.schema';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const OpportunitySkill = sequelize.define<any>('opportunity_skill', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  opportunityId: {
    field: 'opportunity_id',
    type: Sequelize.INTEGER,
  },
  skillId: {
    field: 'skill_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
