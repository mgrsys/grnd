
import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const OpportunityTypes = sequelize.define<any>('opportunity_types', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  opportunityId: {
    field: 'opportunity_id',
    type: Sequelize.INTEGER,
  },
  opportunityTypeId: {
    field: 'opportunity_type_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
