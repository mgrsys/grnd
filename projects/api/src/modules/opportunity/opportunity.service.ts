
import * as Sequelize from 'sequelize';
const Op = Sequelize.Op;

import { Contact } from '@modules/contact/model/contact.relations';
import { Location } from '@modules/location/model/location.schema';
import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { TimePeriod } from '@modules/opportunity/model/time-period.schema';
import { Organisation } from '@modules/organisation/model/organisation.relations';
import { School } from '@modules/school/model/school.relations';
import { Skill } from '@modules/skill/model/skill.relations';
import { Application } from '@modules/application/model/application.relations';
import { Student } from '@modules/student/model/student.relations';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';

export const query = (where, extraIncludes, schoolId?: number) => {
  const opportunityQuery: any = {
    include: [
      {
        model: Organisation,
        required: false,
      }, {
        attributes: ['id', 'name'],
        model: Skill,
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      },
      {
        attributes: ['id', 'name'],
        model: TimePeriod,
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      },
      {
        model: Location,
      }, {
        model: Contact,
      },
      {
        attributes: ['id', 'name', 'tooltip'],
        as: 'opportunityTypes',
        model: OpportunityType,
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      },
      {
        as: 'opportunityDuration',
        model: OpportunityDuration,
      },
      {
        attributes: ['id', 'name'],
        model: Prerequisites,
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      },
      {
        include: {
          model: Student,
          include: [{
            model: School,
            as: 'schools',
            through: {
              attributes: [],
              where: {
                deletedAt: {
                  [Op.eq]: null,
                },
              },
            },
          }, {
            model: Location
          }],
        },
        model: Application,
        required: false,
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      }
    ],
  };

  const schoolInclude: any = {
    attributes: ['id', 'name'],
    model: School,
    through: {
      attributes: [],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    },
  };

  if (schoolId) {
    schoolInclude.where = {
      id: {
        [Op.eq]: schoolId,
      },
    };
  }

  opportunityQuery.include.push(schoolInclude);
  if (extraIncludes) {
    opportunityQuery.include = opportunityQuery.include.concat(extraIncludes);
  }

  opportunityQuery.where = Object.assign({
    // status: 'approved',
    deletedAt: {
      [Op.eq]: null,
    },
  }, where);

  return opportunityQuery;
};
