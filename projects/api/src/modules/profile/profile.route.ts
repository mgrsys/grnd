
import { Context, Callback } from 'aws-lambda';

import ProfileController from './profile.controller';

const profile: ProfileController = new ProfileController();

export const getProfile = 
    (event: any, context: Context, callback: Callback) => { profile.getProfileAction(event, context, callback); }
export const postProfile = 
    (event: any, context: Context, callback: Callback) => { profile.postProfileAction(event, context, callback); }
export const putProfile = 
    (event: any, context: Context, callback: Callback) => { profile.putProfileAction(event, context, callback); }
export const postProfileAddExperience = 
    (event: any, context: Context, callback: Callback) => { profile.addExperienceAction(event, context, callback); }
export const postProfileAddContact = 
    (event: any, context: Context, callback: Callback) => { profile.addContactAction(event, context, callback); }
export const postProfileAddPicture = 
    (event: any, context: Context, callback: Callback) => { profile.addProfilePicture(event, context, callback); }
export const putProfileUpdateMain = 
    (event: any, context: Context, callback: Callback) => { profile.putProfileUpdateMain(event, context, callback); }

export const profileRoutes = {
    getProfile,
    postProfile,
    putProfile,
    postProfileAddExperience,
    postProfileAddContact,
    postProfileAddPicture,
    putProfileUpdateMain
}