// ! These models currently don't exist
// ! even though they are linked to
// ! db. Need to keep this in mind
// ! in future 
export interface UserExperience {}
export interface Skill {}
export interface Contact {

}
export interface Subject {
    id: number;
    subjectName: string;
}

export interface ProfileModel {
    headline: string;
    profilePicture: string;
    subjects: Subject[];
    skillsIHave: Skill[];
    skillsIWant: Skill[];
    emergencyContacts: Contact[];
    endorsements?: null;
    experience?: UserExperience[];
    completeness: Completeness;
}

export interface Completeness {
    overall: boolean;
    about: boolean;
    subjects: boolean;
    skillsIHave: boolean;
    skillsIWant: boolean;
    emergencyContacts: boolean;
}
export class ProfileBuilder {
    private headline: string;
    private profilePicture: string;
    private subjects: Subject[];
    private skillsIHave: Skill[];
    private skillsIWant: Skill[];
    private emergencyContacts: Contact[];
    private endorsements: null;
    private experience: UserExperience[];
    private completeness: Completeness;

    public setHeadline = (headline: string): ProfileBuilder => {
        this.headline = headline;
        return this;
    }
    public setProfilePicture = (url: string): ProfileBuilder => {
        this.profilePicture = url;
        return this;
    }
    public setSubjects = (subjects: Subject[]): ProfileBuilder => {
        this.subjects = subjects;
        return this;
    }
    public setSkillsIHave = (skills: Skill[]): ProfileBuilder => {
        this.skillsIHave = skills;
        return this;
    }
    public setSkillsIWant = (skills: Skill[]): ProfileBuilder => {
        this.skillsIWant = skills;
        return this;
    }
    public setEmergencyContacts = (contacts: Contact[]): ProfileBuilder => {
        this.emergencyContacts = contacts;
        return this;
    }
    public setEndorsements = (endorsements: null): ProfileBuilder => {
        this.endorsements = endorsements;
        return this;
    }
    public setExperience = (experience: UserExperience[]): ProfileBuilder => {
        this.experience = experience;
        return this;
    }
    public build(): ProfileModel {
        
        const completeness: any = {
            about: this.headline && this.headline.length > 0,
            subjects: this.subjects && this.subjects.length > 0,
            skillsIHave: this.skillsIHave && this.skillsIHave.length > 0,
            skillsIWant: this.skillsIWant && this.skillsIWant.length > 0,
            emergencyContacts: this.emergencyContacts && this.emergencyContacts.length > 0,
        };
        completeness.overall = 
            completeness.about && 
            completeness.subjects && 
            completeness.skillsIHave && 
            completeness.skillsIWant && 
            completeness.emergencyContacts;

        return {
            headline: this.headline,
            profilePicture: this.profilePicture,
            subjects: this.subjects,
            skillsIHave: this.skillsIHave,
            skillsIWant: this.skillsIWant,
            emergencyContacts: this.emergencyContacts,
            endorsements: this.endorsements,
            experience: this.experience,
            completeness,
        };

    }

}