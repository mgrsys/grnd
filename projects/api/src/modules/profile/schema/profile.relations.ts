import { Student } from '@modules/student/model/student.schema';
import { Skill } from '@modules/skill/model/skill.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { Subject } from '@modules/subject/schema/subject.schema';

import { Profile } from './profile.schema';
import { ProfileHasSkill } from './profile.has.skill.schema';
import { ProfileWantsSkill } from './profile.wants.skill.schema';
import { ProfileSubject } from './profile.subject.schema';
import { Experience } from './experience.schema';
import { EmergencyContacts } from './emergency.contacts.schema';
import { Contact } from '@modules/contact/model/contact.schema';
import { UserExperience } from './user.experience.schema';
import { ProfileUserExperience } from './profile.user.experience.schema';

Profile.belongsTo(Student, { foreignKey: 'student_id', foreignKeyConstraint: true });

Profile.belongsToMany(Skill, {
  as: 'skillsIHave',
  foreignKey: 'profile_id',
  otherKey: 'skill_id',
  through: ProfileHasSkill,
  foreignKeyConstraint: true,
});

Profile.belongsToMany(Skill, {
  as: 'skillsIWant',
  foreignKey: 'profile_id',
  otherKey: 'skill_id',
  through: ProfileWantsSkill,
  foreignKeyConstraint: true,
});

Profile.belongsToMany(Subject, {
  foreignKey: 'profile_id',
  otherKey: 'subject_id',
  through: ProfileSubject,
  foreignKeyConstraint: true,
});

Profile.belongsToMany(Opportunity, {
  foreignKey: 'profile_id',
  otherKey: 'opportunity_id',
  through: Experience,
  foreignKeyConstraint: true,
});

Profile.belongsToMany(Contact, {
  as: 'emergencyContacts',
  foreignKey: 'profile_id',
  otherKey: 'contact_id',
  through: EmergencyContacts,
  foreignKeyConstraint: true,
});

Profile.belongsToMany(UserExperience, {
  as: 'userExperience',
  foreignKey: 'profile_id',
  otherKey: 'user_experience_id',
  through: ProfileUserExperience,
  foreignKeyConstraint: true,
})

export { Profile };
