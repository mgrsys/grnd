/* tslint:disable:object-literal-sort-keys */
import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';
export const Profile = sequelize.define<any>('profile', {

  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  studentId: {
    field: 'student_id',
    type: Sequelize.STRING,
  },
  headline: {
      field: 'headline',
      type: Sequelize.STRING,
  },
  profilePicture: {
    field: 'profile_picture',
    type: Sequelize.STRING,
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
