import { Skill } from '@modules/skill/model/skill.schema';
import { UserExperience } from './user.experience.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { UserExperienceSkill } from './user.experience.skill.schema';
import { Location } from '@modules/location/model/location.schema';

UserExperience.belongsTo(Location, { foreignKey: 'location_id' });

UserExperience.belongsTo(OpportunityType, {
  as: 'opportunityType',
  foreignKey: 'opportunity_type_id',
});

UserExperience.belongsToMany(Skill, {
  foreignKey: 'user_experience_id',
  otherKey: 'skill_id',
  through: UserExperienceSkill,
})

export { UserExperience };
