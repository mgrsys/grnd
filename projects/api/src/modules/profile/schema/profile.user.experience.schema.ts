import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const ProfileUserExperience = sequelize.define<any>('profile_user_experience', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  profileId: {
    field: 'profile_id',
    type: Sequelize.INTEGER,
  },
  userExperienceId: {
    field: 'user_experience_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
