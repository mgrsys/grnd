import { Subject } from '@modules/subject/schema/subject.schema';
import { Skill } from '@modules/skill/model/skill.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { Op } from 'sequelize';
import { Student } from '@modules/student/model/student.schema';
import { School } from '@modules/school/model/school.schema';
import { Contact } from '@modules/contact/model/contact.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { Location } from '@modules/location/model/location.schema';
import { UserExperience } from './user.experience.relations';

export const getQuery = (studentId: string): any => ({
  include: [
    {
      model: Student,
      include: [{
        model: Location,
        required: false,
      }, {
        model: School,
        as: 'schools',
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      }],
    },
    {
      model: UserExperience,
      as: 'userExperience',
      required: false,
      include: [{
        model: Location,
        required: false,
      }, {
        attributes: ['id', 'name'],
        as: "skills",
        model: Skill,
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      }, {
        model: OpportunityType,
        as: 'opportunityType',
      }],
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: Opportunity,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
  ],
  where: {
    deletedAt: {
      [Op.eq]: null,
    },
    studentId,
  },
});

export const byStudent = (studentId: string): any => ({
  include: [
    {
      model: Contact,
      as: 'emergencyContacts',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'title'],
      model: Subject,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
  ],
  where: {
    deletedAt: {
      [Op.eq]: null,
    },
    studentId,
  },
});

export const byContact = (studentId: string): any => ({
  include: [
    {
      attributes: ['id', 'name'],
      model: Skill,
      as: 'skillsIHave',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: Skill,
      as: 'skillsIWant',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
  ],
  where: {
    deletedAt: {
      [Op.eq]: null,
    },
    studentId,
  },
});

export const getQueryByProfileId = (profileId: string): any => ({
  include: [
    {
      model: Student,
      include: [{
        model: Location,
        required: false,
      }, {
        model: School,
        as: 'schools',
        through: {
          attributes: [],
          where: {
            deletedAt: {
              [Op.eq]: null,
            },
          },
        },
      }],
    },
    {
      attributes: ['id', 'title'],
      model: Subject,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: UserExperience,
      as: 'userExperience',
      required: false,
      include: [
        {
          model: Location,
        },
        {
          attributes: ['id', 'name'],
          as: "skills",
          model: Skill,
          through: {
            attributes: [],
            where: {
              deletedAt: {
                [Op.eq]: null,
              },
            },
          },
        },
        {
          model: OpportunityType,
          as: 'opportunityType',
        },
      ],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: Contact,
      as: 'emergencyContacts',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: Skill,
      as: 'skillsIHave',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      attributes: ['id', 'name'],
      model: Skill,
      as: 'skillsIWant',
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: Opportunity,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
  ],
  where: {
    deletedAt: {
      [Op.eq]: null,
    },
    id: profileId,
  }
});

export const getExperienceById = (userExperienceId: number): any => ({
  include: [
    {
      model: Location,
    },
    {
      attributes: ['id', 'name'],
      as: "skills",
      model: Skill,
      through: {
        attributes: [],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        },
      },
    },
    {
      model: OpportunityType,
      as: 'opportunityType',
    },
  ],
  where: {
    deletedAt: {
      [Op.eq]: null,
    },
    id: userExperienceId,
  },
});
