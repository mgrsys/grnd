import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const ProfileWantsSkill = sequelize.define<any>('profile_wants_skill', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  profileId: {
    field: 'profile_id',
    type: Sequelize.INTEGER,
  },
  skillId: {
    field: 'skill_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
