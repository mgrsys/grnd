import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const UserExperience = sequelize.define<any>('user_experience', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  title: Sequelize.STRING,

  summary: Sequelize.STRING,
  company: Sequelize.STRING,

  locationId: {
    field: 'location_id',
    type: Sequelize.INTEGER,
  },
  opportunityTypeId: {
    field: 'opportunity_type_id',
    type: Sequelize.INTEGER,
  },
  from: Sequelize.DATE,
  to: Sequelize.DATE,
  current: Sequelize.BOOLEAN,
  tasks: {
    field: 'tasks',
    type: Sequelize.JSONB,
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
