import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

export const UserExperienceSkill = sequelize.define<any>('user_experience_skill', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  userExperienceId: {
    field: 'user_experience_id',
    type: Sequelize.INTEGER,
  },
  skillId: {
    field: 'skill_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
