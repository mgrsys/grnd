import { ProfileModel, Completeness } from './profile.model';
import { log, LogLevel } from '@utils/elk.logger';
import { Context, Callback } from 'aws-lambda';
import * as lambda from '@utils/lambda';
import { cognitoIdGetStudent } from '@modules/user/user.service';
import { Profile } from './schema/profile.relations';
import * as queries from './schema/profile.query';
import * as profileService from './profile.service';

export default class ProfileController {
  public constructor() {
    //
  }
  public async getProfileAction(event: any, context: Context, callback: Callback): Promise<void> {
    let mt: number = new Date().valueOf();
    lambda.init(event, context);
    const cognitoId = lambda.getCognitoId(event);

    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    if (!studentModel) throw {
      status: 403,
      error: 'Unauthorized'
    };

    const studentId: string = studentModel.get('id');
    try {
      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'getProfileAction',
        user: studentId,
        data: {
          info: 'User fetching profile',
        },
      });

      // split profile fetch by 3 queries since single query doesn't work fast enough.
      // I suppose it's issue with database itself or Sequelize issue, anyway, the solution
      // works fast and reliable.
      const profileData: any = await Promise.all([
        Profile.findOne(queries.getQuery(studentId)),
        Profile.findOne(queries.byStudent(studentId)),
        Profile.findOne(queries.byContact(studentId)),
      ]);

      if (profileData[0] === null) {
        throw {
          code: 404,
          error: 'Not found',
        };
      }

      const profile = {
        ...(profileData[0].get()),
        ...(profileData[1].get()),
        ...(profileData[2].get()),
      };

      const completeness: Completeness = profileService.completeness(profile);
      const profileWithCompleteness: any = {
        ...profile,
        completeness,
      };

      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'getProfileAction',
        user: studentId,
        data: {
          info: 'Success fetching profile',
          microtime: `${new Date().valueOf() - mt}ms`,
        },
      });

      lambda.callback(callback)(lambda.response(200, profileWithCompleteness));
    } catch (e) {
      console.error(e);
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'getProfileAction',
        user: studentId,
        data: {
          info: 'Error getting profile',
          microtime: `${new Date().valueOf() - mt}ms`,
          error: e,
        },
      });

      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  }

  public async postProfileAction(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const body = lambda.getBody(event);
    const cognitoId = lambda.getCognitoId(event);
    try {

    } catch (e) {
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'postProfileAction',
        user: cognitoIdGetStudent(cognitoId).toString(10) || cognitoId || null,
        data: {
          info: 'Error creating new profile',
          error: e,
          eventBody: body,
        },
      });
      callback(e);
    }
  }

  public async putProfileAction(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const cognitoId: string = lambda.getCognitoId(event);
    const mt: number = new Date().valueOf();
    const body: any = lambda.getBody(event);
    const id: string = lambda.getParams(event).id;
    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    const studentId: string = studentModel.get('id');
    try {
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'putProfileAction',
        user: studentId,
        data: {
          info: 'User updating profile',
        },
      });

      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      if (!body) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!id) throw {
        status: 400,
        error: 'Bad request',
      }

      const profileWithCompleteness: any = await profileService.updateProfile(body, id, studentId);

      lambda.callback(callback)(lambda.response(200, profileWithCompleteness));
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'putProfileAction',
        user: studentId,
        data: {
          info: 'Success updating profile',
          microtime: `${new Date().valueOf() - mt}ms`,
        },
      });
    } catch (e) {
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'putProfileAction',
        user: studentId,
        data: {
          info: 'Error updating profile',
          microtime: `${new Date().valueOf() - mt}ms`,
          error: e,
        },
      });
      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  }

  public async addExperienceAction(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const cognitoId: string = lambda.getCognitoId(event);
    const mt: number = new Date().valueOf();
    const body: any = lambda.getBody(event);
    const id: string = lambda.getParams(event).id;
    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    const studentId: string = studentModel.get('id');
    try {
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addExperienceAction',
        user: studentId,
        data: {
          info: 'User adding experience',
        },
      });

      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      if (!body) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!id) throw {
        status: 400,
        error: 'Bad request',
      }
      const profileWithCompleteness: any = await profileService.addExperience(body, id);

      lambda.callback(callback)(lambda.response(200, profileWithCompleteness));
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addExperienceAction',
        user: studentId,
        data: {
          info: 'Success adding experience',
          microtime: `${new Date().valueOf() - mt}ms`,
        },
      });

    } catch (e) {
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'addExperienceAction',
        user: studentId,
        data: {
          info: 'Error adding experience',
          microtime: `${new Date().valueOf() - mt}ms`,
          error: e,
        },
      });
      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  };

  public async addContactAction(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const cognitoId: string = lambda.getCognitoId(event);
    const mt: number = new Date().valueOf();
    const body: any = lambda.getBody(event);
    const id: string = lambda.getParams(event).id;
    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    const studentId: string = studentModel.get('id');
    try {
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addContactAction',
        user: studentId,
        data: {
          info: 'User adding contact',
        },
      });

      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      if (!body) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!id) throw {
        status: 400,
        error: 'Bad request',
      }
      const profileWithCompleteness: any = await profileService.addContact(body, id);


      lambda.callback(callback)(lambda.response(200, profileWithCompleteness));
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addContactAction',
        user: studentId,
        data: {
          info: 'Success adding contact',
          microtime: `${new Date().valueOf() - mt}ms`,
        },
      });

    } catch (e) {
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'addContactAction',
        user: studentId,
        data: {
          info: 'Error adding contact',
          microtime: `${new Date().valueOf() - mt}ms`,
          error: e,
        },
      });
      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  };
  public async addProfilePicture(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const cognitoId: string = lambda.getCognitoId(event);
    const mt: number = new Date().valueOf();
    const body: any = lambda.getBody(event);
    const id: string = lambda.getParams(event).id;
    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    const studentId: string = studentModel.get('id');
    console.info(body);
    try {
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addProfilePicture',
        user: studentId,
        data: {
          info: 'User adding profile picture',
        },
      });

      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      if (!body) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!id) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!body.fileType) throw {
        status: 400,
        error: 'Bad request',
      }
      const profileWithUrl: any = await profileService.addProfilePicture(body, id);

      lambda.callback(callback)(lambda.response(200, profileWithUrl));
      log({
        level: LogLevel.INFO,
        domain: 'profile',
        action: 'addProfilePicture',
        user: studentId,
        data: {
          info: 'Success adding profile picture',
          microtime: `${new Date().valueOf() - mt}ms`
        },
      });

    } catch (e) {
      log({
        level: LogLevel.ERROR,
        domain: 'profile',
        action: 'addProfilePicture',
        user: studentId,
        data: {
          info: 'Error adding profile picture',
          microtime: `${new Date().valueOf() - mt}ms`,
          error: e,
        },
      });
      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  };
  public async putProfileUpdateMain(event: any, context: Context, callback: Callback): Promise<void> {
    lambda.init(event, context);
    const cognitoId: string = lambda.getCognitoId(event);
    const mt: number = new Date().valueOf();
    const body: any = lambda.getBody(event);
    const id: string = lambda.getParams(event).id;
    const studentModel: any = await cognitoIdGetStudent(cognitoId);
    const studentId: string = studentModel.get('id');
    console.info(new Date().valueOf() + `:INFO:profile:putProfileUpdateMain:${studentId}:{}`);
    try {
      if (!studentId) throw {
        status: 403,
        error: 'Unauthorized',
      };
      if (!body) throw {
        status: 400,
        error: 'Bad request',
      }
      if (!id) throw {
        status: 400,
        error: 'Bad request',
      }
      const updatedProfile: string = await profileService.updateMainSection(body, id, studentId);

      lambda.callback(callback)(lambda.response(200, updatedProfile));
    } catch (e) {
      console.error(new Date().valueOf() + `:ERROR:profile:putProfileUpdateMain:${studentId}:${JSON.stringify({ e })}`);
      lambda.callback(callback)(lambda.error(e.code, e.error));
    }
  };
}
