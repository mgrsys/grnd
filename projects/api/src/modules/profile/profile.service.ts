import { ProfileModel, Completeness } from './profile.model';
import { getQueryByProfileId, getExperienceById } from './schema/profile.query';
import { sequelize } from '@modules/database/database.service';
import { Profile } from './schema/profile.schema';
import { ProfileSubject } from './schema/profile.subject.schema';
import { ProfileHasSkill } from './schema/profile.has.skill.schema';
import { ProfileWantsSkill } from './schema/profile.wants.skill.schema';
import { UserExperience } from './schema/user.experience.relations';
import { UserExperienceSkill } from './schema/user.experience.skill.schema';
import { ProfileUserExperience } from './schema/profile.user.experience.schema';
import { Location } from '@modules/location/model/location.schema';
import { Contact } from '@modules/contact/model/contact.schema';
import { EmergencyContacts } from '@modules/profile/schema/emergency.contacts.schema';
import { Student } from '@modules/student/model/student.relations';
import { Op } from 'sequelize';
import { S3 } from 'aws-sdk';
import { AWS_S3_PROFILE_PICTURE_BUCKET } from '@utils/env';

import Logger from '@utils/builder.logger';

export const ERROR_NOT_FOUND = 'not_found';

export const completeness = (profile: any): Completeness => {
  const about: boolean =
    profile.headline !== null &&
    profile.student.firstName !== null &&
    profile.student.lastName !== null &&
    profile.student.schoolYear !== null &&
    profile.student.residentialLocationId !== null &&
    profile.student.gender !== null;

  const subjects: boolean = profile.subjects.length !== 0;
  const skillsIHave: boolean = profile.skillsIHave.length !== 0;
  const skillsIWant: boolean = profile.skillsIWant.length !== 0;
  const emergencyContacts: boolean = profile.emergencyContacts.length !== 0;
  return {
    about,
    subjects,
    skillsIHave,
    skillsIWant,
    emergencyContacts,
    overall: about && subjects && skillsIHave && skillsIWant,
  };
};

export const updateProfile = async (body: ProfileModel, profileId: string, studentId: string): Promise<any> => {
  //  TODO still need a strategy here to reuse existing m:n
  const logger: Logger = new Logger("profile", "updateProfile");
  logger.user(studentId).info();

  const transaction: any = await sequelize.transaction();

  try {
    const profileQuery: any = await getQueryByProfileId(profileId);
    const profileInstance: any = await Profile.findOne(profileQuery);

    const profileSubjects: any[] = await ProfileSubject.findAll({
      where: {
        profileId,
      }
    });
    const profileHasSkills: any[] = await ProfileHasSkill.findAll({
      where: {
        profileId,
      },
    });
    const profileWantSkills: any[] = await ProfileWantsSkill.findAll({
      where: {
        profileId,
      },
    });
    const profileSubjectDeletes: Promise<any>[] = [];
    const profileSubjectUpserts: Promise<any>[] = [];
    const profileHasSkillDeletes: Promise<any>[] = [];
    const profileHasSkillUpserts: Promise<any>[] = [];
    const profileWantsSkillDeletes: Promise<any>[] = [];
    const profileWantsSkillUpserts: Promise<any>[] = [];
    if (body.subjects && body.subjects.length) {
      profileSubjects.forEach(async (profileSubject: any): Promise<void> => {
        profileSubjectDeletes.push(profileSubject.update({
          deletedAt: new Date().toDateString(),
        }, { transaction, }));
      });

      body.subjects.forEach((async (subject: any): Promise<void> => {
        profileSubjectUpserts.push(ProfileSubject.upsert({
          profileId,
          subjectId: parseInt(subject.id, 10),
        }, { transaction, }));
      }));
    }
    if (body.skillsIHave && body.skillsIHave.length > 0) {
      profileHasSkills.forEach(async (profileHasSkill: any): Promise<void> => {
        profileHasSkillDeletes.push(profileHasSkill.update({
          deletedAt: new Date().toDateString(),
        }, { transaction, }));
      });

      body.skillsIHave.forEach((async (skill: any): Promise<void> => {
        profileHasSkillUpserts.push(ProfileHasSkill.upsert({
          profileId,
          skillId: parseInt(skill.id, 10),
        }, { transaction, }));
      }));
    }
    if (body.skillsIWant && body.skillsIWant.length > 0) {
      profileWantSkills.forEach(async (profileWantsSkill: any): Promise<void> => {
        profileWantsSkillDeletes.push(profileWantsSkill.update({
          deletedAt: new Date().toDateString(),
        }, { transaction, }));
      });

      body.skillsIWant.forEach((async (skill: any): Promise<void> => {
        profileWantsSkillUpserts.push(ProfileWantsSkill.upsert({
          profileId,
          skillId: parseInt(skill.id, 10),
        }, { transaction, }));
      }));
    }
    logger.message("profile updates to be made").withData({
      subjects: body.subjects.length,
      hasSkills: body.skillsIHave.length,
      wantsSkills: body.skillsIWant.length,
    }).send();
    await Promise.all([
      ...profileSubjectDeletes,
      ...profileHasSkillDeletes,
      ...profileWantsSkillDeletes,
    ]);
    await Promise.all([
      ...profileSubjectUpserts,
      ...profileHasSkillUpserts,
      ...profileWantsSkillUpserts,
    ]);

    await transaction.commit();
    logger.success().message("transaction committed").withMicrotime().send();
    const profileRefresh: any = await profileInstance.reload();
    const profileWithCompleteness: any = {
      ...profileRefresh.get(),
      completeness: completeness(profileRefresh.get()),
    }

    return profileWithCompleteness;
  } catch (e) {
    console.error(e);
    logger.error().withMicrotime().withData(e).send();
    await transaction.rollback();
    logger.info().withMicrotime().message("Transaction rolled back").send();
    throw {
      code: 500,
      error: 'Internal server error',
    };
  }
}

export const addExperience = (body: any, id: string): Promise<any> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const errors: any[] = [];
      if (!body.experienceType || !body.experienceType.id || !body.experienceType.name) {
        errors.push("Invalid experience type");
      }
      if (!body.title || body.title === '') {
        errors.push("Invalid title");
      }
      if (!body.summary || body.summary === '') {
        errors.push("Invalid summary");
      }
      if (!body.company || body.company === '') {
        errors.push("Invalid company");
      }
      if (!body.location) {
        errors.push("Invalid location");
      }
      if (!body.from) {
        errors.push("Invalid date from");
      }
      if (!body.current && !body.to) {
        errors.push("Invalid date to");
      }
      if (!body.tasks || body.tasks.length === 0 ||
        (body.tasks.length === 1 && (!body.tasks[0] || body.tasks[0] === ""))) {
        errors.push("Invalid tasks");
      }
      if (!body.skills || body.skills.length === 0) {
        errors.push("Invalid skills");
      }

      if (errors.length > 0) {
        throw errors;
      }
      const userExperienceData: any = {
        title: body.title,
        summary: body.summary,
        company: body.company,
        from: body.from,
        to: body.current ? null : body.to,
        current: body.current,
        tasks: body.tasks,
        opportunityTypeId: body.experienceType.id,
      }
      const userExperience: any = await UserExperience.create(userExperienceData);
      const skillPivots: any[] = [];
      body.skills.forEach(async (skill: any): Promise<void> => {
        if (skill.id) {
          skillPivots.push(await UserExperienceSkill.create({
            skillId: skill.id,
            userExperienceId: userExperience.get().id,
          }));
        }
      });
      await Promise.all(skillPivots);
      const query: any = getExperienceById(userExperience.get().id);
      let userExperienceModel: any = await UserExperience.findOne(query);
      const location: any = await Location.create({
        postcode: body.location.postcode,
        state: body.location.state,
        suburb: body.location.location,
      });
      await userExperienceModel.setLocation(location);
      //  link to profile
      await ProfileUserExperience.create({
        profileId: id,
        userExperienceId: userExperience.get().id,
      });
      const profileModel: any = await Profile.findOne(getQueryByProfileId(id));
      const profileWithCompleteness: any = {
        ...profileModel.get(),
        completeness: completeness(profileModel.get()),
      }
      resolve(profileWithCompleteness);
    } catch (error) {
      reject({
        code: 400,
        error,
      });
    }
  });
};
export const addContact = (body: any, id: string): Promise<any> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const errors: any[] = [];
      if (!body.firstName || body.firstName === '') {
        errors.push("Invalid first name");
      }
      if (!body.lastName || body.lastName === '') {
        errors.push("Invalid last name");
      }
      if (!body.phoneNumber || body.phoneNumber === '') {
        errors.push("Invalid phone number");
      }
      if (!body.email || body.email === '') {
        errors.push("Invalid email");
      }
      if (!body.position || body.position === '') {
        errors.push("Invalid position");
      }
      if (errors.length > 0) {
        throw errors;
      }
      const contactData: any = {
        firstName: body.firstName,
        lastName: body.lastName,
        phoneNumber: body.phoneNumber,
        email: body.email,
        position: body.position,
      }
      const contact: any = await Contact.create(contactData);
      await EmergencyContacts.create({
        profileId: id,
        contactId: contact.get().id
      });
      const query: any = getQueryByProfileId(id);
      const profile: any = await Profile.findOne(query);
      const profileWithCompleteness: any = {
        ...profile.get(),
        completeness: completeness(profile.get()),
      }
      resolve(profileWithCompleteness);
    } catch (error) {
      reject({
        code: 400,
        error,
      });
    }
  });
};
export const addProfilePicture = async (body: any, id: string): Promise<any> => {
  const query: any = getQueryByProfileId(id);
  const profileModel: any = await Profile.findOne(query);
  if (!profileModel) throw {
    code: 404,
    message: "Profile not found",
  }
  const data: Buffer = new Buffer(body.encodedImage.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const s3: S3 = new S3();
  const filePath: string = `${new Date().valueOf()}${id}.${body.fileType.split("/")[1]}`;
  const params: any = {
    Bucket: AWS_S3_PROFILE_PICTURE_BUCKET,
    Key: filePath,
    Body: data,
    ContentEncoding: 'base64',
    ContentType: body.fileType,
    ACL: 'public-read',
  };
  const uploadResult: any = await s3.upload(params).promise();
  const imageUrl: string = uploadResult.Location;
  await profileModel.update({
    profilePicture: imageUrl,
  });
  const profileWithCompleteness: any = {
    ...profileModel.get(),
    completeness: completeness(profileModel.get()),
  }
  return profileWithCompleteness;
}
export const updateMainSection = async (body: any, profileId: string, studentId: string): Promise<any> => {

  const logger: Logger = new Logger("profile", "updateMainAction");
  logger.user(studentId).info();
  const transaction: any = await sequelize.transaction();

  try {

    const studentUpdates: any = {};
    const profileUpdates: any = {};

    const studentInstance: any = await Student.findOne({
      where: {
        id: studentId,
        deletedAt: {
          [Op.eq]: null,
        }
      }
    });
    const profileQuery: any = getQueryByProfileId(profileId);
    const profileInstance: any = await Profile.findOne(profileQuery);

    if (!studentInstance || !profileInstance) {
      throw ERROR_NOT_FOUND;
    };
    if (body.newPictureFileName && body.newPictureFileName.length > 0) {
      profileUpdates.profilePicture = body.newPictureFileName;
    }
    if (body.firstName && body.firstName.length > 0) {

      studentUpdates.firstName = body.firstName;
    }
    if (body.lastName && body.lastName.length > 0) {
      studentUpdates.lastName = body.lastName;
    }
    if (body.headline && body.headline.length > 0) {
      profileUpdates.headline = body.headline;
    }
    if (body.grade && body.grade !== 0) {
      studentUpdates.schoolYear = body.grade;
    }
    if (body.dob && body.dob.length > 0) {
      studentUpdates.dob = body.dob;
    }

    if (body.suburb && Object.keys(body.suburb).length > 0) {
      logger.message("New key: locationModel").withData({
        suburb: body.suburb,
      }).send();
      const locationModel: any = await Location.findOne({
        where: {
          suburb: body.suburb.location,
          postcode: body.suburb.postcode,
        },
        deletedAt: {
          [Op.eq]: null,
        },
      });
      if (!locationModel) {
        const location: any = await Location.create({
          postcode: body.suburb.postcode,
          state: body.suburb.state,
          suburb: body.suburb.location,
        }, { transaction, });
        logger.withMicrotime().message("This location doesn't exist. Created new").withData({
          suburbId: location.get().id,
        }).send();
        studentUpdates.residentialLocationId = location.get().id;
      } else {
        logger.withMicrotime().message("This location does exist").withData({
          suburbId: locationModel.get().id,
        }).send();
        studentUpdates.residentialLocationId = locationModel.get().id;
      }
    }
    if (body.contactNumber && body.contactNumber.length === 10) {
      studentUpdates.contactNumber = body.contactNumber;
    }
    if (body.gender !== null && parseInt(body.gender, 10) !== -1) {
      studentUpdates.gender = body.gender;
    }
    if (Object.keys(studentUpdates).length > 0) {
      logger.message("studentUpdates to be made").withData({
        studentUpdates
      }).send();
      await studentInstance.update(studentUpdates, { transaction, });
    }
    if (Object.keys(profileUpdates).length > 0) {
      logger.message("profileUpdates to be made").withData({
        profileUpdates
      }).send();
      await profileInstance.update(profileUpdates, { transaction, });
    }
    await transaction.commit();
    logger.success().withMicrotime().message("Transaction committed").send();
    await profileInstance.reload();

    const profileWithCompleteness: any = {
      ...profileInstance.get(),
      completeness: completeness(profileInstance.get()),
    }
    return profileWithCompleteness;

  } catch (e) {

    logger.error().withMicrotime().message("Error updating profile or student").withData(e).send();
    await transaction.rollback();
    logger.info().withMicrotime().message("Transaction rolled back").send();
    throw {
      code: 500,
      error: 'Internal server error',
    };

  }

}
