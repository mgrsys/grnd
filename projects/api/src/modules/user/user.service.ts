import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js';

import * as Sequelize from 'sequelize';

import { Organisation } from '@modules/organisation/model/organisation.relations';
import { School } from '@modules/school/model/school.relations';
import { Student } from '@modules/student/model/student.relations';
import { User } from '@modules/user/model/user.relations';

import { AWS_COGNITO_USER_POOL_CLIENT_ID, AWS_COGNITO_USER_POOL_ID, AWS_COGNITO_IDENTITY_POOL_ID, AWS_REGION } from '@utils/env';
import logger from '@utils/logger';
import { StudentModel } from '@modules/student/model/student.model';
import * as AWS from 'aws-sdk';
const Op = Sequelize.Op;

export const COGNITO_ATTRIBUTE_TYPE_EMAIL = 'email';
export const COGNITO_ATTRIBUTE_TYPE_PHONE = 'phone_number';
export const COGNITO_ATTRIBUTE_TYPE_FIRST_NAME = 'given_name';
export const COGNITO_ATTRIBUTE_TYPE_MIDDLE_NAME = 'middle_name';
export const COGNITO_ATTRIBUTE_TYPE_LAST_NAME = 'family_name';
export const COGNITO_ATTRIBUTE_TYPE_USER_ROLE = 'custom:role';

export const COGNITO_USER_ROLE_STUDENT = 'student';
export const COGNITO_USER_ROLE_SCHOOL = 'school';
export const COGNITO_USER_ROLE_ORGANISATION = 'organisation';

export const ERROR_COGNITO_UNKNOWN = 'cognito_unknown_error';
export const ERROR_COGNITO_USER_EXISTS = 'cognito_user_exists';
export const ERROR_COGNITO_INVALID_PASSWORD = 'cognito_invalid_password';

const poolData = {
  ClientId: AWS_COGNITO_USER_POOL_CLIENT_ID,
  UserPoolId: AWS_COGNITO_USER_POOL_ID,
};
const userPool: AmazonCognitoIdentity.CognitoUserPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

const CognitoISP: AWS.CognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({
  region: AWS_REGION,
});

const ISPCredentials: AWS.CognitoIdentityCredentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: AWS_COGNITO_IDENTITY_POOL_ID,
});

AWS.config.credentials = ISPCredentials;

const newCognitoAttribute = (Name: string, Value: string): AmazonCognitoIdentity.CognitoUserAttribute => (
  new AmazonCognitoIdentity.CognitoUserAttribute({
    Name,
    Value,
  })
);

function handleSignupError(err) {
 // logger.log('handleSignupError', err);
  const passwordLengthRegex = /^.*password.*length.*6.*$/;
  if (err && err.code) {
    switch (err.code) {
      case 'InvalidPasswordException':
        return ERROR_COGNITO_INVALID_PASSWORD;
      case 'UsernameExistsException':
        return ERROR_COGNITO_USER_EXISTS;
      default:
        if (passwordLengthRegex.test(err.message)) {
          return ERROR_COGNITO_INVALID_PASSWORD;
        }
        return ERROR_COGNITO_UNKNOWN;
    }
  }
  return ERROR_COGNITO_UNKNOWN;
}

export const createCognitoStudent = async (studentModel: StudentModel): Promise<any> => (
  
  new Promise((resolve: Function, reject: Function): any => {

    console.info(new Date().valueOf() + ":INFO:student:createCognitoStudent:null:{}");

    const userAttributes: AmazonCognitoIdentity.CognitoUserAttribute[] = [];

    if(studentModel.email) userAttributes.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_EMAIL, studentModel.email));
    if(studentModel.firstName) userAttributes.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_FIRST_NAME, studentModel.firstName));
    if(studentModel.lastName) userAttributes.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_LAST_NAME, studentModel.lastName));

    userAttributes.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_USER_ROLE, "student"));

    userPool.signUp(
      studentModel.username, 
      studentModel.password, 
      userAttributes, 
      null, 
      (err: any, result: AmazonCognitoIdentity.ISignUpResult): any => {  
        if (err) {
          reject(handleSignupError(err));
        }
        const cognitoId: string = result && result.userSub;
        resolve(cognitoId);
    }); 
  })
);

export const removeCognitoUserIfExists = async (Username: string): Promise<any> => {
    
  console.info(new Date().valueOf() + ":INFO:student:removeCognitoUserIfExists:null:" + JSON.stringify({Username}));
  //  ! This isn't properly authenticated. Just so you know for now.
  const result: any = await CognitoISP.adminDeleteUser({
    UserPoolId: AWS_COGNITO_USER_POOL_ID,
    Username,
  }).promise();

};

export const ensureCognitoUser = (
  username, password, email, phone, firstName, middleName, lastName, userRole,
) => {
  return new Promise((resolve: Function, reject: Function): any => {
    const attributeList = [];
    attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_EMAIL, email));
    if (phone) {
      attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_PHONE, phone));
    }
    if (firstName) {
      attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_FIRST_NAME, firstName));
    }
    if (middleName) {
      attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_MIDDLE_NAME, middleName));
    }
    if (lastName) {
      attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_LAST_NAME, lastName));
    }
    if (userRole) {
      attributeList.push(newCognitoAttribute(COGNITO_ATTRIBUTE_TYPE_USER_ROLE, userRole));
    }
    return userPool.signUp(username, password, attributeList, null, async (err, result) => {
      
      if (err) {
        console.warn('userPool.signUp error', err, result);
        reject(handleSignupError(err));
      }
      const cognitoId = result && result.userSub;
      const userData = {
        cognitoId,
      };
      try { 
        const user = await User.create(userData);
        resolve(user);
      } catch(e) {
        reject(e);
      }
      
    });
  });
};

export const ensureValidUser = (cognitoId) => {
  if (!cognitoId) {
    throw { code: 403, message: 'Valid user authentication required. CID Unknown.' };
  }
  return User.findOne({ where: { cognitoId } })
    .then(result => {
      if (result) {
        return result;
      }
      throw { code: 403, message: `Valid user authentication required. CID: ${cognitoId}` };
    })
    .catch(e => {
      logger.log('ensureValidUser-findOne-e', e);
      throw { code: 403, message: [
        `Valid user authentication required. Error requesting user:`,
        JSON.stringify(e),
      ].join(' ') };
    });
};

export const isStudent = (user) => {
  return Student.findOne({
    where: {
      deletedAt: {
        [Op.eq]: null,
      },
      userId: user.get('id'),
    },
  })
    .then((student) => {
      return student;
    });
};

export const isOrganisation = (user) => {
  return Organisation.findOne({
    include: [{
      model: User,
      where: {
        id: user.get('id'),
      },
    }],
  })
    .then((organisation) => {
      return organisation;
    });
};

export const isSchool = (user) => {
  return School.findOne({
    include: [{
      model: User,
      where: {
        id: user.get('id'),
      },
    }],
  })
    .then((school) => {
      return school;
    });
};

export const cognitoIdGetOrganisation = (cognitoId) => {
  return Organisation.findOne({
    include: {
      model: User,
      where: {
        cognitoId,
      },
    },
  });
};

export const cognitoIdGetStudent = (cognitoId) => {
  return Student.findOne({
    include: {
      model: User,
      where: {
        cognitoId,
      },
    },
  });
};
