import { User } from './user.schema';

import { OrganisationUser } from '@modules/organisation/model/organisation-user.schema';
import { Organisation } from '@modules/organisation/model/organisation.schema';
import { SchoolUser } from '@modules/school/model/school-user.schema';
import { School } from '@modules/school/model/school.schema';
import { StudentUser } from '@modules/student/model/student-user.schema';
import { Student } from '@modules/student/model/student.schema';

User.belongsToMany(Organisation, {
  foreignKey: 'user_id',
  otherKey: 'organisation_id',
  through: OrganisationUser,
});
User.belongsToMany(Student, {
  foreignKey: 'user_id',
  otherKey: 'student_id',
  through: StudentUser,
});
User.belongsToMany(School, { through: SchoolUser });

export { User };
