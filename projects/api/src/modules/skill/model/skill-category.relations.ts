import { Skill } from './skill.schema';

import { SkillCategorySkill } from '@modules/skill/model/skill-category-skill.schema';
import { SkillCategory } from '@modules/skill/model/skill-category.schema';

SkillCategory.belongsToMany(Skill, {
  foreignKey: 'skill_category_id',
  otherKey: 'skill_id',
  through: SkillCategorySkill,
});

export { SkillCategory };
