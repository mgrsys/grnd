import { Skill } from './skill.schema';

import { OpportunitySkill } from '@modules/opportunity/model/opportunity-skill.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { SkillCategorySkill } from '@modules/skill/model/skill-category-skill.schema';
import { SkillCategory } from '@modules/skill/model/skill-category.schema';

Skill.belongsToMany(Opportunity, {
  foreignKey: 'skill_id',
  otherKey: 'opportunity_id',
  through: OpportunitySkill,
});

Skill.belongsToMany(SkillCategory, {
  as: 'SkillCategories',
  foreignKey: 'skill_id',
  otherKey: 'skill_category_id',
  through: SkillCategorySkill,
});

export { Skill };
