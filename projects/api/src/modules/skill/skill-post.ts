import { Skill } from '@modules/skill/model/skill.relations';
import logger from '@utils/logger';
import * as lambda from '@utils/lambda';

function ensurePostData(body) {
  const errors: any = {};
  if (!body.name) {
    errors.name = 'Name is required';
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

// FIXME use cognitoId to prevent post skills from unregistered users, etc.
function skillPost(body, cognitoId) {
  return new Promise((resolve) => {
    return Promise.resolve()
      .then(() => {
        return ensurePostData(body);
      })
      .then(() => {
        return Skill.create(body);
      })
      .then(skill => {
        return resolve(lambda.response(201, skill));
      })
      .catch(e => {
        logger.log('skillPost', e);
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  const cognitoId = lambda.getCognitoId(event);
  skillPost(body, cognitoId).then(lambda.callback(callback));
};
