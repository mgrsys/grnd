import * as auspostApi from '@utils/auspost';
import * as lambda from '@utils/lambda';

function ensureQueryData(query) {
  const errors: any = {};
  if (!query.suburb) {
    errors.suburb = 'A suburb name is required';
  } else if (query.suburb && query.suburb.length < 3) {
    errors.suburb = 'You must provide at least 3 characters';
  }
  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }
  return true;
}

function getLocation(query) {
  return new Promise((resolve) => {
    const { suburb } = query;
    Promise.resolve()
      .then(() => {
        return ensureQueryData(query);
      })
      .then(() => {
        return auspostApi.suburbSearch(suburb);
      })
      .then((results: any) => {
        return resolve(lambda.response(200, results));
      })
      .catch(e => {
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const query = lambda.getQuery(event);
  getLocation(query).then(lambda.callback(callback));
};
