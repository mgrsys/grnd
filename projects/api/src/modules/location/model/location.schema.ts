import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Location = sequelize.define<any>('location', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  address: {
    field: 'address',
    type: Sequelize.STRING,
  },
  country: {
    field: 'country',
    type: Sequelize.STRING,
  },
  postcode: {
    field: 'postcode',
    type: Sequelize.INTEGER,
  },
  state: {
    field: 'state',
    type: Sequelize.STRING,
  },
  suburb: {
    field: 'suburb',
    type: Sequelize.STRING,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
