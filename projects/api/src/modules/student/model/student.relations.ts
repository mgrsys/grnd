import { Student } from './student.schema';

import { Location } from '@modules/location/model/location.schema';
import { School } from '@modules/school/model/school.schema';
import { StudentSchool } from '@modules/student/model/student-school.schema';
import { StudentUser } from '@modules/student/model/student-user.schema';
import { User } from '@modules/user/model/user.schema';

Student.belongsTo(User, { foreignKey: 'user_id' });
Student.belongsTo(Location, { foreignKey: 'residential_location_id' });
Student.belongsToMany(User, {
  foreignKey: 'student_id',
  otherKey: 'user_id',
  through: StudentUser,
});
Student.hasMany(StudentSchool);
Student.belongsToMany(School, {
  as: 'schools',
  foreignKey: 'student_id',
  otherKey: 'school_id',
  through: StudentSchool,
});

export { Student };
