/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const StudentSchool = sequelize.define<any>('student_school', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  studentId: {
    field: 'student_id',
    type: Sequelize.INTEGER,
  },
  schoolId: {
    field: 'school_id',
    type: Sequelize.INTEGER,
  },
  startDate: {
    field: 'start_date',
    type: Sequelize.DATE,
  },
  endDate: {
    field: 'end_date',
    type: Sequelize.DATE,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
