export const ERROR_STUDENT_BAD_REQUEST = 'student_bad_request';

export interface StudentModel {

    email: string;
    password: string;
    username: string; 
    firstName: string; 
    schoolYear: number;
    schoolEmail: string;
    school: School;
    middleName: null;
    lastName: string;

}
export interface School {
    id: number;
    name: string;
}
export class StudentBuilder {
    private email: string;
    private password: string;
    private username: string;
    private school: School;
    private schoolYear: number;
    private schoolEmail: string;
    private firstName: string;
    private middleName: null;
    private lastName: string;

    public setEmail(email: string): StudentBuilder {
        this.email = email;
        return this;
    }
    public setPassword(password: string): StudentBuilder {
        this.password = password;
        return this;     
    }
    public setUsername(username: string): StudentBuilder {
        this.username = username;
        return this;
    }
    public setSchool(school: School): StudentBuilder {
        this.school = school;
        return this;
    }
    public setSchoolYear(schoolYear: number): StudentBuilder {
        this.schoolYear = schoolYear;
        return this;
    }
    public setSchoolEmail(schoolEmail: string): StudentBuilder {
        this.schoolEmail = schoolEmail;
        return this;
    }
    public setFirstName(firstName: string): StudentBuilder {
        this.firstName = firstName;
        return this;
    }
    public setMiddleName(middleName: null): StudentBuilder {
        this.middleName = middleName;
        return this;
    }
    public setLastName(lastName: string): StudentBuilder {
        this.lastName = lastName;
        return this;
    }
    public build(): StudentModel {
        const errors: any = {};
        if(!this.email || this.email === '') {
            errors.email = true;
        }
        if(!this.password || this.password === '') {
            errors.password = true;
        }
        if(!this.username || this.username === '') {
            errors.username = true;
        }
        if(!this.firstName || this.firstName === '') {
            errors.firstName = true;
        }
        if(!this.school || !this.school.id || !this.school.name) {
            errors.school = true;
        }
        if(!this.schoolYear || (this.schoolYear !== 9 && this.schoolYear !== 10 && this.schoolYear !== 11 && this.schoolYear !== 12)) {
            errors.schoolYear = true;
        }
        if(!this.schoolEmail || this.schoolEmail === '') {
            errors.schoolEmail = true;
        }
        if(!this.lastName || this.lastName === '') {
            errors.lastName = true;
        }
        if(Object.keys(errors).length > 0) throw ERROR_STUDENT_BAD_REQUEST;
        
        const studentModel: StudentModel = {
            email: this.email,
            password: this.password,
            username: this.username,
            school: this.school,
            schoolYear: this.schoolYear,
            schoolEmail: this.schoolEmail,
            firstName: this.firstName,
            middleName: this.middleName,
            lastName: this.lastName,
        };
        return studentModel;
    }

}