/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Student = sequelize.define<any>('student', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  userId: {
    field: 'user_id',
    type: Sequelize.INTEGER,
  },
  firstName: {
    field: 'first_name',
    type: Sequelize.STRING,
  },
  middleName: {
    type: Sequelize.STRING,
    field: 'middle_name',
  },
  lastName: {
    field: 'last_name',
    type: Sequelize.STRING,
  },
  email: {
    field: 'email',
    type: Sequelize.STRING,
  },
  gender: {
    field: 'gender',
    type: Sequelize.INTEGER,
  },
  contactNumber: {
    field: 'contact_number',
    type: Sequelize.STRING,
  },
  dob: {
    field: 'dob',
    type: Sequelize.DATEONLY,
  },
  schoolEmail: {
    field: 'school_email',
    type: Sequelize.STRING,
  },
  schoolYear: {
    field: 'school_year',
    type: Sequelize.STRING,
  },
  residentialLocationId: {
    field: 'residential_location_id',
    type: Sequelize.INTEGER,
  },
  signupToken: {
    field: 'signup_token',
    type: Sequelize.STRING,
  },
  picture: {
    type: Sequelize.TEXT,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
