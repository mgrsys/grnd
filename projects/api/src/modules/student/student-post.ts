import crypto from 'crypto';

import { Student } from '@modules/student/model/student.relations';
import * as user from '@modules/user/user.service';

import * as lambda from '@utils/lambda';
import { StudentModel, StudentBuilder, ERROR_STUDENT_BAD_REQUEST } from './model/student.model';
import { Profile } from '@modules/profile/schema/profile.relations';
import { School } from '@modules/school/model/school.relations';

import { sequelize } from '@modules/database/database.service';
import * as Sequelize from 'sequelize';
import { User } from '@modules/user/model/user.schema';

const ERROR_STUDENT_EXISTS: string = 'student_exists';
const ERROR_SCHOOL_NOT_FOUND: string = 'school_not_found';
const ERROR_PROFILE_EXISTS: string = 'profile_exists';

const ensurePostData = (body: any): boolean => {
  console.info(new Date().valueOf() + ":INFO:student:ensurePostData:null:{}");
  const passwordRegex: RegExp = /^.*[A-Z]{1,}.*$/;
  const errors: any = {};
  if (!body.password) {
    errors.password = 'Password is required';
  } else if (!passwordRegex.test(body.password) || body.password.length < 8) {
    errors.password = [
      'Password must have at least 8 characters,',
      '1 uppercase letter'
    ].join(' ');
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

const userCreate = (transaction: any): Promise<Sequelize.Instance<any>> => {
  console.info(new Date().valueOf() + ":INFO:student:userCreate:null:{}");
  return User.create({}, { transaction });
}

const studentCreate = async (studentData: any, userInstance: Sequelize.Instance<any>, transaction: any): Promise<any> => {
  console.info(new Date().valueOf() + ":INFO:student:studentCreate:null:" + JSON.stringify({
    email: studentData.email,
  }));
  const studentModel: any = await Student.findOne({
    attributes: ['id'],
    where: {
      email: studentData.email,
    },
  });

  if (studentModel) throw ERROR_STUDENT_EXISTS;

  const signupToken = crypto.createHash('sha256').update(`${studentData.email}${Date.now()}`)
    .digest('hex');

  const createStudentData: any = {
    ...studentData,
    signupToken,
    userId: userInstance.getDataValue("id"),
  };
  return Student.create(createStudentData, { transaction, });
}

const addSchoolToStudent = async (studentInstance: Sequelize.Instance<any>, studentModel: any, transaction: any): Promise<any> => {
  console.info(new Date().valueOf() + ":INFO:student:addSchoolToStudent:null:" + JSON.stringify({
    schoolId: studentModel.school.email,
  }));
  const schoolInstance: Sequelize.Instance<any> = await School.findById(studentModel.school.id);
  if(!schoolInstance) throw ERROR_SCHOOL_NOT_FOUND;
  return studentInstance.addSchool(schoolInstance, { transaction, });
}

const profileCreate = async (studentInstance: Sequelize.Instance<any>, transaction: any): Promise<any> => {
  const createParams: any = { studentId: studentInstance.getDataValue("id") };
  const searchParams: any = {
    attributes: ["id"],
    where: createParams,
  };

  console.info(new Date().valueOf() + ":INFO:student:profileCreate:null:" + JSON.stringify(createParams));

  const profileExists: Sequelize.Instance<any> | null = await Profile.findOne(searchParams);
  if(profileExists) throw ERROR_PROFILE_EXISTS;
  return Profile.create(createParams, { transaction, });
}


const addCognitoIdToUser = (userInstance: Sequelize.Instance<any>, cognitoId: string, transaction: any): Promise<Sequelize.Instance<any>> => {
  console.info(new Date().valueOf() + ":INFO:student:addCognitoIdToUser:null:" + JSON.stringify({ userId: userInstance.get("id"), cognitoId }));
  return userInstance.update({
    cognitoId,
  }, { transaction, });
}

const studentPost = async (body: any): Promise<any> => {
  console.info(new Date().valueOf() + ":INFO:student:studentPost:null:{}");
  //  Initialise transaction. Declaring the transaction here enforces
  //  manual commit and rollback. Because non-sequelize methods can
  //  fail within the studentPost, when they do, a manual rollback
  //  can be triggered.

  const transaction: any = await sequelize.transaction();
  const cognitoUser: any = {
    hasCreated: false,
    username: null,
    password: null,
  };
  return new Promise(async (resolve: Function, reject: Function): Promise<any> => {

    try {

      //  Ensure that the password is present and valid
      ensurePostData(body);
      //  Build a student entity. If any of the fields are invalid,
      //  the .build() method will throw an error accordingly.
      const studentModel: StudentModel = new StudentBuilder()
        .setEmail(body.email)
        .setPassword(body.password)
        .setUsername(body.email)
        .setSchool(body.school)
        .setSchoolYear(parseInt(body.schoolYear, 10))
        .setSchoolEmail(body.schoolEmail)
        .setFirstName(body.firstName)
        .setMiddleName(null)
        .setLastName(body.lastName)
        .build();
      //  Create a user instance, this is an empty user instance with no associated
      //  cognitoId
      const userInstance: Sequelize.Instance<any> = await userCreate(transaction);
      //  Create a student instance, with the appropriate first, last name etc, and userId of user
      const studentInstance: Sequelize.Instance<any> = await studentCreate(studentModel, userInstance, transaction);
      //  Add the school (if it exists) to the student, otherwise throw an error.
      await addSchoolToStudent(studentInstance, studentModel, transaction);
      //  Create a profile instance, with the studentId just created
      const profileInstance: Sequelize.Instance<any> = await profileCreate(studentInstance, transaction);
      //  Through userPool, add a new student user in cognito.
      const cognitoUserId: string = await user.createCognitoStudent(studentModel);
      //  Rolling back the transaction in event of failure will not remove the cognito
      //  user. Thus, if created, a flag is set here so that in the event of a rollback
      //  the cognitoUser can also be removed
      cognitoUser.hasCreated = true;
      cognitoUser.username = studentModel.username;
      cognitoUser.password = studentModel.password;
      //  Add that cognitoUsers id onto the original user model.
      await addCognitoIdToUser(userInstance, cognitoUserId, transaction);
      //  If errorless, commit this transaction and resolve the user instance
      await transaction.commit();
      console.info(new Date().valueOf() + ":SUCCESS:student:studentPost:null:" + JSON.stringify({success: true}));
      resolve(lambda.response(201, userInstance.get()));

    } catch(e) {

      console.error(new Date().valueOf() + ":ERROR:student:studentPost:null:" + e);

      const error: any = { code: 0, message: ''};

      switch (e) {
        case ERROR_STUDENT_BAD_REQUEST:
          error.code = 400; error.message = "Bad Request";
        break;
        case ERROR_STUDENT_EXISTS:
          error.code = 409; error.message = "User Exists";
        break;
        case ERROR_PROFILE_EXISTS:
          error.code = 409; error.message = "User Exists";
        break;
        case user.ERROR_COGNITO_USER_EXISTS:
          error.code = 409; error.message = "User Exists";
        break;
        case user.ERROR_COGNITO_INVALID_PASSWORD:
          error.code = 400; error.message = "Invalid Password";
        break;
        default:
          error.code = 500; error.message = "Internal Server Error";
      }

      try {
        await transaction.rollback();
        //  If the hasCreated flag has been set, the user needs to be
        //  remove from cognito
        if(cognitoUser.hasCreated) {
          await user.removeCognitoUserIfExists(cognitoUser.username);
        }
      } catch(e) {
        console.error("Error rolling back transaction, or removing cognito user Error: " + JSON.stringify(e));
      } finally {
        reject(error);
      }
    }

  });
}

export default (event, context, callback): void => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  studentPost(body)
      .then((response: any) => {
        lambda.callback(callback)(response);
      }).catch((e: any) => {
        lambda.callback(callback)(lambda.error(e.code, e.message));
      });
};
