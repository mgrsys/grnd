import { Student } from '@modules/student/model/student.relations';
import * as lambda from '@utils/lambda';

function getStudent(id) {
  return new Promise((resolve) => {
    Student.findById(id)
      .then(student => {
        if (!student) {
          throw { code: 404, message: 'Student Not Found' };
        }
        return student;
      })
      .then(studentModel => {
        const data = studentModel.get();
        return resolve(lambda.response(200, data));
      })
      .catch(e => {
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  getStudent(id).then(lambda.callback(callback));
};
