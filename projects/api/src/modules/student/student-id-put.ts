import * as Sequelize from 'sequelize';

import { Location } from '@modules/location/model/location.schema';
import { School } from '@modules/school/model/school.relations';
import { Student } from '@modules/student/model/student.relations';
import { StudentModel } from '@modules/student/model/student.model';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

const Op = Sequelize.Op;

function ensureAuthorization(id, authorization) {
  return new Promise((resolve, reject) => {
    const unauthorizedResponse = { code: 401, message: 'Unauthorized' };
    if (!authorization || typeof authorization !== 'string') {
      return reject(unauthorizedResponse);
    }

    if (authorization.indexOf('SignupToken') === 0) {
      const signupToken = authorization.replace('SignupToken', '').trim();
      Student.findOne({
        where: {
          id: {
            [Op.eq]: id,
          },
          signup_token: {
            [Op.eq]: signupToken,
          },
        },
      }).then(studentModel => {
        if (!studentModel) {
          throw unauthorizedResponse;
        } else {
          return resolve(studentModel);
        }
      });
    } else {
      return reject(unauthorizedResponse);
    }
  });
}

function ensureStudentLocation(studentModel, location, locationType) {
  if (location && location.location) {
    const locationData = Object.assign(location, {
      suburb: location.location,
    });
    return Promise.resolve().then(() => {
      return Location.create(locationData);
    })
      .then(locationModel => {
        const data = locationModel.get();
        return studentModel.update({
          [locationType]: data.id,
        });
      });
  }
  return studentModel;
}

function ensureStudentSchool(studentModel, school) {
  if (school && (school.id || school.name)) {
    return Promise.resolve()
      .then(() => {
        if (school.id) {
          return School.findById(school.id);
        }
        if (school.name) {
          school.verified = false;
          return School.create(school);
        }
        return null;
      })
      .then(schoolModel => {
        return studentModel.addSchool(schoolModel).then(() => {
          return studentModel;
        });
      });
  }
  return studentModel;
}

const updateStudent = async (id, studentData: any, authorization): Promise<any> => {
  try {
    let studentModel: Sequelize = await ensureAuthorization(id, authorization); // TODO add proper types into repos
    studentModel = await ensureStudentLocation(studentModel, studentData.location, 'residentialLocationId');
    studentModel = await ensureStudentSchool(studentModel, studentData.school);
    studentModel = await studentModel.update(studentData);

    const data = studentModel.get();
    return lambda.response(200, data);
  } catch (e) {
    logger.log('updateStudent', e);
    return lambda.error(e.code, e.message);
  }
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  const authorization = lambda.getAuthorization(event);
  const body = lambda.getBody(event);
  updateStudent(id, body, authorization).then(lambda.callback(callback));
};
