/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Application = sequelize.define<any>('application', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  opportunityId: {
    field: 'opportunity_id',
    type: Sequelize.INTEGER,
  },
  studentId: {
    field: 'student_id',
    type: Sequelize.INTEGER,
  },
  applicationStatusId: {
    field: 'application_status_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
