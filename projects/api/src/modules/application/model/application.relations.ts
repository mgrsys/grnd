import { Application } from './application.schema';

import { ApplicationStatus } from '@modules/application/model/application-status.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { Student } from '@modules/student/model/student.schema';

Application.belongsTo(Student, { foreignKey: 'student_id' });
Application.belongsTo(Opportunity, { foreignKey: 'opportunity_id' });
Application.belongsTo(ApplicationStatus, { foreignKey: 'application_status_id' });

export { Application };
