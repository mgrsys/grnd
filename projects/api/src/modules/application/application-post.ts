import { APIGatewayEvent, Callback, Context } from 'aws-lambda';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

import { Application } from '@modules/application/model/application.relations';
import * as user from '@modules/user/user.service';

function ensurePostData(body) {
  const errors: any = {};

  if (!body.opportunityId) {
    errors.title = 'An Opportunity Id is required';
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

function ensureStudent(student) {
  if (!student || !student.get('id')) {
    throw { code: 403, message: 'Invalid Student' };
  }
}

async function applicationPost(body, cognitoId) {
  try {
    await ensurePostData(body);

    const student = await user.cognitoIdGetStudent(cognitoId);
    await ensureStudent(student);

    body.studentId = student.get('id');
    const application = await Application.create(body);

    return lambda.response(201, application.get());
  } catch (e) {
    logger.log('applicationPost', e);
    return lambda.error(e.code, e.message);
  }
}

export default (event: APIGatewayEvent, context: Context, callback: Callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  const cognitoId = lambda.getCognitoId(event);
  applicationPost(body, cognitoId).then(lambda.callback(callback));
};
