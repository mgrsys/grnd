import * as Sequelize from 'sequelize';

export const Timestamps = {
  created_at: Sequelize.DATE,
  deleted_at: Sequelize.DATE,
  updated_at: Sequelize.DATE,
};
