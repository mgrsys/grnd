import * as Sequelize from 'sequelize';

export const Authors = {
  created_by: {
    references: {
      key: 'id',
      model: 'users',
    },
    type: Sequelize.INTEGER,
  },
  deleted_by: {
    references: {
      key: 'id',
      model: 'users',
    },
    type: Sequelize.INTEGER,
  },
  updated_by: {
    references: {
      key: 'id',
      model: 'users',
    },
    type: Sequelize.INTEGER,
  },
};
