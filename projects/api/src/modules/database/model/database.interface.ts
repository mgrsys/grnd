export interface IAuthor {
  created_by?: number;
  deleted_by?: number;
  updated_by?: number;
}

export interface ITimestamp {
  created_at?: Date;
  deleted_at?: Date;
  updated_at?: Date;
}
