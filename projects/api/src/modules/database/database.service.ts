import * as Sequelize from 'sequelize';

import * as Env from './../../utils/env';
const sequelize = new Sequelize({
  database: Env.DB_NAME,
  define: {
    freezeTableName: true,
    paranoid: true,
    underscored: false,
  },
  dialect: 'postgres',
  host: Env.DB_HOST,
  // logging: Env.API_ENV === 'production' ? null : console.info,
  logging: null,
  password: Env.DB_PASS,
  username: Env.DB_USER,
});

sequelize.bulkUpsert = (model, data) => {
  if (!data || data.length < 1) {
    return null;
  }
  if (!model || !model.upsert) {
    throw Error('Invalid model provided to bulkUpsert');
  }
  const promises = data.map(item => model.upsert(item));
  return Promise.all(promises);
};
sequelize.bulkReplace = async (model, data, id) => {
  try {
    // ! Todo add check if data has changed, if not, don't replace
    if (!data || data.length < 1) {
      return null;
    }
    if (!model || !model.upsert) {
      throw Error('Invalid model provided to bulkReplace');
    } 
    await model.update({
      deletedAt: new Date().toDateString(),
    }, {
      where: { opportunityId: id },
    });
    const inserts = data.map(item => model.upsert(item));
    return Promise.all(inserts);

  } catch(e) {
    console.error("Error: ", e);
    return null;
  }
};
sequelize.authenticate();

export {
  sequelize,
};
