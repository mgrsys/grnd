export const testMultipleField = {
  1: { name: 'First Field Name' },
  2: { name: 'Second Field Name' },
  3: { name: 'Third Field Name' },
};

export const testSchools = {
  1: { name: 'First School Name' },
  2: { name: 'Second School Name' },
};

export const testOpportunities = {
  1: {
    description: 'Mining Company',
    status: 'approved',
    title: 'New Experience Job',
  },
  2: {
    description: 'Working at cash register',
    status: 'approved',
    title: 'Supermarket Cashier',
  },
};

export const testSubFieldSkill = {
  1: {
    name: 'First Skill Category',
    skills: {
      1: { name: 'First Field Name' },
      2: { name: 'Second Field Name' },
    },
  },
  2: {
    name: 'Second Skill Category',
    skills: {
      3: { name: 'Third Field Name' },
    },
  },
};

export const testUserStudent = {
  cognitoId: 'test-student',
};

export const testUserSchool = {
  cognitoId: 'test-school',
};

export const testUserOrganisation = {
  cognitoId: 'test-organisation',
};

export const testStudent = {
  email: 'test@student.com',
  firstName: 'Student',
  lastName: 'Tester',
};

export const testOrganisation = {
  description: 'Global Conglomerate',
  email: 'to@evil.com',
  name: 'Evil Corportation',
};
