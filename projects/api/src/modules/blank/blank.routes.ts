import express from 'express';

import { BlankController } from '@modules/blank/blank.controller';
import { asyncHandler } from '@utils/async';

const blankRoutes = express.Router();

blankRoutes.get('/', asyncHandler(
  async (req, res, next) => await new BlankController(req, res, next).listBlanks()),
);

export { blankRoutes };
