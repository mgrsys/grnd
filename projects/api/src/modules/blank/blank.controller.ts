// import * as Sequelize from 'sequelize';

// import { IBlank } from '@modules/blank/model/blank.interface';
// import { Blank } from '@modules/blank/model/blank.schema';

// import {
//   BadRequestException,
//   InternalServerErrorException,
//   UnauthorizedException,
// } from '@utils/exception';

export class BlankController {

  constructor(private req, private res, public next) {}

  public async listBlanks() {
    this.res.status = 200;
    return this.res.send({
        // tslint:disable-next-line:ter-indent
        test: 'BlankController route successful',
    });
  }

}
