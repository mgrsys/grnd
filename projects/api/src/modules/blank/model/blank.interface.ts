import * as Sequelize from 'sequelize';

import { IAuthor, ITimestamp } from '@modules/database/model/database.interface';

export interface IBlank extends IAuthor, ITimestamp {
  id?: number;
}

export interface IBlankAddModel {
  email: string;
  password: string;
}

export interface IBlankModel extends Sequelize.Model<
  IBlank,
  IBlankModel,
  IBlankAddModel
  > { }
