import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Blank = sequelize.define<IBlankModel>('blank', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  email: Sequelize.STRING,
  password: Sequelize.STRING,

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
