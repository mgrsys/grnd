import { APIGatewayEvent, Callback, Context } from 'aws-lambda';

import * as _ from 'lodash';
import * as Sequelize from 'sequelize';

import * as lambda from '@utils/lambda';

import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';
import { TimePeriod } from '@modules/opportunity/model/time-period.schema';
import { SkillCategory } from '@modules/skill/model/skill-category.relations';
import { Skill } from '@modules/skill/model/skill.relations';
import { Subject } from '@modules/subject/schema/subject.schema';
const Op = Sequelize.Op;

function findAllDuration(): Promise<any> {
  return new Promise((resolve, reject) => {
    const duration = {};
    OpportunityDuration.findAll({
      attributes: ['id', 'name'],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    })
      .then((results: any) => {
        if (results.length > 0) {
          _.forEach(results, (model) => {
            duration[model.id] = { name: model.name };
          });
        }
        return duration;
      })
      .then((results: any) => {
        return resolve(results);
      })
      .catch(reject);
  });
}

function findAllTimePeriod(): Promise<any> {
  return new Promise((resolve, reject) => {
    const timePeriod = {};
    TimePeriod.findAll({
      attributes: ['id', 'name'],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    })
      .then((results: any) => {
        if (results.length > 0) {
          _.forEach(results, (model) => {
            timePeriod[model.id] = { name: model.name };
          });
        }
        return timePeriod;
      })
      .then((results: any) => {
        return resolve(results);
      })
      .catch(reject);
  });
}

const findAllSubjects = (): Promise<any> =>
  new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    const subjects = {};
    try {
      const results: any[] = await Subject.findAll({
        attributes: ['id', 'title'],
        where: {
          deletedAt: {
            [Op.eq]: null,
          },
        }
      });
      results.forEach((result: any): void => {
        subjects[result.id] = result.title;
      });
      resolve(subjects);
    } catch(e) {
      reject(e);
    }
  });
function findAllType(): Promise<any> {
  return new Promise((resolve, reject) => {
    const type = {};
    OpportunityType.findAll({
      attributes: ['id', 'name', 'tooltip'],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    })
      .then((results: any) => {
        if (results.length > 0) {
          _.forEach(results, (model) => {
            type[model.id] = { name: model.name, tooltip: model.tooltip };
          });
        }
        return type;
      })
      .then((results: any) => {
        return resolve(results);
      })
      .catch(reject);
  });
}

function findAllSkillCategory(): Promise<any> {
  return new Promise((resolve, reject) => {
    const skillCategory = {};
    SkillCategory.findAll({
      include: [{
        attributes: ['name', 'id'],
        model: Skill,
      }],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    })
      .each(result => {
        skillCategory[result.id] = {
          name: result.name,
          skills: result.skills.reduce((skillsObj, model) => {
            skillsObj[model.id] = { name: model.name }; return skillsObj;
          }, {}),
        };
      })
      .then(() => {
        return resolve(skillCategory);
      })
      .catch(reject);
  });
}

function findAllSkills(): Promise<any> {
  const skills = {};
  return Skill.findAll({
    where: {
      deletedAt: {
        [Op.eq]: null,
      },
    },
  })
    .each(result => {
      skills[result.id] = {
        name: result.name,
      };
    })
    .then(() => {
      return skills;
    });
}

function findAllPrerequisites(): Promise<any> {
  return new Promise((resolve, reject) => {
    const prerequisites = {};
    Prerequisites.findAll({
      attributes: ['id', 'name'],
      where: {
        deletedAt: {
          [Op.eq]: null,
        },
      },
    })
      .then((results: any) => {
        if (results.length > 0) {
          _.forEach(results, (model) => {
            prerequisites[model.id] = { name: model.name };
          });
        }
        return prerequisites;
      })
      .then((results: any) => {
        return resolve(results);
      })
      .catch(reject);
  });
}

async function getData(): Promise<any> {
  let data: any;

  try {
    const durations = await findAllDuration();
    const timePeriod = await findAllTimePeriod();
    const type = await findAllType();
    const skillCategories = await findAllSkillCategory();
    const skills = await findAllSkills();
    const prerequisites = await findAllPrerequisites();
    const subjects = await findAllSubjects();

    data = {
      opportunity: {
        durations,
        prerequisites,
        skillCategories,
        skills,
        timePeriod,
        type,
      },
      subjects,
    };
  } catch (e) {
    console.error('getData() error:', e);
    return lambda.error(e.code, e.message);
  }

  return lambda.response(200, data);

}

export default (event: APIGatewayEvent, context: Context, callback: Callback) => {
  lambda.init(event, context);
  getData().then(lambda.callback(callback));
};
