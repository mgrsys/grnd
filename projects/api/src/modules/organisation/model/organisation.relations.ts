import { Organisation } from './organisation.schema';

import { Contact } from '@modules/contact/model/contact.schema';
import { Location } from '@modules/location/model/location.schema';
import { OrganisationContact } from '@modules/organisation/model/organisation-contact.schema';
import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { OrganisationUser } from '@modules/organisation/model/organisation-user.schema';
import { School } from '@modules/school/model/school.schema';
import { User } from '@modules/user/model/user.schema';

// Blank.belongsTo(Relation);
// Blank.hasMany(Relation);

Organisation.belongsTo(Location, { foreignKey: 'location_id' });
Organisation.belongsToMany(User, { through: 'organisation_user' });
Organisation.belongsToMany(Contact, {
  foreignKey: 'organisation_id',
  otherKey: 'contact_id',
  through: OrganisationContact,
});
Organisation.belongsToMany(School, {
  foreignKey: 'organisation_id',
  otherKey: 'school_id',
  through: OrganisationSchool,
});
Organisation.belongsToMany(User, {
  foreignKey: 'organisation_id',
  otherKey: 'user_id',
  through: OrganisationUser,
});

export { Organisation };
