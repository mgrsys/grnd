/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const OrganisationSchool = sequelize.define<any>('organisation_school', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  organisationId: {
    field: 'organisation_id',
    type: Sequelize.INTEGER,
  },
  schoolId: {
    field: 'school_id',
    type: Sequelize.INTEGER,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
