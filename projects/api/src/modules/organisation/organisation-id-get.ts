
import * as user from '@modules/user/user.service';

import { Contact } from '@modules/contact/model/contact.relations';
import { Organisation } from '@modules/organisation/model/organisation.relations';
import { School } from '@modules/school/model/school.relations';

import * as lambda from '@utils/lambda';

const ORGANISATION_SELF = 'me';

function getOrganisation(id, cognitoId) {
  return new Promise((resolve) => {
    return Promise.resolve()
      .then(() => {
        return user.cognitoIdGetOrganisation(cognitoId);
      })
      .then(organisation => {
        let includes = {};
        let orgId = id;
        if (
          organisation
          && (id === ORGANISATION_SELF || Number(organisation.get('id')) === Number(id))
        ) {
          orgId = organisation.get('id');
          includes = {
            include: [
              {
                attributes: ['id', 'name'],
                model: School,
                required: false,
              }, {
                model: Contact,
                required: false,
              },
            ],
          };
        }
        return Organisation.findById(orgId, includes);
      })
      .then(organisation => {
        if (!organisation) {
          throw { code: 404, message: 'Organisation Not Found' };
        }

        return organisation;
      })
      .then(organisationModel => {
        const data = organisationModel.get();
        return resolve(lambda.response(200, data));
      })
      .catch(e => {
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  const cognitoId = lambda.getCognitoId(event);
  getOrganisation(id, cognitoId).then(lambda.callback(callback));
};
