import * as _ from 'lodash';

import * as user from '@modules/user/user.service';

import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { Organisation } from '@modules/organisation/model/organisation.relations';
import { School } from '@modules/school/model/school.relations';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

function ensurePostData(body) {
  const errors: any = {};
  if (!body.name) {
    errors.name = 'Name is required';
  }
  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }
  return true;
}

function ensureOrganisationSchool(organisationModel, organisationData) {
  if (_.isArray(organisationData.schoolIds) && organisationData.schoolIds.length > 0) {
    const schoolData = _.map(organisationData.schoolIds, schoolId => {
      return {
        organisationId: organisationModel.id,
        schoolId,
      };
    });
    return OrganisationSchool.bulkUpsert(schoolData);
  }
  return;
}

function organisationPost(body) {
  let organisationModel;
  return Promise.resolve()
    .then(() => {
      return ensurePostData(body);
    })
    .then(userRecord => {
      return Organisation.create(body);
    })
    .then(organisation => {
      organisationModel = organisation;
      if (body.username && body.password) {
        return user.ensureCognitoUser(
          body.username, body.password, body.username, null, null, null, null, 'organisation',
        );
      }
      return null;
    })
    .then(userModel => {
      if (userModel) {
        return organisationModel.addUser(userModel);
      }
      return null;
    })
    .then(() => {
      return ensureOrganisationSchool(organisationModel, body);
    })
    .then(() => {
      return Organisation.findOne({
        include: {
          attributes: ['id', 'name'],
          model: School,
          required: false,
          through: { attributes: [] },
        },
        where: {
          id: organisationModel.get('id'),
        },
      });
    })
    .then(model => {
      const data = model.get();
      return lambda.response(201, data);
    })
    .catch(e => {
      logger.log('organisationPost', e);
      return lambda.error(e.code, e.message);
    });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  organisationPost(body).then(lambda.callback(callback));
};
