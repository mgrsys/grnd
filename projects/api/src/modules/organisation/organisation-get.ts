import * as Sequelize from 'sequelize';

import { Organisation } from '@modules/organisation/model/organisation.relations';
import * as lambda from '@utils/lambda';

const Op = Sequelize.Op;

function ensureQueryData(query) {
  const errors: any = {};
  if (!query.name) {
    errors.name = 'A name is required';
  }
  if (query.name && query.name.length < 3) {
    errors.name = 'You must enter at least 3 characters';
  }
  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }
  return true;
}

function findOrganisationByName(name) {
  return new Promise((resolve, reject) => {
    Organisation.findAll({
      where: {
        name: {
          [Op.iLike]: `%${name}%`,
        },
      },
    }).then((results: any) => {
      if (!results.length) {
        throw { code: 404, message: 'Organisation Not Found' };
      }
      return results;
    })
      .then((results: any) => {
        return resolve(results);
      })
      .catch(reject);
  });
}

function getOrganisation(query) {
  return new Promise((resolve) => {
    const { name } = query;
    Promise.resolve()
      .then(() => {
        return ensureQueryData(query);
      })
      .then(() => {
        return findOrganisationByName(name);
      })
      .then((results: any) => {
        return resolve(lambda.response(200, results));
      })
      .catch(e => {
        return resolve(lambda.error(e.code, e.message));
      });
  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const query = lambda.getQuery(event);
  getOrganisation(query).then(lambda.callback(callback));
};
