import { School } from '@modules/school/model/school.schema';

import { OpportunitySchool } from '@modules/opportunity/model/opportunity-school.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { Organisation } from '@modules/organisation/model/organisation.schema';
import { SchoolUser } from '@modules/school/model/school-user.schema';
import { StudentSchool } from '@modules/student/model/student-school.schema';
import { Student } from '@modules/student/model/student.schema';
import { User } from '@modules/user/model/user.schema';

School.belongsToMany(Organisation, {
  foreignKey: 'school_id',
  otherKey: 'organisation_id',
  through: OrganisationSchool,
});
School.belongsToMany(Opportunity, {
  foreignKey: 'school_id',
  otherKey: 'opportunity_id',
  through: OpportunitySchool,
});
School.belongsToMany(Student, {
  foreignKey: 'school_id',
  otherKey: 'student_id',
  through: StudentSchool,
});
School.belongsToMany(User, { through: SchoolUser });

export { School };
