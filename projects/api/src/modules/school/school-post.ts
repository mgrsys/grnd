import { School } from '@modules/school/model/school.relations';
import * as user from '@modules/user/user.service';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

function ensurePostData(body) {
  const errors: any = {};
  if (!body.name) {
    errors.name = 'Name is required';
  }
  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }
  return true;
}

function schoolPost(body) {
  return new Promise((resolve) => {
    let schoolModel;
    Promise.resolve()
      .then(() => {
        return ensurePostData(body);
      })
      .then(() => {
        return School.create(body);
      })
      .then(school => {
        schoolModel = school;
        if (body.username && body.password) {
          return user.ensureCognitoUser(
            body.username, body.password, body.username, null, null, null, null, 'school',
          );
        }
        return null;
      })
      .then(userModel => {
        if (userModel) {
          return schoolModel.addUser(userModel);
        }
        return null;
      })
      .then(() => {
        const data = schoolModel.get();
        return resolve(lambda.response(201, data));
      })
      .catch(e => {
        logger.log('schoolPost', e);
        return resolve(lambda.error(e.code, e.message));
      });

  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  schoolPost(body).then(lambda.callback(callback));
};
