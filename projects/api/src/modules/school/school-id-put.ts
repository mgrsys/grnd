
import * as user from '@modules/user/user.service';

import { School } from '@modules/school/model/school.relations';

import * as lambda from '@utils/lambda';
import logger from '@utils/logger';

function updateSchool(id, body, authorization?: any) {
  if (authorization) {
    console.warn('school-id-put.ts: authorization check is no-op');
  }
  return new Promise((resolve) => {
    let schoolModel;
    Promise.resolve()
      .then(() => {
        return School.findById(id);
      })
      .then(school => {
        if (!school) {
          throw { code: 404, message: 'School Not Found' };
        }
        return school.update(body);
      })
      .then(school => {
        schoolModel = school;
        if (body.username && body.password) {
          return user.ensureCognitoUser(
            body.username, body.password, body.username, null, null, null, null, 'school',
          );
        }
        return null;
      })
      .then(userModel => {
        if (userModel) {
          return schoolModel.addUser(userModel);
        }
        return null;
      })
      .then(() => {
        const data = schoolModel.get();
        return resolve(lambda.response(200, data));
      })
      .catch(e => {
        logger.log('schoolPost', e);
        return resolve(lambda.error(e.code, e.message));
      });

  });
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const { id } = lambda.getParams(event);
  const authorization = lambda.getAuthorization(event);
  const body = lambda.getBody(event);
  updateSchool(id, body, authorization).then(lambda.callback(callback));
};
