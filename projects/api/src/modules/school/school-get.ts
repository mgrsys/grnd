import * as Sequelize from 'sequelize';
import { School } from '@modules/school/model/school.relations';
import * as lambda from '@utils/lambda';
import * as user from '@modules/user/user.service';

const Op = Sequelize.Op;

function ensureQueryData(query) {
  const errors: any = {};

  if (query.name.length < 3) {
    errors.name = 'You must enter at least 3 characters';
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

const findSchoolByName = async (name): Promise<any> => {
  try {
    const results = await School.findAll({
      where: {
        name: {
          [Op.iLike]: `%${name}%`,
        },
        verified: {
          [Op.eq]: true,
        },
      },
    });

    if (!results.length) {
      throw { code: 404, message: 'School Not Found' };
    }

    return results;
  } catch (e) {
    throw e;
  }
}

const getSchool = async (query, cognitoId) => {
  try {
    const { name } = query;
    if (name) {
      await ensureQueryData(query);

      const results = await findSchoolByName(name);
      return lambda.response(200, results);
    }

    const userModel = await user.ensureValidUser(cognitoId);
    const school = await user.isSchool(userModel);
    return lambda.response(200, [school]);
  } catch (e) {
    return lambda.error(e.code, e.message);
  }
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const query = lambda.getQuery(event);
  const cognitoId = lambda.getCognitoId(event);
  getSchool(query, cognitoId).then(lambda.callback(callback));
};
