import {
  compileTemplate,
  sendMail,
} from '@utils/mail';

import { Callback, Context } from 'aws-lambda';

import { API_URL, AWS_COGNITO_USER_POOL_ID } from '@utils/env';

import * as _ from 'lodash';

// tslint:disable-next-line:import-name
import SignupTemplate from '@modules/signup/signup.template';

export default (event: any, context: Context, callback: Callback): void => {
  context.callbackWaitsForEmptyEventLoop = false;
  if (event.userPoolId === AWS_COGNITO_USER_POOL_ID) {
    
    if (event.triggerSource === 'CustomMessage_SignUp') {

      const template = compileTemplate(SignupTemplate, {
        code: event.request.codeParameter,
        firstName: event.request.userAttributes.given_name,
        username: event.request.userAttributes.email,
      });

      event.response.smsMessage = `Welcome to Grandshake, ${event.request.userAttributes.given_name}`;
      event.response.emailSubject = `Welcome to Grandshake, ${event.request.userAttributes.given_name}`;
      event.response.emailMessage = template.replace(/\u00A0/g, '');

    }
  }
  console.info('response', JSON.stringify(event.response, null, 2));
  callback(null, event);

};
