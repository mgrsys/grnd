import { Contact } from '@modules/contact/model/contact.relations';
import { User } from '@modules/user/model/user.relations';
import { Organisation } from '@modules/organisation/model/organisation.relations';
import { OrganisationContact } from '@modules/organisation/model/organisation-contact.schema';
import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { OrganisationUser } from '@modules/organisation/model/organisation-user.schema';

import logger from '@utils/logger';
import * as userService from '@modules/user/user.service';
import * as lambda from '@utils/lambda';

import * as AWS from 'aws-sdk';

import {
  AWS_COGNITO_USER_POOL_CLIENT_ID,
  AWS_COGNITO_USER_POOL_ID,
} from '@utils/env';

function ensurePostData(body) {
  const errors: any = {};

  if (!body.accountName) {
    errors.name = 'Account Name is required';
  }

  if (!body.fullName) {
    errors.name = 'Full Name is required';
  }

  if (!body.accountEmail) {
    errors.name = 'Account email is required';
  }

  if (!body.password) {
    errors.name = 'Temporary password is required';
  }

  if (Object.keys(errors).length > 0) {
    throw { code: 400, message: errors };
  }

  return true;
}

async function contactPost(body, cognitoId) {
  try {
    await ensurePostData(body);

    // create new user
    const newCognitoId = await getNewCognitoId(body);
    const user = await User.create({ cognitoId: newCognitoId });

    // create contact with the same data
    const {
      accountName,
      fullName,
      accountEmail,
    } = body;
    const contact = await Contact.create({
      // hope it will contain both
      firstName: fullName.split(' ')[0],
      lastName: fullName.split(' ')[1],
      email: accountEmail,
    });

    // create organisation
    const organisation = await Organisation.create({
      name: accountName,
      email: accountEmail,
    });

    // add relation to newly created contact
    await OrganisationContact.create({
      organisationId: organisation.get('id'),
      contactId: contact.get('id'),
    });

    // add relation to newly created user
    await OrganisationUser.create({
      organisationId: organisation.get('id'),
      userId: user.get('id'),
    });

    // add relation to school from the creator
    const creator = await User.findOne({ where: { cognitoId } });
    const school = await userService.isSchool(creator);

    if (school) {
      await OrganisationSchool.create({
        organisationId: organisation.get('id'),
        schoolId: school.get('id'),
      });
    }

    return lambda.response(201, { cognitoId: newCognitoId });
  } catch (e) {
    logger.log('contactPost', e);
    return lambda.error(e.code, e.message);
  }
}

async function getNewCognitoId({ accountEmail, password }: { accountEmail: string, password: string }) {
  const credentials = new AWS.SharedIniFileCredentials({ profile: 'grandshake' });
  AWS.config.credentials = credentials;

  const idp = new AWS.CognitoIdentityServiceProvider({
    region: 'ap-southeast-2',
  });

  const { User: { Attributes } } = await idp.adminCreateUser({
    Username: accountEmail,
    UserAttributes: [{ Name: 'email', Value: accountEmail }, { Name: 'custom:role', Value: 'organisation' }],
    TemporaryPassword: password,
    UserPoolId: AWS_COGNITO_USER_POOL_ID,
    ForceAliasCreation: false,
    DesiredDeliveryMediums: ['EMAIL'],
  }, null).promise();

  const { Value: cognitoId } = Attributes.find(({ Name }) => Name === 'sub');
  const { Session: session } =
    await idp.adminInitiateAuth({
      AuthFlow: 'ADMIN_NO_SRP_AUTH',
      AuthParameters: {
        PASSWORD: password,
        USERNAME: accountEmail,
      },
      ClientId: AWS_COGNITO_USER_POOL_CLIENT_ID,
      UserPoolId: AWS_COGNITO_USER_POOL_ID,
    }, null).promise();

  await idp.adminRespondToAuthChallenge({
    ChallengeName: 'NEW_PASSWORD_REQUIRED',
    ChallengeResponses: {
      NEW_PASSWORD: password,
      USERNAME: accountEmail,
    },
    ClientId: AWS_COGNITO_USER_POOL_CLIENT_ID,
    UserPoolId: AWS_COGNITO_USER_POOL_ID,
    Session: session,
  }, null).promise()

  return cognitoId;
}

export default (event, context, callback) => {
  lambda.init(event, context);
  const body = lambda.getBody(event);
  const cognitoId = lambda.getCognitoId(event);
  contactPost(body, cognitoId).then(lambda.callback(callback));
};
