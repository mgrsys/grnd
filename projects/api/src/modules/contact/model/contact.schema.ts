/* tslint:disable:object-literal-sort-keys */

import * as Sequelize from 'sequelize';

import { sequelize } from '@modules/database/database.service';

// import { IBlankModel } from '@modules/blank/model/blank.interface';

export const Contact = sequelize.define<any>('contact', {
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },

  firstName: {
    field: 'first_name',
    type: Sequelize.STRING,
  },
  middleName: {
    field: 'middle_name',
    type: Sequelize.STRING,
  },
  lastName: {
    field: 'last_name',
    type: Sequelize.STRING,
  },
  email: {
    name: 'email',
    type: Sequelize.STRING,
  },
  position: {
    name: 'position',
    type: Sequelize.STRING,
  },
  phoneNumber: {
    field: 'phone_number',
    type: Sequelize.STRING,
  },
  picture: {
    type: Sequelize.TEXT,
  },

  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
}, {
  paranoid: true,
  underscored: true,
});
