import { Contact } from './contact.schema';

import { OrganisationContact } from '@modules/organisation/model/organisation-contact.schema';
import { Organisation } from '@modules/organisation/model/organisation.schema';

Contact.belongsToMany(Organisation, {
  foreignKey: 'contact_id',
  otherKey: 'organisation_id',
  through: OrganisationContact,
});

export { Contact };
