
import applicationPost from '@modules/application/application-post';
import dataGet from '@modules/data-get';
import locationGet from '@modules/location/location-get';
import opportunityGet from '@modules/opportunity/opportunity-get';
import opportunityIdDelete from '@modules/opportunity/opportunity-id-delete';
import opportunityIdGet from '@modules/opportunity/opportunity-id-get';
import opportunityIdPut from '@modules/opportunity/opportunity-id-put';
import opportunityPost from '@modules/opportunity/opportunity-post';
import organisationGet from '@modules/organisation/organisation-get';
import organisationIdGet from '@modules/organisation/organisation-id-get';
import organisationPost from '@modules/organisation/organisation-post';
import schoolGet from '@modules/school/school-get';
import schoolIdPut from '@modules/school/school-id-put';
import schoolPost from '@modules/school/school-post';
import studentIdGet from '@modules/student/student-id-get';
import studentIdPut from '@modules/student/student-id-put';
import studentPost from '@modules/student/student-post';
import skillPost from '@modules/skill/skill-post';
import contactPost from '@modules/contact/contact-post';

import signupTrigger from '@modules/signup/signup-trigger';

import { getProfile, putProfileUpdateMain, putProfile, postProfileAddExperience, postProfileAddContact, postProfileAddPicture } from '@modules/profile/profile.route';

export {
  applicationPost,
  dataGet,
  locationGet,
  opportunityGet,
  opportunityIdGet,
  opportunityIdPut,
  opportunityIdDelete,
  opportunityPost,
  organisationGet,
  organisationIdGet,
  organisationPost,
  schoolGet,
  schoolIdPut,
  schoolPost,

  signupTrigger,

  studentIdGet,
  studentIdPut,
  studentPost,

  getProfile,
  putProfile,
  postProfileAddExperience,
  postProfileAddContact,
  postProfileAddPicture,
  putProfileUpdateMain,

  skillPost,
  contactPost,
};
