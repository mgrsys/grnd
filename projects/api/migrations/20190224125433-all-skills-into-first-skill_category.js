'use strict';

module.exports = {
  async up(queryInterface) {
    return queryInterface.sequelize.query(
      'UPDATE skill_category_skill SET skill_category_id = 1 WHERE id IS NOT NULL'
    );
  },

  async down() {
    // no down migration
  }
};
