'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `
        INSERT INTO time_period (id, name, created_at, updated_at)
        VALUES (6, 'Other', NOW(), NOW())
      `
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `
        DELETE FROM time_period WHERE name = 'Other'
      `
    );
  }
};
