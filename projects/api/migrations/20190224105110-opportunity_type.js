module.exports = {
  async up(queryInterface) {
    await queryInterface.sequelize.query(
      'DELETE FROM opportunity_type WHERE id > 6'
    );

    return queryInterface.sequelize.query(
      `
        INSERT INTO opportunity_type (id, name, created_at, updated_at) VALUES
        (7, 'Certificate I (VET Course)', NOW(), NOW()),
        (8, 'Certificate II (VET Course)', NOW(), NOW()),
        (9, 'Certificate III (VET Course)', NOW(), NOW()),
        (10, 'Certificate IV (VET Course)', NOW(), NOW()),
        (11, 'Diploma (VET Course)', NOW(), NOW()),
        (12, 'Day tour', NOW(), NOW()),
        (13, 'Event', NOW(), NOW()),
        (14, 'Other', NOW(), NOW())
      `
    );
  },

  async down() {
    // no down migration
  }
};
