'use strict';

module.exports = {
  async up(queryInterface) {
    // there was a duplicated value
    await queryInterface.sequelize.query(
      `DELETE FROM subject WHERE id IN (SELECT id FROM subject WHERE title = 'Chemistry' LIMIT 1)`
    );

    return queryInterface.sequelize.query(
      `
        INSERT INTO subject (title, created_at, updated_at) VALUES
        ('Aerospace Systems', NOW(), NOW()),
        ('Business Technology', NOW(), NOW()),
        ('CAD', NOW(), NOW()),
        ('Career Education', NOW(), NOW()),
        ('Chinese Extension', NOW(), NOW()),
        ('Design', NOW(), NOW()),
        ('Digital Solutions', NOW(), NOW()),
        ('Earth & Environmental Science', NOW(), NOW()),
        ('Electronics', NOW(), NOW()),
        ('Engineering', NOW(), NOW()),
        ('English & Literature Extension', NOW(), NOW()),
        ('English as an Additional Language', NOW(), NOW()),
        ('English Extension', NOW(), NOW()),
        ('Essential English', NOW(), NOW()),
        ('Essential Mathematics', NOW(), NOW()),
        ('Furnishing', NOW(), NOW()),
        ('General Mathematics', NOW(), NOW()),
        ('German Extension', NOW(), NOW()),
        ('Health', NOW(), NOW()),
        ('Health & Physical Education', NOW(), NOW()),
        ('History', NOW(), NOW()),
        ('Hospitality', NOW(), NOW()),
        ('Information & Communication Technology Skills', NOW(), NOW()),
        ('Information Technology', NOW(), NOW()),
        ('Literature', NOW(), NOW()),
        ('Mathematics Extension', NOW(), NOW()),
        ('Mathematics Methods', NOW(), NOW()),
        ('Media', NOW(), NOW()),
        ('Multi Media Studies', NOW(), NOW()),
        ('Music Extension', NOW(), NOW()),
        ('Philosophy', NOW(), NOW()),
        ('Psychology', NOW(), NOW()),
        ('QLD Ballet', NOW(), NOW()),
        ('Science', NOW(), NOW()),
        ('Science Core', NOW(), NOW()),
        ('Science Extension', NOW(), NOW()),
        ('Specialist Mathematics', NOW(), NOW()),
        ('Sport & Recreation', NOW(), NOW()),
        ('Sports Performance Analysis', NOW(), NOW()),
        ('Tennis Excellence', NOW(), NOW())
      `
    );
  },

  down: () => {
    // no down migration
  }
};
