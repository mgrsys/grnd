module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query(
      `CREATE INDEX location_id_user_ex ON user_experience(location_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX location_id_student ON student(residential_location_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX student_id_profile ON profile(student_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX skill_id_profile_has_skill ON profile_has_skill(skill_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX skill_id_profile_wants_skill ON profile_wants_skill(skill_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX profile_id_profile_has_skill ON profile_has_skill(profile_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX profile_id_profile_wants_skill ON profile_wants_skill(profile_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX skill_id_user_ex ON user_experience_skill(skill_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX opportunity_type_id_user_ex ON user_experience(opportunity_type_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX profile_id_emergency_contacts ON emergency_contacts(profile_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX subject_id_profile_subject ON profile_subject(subject_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX profile_id_profile_subject ON profile_subject(profile_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX school_id_student ON student_school(school_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX profile_id_user_ex ON profile_user_experience(profile_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX user_ex_id_user_ex ON profile_user_experience(user_experience_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX student_id_school ON student_school(student_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX user_ex_skill_id_user_ex ON user_experience_skill(user_experience_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX contact_id_emergency_contacts ON emergency_contacts(contact_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX prerequisites_id_opportunity ON opportunity_prerequisites(prerequisites_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX opportunity_id_opportunity_prerequisites ON opportunity_prerequisites(opportunity_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX skill_id_opportunity_skill ON opportunity_skill(skill_id)`
    );

    await queryInterface.sequelize.query(
      `CREATE INDEX opportunity_id_opportunity_skill ON opportunity_skill(opportunity_id)`
    );
  },

  down: (queryInterface, Sequelize) => {
    // no down, just speed up
  }
};
