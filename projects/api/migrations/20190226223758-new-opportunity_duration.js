'use strict';

module.exports = {
  async up(queryInterface) {
    await queryInterface.sequelize.query(
      `UPDATE opportunity_duration SET name = 'Day/s' WHERE id = 1`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_duration SET name = 'Month/s' WHERE id = 2`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_duration SET name = 'Year/s' WHERE id = 3`
    );

    await queryInterface.sequelize.query(
      `DELETE FROM opportunity_duration WHERE id > 3`
    );

    await queryInterface.sequelize.query(
      'ALTER TABLE opportunity RENAME COLUMN other_duration TO duration'
    );

    await queryInterface.sequelize.query(
      'UPDATE opportunity SET duration = 1, opportunity_duration_id = 2 WHERE opportunity_duration_id IN (4, 5)'
    );

    return queryInterface.sequelize.query(
      'ALTER TABLE opportunity_duration DROP COLUMN length_days'
    );
  },

  down: () => {
    // no down migration
  }
};
