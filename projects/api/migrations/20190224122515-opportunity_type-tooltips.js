'use strict';

module.exports = {
  async up(queryInterface) {
    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'Work experience is designed to get a feel for working life, learn about a job or industry first hand and exposure to on-the-job skills.' WHERE name = 'Work Experience'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'Structured workplace learning provides students with the opportunity to integrate on-the-job experience with secondary study.' WHERE name = 'Structured W/E'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'A job shadow can be as simple as an hour-long shadowing a worker, to an extended week-long stay allowing interaction with numerous staff and observation of a variety of activities at a workplace.' WHERE name = 'Job Shadow'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'Depending on the type of employment that’s being offered (whether it’s permanent, part-time or casual), you will get paid for your time.' WHERE name = 'Paid Work'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'A traineeship is a training agreement between the trainee and their respective employer whereby the employer agrees to train the trainee in a specific industry, and the trainee agrees to work and learn.' WHERE name = 'Traineeship'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'An apprenticeship is a structured training arrangement of usually three and a half or four years duration. The training combines practical experience at work with complementary off-the-job training with a Registered Training Organisation (RTO).' WHERE name = 'Apprenticeship'`
    );

    await queryInterface.sequelize.query(
      `UPDATE opportunity_type SET tooltip = 'Vocational Education and Training (VET) is an education and training that focuses on providing skills for work. Certificate I to IV: These courses provide introductory skills and training. They teach industry-specific knowledge and skills in communication, literacy and numeracy, and teamwork. They vary in length from six months to two years.' WHERE name LIKE '%VET Course%'`
    );

    return queryInterface.sequelize.query(
      `UPDATE opportunity_type SET name = 'Structured Work Learning' WHERE name = 'Structured W/E'`
    );
  },

  async down() {
    // no down migration
  }
};
