'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(
      'ALTER TABLE opportunity RENAME COLUMN working_hours_id TO working_hours'
    );
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query(
      'ALTER TABLE opportunity RENAME COLUMN working_hours TO working_hours_id'
    );
  }
};
