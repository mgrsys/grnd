import { generateHash } from './../src/utils/hash';

const args = process.argv.slice(2);

if (!args.length) {
  console.error('Must provide password as first parameter');
  process.exit();
}

(async () => {
  const password = args[0];
  const hash = await generateHash(password);
  console.info('password', password);
  console.info('hash', hash);
  process.exit();
})();
