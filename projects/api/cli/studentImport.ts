import * as dotenv from 'dotenv';
dotenv.config();

import * as parse from 'csv-parse';
import * as fs from 'fs';
import * as ora from 'ora';

import { School } from '@modules/school/model/school.relations';
import { StudentSchool } from '@modules/student/model/student-school.schema';
import { Student } from '@modules/student/model/student.relations';

const spinner = ora();

// Main Thread
(async () => {
  try {
    // Inputs
    const schoolName = 'Kelvin Grove State College';
    const inputCsv = './tmp/kgsc_import.csv';

    // Get School
    spinner.start(`Finding record for school "${schoolName}"`);
    const school = await getSchool(schoolName);
    spinner.succeed(`Found school, School ID ${school.id}`);

    // Read Students
    spinner.start('Reading students from file.. \n');
    const studentData = await readStudentCsv(inputCsv);
    spinner.succeed(`Total students to import: ${studentData.length}`);

    // Create Student Records with StudentSchool relation
    spinner.start('Creating and associating students with school.. \n');
    await registerStudents(studentData, school.id);
    spinner.succeed('Students registered!');

  } catch (e) {
    spinner.fail('Error importing students:');
    console.error(e);
  }
  process.exit();
})();

// Functions
async function getSchool(schoolName) {
  const school = await School.findOne({ where: { name: schoolName } });
  if (!school) {
    throw Error(`Unable to find school with name ${schoolName}`);
  }
  return school;
}

async function readStudentCsv(input: string): Promise<any> {
  return new Promise((res, rej) => {
    const csvData = [];
    fs.createReadStream(input)
      .pipe(parse({ delimiter: ',' }))
      .on('data', csvrow => csvData.push(csvrow))
      .on('end', () => res(csvData.slice(1)))
      .on('error', err => rej(err));
  });
}

async function registerStudents(students: string[], schoolId: number) {
  const newStudents = students.map((x) => ({
    dob: x[3],
    email: x[4],
    firstName: x[1],
    lastName: x[2],
    schoolYear: x[0],

    student_schools: [
      { schoolId },
    ],
  }));
  for (const newStudent of newStudents) {
    await Student.create(newStudent, {
      include: [StudentSchool],
    });
  }
  return;
}
