import * as dotenv from 'dotenv';
dotenv.config();

import * as AWS from 'aws-sdk';
import * as inquirer from 'inquirer';
import * as ora from 'ora';

import {
  AWS_COGNITO_USER_POOL_CLIENT_ID,
  AWS_COGNITO_USER_POOL_ID,
} from './../src/utils/env';

// Set Grandshake profile
const credentials = new AWS.SharedIniFileCredentials({ profile: 'grandshake' });
AWS.config.credentials = credentials;

// TMP
// (async () => {
//   const result = await getUserSession();
//   console.log('result', result);
//   process.exit();
// })();

(async () => {
  const option = await mainMenu();
  switch (option) {
    case 'create user':
      await createUser();
      break;
    default:
      console.info('No valid option selected!');
  }
  process.exit();
})();

async function mainMenu(): Promise<string> {
  const answers = await inquirer
    .prompt([
      {
        choices: ['create user'],
        message: 'Select an option',
        name: 'option',
        type: 'list',
      },
    ]);
  return answers.option;
}

async function createUser() {
  const answers = await inquirer
    .prompt([
      {
        message: 'Target user email address',
        name: 'email',
        type: 'input',
        validate: input => !!input,
      },
      {
        default: 'Grand@2018',
        message: 'Password',
        name: 'password',
        type: 'input',
        validate: input => !!input,
      },
      {
        choices: ['student', 'organisation', 'school'],
        message: 'User role',
        name: 'role',
        type: 'list',
      },
      {
        default: false,
        message: 'Are you sure you want to create this user?',
        name: 'confirm',
        type: 'confirm',
      },
    ]);
  if (!answers.confirm) {
    console.info('Confirmation failed, exiting');
    return;
  }

  const spinner = ora('Creating user.. \n');
  spinner.start();

  console.info('answers', answers);
  const session = await getUserSession();
  console.info('session', session);
  spinner.stop();
  return;
}

async function getUserSession() {
  const idp = new AWS.CognitoIdentityServiceProvider({
    region: 'ap-southeast-2',
  });
  const initiateAuth = await idp.adminInitiateAuth({
    AuthFlow: 'ADMIN_NO_SRP_AUTH',
    AuthParameters: {
      PASSWORD: 'Grand@2018',
      USERNAME: 'dan@danitt.com',
    },
    ClientId: AWS_COGNITO_USER_POOL_CLIENT_ID,
    UserPoolId: AWS_COGNITO_USER_POOL_ID,
  }, null).promise();
  return initiateAuth;
}
