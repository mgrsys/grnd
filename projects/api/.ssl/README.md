# Setting up development environment with SSL
1. Run the following in this folder to generate your own certificate files, using defaults
```bash
  $ openssl req -config cert.conf -new -x509 -newkey rsa:2048 -nodes -days 365 -out cert.pem
  # Optional: inspect certificate contents
  $ openssl x509 -in cert.pem -text -noout
```

2. Add certificate to Keychain + Trust
- open Keychain Assistant
- drag cert.pem into certificates
- find certificate in list, double click to open, expand 'Trust' section, and select 'Always Trust'

3. Update /etc/hosts, add this line to end of file:
  127.0.0.1       api.grandshake.test
