import * as dotenv from 'dotenv';
dotenv.config();

import { ApplicationStatus } from '@modules/application/model/application-status.schema';
import { Application } from '@modules/application/model/application.schema';
import { Contact } from '@modules/contact/model/contact.schema';
import { Industry } from '@modules/industry/model/industry.schema';
import { Location } from '@modules/location/model/location.schema';
import { OpportunityDuration } from '@modules/opportunity/model/opportunity-duration.schema';
import { OpportunitySchool } from '@modules/opportunity/model/opportunity-school.schema';
import { OpportunitySkill } from '@modules/opportunity/model/opportunity-skill.schema';
import { OpportunityTimePeriod } from '@modules/opportunity/model/opportunity-time-period.schema';
import { OpportunityPrerequisites } from '@modules/opportunity/model/opportunity-prerequisites.schema';
import { OpportunityType } from '@modules/opportunity/model/opportunity-type.schema';
import { OpportunityTypes } from '@modules/opportunity/model/opportunity-types.schema';
import { Opportunity } from '@modules/opportunity/model/opportunity.schema';
import { OrganisationContact } from '@modules/organisation/model/organisation-contact.schema';
import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { OrganisationUser } from '@modules/organisation/model/organisation-user.schema';
import { Organisation } from '@modules/organisation/model/organisation.schema';
import { Prerequisites } from '@modules/opportunity/model/prerequisites.schema';
import { SchoolUser } from '@modules/school/model/school-user.schema';
import { School } from '@modules/school/model/school.schema';
import { SkillCategorySkill } from '@modules/skill/model/skill-category-skill.schema';
import { SkillCategory } from '@modules/skill/model/skill-category.schema';
import { Skill } from '@modules/skill/model/skill.schema';
import { StudentSchool } from '@modules/student/model/student-school.schema';
import { StudentUser } from '@modules/student/model/student-user.schema';
import { Student } from '@modules/student/model/student.schema';
import { Subject } from '@modules/subject/schema/subject.schema';

import { User } from '@modules/user/model/user.schema';
import { Profile } from '@modules/profile/schema/profile.schema';
import { ProfileHasSkill } from '@modules/profile/schema/profile.has.skill.schema';
import { ProfileWantsSkill } from '@modules/profile/schema/profile.wants.skill.schema';
import { ProfileSubject } from '@modules/profile/schema/profile.subject.schema';
import { ProfileUserExperience } from '@modules/profile/schema/profile.user.experience.schema';
import { Experience } from '@modules/profile/schema/experience.schema';
import { EmergencyContacts } from '@modules/profile/schema/emergency.contacts.schema';
import { UserExperience } from '@modules/profile/schema/user.experience.schema';
import { UserExperienceSkill } from '@modules/profile/schema/user.experience.skill.schema';

const models = [
  ApplicationStatus,
  Application,
  Contact,
  Industry,
  Location,
  OpportunityDuration,
  OpportunityPrerequisites,
  OpportunitySchool,
  OpportunitySkill,
  OpportunityTimePeriod,
  OpportunityType,
  Opportunity,
  OpportunityTypes,
  OrganisationContact,
  OrganisationSchool,
  OrganisationUser,
  Organisation,
  Prerequisites,
  Profile,
  ProfileHasSkill,
  ProfileWantsSkill,
  ProfileSubject,
  Experience,
  EmergencyContacts,
  SchoolUser,
  School,
  SkillCategorySkill,
  SkillCategory,
  Skill,
  StudentSchool,
  StudentUser,
  Student,
  Subject,
  User,
  UserExperience,
  UserExperienceSkill,
  ProfileUserExperience,
];

(async () => {
  try {
    await Promise.all(models.map(model => model.sync()));
  } catch (e) {
    console.error('Unable to sync models', e);
  }

  process.exit();
})();
