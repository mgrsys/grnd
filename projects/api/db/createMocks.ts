import * as dotenv from 'dotenv';
dotenv.config();
import * as _ from 'lodash';

import * as mockData from 'src/modules/database/database.mocks';

import { Opportunity } from '@modules/opportunity/model/opportunity.relations';
import { OrganisationSchool } from '@modules/organisation/model/organisation-school.schema';
import { OrganisationUser } from '@modules/organisation/model/organisation-user.schema';
import { Organisation } from '@modules/organisation/model/organisation.relations';
import { School } from '@modules/school/model/school.relations';
import { StudentSchool } from '@modules/student/model/student-school.schema';
import { Student } from '@modules/student/model/student.relations';
import { User } from '@modules/user/model/user.relations';

const testOpportunities = mockData.testOpportunities;
let testOrganisationModel = null;
let testSchoolModels = null;

const removeKeys = (objectWithKeys) =>  _.map(objectWithKeys, n => n);

Promise.resolve()
  .then(() => {
    return School.bulkCreate(removeKeys(mockData.testSchools), { returning: true });
  })
  .then((result) => {
    testSchoolModels = result;
    return Organisation.create(mockData.testOrganisation);
  })
  .then((result) => {
    testOrganisationModel = result;
    const relatedSchoolModels = [];
    _.forEach(testSchoolModels, school => {
      relatedSchoolModels.push({
        organisationId: testOrganisationModel.get('id'),
        schoolId: school.get('id'),
      });
    });
    return OrganisationSchool.bulkCreate(relatedSchoolModels);
  })
  .then(() => {
    const relatedOpportunities = removeKeys(testOpportunities);
    _.forEach(relatedOpportunities, opportunity => {
      opportunity.organisation_id = testOrganisationModel.get('id');
    });
    return Opportunity.bulkCreate(relatedOpportunities);
  })
  .then(() => {
    return User.create(mockData.testUserOrganisation);
  })
  .then((organisationUser) => {
    return OrganisationUser.create({
      organisationId: testOrganisationModel.get('id'),
      userId: organisationUser.get('id'),
    });
  })
  .then(() => {
    return User.create(mockData.testUserStudent);
  })
  .then((studentUser) => {
    const relatedStudent: any = mockData.testStudent;
    relatedStudent.userId = studentUser.get('id');
    return Student.create(relatedStudent);
  })
  .then(student => {
    return StudentSchool.create({ studentId: student.get('id'), schoolId: 1 });
  })
  .then(() => {
    return User.create(mockData.testUserSchool);
  })
  .then(() => process.exit());
