'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const opportunityDurationData = [
      { 
        name: "1 day",
        length_days: 1
      },
      { 
        name: "a few days",
        length_days: 3
      },
      { 
        name: "1 week",
        length_days: 14
      },
      { 
        name: "1 month",
        length_days: 30
      },
      { 
        name: "3 months",
        length_days: 90
      },
      { 
        name: "Flexible"
      }
    ];

    for(var i = 0, len = opportunityDurationData.length; i < len; i++) {
      let opportunityDuration = opportunityDurationData[i];
      opportunityDuration.created_at = new Date();
      opportunityDuration.updated_at = new Date();
    }

    return queryInterface.bulkInsert('opportunity_duration', opportunityDurationData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('opportunity_duration', null, {});
  }
};
