'use strict';


module.exports = {
  up: (queryInterface, Sequelize) => {
    const schoolData = [
      {
        name:  'Abercorn State School'
      }, {
        name:  'Abergowrie State School'
      }, {
        name:  'Acacia Ridge State School'
      }, {
        name:  'Agnes Water State School'
      }, {
        name:  'Airville State School'
      }, {
        name:  'Aitkenvale State School'
      }, {
        name:  'Albany Creek State High School'
      }, {
        name:  'Albany Creek State School'
      }, {
        name:  'Albany Hills State School'
      }, {
        name:  'Albert State School'
      }, {
        name:  'Aldridge State High School'
      }, {
        name:  'Alexandra Bay State School'
      }, {
        name:  'Alexandra Hills State High School'
      }, {
        name:  'Alexandra Hills State School'
      }, {
        name:  'Algester State School'
      }, {
        name:  'Allenstown State School'
      }, {
        name:  'Alligator Creek State School'
      }, {
        name:  'Allora P-10 State School'
      }, {
        name:  'Alloway State School'
      }, {
        name:  'Aloomba State School'
      }, {
        name:  'Alpha State School'
      }, {
        name:  'Amamoor State School'
      }, {
        name:  'Amberley District State School'
      }, {
        name:  'Ambrose State School'
      }, {
        name:  'Amiens State School'
      }, {
        name:  'Anakie State School'
      }, {
        name:  'Andergrove State School'
      }, {
        name:  'Annandale State School'
      }, {
        name:  'Applethorpe State School'
      }, {
        name:  'Aramac State School'
      }, {
        name:  'Aratula State School'
      }, {
        name:  'Arcadia Valley State School'
      }, {
        name:  'Arundel State School'
      }, {
        name:  'Ascot State School'
      }, {
        name:  'Ashgrove State School'
      }, {
        name:  'Ashmore State School'
      }, {
        name:  'Ashwell State School'
      }, {
        name:  'Aspley East State School'
      }, {
        name:  'Aspley Special School'
      }, {
        name:  'Aspley State High School'
      }, {
        name:  'Aspley State School'
      }, {
        name:  'Atherton State High School'
      }, {
        name:  'Atherton State School'
      }, {
        name:  'Augathella State School'
      }, {
        name:  'Augusta State School'
      }, {
        name:  'Aurukun State School'
      }, {
        name:  'Aviation High'
      }, {
        name:  'Avoca State School'
      }, {
        name:  'Avondale State School'
      }, {
        name:  'Ayr State High School'
      }, {
        name:  'Ayr State School'
      }, {
        name:  'Babinda State School'
      }, {
        name:  'Back Plains State School'
      }, {
        name:  'Bajool State School'
      }, {
        name:  'Balaclava State School'
      }, {
        name:  'Bald Hills State School'
      }, {
        name:  'Ballandean State School'
      }, {
        name:  'Balmoral State High School'
      }, {
        name:  'Bambaroo State School'
      }, {
        name:  'Banana State School'
      }, {
        name:  'Banksia Beach State School'
      }, {
        name:  'Baralaba State School'
      }, {
        name:  'Barcaldine Prep-12 State School'
      }, {
        name:  'Bardon State School'
      }, {
        name:  'Bargara State School'
      }, {
        name:  'Barkly Highway State School'
      }, {
        name:  'Barrett Adolescent Centre Special School'
      }, {
        name:  'Bartle Frere State School'
      }, {
        name:  'Bauhinia State School'
      }, {
        name:  'Bauple State School'
      }, {
        name:  'Bay View State School'
      }, {
        name:  'Beachmere State School'
      }, {
        name:  'Beaconsfield State School'
      }, {
        name:  'Beaudesert State High School'
      }, {
        name:  'Beaudesert State School'
      }, {
        name:  'Bedourie State School'
      }, {
        name:  'Beechmont State School'
      }, {
        name:  'Beenleigh Special School'
      }, {
        name:  'Beenleigh State High School'
      }, {
        name:  'Beenleigh State School'
      }, {
        name:  'Beerburrum State School'
      }, {
        name:  'Beerwah State High School'
      }, {
        name:  'Beerwah State School'
      }, {
        name:  'Begonia State School'
      }, {
        name:  'Belgian Gardens State School'
      }, {
        name:  'Bell State School'
      }, {
        name:  'Bellbird Park State Secondary College'
      }, {
        name:  'Bellenden Ker State School'
      }, {
        name:  'Bellevue Park State School'
      }, {
        name:  'Bellmere State School'
      }, {
        name:  'Belmont State School'
      }, {
        name:  'Benaraby State School'
      }, {
        name:  'Benarkin State School'
      }, {
        name:  'Benowa State High School'
      }, {
        name:  'Benowa State School'
      }, {
        name:  'Bentley Park College'
      }, {
        name:  'Berrinba East State School'
      }, {
        name:  'Berserker Street State School'
      }, {
        name:  'Biboohra State School'
      }, {
        name:  'Biddeston State School'
      }, {
        name:  'Biggenden State School'
      }, {
        name:  'Biggera Waters State School'
      }, {
        name:  'Biloela State High School'
      }, {
        name:  'Biloela State School'
      }, {
        name:  'Binjour Plateau State School'
      }, {
        name:  'Birdsville State School'
      }, {
        name:  'Birkdale South State School'
      }, {
        name:  'Birkdale State School'
      }, {
        name:  'Blackall State School'
      }, {
        name:  'Blackbutt State School'
      }, {
        name:  'Blackwater North State School'
      }, {
        name:  'Blackwater State High School'
      }, {
        name:  'Blackwater State School'
      }, {
        name:  'Blair State School'
      }, {
        name:  'Blenheim State School'
      }, {
        name:  'Bli Bli State School'
      }, {
        name:  'Bloomfield River State School'
      }, {
        name:  'Bloomsbury State School'
      }, {
        name:  'Bluewater State School'
      }, {
        name:  'Bluff State School'
      }, {
        name:  'Bohlevale State School'
      }, {
        name:  'Bollon State School'
      }, {
        name:  'Boonah State High School'
      }, {
        name:  'Boonah State School'
      }, {
        name:  'Boondall State School'
      }, {
        name:  'Booyal Central State School'
      }, {
        name:  'Boronia Heights State School'
      }, {
        name:  'Bororen State School'
      }, {
        name:  'Bouldercombe State School'
      }, {
        name:  'Boulia State School'
      }, {
        name:  'Bounty Boulevard State School'
      }, {
        name:  'Bowen State High School'
      }, {
        name:  'Bowen State School'
      }, {
        name:  'Bowenville State School'
      }, {
        name:  'Boyne Island State School'
      }, {
        name:  'Boynewood State School'
      }, {
        name:  'Bracken Ridge State High School'
      }, {
        name:  'Bracken Ridge State School'
      }, {
        name:  'Brandon State School'
      }, {
        name:  'Branyan Road State School'
      }, {
        name:  'Brassall State School'
      }, {
        name:  'Bray Park State High School'
      }, {
        name:  'Bray Park State School'
      }, {
        name:  'Bremer State High School'
      }, {
        name:  'Bribie Island State High School'
      }, {
        name:  'Bribie Island State School'
      }, {
        name:  'Brigalow State School'
      }, {
        name:  'Brighton State School'
      }, {
        name:  'Brightwater State School'
      }, {
        name:  'Brisbane Bayside State College'
      }, {
        name:  'Brisbane Central State School'
      }, {
        name:  'Brisbane School of Distance Education'
      }, {
        name:  'Brisbane State High School'
      }, {
        name:  'Brisbane Youth Education and Training Centre'
      }, {
        name:  'Broadbeach State School'
      }, {
        name:  'Broadwater State School'
      }, {
        name:  'Brookfield State School'
      }, {
        name:  'Brookstead State School'
      }, {
        name:  'Brooweena State School'
      }, {
        name:  'Browns Plains State High School'
      }, {
        name:  'Browns Plains State School'
      }, {
        name:  'Bucasia State School'
      }, {
        name:  'Buddina State School'
      }, {
        name:  'Buderim Mountain State School'
      }, {
        name:  'Builyan State School'
      }, {
        name:  'Bulimba State School'
      }, {
        name:  'Bullyard State School'
      }, {
        name:  'Bundaberg Central State School'
      }, {
        name:  'Bundaberg East State School'
      }, {
        name:  'Bundaberg North State High School'
      }, {
        name:  'Bundaberg North State School'
      }, {
        name:  'Bundaberg South State School'
      }, {
        name:  'Bundaberg Special School'
      }, {
        name:  'Bundaberg State High School'
      }, {
        name:  'Bundaberg West State School'
      }, {
        name:  'Bundamba State School'
      }, {
        name:  'Bundamba State Secondary College'
      }, {
        name:  'Bungunya State School'
      }, {
        name:  'Bunker\'s Hill State School'
      }, {
        name:  'Buranda State School'
      }, {
        name:  'Burdekin School'
      }, {
        name:  'Burketown State School'
      }, {
        name:  'Burleigh Heads State School'
      }, {
        name:  'Burnett Heads State School'
      }, {
        name:  'Burnett State College'
      }, {
        name:  'Burnside State High School'
      }, {
        name:  'Burnside State School'
      }, {
        name:  'Burpengary Meadows State School'
      }, {
        name:  'Burpengary State School'
      }, {
        name:  'Burpengary State Secondary College'
      }, {
        name:  'Burra Burri State School'
      }, {
        name:  'Burrowes State School'
      }, {
        name:  'Butchers Creek State School'
      }, {
        name:  'Bwgcolman Community School'
      }, {
        name:  'Byfield State School'
      }, {
        name:  'Bymount East State School'
      }, {
        name:  'Caboolture East State School'
      }, {
        name:  'Caboolture Special School'
      }, {
        name:  'Caboolture State High School'
      }, {
        name:  'Caboolture State School'
      }, {
        name:  'Cairns School of Distance Education'
      }, {
        name:  'Cairns State High School'
      }, {
        name:  'Cairns State Special School'
      }, {
        name:  'Cairns West State School'
      }, {
        name:  'Calamvale Community College'
      }, {
        name:  'Calamvale Special School'
      }, {
        name:  'Calen District State College'
      }, {
        name:  'Calliope State School'
      }, {
        name:  'Caloundra State High School'
      }, {
        name:  'Caloundra State School'
      }, {
        name:  'Cambooya State School'
      }, {
        name:  'Cameron Downs State School'
      }, {
        name:  'Camira State School'
      }, {
        name:  'Camooweal State School'
      }, {
        name:  'Camp Hill State Infants and Primary School'
      }, {
        name:  'Caningeraba State School'
      }, {
        name:  'Cannon Hill State School'
      }, {
        name:  'Cannonvale State School'
      }, {
        name:  'Canungra State School'
      }, {
        name:  'Capalaba State College'
      }, {
        name:  'Cape York Aboriginal Australian Academy'
      }, {
        name:  'Capella State High School'
      }, {
        name:  'Capella State School'
      }, {
        name:  'Capricornia (Emerald Campus) School of Distance Education'
      }, {
        name:  'Caravonica State School'
      }, {
        name:  'Carbrook State School'
      }, {
        name:  'Cardwell State School'
      }, {
        name:  'Carina State School'
      }, {
        name:  'Carmila State School'
      }, {
        name:  'Carole Park State School'
      }, {
        name:  'Cavendish Road State High School'
      }, {
        name:  'Cawarral State School'
      }, {
        name:  'Cecil Plains State School'
      }, {
        name:  'Cedar Creek State School'
      }, {
        name:  'Centenary Heights State High School'
      }, {
        name:  'Centenary State High School'
      }, {
        name:  'Chancellor State College'
      }, {
        name:  'Chapel Hill State School'
      }, {
        name:  'Charleville School of Distance Education'
      }, {
        name:  'Charleville State High School'
      }, {
        name:  'Charleville State School'
      }, {
        name:  'Charters Towers Central State School'
      }, {
        name:  'Charters Towers School of Distance Education'
      }, {
        name:  'Charters Towers State High School'
      }, {
        name:  'Chatswood Hills State School'
      }, {
        name:  'Chatsworth State School'
      }, {
        name:  'Chelona State School'
      }, {
        name:  'Cherbourg State School'
      }, {
        name:  'Chevallum State School'
      }, {
        name:  'Childers State School'
      }, {
        name:  'Chillagoe State School'
      }, {
        name:  'Chinchilla State High School'
      }, {
        name:  'Chinchilla State School'
      }, {
        name:  'Churchill State School'
      }, {
        name:  'Clare State School'
      }, {
        name:  'Claremont Special School'
      }, {
        name:  'Clarendon State School'
      }, {
        name:  'Clarke Creek State School'
      }, {
        name:  'Clermont State High School'
      }, {
        name:  'Clermont State School'
      }, {
        name:  'Cleveland District State High School'
      }, {
        name:  'Cleveland Education and Training Centre'
      }, {
        name:  'Cleveland State School'
      }, {
        name:  'Clifford Park Special School'
      }, {
        name:  'Clifton State High School'
      }, {
        name:  'Clifton State School'
      }, {
        name:  'Clinton State School'
      }, {
        name:  'Cloncurry State School P-12'
      }, {
        name:  'Clontarf Beach State High School'
      }, {
        name:  'Clontarf Beach State School'
      }, {
        name:  'Clover Hill State School'
      }, {
        name:  'Cloyna State School'
      }, {
        name:  'Coalstoun Lakes State School'
      }, {
        name:  'Collingwood Park State School'
      }, {
        name:  'Collinsville State High School'
      }, {
        name:  'Collinsville State School'
      }, {
        name:  'Comet State School'
      }, {
        name:  'Condamine State School'
      }, {
        name:  'Coningsby State School'
      }, {
        name:  'Conondale State School'
      }, {
        name:  'Cooktown State School'
      }, {
        name:  'Coolabunia State School'
      }, {
        name:  'Coolangatta State School'
      }, {
        name:  'Coolnwynpin State School'
      }, {
        name:  'Coolum State High School'
      }, {
        name:  'Coolum State School'
      }, {
        name:  'Coombabah State High School'
      }, {
        name:  'Coombabah State School'
      }, {
        name:  'Coomera Rivers State School'
      }, {
        name:  'Coomera Springs State School'
      }, {
        name:  'Coomera State School'
      }, {
        name:  'Coominya State School'
      }, {
        name:  'Coopers Plains State School'
      }, {
        name:  'Cooran State School'
      }, {
        name:  'Cooroy State School'
      }, {
        name:  'Coorparoo Secondary College'
      }, {
        name:  'Coorparoo State School'
      }, {
        name:  'Coowonga State School'
      }, {
        name:  'Cooyar State School'
      }, {
        name:  'Coppabella State School'
      }, {
        name:  'Cordalba State School'
      }, {
        name:  'Corinda State High School'
      }, {
        name:  'Corinda State School'
      }, {
        name:  'Craigslea State High School'
      }, {
        name:  'Craigslea State School'
      }, {
        name:  'Cranbrook State School'
      }, {
        name:  'Crawford State School'
      }, {
        name:  'Crescent Lagoon State School'
      }, {
        name:  'Crestmead State School'
      }, {
        name:  'Crow\'s Nest State School'
      }, {
        name:  'Croydon State School'
      }, {
        name:  'Cunnamulla P-12 State School'
      }, {
        name:  'Currajong State School'
      }, {
        name:  'Currimundi Special School'
      }, {
        name:  'Currimundi State School'
      }, {
        name:  'Currumbin Community Special School'
      }, {
        name:  'Currumbin State School'
      }, {
        name:  'Currumbin Valley State School'
      }, {
        name:  'Dagun State School'
      }, {
        name:  'Daintree State School'
      }, {
        name:  'Daisy Hill State School'
      }, {
        name:  'Dajarra State School'
      }, {
        name:  'Dakabin State High School'
      }, {
        name:  'Dakabin State School'
      }, {
        name:  'Dalby South State School'
      }, {
        name:  'Dalby State High School'
      }, {
        name:  'Dalby State School'
      }, {
        name:  'Dallarnil State School'
      }, {
        name:  'Dalveen State School'
      }, {
        name:  'Darling Heights State School'
      }, {
        name:  'Darling Point Special School'
      }, {
        name:  'Darlington State School'
      }, {
        name:  'Darra State School'
      }, {
        name:  'Dayboro State School'
      }, {
        name:  'Deception Bay North State School'
      }, {
        name:  'Deception Bay State High School'
      }, {
        name:  'Deception Bay State School'
      }, {
        name:  'Deebing Heights State School'
      }, {
        name:  'Delaneys Creek State School'
      }, {
        name:  'Denison State School'
      }, {
        name:  'Depot Hill State School'
      }, {
        name:  'Dimbulah State School'
      }, {
        name:  'Dingo State School'
      }, {
        name:  'Dirranbandi P-10 State School'
      }, {
        name:  'Doomadgee State School'
      }, {
        name:  'Drayton State School'
      }, {
        name:  'Drillham State School'
      }, {
        name:  'Duaringa State School'
      }, {
        name:  'Dulacca State School'
      }, {
        name:  'Dundula State School'
      }, {
        name:  'Dunkeld State School'
      }, {
        name:  'Dunwich State School'
      }, {
        name:  'Durack State School'
      }, {
        name:  'Durong South State School'
      }, {
        name:  'Dutton Park State School'
      }, {
        name:  'Dysart State High School'
      }, {
        name:  'Dysart State School'
      }, {
        name:  'Eagle Junction State School'
      }, {
        name:  'Eagleby South State School'
      }, {
        name:  'Eagleby State School'
      }, {
        name:  'Earnshaw State College'
      }, {
        name:  'East Ayr State School'
      }, {
        name:  'East Brisbane State School'
      }, {
        name:  'Eatons Hill State School'
      }, {
        name:  'Edens Landing State School'
      }, {
        name:  'Edge Hill State School'
      }, {
        name:  'Eidsvold State School'
      }, {
        name:  'Eight Mile Plains State School'
      }, {
        name:  'Eimeo Road State School'
      }, {
        name:  'El Arish State School'
      }, {
        name:  'Elanora State High School'
      }, {
        name:  'Elanora State School'
      }, {
        name:  'Elimbah State School'
      }, {
        name:  'Elliott Heads State School'
      }, {
        name:  'Emerald North State School'
      }, {
        name:  'Emerald State High School'
      }, {
        name:  'Emerald State School'
      }, {
        name:  'Emu Creek State School'
      }, {
        name:  'Emu Park State School'
      }, {
        name:  'Enoggera State School'
      }, {
        name:  'Eromanga State School'
      }, {
        name:  'Esk State School'
      }, {
        name:  'Eton State School'
      }, {
        name:  'Eudlo State School'
      }, {
        name:  'Eulo State School'
      }, {
        name:  'Eumundi State School'
      }, {
        name:  'Eungella State School'
      }, {
        name:  'Everton Park State High School'
      }, {
        name:  'Everton Park State School'
      }, {
        name:  'Fairview Heights State School'
      }, {
        name:  'Farleigh State School'
      }, {
        name:  'Farnborough State School'
      }, {
        name:  'Federal State School'
      }, {
        name:  'Feluga State School'
      }, {
        name:  'Fernbrooke State School'
      }, {
        name:  'Fernvale State School'
      }, {
        name:  'Ferny Grove State High School'
      }, {
        name:  'Ferny Grove State School'
      }, {
        name:  'Ferny Hills State School'
      }, {
        name:  'Fig Tree Pocket State School'
      }, {
        name:  'Finch Hatton State School'
      }, {
        name:  'Fitzgerald State School'
      }, {
        name:  'Flagstone Creek State School'
      }, {
        name:  'Flagstone State Community College'
      }, {
        name:  'Flagstone State School'
      }, {
        name:  'Flying Fish Point State School'
      }, {
        name:  'Forest Hill State School'
      }, {
        name:  'Forest Lake State High School'
      }, {
        name:  'Forest Lake State School'
      }, {
        name:  'Forrest Beach State School'
      }, {
        name:  'Forsayth State School'
      }, {
        name:  'Freestone State School'
      }, {
        name:  'Frenchville State School'
      }, {
        name:  'Freshwater State School'
      }, {
        name:  'Gabbinbar State School'
      }, {
        name:  'Garbutt State School'
      }, {
        name:  'Gargett State School'
      }, {
        name:  'Gatton State School'
      }, {
        name:  'Gaven State School'
      }, {
        name:  'Gayndah State School'
      }, {
        name:  'Geebung Special School'
      }, {
        name:  'Geebung State School'
      }, {
        name:  'Geham State School'
      }, {
        name:  'Georgetown State School'
      }, {
        name:  'Gilston State School'
      }, {
        name:  'Gin Gin State High School'
      }, {
        name:  'Gin Gin State School'
      }, {
        name:  'Gindie State School'
      }, {
        name:  'Giru State School'
      }, {
        name:  'Givelda State School'
      }, {
        name:  'Gladstone Central State School'
      }, {
        name:  'Gladstone South State School'
      }, {
        name:  'Gladstone State High School'
      }, {
        name:  'Gladstone West State School'
      }, {
        name:  'Glamorgan Vale State School'
      }, {
        name:  'Glass House Mountains State School'
      }, {
        name:  'Glen Aplin State School'
      }, {
        name:  'Glenala State High School'
      }, {
        name:  'Glenden State School'
      }, {
        name:  'Gleneagle State School'
      }, {
        name:  'Glenella State School'
      }, {
        name:  'Glenmore State High School'
      }, {
        name:  'Glenmore State School'
      }, {
        name:  'Glenmorgan State School'
      }, {
        name:  'Glennie Heights State School'
      }, {
        name:  'Glenore Grove State School'
      }, {
        name:  'Glenvale State School'
      }, {
        name:  'Glenview State School'
      }, {
        name:  'Glenwood State School'
      }, {
        name:  'Gogango State School'
      }, {
        name:  'Golden Beach State School'
      }, {
        name:  'Gooburrum State School'
      }, {
        name:  'Goodna Special School'
      }, {
        name:  'Goodna State School'
      }, {
        name:  'Goodwood State School'
      }, {
        name:  'Goombungee State School'
      }, {
        name:  'Goomeri State School'
      }, {
        name:  'Goondi State School'
      }, {
        name:  'Goondiwindi State High School'
      }, {
        name:  'Goondiwindi State School'
      }, {
        name:  'Goovigen State School'
      }, {
        name:  'Gordonvale State High School'
      }, {
        name:  'Gordonvale State School'
      }, {
        name:  'Gowrie State School'
      }, {
        name:  'Gracemere State School'
      }, {
        name:  'Graceville State School'
      }, {
        name:  'Grand Avenue State School'
      }, {
        name:  'Grandchester State School'
      }, {
        name:  'Grantham State School'
      }, {
        name:  'Granville State School'
      }, {
        name:  'Greenbank State School'
      }, {
        name:  'Greenlands State School'
      }, {
        name:  'Greenmount State School'
      }, {
        name:  'Greenslopes State School'
      }, {
        name:  'Greenvale State School'
      }, {
        name:  'Griffin State School'
      }, {
        name:  'Grosmont State School'
      }, {
        name:  'Grovely State School'
      }, {
        name:  'Guluguba State School'
      }, {
        name:  'Gumdale State School'
      }, {
        name:  'Gumlu State School'
      }, {
        name:  'Gunalda State School'
      }, {
        name:  'Gundiah State School'
      }, {
        name:  'Gympie Central State School'
      }, {
        name:  'Gympie East State School'
      }, {
        name:  'Gympie South State School'
      }, {
        name:  'Gympie Special School'
      }, {
        name:  'Gympie State High School'
      }, {
        name:  'Gympie West State School'
      }, {
        name:  'Haden State School'
      }, {
        name:  'Haigslea State School'
      }, {
        name:  'Halifax State School'
      }, {
        name:  'Hambledon State School'
      }, {
        name:  'Hamilton Island State School'
      }, {
        name:  'Hamilton State School'
      }, {
        name:  'Hampden State School'
      }, {
        name:  'Hannaford State School'
      }, {
        name:  'Happy Valley State School'
      }, {
        name:  'Harlaxton State School'
      }, {
        name:  'Harlin State School'
      }, {
        name:  'Harris Fields State School'
      }, {
        name:  'Harristown State High School'
      }, {
        name:  'Harristown State School'
      }, {
        name:  'Harrisville State School'
      }, {
        name:  'Hatton Vale State School'
      }, {
        name:  'Hayman Island State School'
      }, {
        name:  'Healy State School'
      }, {
        name:  'Heatley Secondary College'
      }, {
        name:  'Heatley State School'
      }, {
        name:  'Hebel State School'
      }, {
        name:  'Helens Hill State School'
      }, {
        name:  'Helensvale State High School'
      }, {
        name:  'Helensvale State School'
      }, {
        name:  'Helidon State School'
      }, {
        name:  'Hendra State School'
      }, {
        name:  'Herberton State School'
      }, {
        name:  'Hercules Road State School'
      }, {
        name:  'Hermit Park State School'
      }, {
        name:  'Hervey Bay Special School'
      }, {
        name:  'Hervey Bay State High School'
      }, {
        name:  'Highfields State School'
      }, {
        name:  'Highfields State Secondary College'
      }, {
        name:  'Highland Reserve State School'
      }, {
        name:  'Hilder Road State School'
      }, {
        name:  'Hilliard State School'
      }, {
        name:  'Hillview State School'
      }, {
        name:  'Holland Park State High School'
      }, {
        name:  'Holland Park State School'
      }, {
        name:  'Home Hill State High School'
      }, {
        name:  'Home Hill State School'
      }, {
        name:  'Homebush State School'
      }, {
        name:  'Homestead State School'
      }, {
        name:  'Howard State School'
      }, {
        name:  'Hughenden State School'
      }, {
        name:  'Humpybong State School'
      }, {
        name:  'Ilfracombe State School'
      }, {
        name:  'Inala State School'
      }, {
        name:  'Indooroopilly State High School'
      }, {
        name:  'Indooroopilly State School'
      }, {
        name:  'Ingham State High School'
      }, {
        name:  'Ingham State School'
      }, {
        name:  'Ingleside State School'
      }, {
        name:  'Inglewood State School'
      }, {
        name:  'Injune P-10 State School'
      }, {
        name:  'Innisfail East State School'
      }, {
        name:  'Innisfail State College'
      }, {
        name:  'Innisfail State School'
      }, {
        name:  'Ipswich Central State School'
      }, {
        name:  'Ipswich East State School'
      }, {
        name:  'Ipswich North State School'
      }, {
        name:  'Ipswich Special School'
      }, {
        name:  'Ipswich State High School'
      }, {
        name:  'Ipswich West Special School'
      }, {
        name:  'Ipswich West State School'
      }, {
        name:  'Ironside State School'
      }, {
        name:  'Irvinebank State School'
      }, {
        name:  'Isabella State School'
      }, {
        name:  'Isis District State High School'
      }, {
        name:  'Isisford State School'
      }, {
        name:  'Ithaca Creek State School'
      }, {
        name:  'Jambin State School'
      }, {
        name:  'Jamboree Heights State School'
      }, {
        name:  'James Nash State High School'
      }, {
        name:  'Jandowae Prep-10 State School'
      }, {
        name:  'Jarvisfield State School'
      }, {
        name:  'Jericho State School'
      }, {
        name:  'Jimboomba State School'
      }, {
        name:  'Jimbour State School'
      }, {
        name:  'Jindalee State School'
      }, {
        name:  'Jinibara State School'
      }, {
        name:  'Jondaryan State School'
      }, {
        name:  'Jones Hill State School'
      }, {
        name:  'Julatten State School'
      }, {
        name:  'Julia Creek State School'
      }, {
        name:  'Junction Park State School'
      }, {
        name:  'Jundah State School'
      }, {
        name:  'Kaimkillenbun State School'
      }, {
        name:  'Kairi State School'
      }, {
        name:  'Kalamia State School'
      }, {
        name:  'Kalbar State School'
      }, {
        name:  'Kalkie State School'
      }, {
        name:  'Kallangur State School'
      }, {
        name:  'Kandanga State School'
      }, {
        name:  'Karalee State School'
      }, {
        name:  'Karara State School'
      }, {
        name:  'Karumba State School'
      }, {
        name:  'Kawana Waters State College'
      }, {
        name:  'Kawungan State School'
      }, {
        name:  'Kedron State High School'
      }, {
        name:  'Kedron State School'
      }, {
        name:  'Keebra Park State High School'
      }, {
        name:  'Kelso State School'
      }, {
        name:  'Kelvin Grove State College'
      }, {
        name:  'Kenilworth State Community College'
      }, {
        name:  'Kenmore South State School'
      }, {
        name:  'Kenmore State High School'
      }, {
        name:  'Kenmore State School'
      }, {
        name:  'Kennedy State School'
      }, {
        name:  'Kentville State School'
      }, {
        name:  'Kepnock State High School'
      }, {
        name:  'Keppel Sands State School'
      }, {
        name:  'Kia-Ora State School'
      }, {
        name:  'Kilcoy State High School'
      }, {
        name:  'Kilcoy State School'
      }, {
        name:  'Kilcummin State School'
      }, {
        name:  'Kilkivan State School'
      }, {
        name:  'Killarney P-10 State School'
      }, {
        name:  'Kimberley Park State School'
      }, {
        name:  'Kin Kin State School'
      }, {
        name:  'Kin Kora State School'
      }, {
        name:  'Kindon State School'
      }, {
        name:  'Kingaroy State High School'
      }, {
        name:  'Kingaroy State School'
      }, {
        name:  'Kingsthorpe State School'
      }, {
        name:  'Kingston State College'
      }, {
        name:  'Kingston State School'
      }, {
        name:  'Kioma State School'
      }, {
        name:  'Kippa-Ring State School'
      }, {
        name:  'Kirwan State High School'
      }, {
        name:  'Kirwan State School'
      }, {
        name:  'Kogan State School'
      }, {
        name:  'Kolan South State School'
      }, {
        name:  'Koumala State School'
      }, {
        name:  'Kowanyama State School'
      }, {
        name:  'Kruger State School'
      }, {
        name:  'Kulpi State School'
      }, {
        name:  'Kuluin State School'
      }, {
        name:  'Kumbia State School'
      }, {
        name:  'Kuraby Special School'
      }, {
        name:  'Kuraby State School'
      }, {
        name:  'Kuranda District State College'
      }, {
        name:  'Kurwongbah State School'
      }, {
        name:  'Labrador State School'
      }, {
        name:  'Laidley District State School'
      }, {
        name:  'Laidley State High School'
      }, {
        name:  'Lake Clarendon State School'
      }, {
        name:  'Lakeland State School'
      }, {
        name:  'Lakes Creek State School'
      }, {
        name:  'Landsborough State School'
      }, {
        name:  'Laura State School'
      }, {
        name:  'Lawnton State School'
      }, {
        name:  'Leichhardt State School'
      }, {
        name:  'Leyburn State School'
      }, {
        name:  'Linville State School'
      }, {
        name:  'Lochington State School'
      }, {
        name:  'Lockhart State School'
      }, {
        name:  'Lockrose State School'
      }, {
        name:  'Lockyer District State High School'
      }, {
        name:  'Logan City Special School'
      }, {
        name:  'Logan Reserve State School'
      }, {
        name:  'Logan Village State School'
      }, {
        name:  'Loganholme State School'
      }, {
        name:  'Loganlea State High School'
      }, {
        name:  'Longreach School of Distance Education'
      }, {
        name:  'Longreach State High School'
      }, {
        name:  'Longreach State School'
      }, {
        name:  'Lota State School'
      }, {
        name:  'Lower Tully State School'
      }, {
        name:  'Lowmead State School'
      }, {
        name:  'Lowood State High School'
      }, {
        name:  'Lowood State School'
      }, {
        name:  'Lundavra State School'
      }, {
        name:  'Ma Ma Creek State School'
      }, {
        name:  'Mabel Park State High School'
      }, {
        name:  'Mabel Park State School'
      }, {
        name:  'MacGregor State High School'
      }, {
        name:  'MacGregor State School'
      }, {
        name:  'Machans Beach State School'
      }, {
        name:  'Mackay Central State School'
      }, {
        name:  'Mackay District Special School'
      }, {
        name:  'Mackay North State High School'
      }, {
        name:  'Mackay North State School'
      }, {
        name:  'Mackay Northern Beaches State High School'
      }, {
        name:  'Mackay State High School'
      }, {
        name:  'Mackay West State School'
      }, {
        name:  'Mackenzie River State School'
      }, {
        name:  'Mackenzie State Primary School'
      }, {
        name:  'Mackenzie State Special School'
      }, {
        name:  'Macknade State School'
      }, {
        name:  'MacLeay Island State School'
      }, {
        name:  'Magnetic Island State School'
      }, {
        name:  'Maidavale State School'
      }, {
        name:  'Malanda State High School'
      }, {
        name:  'Malanda State School'
      }, {
        name:  'Maleny State High School'
      }, {
        name:  'Maleny State School'
      }, {
        name:  'Mango Hill State School'
      }, {
        name:  'Manly State School'
      }, {
        name:  'Manly West State School'
      }, {
        name:  'Mansfield State High School'
      }, {
        name:  'Mansfield State School'
      }, {
        name:  'Mapleton State School'
      }, {
        name:  'Marburg State School'
      }, {
        name:  'Mareeba State High School'
      }, {
        name:  'Mareeba State School'
      }, {
        name:  'Marian State School'
      }, {
        name:  'Marlborough State School'
      }, {
        name:  'Marmor State School'
      }, {
        name:  'Maroochydore State High School'
      }, {
        name:  'Maroochydore State School'
      }, {
        name:  'Maroon State School'
      }, {
        name:  'Maroondan State School'
      }, {
        name:  'Marsden State High School'
      }, {
        name:  'Marsden State School'
      }, {
        name:  'Marshall Road State School'
      }, {
        name:  'Mary Valley State College'
      }, {
        name:  'Maryborough Central State School'
      }, {
        name:  'Maryborough Special School'
      }, {
        name:  'Maryborough State High School'
      }, {
        name:  'Maryborough West State School'
      }, {
        name:  'Maryvale State School'
      }, {
        name:  'Mayfield State School'
      }, {
        name:  'McDonnell Creek State School'
      }, {
        name:  'McDowall State School'
      }, {
        name:  'McIlwraith State School'
      }, {
        name:  'Meandarra State School'
      }, {
        name:  'Mena Creek State School'
      }, {
        name:  'Meridan State College'
      }, {
        name:  'Merinda State School'
      }, {
        name:  'Meringandan State School'
      }, {
        name:  'Merrimac State High School'
      }, {
        name:  'Merrimac State School'
      }, {
        name:  'Miallo State School'
      }, {
        name:  'Miami State High School'
      }, {
        name:  'Miami State School'
      }, {
        name:  'Middle Park State School'
      }, {
        name:  'Middle Ridge State School'
      }, {
        name:  'Middlemount Community School'
      }, {
        name:  'Miles State High School'
      }, {
        name:  'Miles State School'
      }, {
        name:  'Millaa Millaa State School'
      }, {
        name:  'Millaroo State School'
      }, {
        name:  'Millchester State School'
      }, {
        name:  'Millmerran State School'
      }, {
        name:  'Milman State School'
      }, {
        name:  'Milpera State High School'
      }, {
        name:  'Milton State School'
      }, {
        name:  'Minden State School'
      }, {
        name:  'Minimbah State School'
      }, {
        name:  'Mirani State High School'
      }, {
        name:  'Mirani State School'
      }, {
        name:  'Miriam Vale State School'
      }, {
        name:  'Mirriwinni State School'
      }, {
        name:  'Mission Beach State School'
      }, {
        name:  'Mistake Creek State School'
      }, {
        name:  'Mitchell State School'
      }, {
        name:  'Mitchelton Special School'
      }, {
        name:  'Mitchelton State High School'
      }, {
        name:  'Mitchelton State School'
      }, {
        name:  'Moffatdale State School'
      }, {
        name:  'Moggill State School'
      }, {
        name:  'Monkland State School'
      }, {
        name:  'Monogorilby State School'
      }, {
        name:  'Monto State High School'
      }, {
        name:  'Monto State School'
      }, {
        name:  'Montville State School'
      }, {
        name:  'Mooloolaba State School'
      }, {
        name:  'Mooloolah State School'
      }, {
        name:  'Moonie State School'
      }, {
        name:  'Moore Park State School'
      }, {
        name:  'Moorooka State School'
      }, {
        name:  'Moranbah East State School'
      }, {
        name:  'Moranbah State High School'
      }, {
        name:  'Moranbah State School'
      }, {
        name:  'Morayfield East State School'
      }, {
        name:  'Morayfield State High School'
      }, {
        name:  'Morayfield State School'
      }, {
        name:  'Moreton Downs State School'
      }, {
        name:  'Morningside State School'
      }, {
        name:  'Mornington Island State School'
      }, {
        name:  'Morven State School'
      }, {
        name:  'Mossman State High School'
      }, {
        name:  'Mossman State School'
      }, {
        name:  'Mount Alford State School'
      }, {
        name:  'Mount Archer State School'
      }, {
        name:  'Mount Cotton State School'
      }, {
        name:  'Mount Crosby State School'
      }, {
        name:  'Mount Fox State School'
      }, {
        name:  'Mount Garnet State School'
      }, {
        name:  'Mount Gravatt East State School'
      }, {
        name:  'Mount Gravatt State High School'
      }, {
        name:  'Mount Gravatt State School'
      }, {
        name:  'Mount Isa Central State School'
      }, {
        name:  'Mount Isa School of the Air'
      }, {
        name:  'Mount Isa Special School'
      }, {
        name:  'Mount Kilcoy State School'
      }, {
        name:  'Mount Larcom State School'
      }, {
        name:  'Mount Marrow State School'
      }, {
        name:  'Mount Mee State School'
      }, {
        name:  'Mount Molloy State School'
      }, {
        name:  'Mount Morgan Central State School'
      }, {
        name:  'Mount Morgan State High School'
      }, {
        name:  'Mount Murchison State School'
      }, {
        name:  'Mount Nebo State School'
      }, {
        name:  'Mount Ommaney Special School'
      }, {
        name:  'Mount Perry State School'
      }, {
        name:  'Mount Samson State School'
      }, {
        name:  'Mount Surprise State School'
      }, {
        name:  'Mount Sylvia State School'
      }, {
        name:  'Mount Tarampa State School'
      }, {
        name:  'Mount Tyson State School'
      }, {
        name:  'Mount Warren Park State School'
      }, {
        name:  'Mount Whitestone State School'
      }, {
        name:  'Mountain Creek State High School'
      }, {
        name:  'Mountain Creek State School'
      }, {
        name:  'Moura State High School'
      }, {
        name:  'Moura State School'
      }, {
        name:  'Mourilyan State School'
      }, {
        name:  'Mudgeeraba Creek State School'
      }, {
        name:  'Mudgeeraba Special School'
      }, {
        name:  'Mudgeeraba State School'
      }, {
        name:  'Mulgildie State School'
      }, {
        name:  'Mundingburra State School'
      }, {
        name:  'Mundoo State School'
      }, {
        name:  'Mundubbera State School'
      }, {
        name:  'Mungallala State School'
      }, {
        name:  'Mungar State School'
      }, {
        name:  'Murarrie State School'
      }, {
        name:  'Murgon State High School'
      }, {
        name:  'Murgon State School'
      }, {
        name:  'Murphy\'s Creek State School'
      }, {
        name:  'Murray River Upper State School'
      }, {
        name:  'Murray\'s Bridge State School'
      }, {
        name:  'Murrumba State Secondary College'
      }, {
        name:  'Musgrave Hill State School'
      }, {
        name:  'Mutarnee State School'
      }, {
        name:  'Mutchilba State School'
      }, {
        name:  'Mutdapilly State School'
      }, {
        name:  'Muttaburra State School'
      }, {
        name:  'Nagoorin State School'
      }, {
        name:  'Nambour Special School'
      }, {
        name:  'Nambour State College'
      }, {
        name:  'Nanango State High School'
      }, {
        name:  'Nanango State School'
      }, {
        name:  'Narangba State School'
      }, {
        name:  'Narangba Valley State High School'
      }, {
        name:  'Narangba Valley State School'
      }, {
        name:  'Narbethong State Special School'
      }, {
        name:  'Nashville State School'
      }, {
        name:  'Nebo State School'
      }, {
        name:  'Nerang State High School'
      }, {
        name:  'Nerang State School'
      }, {
        name:  'New Farm State School'
      }, {
        name:  'Newmarket State School'
      }, {
        name:  'Newtown State School'
      }, {
        name:  'Nobby State School'
      }, {
        name:  'Noosa District State High School'
      }, {
        name:  'Noosaville State School'
      }, {
        name:  'Norfolk Village State School'
      }, {
        name:  'Norman Park State School'
      }, {
        name:  'Normanton State School'
      }, {
        name:  'Norris Road State School'
      }, {
        name:  'North Arm State School'
      }, {
        name:  'North Eton State School'
      }, {
        name:  'North Lakes State College'
      }, {
        name:  'North Rockhampton State High School'
      }, {
        name:  'Northern Beaches State High School'
      }, {
        name:  'Northern Peninsula Area State College'
      }, {
        name:  'Northgate State School'
      }, {
        name:  'Northview State School'
      }, {
        name:  'Norville State School'
      }, {
        name:  'Numinbah Valley State School'
      }, {
        name:  'Nundah State School'
      }, {
        name:  'Nursery Road State Special School'
      }, {
        name:  'Oakenden State School'
      }, {
        name:  'Oakey State High School'
      }, {
        name:  'Oakey State School'
      }, {
        name:  'Oakleigh State School'
      }, {
        name:  'Oakwood State School'
      }, {
        name:  'One Mile State School'
      }, {
        name:  'Oonoonba State School'
      }, {
        name:  'Orion State School'
      }, {
        name:  'Ormeau State School'
      }, {
        name:  'Ormeau Woods State High School'
      }, {
        name:  'Ormiston State School'
      }, {
        name:  'Osborne State School'
      }, {
        name:  'Oxenford State School'
      }, {
        name:  'Oxley State School'
      }, {
        name:  'Pacific Paradise State School'
      }, {
        name:  'Pacific Pines State High School'
      }, {
        name:  'Pacific Pines State School'
      }, {
        name:  'Pallara State School'
      }, {
        name:  'Palm Beach State School'
      }, {
        name:  'Palm Beach-Currumbin State High School'
      }, {
        name:  'Palmwoods State School'
      }, {
        name:  'Park Avenue State School'
      }, {
        name:  'Park Lake State School'
      }, {
        name:  'Park Ridge State High School'
      }, {
        name:  'Park Ridge State School'
      }, {
        name:  'Parke State School'
      }, {
        name:  'Parkhurst State School'
      }, {
        name:  'Parramatta State School'
      }, {
        name:  'Patrick Estate State School'
      }, {
        name:  'Patricks Road State School'
      }, {
        name:  'Payne Road State School'
      }, {
        name:  'Peachester State School'
      }, {
        name:  'Peak Crossing State School'
      }, {
        name:  'Pentland State School'
      }, {
        name:  'Peregian Springs State School'
      }, {
        name:  'Petrie State School'
      }, {
        name:  'Petrie Terrace State School'
      }, {
        name:  'Pialba State School'
      }, {
        name:  'Pilton State School'
      }, {
        name:  'Pimlico State High School'
      }, {
        name:  'Pimpama State Primary College'
      }, {
        name:  'Pimpama State School'
      }, {
        name:  'Pimpama State Secondary College'
      }, {
        name:  'Pindi Pindi State School'
      }, {
        name:  'Pine Rivers Special School'
      }, {
        name:  'Pine Rivers State High School'
      }, {
        name:  'Pinnacle State School'
      }, {
        name:  'Pioneer State High School'
      }, {
        name:  'Pittsworth State High School'
      }, {
        name:  'Pittsworth State School'
      }, {
        name:  'Pomona State School'
      }, {
        name:  'Pormpuraaw State School'
      }, {
        name:  'Port Curtis Road State School'
      }, {
        name:  'Port Douglas State School'
      }, {
        name:  'Pozieres State School'
      }, {
        name:  'Prairie State School'
      }, {
        name:  'Prenzlau State School'
      }, {
        name:  'Proserpine State High School'
      }, {
        name:  'Proserpine State School'
      }, {
        name:  'Prospect Creek State School'
      }, {
        name:  'Proston State School'
      }, {
        name:  'Pullenvale State School'
      }, {
        name:  'Pumicestone State School'
      }, {
        name:  'Queens Beach State School'
      }, {
        name:  'Queensland Academy for Creative Industries'
      }, {
        name:  'Queensland Academy for Health Sciences'
      }, {
        name:  'Queensland Academy for Science Mathematics and Technology'
      }, {
        name:  'Quilpie State College'
      }, {
        name:  'Quinalow Prep-10 State School'
      }, {
        name:  'Raceview State School'
      }, {
        name:  'Railway Estate State School'
      }, {
        name:  'Rainbow Beach State School'
      }, {
        name:  'Rainworth State School'
      }, {
        name:  'Ramsay State School'
      }, {
        name:  'Rangeville State School'
      }, {
        name:  'Rasmussen State School'
      }, {
        name:  'Rathdowney State School'
      }, {
        name:  'Ravenshoe State School'
      }, {
        name:  'Ravenswood State School'
      }, {
        name:  'Red Hill Special School'
      }, {
        name:  'Redbank Plains State High School'
      }, {
        name:  'Redbank Plains State School'
      }, {
        name:  'Redbank State School'
      }, {
        name:  'Redcliffe Special School'
      }, {
        name:  'Redcliffe State High School'
      }, {
        name:  'Redland Bay State School'
      }, {
        name:  'Redland District Special School'
      }, {
        name:  'Redlynch State College'
      }, {
        name:  'Regents Park State School'
      }, {
        name:  'Richlands East State School'
      }, {
        name:  'Richmond Hill State School'
      }, {
        name:  'Richmond State School'
      }, {
        name:  'Ridgelands State School'
      }, {
        name:  'Riverview State School'
      }, {
        name:  'Roadvale State School'
      }, {
        name:  'Robertson State School'
      }, {
        name:  'Robina State High School'
      }, {
        name:  'Robina State School'
      }, {
        name:  'Rochedale South State School'
      }, {
        name:  'Rochedale State High School'
      }, {
        name:  'Rochedale State School'
      }, {
        name:  'Rockhampton North Special School'
      }, {
        name:  'Rockhampton Special School'
      }, {
        name:  'Rockhampton State High School'
      }, {
        name:  'Rocklea State School'
      }, {
        name:  'Rockville State School'
      }, {
        name:  'Rolleston State School'
      }, {
        name:  'Rollingstone State School'
      }, {
        name:  'Roma State College'
      }, {
        name:  'Ropeley State School'
      }, {
        name:  'Rosedale State School'
      }, {
        name:  'Rosella Park School'
      }, {
        name:  'Rosewood State High School'
      }, {
        name:  'Rosewood State School'
      }, {
        name:  'Rossville State School'
      }, {
        name:  'Runcorn Heights State School'
      }, {
        name:  'Runcorn State High School'
      }, {
        name:  'Runcorn State School'
      }, {
        name:  'Russell Island State School'
      }, {
        name:  'Ryeford State School'
      }, {
        name:  'Salisbury State School'
      }, {
        name:  'Samford State School'
      }, {
        name:  'Sandgate District State High School'
      }, {
        name:  'Sandgate State School'
      }, {
        name:  'Sandy Strait State School'
      }, {
        name:  'Sarina State High School'
      }, {
        name:  'Sarina State School'
      }, {
        name:  'Scarborough State School'
      }, {
        name:  'Scottville State School'
      }, {
        name:  'Seaforth State School'
      }, {
        name:  'Serviceton South State School'
      }, {
        name:  'Seven Hills State School'
      }, {
        name:  'Severnlea State School'
      }, {
        name:  'Seville Road State School'
      }, {
        name:  'Shailer Park State High School'
      }, {
        name:  'Shailer Park State School'
      }, {
        name:  'Sharon State School'
      }, {
        name:  'Sherwood State School'
      }, {
        name:  'Shorncliffe State School'
      }, {
        name:  'Silkstone State School'
      }, {
        name:  'Silkwood State School'
      }, {
        name:  'Slade Point State School'
      }, {
        name:  'Somerset Hills State School'
      }, {
        name:  'South Johnstone State School'
      }, {
        name:  'Southbrook Central State School'
      }, {
        name:  'Southport Special School'
      }, {
        name:  'Southport State High School'
      }, {
        name:  'Southport State School'
      }, {
        name:  'Spinifex State College - Mount Isa Education and Training Precinct'
      }, {
        name:  'Springbrook State School'
      }, {
        name:  'Springfield Central State High School'
      }, {
        name:  'Springfield Central State School'
      }, {
        name:  'Springfield Lakes State School'
      }, {
        name:  'Springsure State School'
      }, {
        name:  'Springwood Central State School'
      }, {
        name:  'Springwood Road State School'
      }, {
        name:  'Springwood State High School'
      }, {
        name:  'St Bernard State School'
      }, {
        name:  'St George State High School'
      }, {
        name:  'St George State School'
      }, {
        name:  'St Helens State School'
      }, {
        name:  'St Lawrence State School'
      }, {
        name:  'Stafford Heights State School'
      }, {
        name:  'Stafford State School'
      }, {
        name:  'Stanthorpe State High School'
      }, {
        name:  'Stanthorpe State School'
      }, {
        name:  'Stanwell State School'
      }, {
        name:  'Stonehenge State School'
      }, {
        name:  'Strathpine State School'
      }, {
        name:  'Strathpine West State School'
      }, {
        name:  'Stretton State College'
      }, {
        name:  'Sunbury State School'
      }, {
        name:  'Sunnybank Hills State School'
      }, {
        name:  'Sunnybank Special School'
      }, {
        name:  'Sunnybank State High School'
      }, {
        name:  'Sunnybank State School'
      }, {
        name:  'Sunset State School'
      }, {
        name:  'Sunshine Beach State High School'
      }, {
        name:  'Sunshine Beach State School'
      }, {
        name:  'Surat State School'
      }, {
        name:  'Surfers Paradise State School'
      }, {
        name:  'Swayneville State School'
      }, {
        name:  'Taabinga State School'
      }, {
        name:  'Tagai State College'
      }, {
        name:  'Taigum State School'
      }, {
        name:  'Talara Primary College'
      }, {
        name:  'Tallebudgera State School'
      }, {
        name:  'Talwood State School'
      }, {
        name:  'Tambo State School'
      }, {
        name:  'Tamborine Mountain State High School'
      }, {
        name:  'Tamborine Mountain State School'
      }, {
        name:  'Tamrookum State School'
      }, {
        name:  'Tanduringie State School'
      }, {
        name:  'Tannum Sands State High School'
      }, {
        name:  'Tannum Sands State School'
      }, {
        name:  'Tannymorel State School'
      }, {
        name:  'Tara Shire State College'
      }, {
        name:  'Tarampa State School'
      }, {
        name:  'Taranganba State School'
      }, {
        name:  'Taroom State School'
      }, {
        name:  'Teelba State School'
      }, {
        name:  'Tent Hill Lower State School'
      }, {
        name:  'Tewantin State School'
      }, {
        name:  'Texas P-10 State School'
      }, {
        name:  'Thabeban State School'
      }, {
        name:  'Thallon State School'
      }, {
        name:  'Thangool State School'
      }, {
        name:  'Thargomindah State School'
      }, {
        name:  'The Caves State School'
      }, {
        name:  'The Gap State High School'
      }, {
        name:  'The Gap State School'
      }, {
        name:  'The Gums State School'
      }, {
        name:  'The Hall State School'
      }, {
        name:  'The Summit State School'
      }, {
        name:  'The Willows State School'
      }, {
        name:  'Theebine State School'
      }, {
        name:  'Theodore State School'
      }, {
        name:  'Thornlands State School'
      }, {
        name:  'Thornton State School'
      }, {
        name:  'Thulimbah State School'
      }, {
        name:  'Thuringowa State High School'
      }, {
        name:  'Tiaro State School'
      }, {
        name:  'Tieri State School'
      }, {
        name:  'Tin Can Bay State School'
      }, {
        name:  'Tinana State School'
      }, {
        name:  'Tingalpa State School'
      }, {
        name:  'Tingoora State School'
      }, {
        name:  'Tivoli State School'
      }, {
        name:  'Tolga State School'
      }, {
        name:  'Toobanna State School'
      }, {
        name:  'Toogoolawah State High School'
      }, {
        name:  'Toogoolawah State School'
      }, {
        name:  'Toolooa State High School'
      }, {
        name:  'Toowong State School'
      }, {
        name:  'Toowoomba East State School'
      }, {
        name:  'Toowoomba North State School'
      }, {
        name:  'Toowoomba State High School'
      }, {
        name:  'Toowoomba West Special School'
      }, {
        name:  'Torbanlea State School'
      }, {
        name:  'Torquay State School'
      }, {
        name:  'Townsville Central State School'
      }, {
        name:  'Townsville Community Learning Centre - A State Special School'
      }, {
        name:  'Townsville South State School'
      }, {
        name:  'Townsville State High School'
      }, {
        name:  'Townsville West State School'
      }, {
        name:  'Townview State School'
      }, {
        name:  'Trebonne State School'
      }, {
        name:  'Tresswell State School'
      }, {
        name:  'Trinity Bay State High School'
      }, {
        name:  'Tropical North Learning Academy - Smithfield State High School'
      }, {
        name:  'Tropical North Learning Academy - Trinity Beach State School'
      }, {
        name:  'Tullawong State High School'
      }, {
        name:  'Tullawong State School'
      }, {
        name:  'Tully State High School'
      }, {
        name:  'Tully State School'
      }, {
        name:  'Two Mile State School'
      }, {
        name:  'Ubobo State School'
      }, {
        name:  'Undurba State School'
      }, {
        name:  'Upper Brookfield State School'
      }, {
        name:  'Upper Coomera State College'
      }, {
        name:  'Upper Mount Gravatt State School'
      }, {
        name:  'Urandangi State School'
      }, {
        name:  'Urangan Point State School'
      }, {
        name:  'Urangan State High School'
      }, {
        name:  'Vale View State School'
      }, {
        name:  'Valkyrie State School'
      }, {
        name:  'Varsity College'
      }, {
        name:  'Veresdale Scrub State School'
      }, {
        name:  'Victoria Park State School'
      }, {
        name:  'Victoria Plantation State School'
      }, {
        name:  'Victoria Point State High School'
      }, {
        name:  'Victoria Point State School'
      }, {
        name:  'Vienna Woods State School'
      }, {
        name:  'Vincent State School'
      }, {
        name:  'Virginia State School'
      }, {
        name:  'Walkamin State School'
      }, {
        name:  'Walkerston State School'
      }, {
        name:  'Walkervale State School'
      }, {
        name:  'Wallangarra State School'
      }, {
        name:  'Wallaville State School'
      }, {
        name:  'Walloon State School'
      }, {
        name:  'Wallumbilla State School'
      }, {
        name:  'Wamuran State School'
      }, {
        name:  'Wandoan State School P-10'
      }, {
        name:  'Waraburra State School'
      }, {
        name:  'Warra State School'
      }, {
        name:  'Warrigal Road State School'
      }, {
        name:  'Warrill View State School'
      }, {
        name:  'Wartburg State School'
      }, {
        name:  'Warwick Central State School'
      }, {
        name:  'Warwick East State School'
      }, {
        name:  'Warwick State High School'
      }, {
        name:  'Warwick West State School'
      }, {
        name:  'Waterford State School'
      }, {
        name:  'Waterford West State School'
      }, {
        name:  'Watson Road State School'
      }, {
        name:  'Wavell Heights State School'
      }, {
        name:  'Wavell State High School'
      }, {
        name:  'Weir State School'
      }, {
        name:  'Wellcamp State School'
      }, {
        name:  'Wellers Hill State School'
      }, {
        name:  'Wellington Point State High School'
      }, {
        name:  'Wellington Point State School'
      }, {
        name:  'West End State School'
      }, {
        name:  'Western Cape College'
      }, {
        name:  'Western Suburbs State Special School'
      }, {
        name:  'Westmar State School'
      }, {
        name:  'Westwood State School'
      }, {
        name:  'Wheatlands State School'
      }, {
        name:  'Wheatvale State School'
      }, {
        name:  'White Rock State School'
      }, {
        name:  'Whites Hill State College'
      }, {
        name:  'Whitfield State School'
      }, {
        name:  'Widgee State School'
      }, {
        name:  'William Duncan State School'
      }, {
        name:  'William Ross State High School'
      }, {
        name:  'Wilsonton State High School'
      }, {
        name:  'Wilsonton State School'
      }, {
        name:  'Wilston State School'
      }, {
        name:  'Windaroo State School'
      }, {
        name:  'Windaroo Valley State High School'
      }, {
        name:  'Windera State School'
      }, {
        name:  'Windorah State School'
      }, {
        name:  'Windsor State School'
      }, {
        name:  'Winfield State School'
      }, {
        name:  'Winton State School'
      }, {
        name:  'Wishart State School'
      }, {
        name:  'Withcott State School'
      }, {
        name:  'Wolvi State School'
      }, {
        name:  'Wondai State School'
      }, {
        name:  'Wondall Heights State School'
      }, {
        name:  'Wonga Beach State School'
      }, {
        name:  'Woodcrest State College'
      }, {
        name:  'Woodford State School'
      }, {
        name:  'Woodhill State School'
      }, {
        name:  'WoodLinks State School'
      }, {
        name:  'Woodridge North State School'
      }, {
        name:  'Woodridge State High School'
      }, {
        name:  'Woodridge State School'
      }, {
        name:  'Woodstock State School'
      }, {
        name:  'Woody Point Special School'
      }, {
        name:  'Woolooga State School'
      }, {
        name:  'Wooloowin State School'
      }, {
        name:  'Woombye State School'
      }, {
        name:  'Woongarra State School'
      }, {
        name:  'Woongoolba State School'
      }, {
        name:  'Woorabinda State School'
      }, {
        name:  'Wooroolin State School'
      }, {
        name:  'Woree State High School'
      }, {
        name:  'Woree State School'
      }, {
        name:  'Worongary State School'
      }, {
        name:  'Wowan State School'
      }, {
        name:  'Wulguru State School'
      }, {
        name:  'Wyandra State School'
      }, {
        name:  'Wynnum State High School'
      }, {
        name:  'Wynnum State School'
      }, {
        name:  'Wynnum West State School'
      }, {
        name:  'Wyreema State School'
      }, {
        name:  'Yandaran State School'
      }, {
        name:  'Yandina State School'
      }, {
        name:  'Yangan State School'
      }, {
        name:  'Yarrabah State School'
      }, {
        name:  'Yarraman State School'
      }, {
        name:  'Yarrilee State School'
      }, {
        name:  'Yarwun State School'
      }, {
        name:  'Yelarbon State School'
      }, {
        name:  'Yeppoon State High School'
      }, {
        name:  'Yeppoon State School'
      }, {
        name:  'Yeronga State High School'
      }, {
        name:  'Yeronga State School'
      }, {
        name:  'Yorkeys Knob State School'
      }, {
        name:  'Yowah State School'
      }, {
        name:  'Yugumbir State School'
      }, {
        name:  'Yuleba State School'
      }, {
        name:  'Yungaburra State School'
      }, {
        name:  'Zillmere State School'
      }, {
        name:  'Baringa State Primary School'
      }, {
        name:  'North Shore State School'
      }, {
        name:  'Picnic Creek State School'
      }, {
        name:  'Queensland Pathways State College'
      }, {
        name:  'Yarrabilba State School'
      }
    ];

    for(var i = 0, len = schoolData.length; i < len; i++) {
      let school = schoolData[i];
      school.verified = true;
      school.created_at = new Date();
      school.updated_at = new Date();
    }

    return queryInterface.bulkInsert('school', schoolData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('school', null, {});
  }
};
