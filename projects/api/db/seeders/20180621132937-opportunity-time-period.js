'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const opportunityTimePeriodData = [
      { name: "Weekdays" },
      { name: "Weekends" },
      { name: "School Break" },
      { name: "Evenings" },
      { name: "Flexible" }
    ];

    for(var i = 0, len = opportunityTimePeriodData.length; i < len; i++) {
      let opportunityTimePeriod = opportunityTimePeriodData[i];
      opportunityTimePeriod.created_at = new Date();
      opportunityTimePeriod.updated_at = new Date();
    }

    return queryInterface.bulkInsert('opportunity_time_period', opportunityTimePeriodData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('opportunity_time_period', null, {});
  }
};
