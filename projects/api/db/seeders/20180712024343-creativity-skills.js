'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 5;
    let skillData = [
      {
        name: "Problem Solving",
        description: "The process of working through details of a problem to reach a solution. Problem solving may include mathematical or systematic operations and can be a gauge of an individual's critical thinking skills."
      },
      {
        name: "Critical Thinking",
        description: "Objective examination of assumptions (adopted rules of thumb) underlying current beliefs to assess their correctness and legitimacy, and thus to validate or invalidate the beliefs."
      },
      {
        name: "Innovation",
        description: "The process of translating an idea or invention into a good or service that creates value or for which customers will pay. To be called an innovation, an idea must be replicable at an economical cost and must satisfy a specific need."
      },
      {
        name: "Design Sense",
        description: "Design sense is the ability or instinct of a person to plan the final look or functionality of something before it is made, usually by preparing a blueprint or some drawings."
      },
      {
        name: "Artistic Sense",
        description: "Artistic sense as a skill refers to one’s ability of being appreciative of creativity in work. It is the ability to explore, experiment, and push past the conventional bounds."
      }
    ];

    for (let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for (var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
