'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 6;
    let skillData = [
      {
        name: "Organisation",
        description: "the ability to use your time, energy, resources, etc. in an effective way so that you achieve the things you want to achieve"
      },
      {
        name: "Planning",
        description: "Planning is the process of thinking about the activities required to achieve a desired goal."
      },
      {
        name: "Scheduling",
        description: "a plan of procedure, usually written, for a proposed objective, especially with reference to the sequence of and time allotted for each item or operation necessary to its completion"
      },
      {
        name: "Time Management",
        description: "the analysis of how working hours are spent and the prioritisation of tasks in order to maximise personal efficiency in the workplace"
      },
      {
        name: "Meeting Management",
        description: "Onces ability to run effective meetings with their management skills."
      },
      {
        name: "Technology Savvy",
        description: "knowing a lot about modern technology, computers, smart phones, etc."
      },
      {
        name: "Technology Trend Awareness",
        description: "Technology trend awareness as a skill refers to being mindful of the technology that is recently becoming popular and is readily accepted in the market."
      },
      {
        name: "Business Trend Awareness",
        description: "Business trend awareness as a skill refers to one’s ability to be conscious of the changing ways in which businesses are developing."
      },
      {
        name: "Research",
        description: "the systematic investigation into and study of materials and sources in order to establish facts and reach new conclusions"
      },
      {
        name: "Business Etiquette",
        description: "Business etiquette is about building relationships with other people. Etiquette is not about rules & regulations but is about providing basic social comfort and creating an environment where others feel comfortable and secure."
      },
      {
        name: "Business Ethics",
        description: "Business ethics, connotes the form of applied ethics, which studies ethical principles, morals and problems that take place in the business environment. It is nothing but the integration of day to day morals and ethical norms to business and applies to all types of business."
      },
      {
        name: "Diversity Awareness",
        description: "Diversity awareness is one’s ability to embrace the uniqueness of all individuals along several dimensions such as race, religious beliefs, ethnicity, age, gender, physical abilities, political beliefs, and socio-economic status."
      },
      {
        name: "Disability Awareness",
        description: "Disability awareness as a skill refers to being mindful of the disabilities of people and managing to communicate and work with them effectively."
      },
      {
        name: "Intercultural Competence",
        description: "the ability to communicate and behave in appropriate ways with those who are culturally different—and to co-create shared spaces, teams, and organisations that are inclusive, effective, innovative, and satisfying."
      },
      {
        name: "Customer Service",
        description: "Customer service is the act of taking care of the customer's needs by providing and delivering professional, helpful, high quality service and assistance before, during, and after the customer's requirements are met."
      },
      {
        name: "Entrepreneurial Thinking",
        description: "Entrepreneurial thinking skills refer to the ability to identify marketplace opportunities and discover the most appropriate ways and time to capitalize on them"
      },
      {
        name: "Facilitation",
        description: "the act of helping other people to deal with a process or reach an agreement or solution without getting directly involved in the process, discussion, etc. yourself"
      },
      {
        name: "Collaborating",
        description: "work jointly on an activity or project."
      }
    ];

    for(let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for(var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
