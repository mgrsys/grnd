'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const opportunityTypeData = [
      { name: "Work Experience" },
      { name: "Structured W/E" },
      { name: "Job Shadow" },
      { name: "Pathway" },
      { name: "Traineeship" },
      { name: "Apprenticeship" },
      { name: "Certificate I (VET Course)" },
      { name: "Certificate II (VET Course)" },
      { name: "Certificate III (VET Course)" },
      { name: "Certificate IV (VET Course)" },
      { name: "Diploma (VET Course)" },
      { name: "Day tour" },
      { name: "Event" },
      { name: "Other" },
    ];

    for(var i = 0, len = opportunityTypeData.length; i < len; i++) {
      let opportunityType = opportunityTypeData[i];
      opportunityType.created_at = new Date();
      opportunityType.updated_at = new Date();
    }

    return queryInterface.bulkInsert('opportunity_type', opportunityTypeData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('opportunity_type', null, {});
  }
};
