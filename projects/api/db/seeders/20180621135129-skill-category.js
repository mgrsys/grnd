'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const skillCategoryData = [
      { name: "Communication" },
      { name: "Leadership" },
      { name: "Interpersonal" },
      { name: "Personal" },
      { name: "Creativity" },
      { name: "Professional" }
    ];

    for(var i = 0, len = skillCategoryData.length; i < len; i++) {
      let skillCategory = skillCategoryData[i];
      skillCategory.created_at = new Date();
      skillCategory.updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill_category', skillCategoryData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('skill_category', null, {});
  }
};
