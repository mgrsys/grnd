'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 3;
    let skillData = [
      {
        name: "Networking",
        description: "the exchange of information or services among individuals, groups, or institutions; specifically, the cultivation of productive relationships for employment or business"
      },
      {
        name: "Interpersonal Relationships",
        description: "Interpersonal skills are the skills used by a person to interact with others properly. In the business domain, the term generally refers to an employee's ability to get along with others while getting the job done."
      },
      {
        name: "Dealing with Difficult People",
        description: "ability to effectively cope with such people while maintaining a healthy work environment is known as the skill of dealing with difficult people."
      },
      {
        name: "Conflict Resolution",
        description: "Conflict resolution is the process by which two or more parties engaged in a disagreement, dispute, or debate reach an agreement resolving it."
      },
      {
        name: "Personal Branding",
        description: "Personal branding is the process of creating a recognisable professional name and reputation for yourself or your company/business"
      },
      {
        name: "Office Politics",
        description: "the activities, attitudes, or behaviors that are used to get or keep power or an advantage within a business or company"
      }
    ];

    for(let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for(var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
