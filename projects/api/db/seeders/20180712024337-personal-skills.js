'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 4;
    let skillData = [
      {
        name: "Emotional Intelligence",
        description: "the capacity to be aware of, control, and express one's emotions, and to handle interpersonal relationships judiciously and empathetically."
      },
      {
        name: "Self Awareness",
        description: "Conscious knowledge of one's own character, feelings, motives, and desires"
      },
      {
        name: "Stress Management",
        description: "Stress management is a wide spectrum of techniques aimed at controlling a person's level of stress, especially chronic stress, usually for the purpose of improving everyday functioning."
      },
      {
        name: "Tolerance of Change and Uncertainty",
        description: "Tolerance of change and uncertainty refers to the ability to adjust to changes in trends or modes of operations in an organisation or society"
      },
      {
        name: "Taking Criticism",
        description: "Taking criticism is the ability of a person to accept constructive criticisms for improvement, and being able to withstand the pressure of unfair or dispiriting criticisms while motivating to work harder and better instead of giving up."
      },
      {
        name: "Self Confidence",
        description: "a feeling of trust in one's abilities, qualities, and judgement"
      },
      {
        name: "Adaptability",
        description: "Ability of one to alter itself or its responses to the changed circumstances or environment and learn from experience."
      },
      {
        name: "Resilience",
        description: "the capacity to absorb the impact of the failure or a significant disturbance in its environment, and to still continue to recover quickly from difficulties"
      },
      {
        name: "Assertiveness",
        description: "behaving confidently and able to say in a direct way what you want or believe"
      },
      {
        name: "Competitiveness",
        description: "the quality of being as good as or better than others of a comparable nature"
      },
      {
        name: "Friendliness",
        description: "characteristic of or befitting a friend; showing friendship, being kind; helpful"
      },
      {
        name: "Enthusiasm",
        description: "a feeling of energetic interest in a particular subject or activity and an eagerness to be involved in it."
      },
      {
        name: "Empathy",
        description: "the action of understanding, being aware of, being sensitive to, and vicariously experiencing the feelings, thoughts, and experience of another of either the past or present without having the feelings, thoughts, and experience fully communicated in an objectively explicit manner"
      },
      {
        name: "Selling",
        description: "the activity of making products and services available so that people buy them"
      },
      {
        name: "Inspiring",
        description: "encouraging, or making you feel you want to do something"
      },
      {
        name: "Persuasion",
        description: "the action or process of persuading someone or of being persuaded to do or believe something"
      },
      {
        name: "Negotiation",
        description: "discussion aimed at reaching an agreement."
      },
      {
        name: "Motivating",
        description: "provide (someone) with a reason for doing something"
      }
    ];

    for(let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for(var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
