'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 1;
    let skillData = [
      {
        name: "Verbal Communication",
        description: "The sharing of information between individuals by using speech. Individuals working within a business need to effectively use verbal communication that employs readily understood spoken words, as well as ensuring that the enunciation, stress and tone of voice with which the words are expressed is appropriate."
      },
      {
        name: "Body Language",
        description: "Silent (non-verbal) messages communicated through the sender's body movements, facial expressions, voice tone and loudness, etc. In social psychology, all behavior in presence of another person is considered communication."
      },
      {
        name: "Physical Communication",
        description: "It is a form of communication where symbols, signs and gestures are used. Unlike verbal communication which involves the use of sound and the ear for hearing, physical communication involves the use of the eyes for seeing and other parts of the body for gesturing."
      },
      {
        name: "Visual Communication",
        description: "The expression of ideas and information using visual forms or aids. Body language including gestures are part of such communication. Presentations may also included photographs, charts, graphs, and diagrams to enforce or demonstrate ideas or data."
      },
      {
        name: "Writing",
        description: "The activity or skill of writing."
      },
      {
        name: "Storytelling",
        description: "The activity of telling or writing stories."
      },
      {
        name: "Humor",
        description: "The quality of being amusing or comic, especially as expressed in literature or speech"
      },
      {
        name: "Quick-wittedness",
        description: "Quick in perception and understanding : mentally alert"
      },
      {
        name: "Listening",
        description: "The act of mindfully hearing and attempting to comprehend the meaning of words spoken by another in a conversation or speech. Activity listening is an important business communication skill, and it can involve making sounds that indicate attentiveness, as well as the listener giving feedback in the form of a paraphrased rendition of what has been said by the other party for their confirmation."
      },
      {
        name: "Presentation Skills",
        description: "Presentation skills are the skills you need in delivering effective and engaging presentations to a variety of audiences"
      },
      {
        name: "Public Speaking",
        description: "Public speaking (also called oratory or oration) is the process or act of performing a speech to a live audience. This type of speech is deliberately structured with three general purposes: to inform, to persuade and to entertain."
      }
    ];

    for (let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for (var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('skill_category_skill', {}, {})
      .then(() => {
        return queryInterface.bulkDelete('skill', null, {});
      });
  }
};
