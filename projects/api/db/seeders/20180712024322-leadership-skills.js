'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let skillCategoryId = 2;
    let skillData = [
      {
        name: "Team Building",
        description: "the action or process of causing a group of people to work together effectively as a team, especially by means of activities and events designed to increase motivation and promote cooperation"
      },
      {
        name: "Strategic Planning",
        description: "A systematic process of envisioning a desired future, and translating this vision into broadly defined goals or objectives and a sequence of steps to achieve them."
      },
      {
        name: "Coaching",
        description: "Coaching is a form of development in which a person called a coach supports a learner in achieving a specific personal or professional goal by providing training and guidance"
      },
      {
        name: "Mentoring",
        description: "the activity of supporting and advising someone with less experience to help them develop in their work"
      },
      {
        name: "Delegation",
        description: "the act of delegating a particular job, duty, right, etc. to someone"
      },
      {
        name: "Diplomacy",
        description: " skill in dealing with people without offending or upsetting them"
      },
      {
        name: "Giving Feedback",
        description: "Giving feedback is a task you perform again and again as a manager or supervisor, letting people know where they are and where to go next in terms of expectations and goals - yours, their own, and the organisations."
      },
      {
        name: "Managing Difficult Conversations",
        description: "An individual’s ability to handle his emotions in such circumstances and convey information in an extremely sensitive manner is referred to as his skills of managing difficult conversations"
      },
      {
        name: "Decision Making",
        description: "the process of deciding about something important, especially in a group of people or in an organisation."
      },
      {
        name: "Performance Management",
        description: "Performance management skills refer to the ability of an individual to ensure that the work of those working under his supervision supports and furthers the goals and objectives of the organisation."
      },
      {
        name: "Supervising",
        description: "the act of watching a person or activity and making certain that everything is done correctly, safely, etc."
      },
      {
        name: "Managing",
        description: "to be in charge of and control a company, department, project, team, etc."
      },
      {
        name: "Managing Remote Teams",
        description: "A remote team refers to a group of people working together to serve a common purpose, without being co-located i.e. they are geographically dispersed"
      },
      {
        name: "Crisis Management",
        description: "the actions that are taken to deal with an emergency or difficult situation in an organised way:"
      }
    ];

    for (let i in skillData) {
      skillData[i].created_at = new Date();
      skillData[i].updated_at = new Date();
    }

    return queryInterface.bulkInsert('skill', skillData, { returning: true })
      .then((skills) => {
        let skillCategorySkillData = [];
        for (var i = 0, len = skills.length; i < len; i++) {
          skillCategorySkillData.push({
            skill_category_id: skillCategoryId,
            skill_id: skills[i].id,
            created_at: new Date(),
            updated_at: new Date()
          });
        }
        return queryInterface.bulkInsert('skill_category_skill', skillCategorySkillData, {});
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
