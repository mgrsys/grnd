import * as dotenv from 'dotenv';
dotenv.config();

import { Application as ApplicationR } from '@modules/application/model/application.relations';
import { Contact as ContactR } from '@modules/contact/model/contact.relations';
import { Opportunity as OpportunityR } from '@modules/opportunity/model/opportunity.relations';
import { Organisation as OrganisationR } from '@modules/organisation/model/organisation.relations';
import { School as SchoolR } from '@modules/school/model/school.relations';
import { SkillCategory as SkillCategoryR } from '@modules/skill/model/skill-category.relations';
import { Skill as SkillR } from '@modules/skill/model/skill.relations';
import { Student as StudentR } from '@modules/student/model/student.relations';
import { User as UserR } from '@modules/user/model/user.relations';
import { Profile as ProfileR } from '@modules/profile/schema/profile.relations';
import { UserExperience as UserExperienceR } from '@modules/profile/schema/user.experience.relations';
const relations = [
  ApplicationR,
  ContactR,
  OpportunityR,
  OrganisationR,
  ProfileR,
  SchoolR,
  SkillCategoryR,
  SkillR,
  StudentR,
  UserR,
  UserExperienceR,
];

(async () => {

  try {
    await UserR.sync();
    await StudentR.sync();
    await OpportunityR.sync();
    await ApplicationR.sync();
    await ContactR.sync();
    await OrganisationR.sync();
    await ProfileR.sync();
    await SchoolR.sync();
    await SkillCategoryR.sync();
    await SkillR.sync();
    await UserExperienceR.sync();
  } catch (e) {
    console.error('Unable to sync relations', e);
  }

  process.exit();

})();
