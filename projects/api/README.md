# Student Portal Back End

## Creating a organisation

The current live endpoint url is: https://0agmngbp2j.execute-api.ap-southeast-2.amazonaws.com/prod

All endpoints will accept `application/json`

### GET /school

This will return you the ID of a school you are looking for

It takes a query param ?name.

eg: 

`https://0agmngbp2j.execute-api.ap-southeast-2.amazonaws.com/prod/school?name=kelvin`

### POST /organisation

This endpoint will create an organisation. The post looks somethjing like

```
{
  "name": "Test org", /* Required */
  "username": "test@testorg.com",
  "password": "Test123!",
  "schoolIds": [ 593 ]
}
```