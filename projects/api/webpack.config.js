require('dotenv').config();
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const slsw = require('serverless-webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const webpack = require('webpack');

// Environment variable mappings + defaults
const envVars = {
  API_ENV: null,
  API_URL: null,
  APP_URL: null,
  DB_USER: null,
  DB_PASS: null,
  DB_NAME: null,
  DB_HOST: null,
  POSTCODE_API_HOST: null,
  POSTCODE_API_KEY: null,
  AWS_COGNITO_USER_POOL_ID: null,
  AWS_COGNITO_USER_POOL_CLIENT_ID: null,
  AWS_S3_PROFILE_PICTURE_BUCKET: null,
  AWS_COGNITO_IDENTITY_POOL_ID: null,
  AWS_REGION: null,
  LOG_ENABLED: null,
  SEQUELIZE_LOG_ENABLED: null,
  AWS_SES_ACCESS_KEY: null,
  AWS_SES_SECRET_KEY: null,
  MAIL_TRANSPORT: null,
  MAIL_ADMIN: null,
  MAIL_FROM: null,
};

const config = {
  entry: slsw.lib.entries,
  devtool: 'source-map',
  externals: nodeExternals(),
  mode: "development",
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.hbs'],
    plugins: [
      new TsconfigPathsPlugin(),
    ],
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
  },
  plugins: [
    new webpack.EnvironmentPlugin(envVars),
    new webpack.ProvidePlugin({
      fetch: 'isomorphic-fetch'
    })
  ],
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(sql|hbs)$/,
        use: 'raw-loader'
      },
      {
        test: /\.ts(x?)$/,
        loader: 'ts-loader'
      },
    ],
  },
};

module.exports = config;
