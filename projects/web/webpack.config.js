require('dotenv').config();

const path = require('path');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const webpack = require('webpack');

// Environment variable mappings + defaults
const envVars = {
  APP_ENV: 'development',
  API_URL: null,
  AWS_COGNITO_IDENTITY_POOL_ID: null,
  AWS_REGION: null,
  AWS_COGNITO_USER_POOL_ID: null,
  AWS_COGNITO_USER_POOL_CLIENT_ID: null,
  AWS_S3_PROFILE_PICTURE_BUCKET: null,
};

const config = {
  entry: {
    main: './src/index.tsx',
  },
  output: {
    path: path.resolve('./dist'),
    publicPath: '/',
    filename: '[name].[chunkhash:12].js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.gql', '.graphql'],
    plugins: [
      new TsconfigPathsPlugin()
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract('css-loader'),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract(['css-loader', 'sass-loader']),
      },
      {
        test: /\.(gif|jpe?g|png)$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: '[name].[hash:12].[ext]',
        },
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader',
        options: {
          mimetype: 'application/font-woff',
        },
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash:12].[ext]',
        },
      },
      {
        test: /\.ts(x?)$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          experimentalWatchApi: true,
        },
      },
    ],
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin('dist'),
    new CopyWebpackPlugin([
      { from: 'public' },
    ]),
    new ExtractTextPlugin('[name].[chunkhash:12].css'),
    // new HardSourceWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new SWPrecacheWebpackPlugin({
      cacheId: 'grandshake-cache',
      dontCacheBustUrlsMatching: /\.\w{12}\./,
      filename: 'service-worker.js',
      minify: true,
      navigateFallback: '/index.html',
      staticFileGlobsIgnorePatterns: [/\.map$/],
    }),
    new webpack.EnvironmentPlugin(envVars),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true,
    overlay: true,
  },
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
  mode: process.env.APP_ENV || 'development'
};

// Validate ENV
if (!process.env.APP_ENV) {
  console.warn('No environment variable set for APP_ENV - did you set up .env?');
}


// Development build
if (process.env.APP_ENV === 'development') {
  // General build optimisation
  config.output.pathinfo = false;
  config.optimization = {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
  };
}

module.exports = config;
