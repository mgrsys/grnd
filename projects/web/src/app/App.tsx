import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import styled from 'styled-components';

// Components
import ErrorPage from '@pages/auth/ErrorPage';
import SigninPage from '@pages/auth/SigninPage';
import DashboardPage from '@pages/dashboard/DashboardPage';
import NewOpportunityPage from '@pages/opportunity/NewOpportunityPage';
import OpportunityPage from '@pages/opportunity/OpportunityPage';
import StudentSignupPage from '@pages/student/StudentSignupPage';
import { DataLoader } from '@components/DataLoader';
import { withUser } from '@components/UserLoader';
import { setUser } from '@models/user';
import { AlertHandler } from '@components/alert.handler.tsx';
import ProfilePage from '@pages/profile/ProfilePage';
import ContactsPage from '@pages/contacts/ContactsPage';

interface IProps {
  setUser: (user: any) => any;
}

const App = styled.section`
  min-height: 100vh;
  display: flex;
  flex: 1;
`;

class AppComponent extends React.Component<IProps, any> {
  public render() {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <App>
          <DataLoader>
            <Switch>
              <Route exact path="/" component={withUser(DashboardPage)} />
              <Route exact path="/signin" component={SigninPage} />
              <Route exact path="/student/signup" component={StudentSignupPage} />
              <Route exact path="/opportunity/new" component={withUser(NewOpportunityPage)} />
              <Route exact path="/opportunity/:id" component={withUser(OpportunityPage)} />
              <Route exact path="/profile" component={withUser(ProfilePage, true)} />
              <Route exact path="/contacts" component={withUser(ContactsPage)} />
              <Route component={ErrorPage} />
            </Switch>
          </DataLoader>
          <AlertHandler />
        </App>
      </MuiPickersUtilsProvider>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    setUser: (user) => {
      return dispatch(setUser(user));
    },
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(AppComponent);
