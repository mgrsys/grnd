import { get } from 'lodash';

export const getUserId = (user) => {
  const userDetails = String(get(user, 'id', null)).split(':');
  return userDetails.length > 0 ? userDetails[1] : null;
};

export const getUserRole = (user) => {
  return get(user, 'attributes["custom:role"]', null);
};
