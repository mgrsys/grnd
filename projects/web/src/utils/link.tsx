import * as React from 'react';
import { Link as DomLink } from 'react-router-dom';

export const Link = (to: string) => (props: any) => <DomLink to={to} {...props } />;
