import * as React from 'react';
import Container from './styled';

interface IProps {
  children: React.ReactNode;
}

class ContainerComponent extends React.Component<IProps, any> {
  public render() {
    const { children } = this.props;
    return (
      <Container>{children}</Container>
    );
  }
}

export default ContainerComponent;
