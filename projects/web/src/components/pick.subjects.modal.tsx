import * as React from 'react';
import { connect } from 'react-redux';
import { Theme, withStyles, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Select, FormControl, InputLabel, MenuItem, Typography, Chip } from '@material-ui/core';
import { Dispatch } from 'redux';
import { newAlert } from '@actions/global';
import { AlertLevel } from './global.model';

interface IProps {
  classes?: any;
  subjects?: any;
  selectedSubjects: any[];
  onChange: any;
  onCancel: any;
  onSave: any;
  onClose: any;
  dispatch?: any;
  open: boolean;

}
interface IState {
}

const styles = (theme: Theme) => ({
  root: {
    width: '960px',
    [theme.breakpoints.down('md')]: {
      width: '80%'
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      margin: theme.spacing.unit
    },
  },
  dropdown: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2,
  },
  subjectChip: {
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  chipsContainer: {
    paddingTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2,
  }
});

class PickSubjectsModal extends React.Component<IProps, IState> {

  public handleChange = (event: any): void => {
    this.props.onChange(event.target.value);
  }

  public removeSubject = (id: string): void => {
    const selectedSubjects: any[] = [...this.props.selectedSubjects];
    const newSubjects = selectedSubjects.filter((subject: any): boolean => subject.id.toString() !== id);
    this.props.onChange(newSubjects.map((subject: any): string => subject.id.toString()));
  }

  public onSave = (): void => {
    const selectedSubjects: any[] = this.props.selectedSubjects;
    const dispatch: Dispatch = this.props.dispatch;
    if (selectedSubjects.length === 0) {
      dispatch(newAlert({
        level: AlertLevel.WARNING,
        body: "You must select at least one subject",
      }));
      return;
    }
    this.props.onSave();
  }

  public render() {
    const { classes, subjects } = this.props;
    const selectedSubjects: any[] = this.props.selectedSubjects;
    const selectedIds: string[] = selectedSubjects.map((subject: any): string => subject.id.toString());
    const allPicked = !(selectedSubjects.length < 12);

    // sorted alpabetically stuff
    const availableSubjects = Object.entries(subjects).sort(
      (el1, el2) => {
        if (el1[1] < el2[1]) {
          return -1;
        }

        return 0;
      }
    );

    return (
      <Dialog
        open={this.props.open}
        aria-labelledby="form-dialog-title"
        onClose={this.props.onClose}
      >
        <DialogTitle id="form-dialog-title">Update your subjects</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, please enter the subjects that you are currently studying. You may enter as many subjects as you like.
          </DialogContentText>
          <form autoComplete="off">
            <FormControl className={classes.dropdown}>
              <InputLabel htmlFor="age-simple">Subjects</InputLabel>
              <Select
                value={selectedIds}
                onChange={this.handleChange}
                multiple
                disabled={allPicked}
                inputProps={{
                  name: 'subjects',
                  id: 'subjects',
                }}
              >
                {availableSubjects.map(([id, title]): React.ReactNode => (
                  <MenuItem key={id} value={id}>{title}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </form>
          <Typography variant="subheading">Selected subjects</Typography>
          <div className={classes.chipsContainer}>
            {selectedSubjects.map((subject: any): React.ReactNode => (
              <Chip
                className={classes.subjectChip}
                key={subject.id}
                label={subject.title}
                onDelete={() => this.removeSubject(subject.id)}
              />
            ))}
            {selectedSubjects.length === 0 && (
              <Typography variant="caption">No subjects selected.</Typography>
            )}
          </div>

        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={this.props.onCancel}>
            Cancel
          </Button>
          <Button color="primary" onClick={this.onSave}>
            Update
          </Button>
        </DialogActions>
      </Dialog>
    )
  }

}

const mapStateToProps = (state: any): any => ({
  subjects: state.data.subjects || {},
});

export default withStyles(styles)(connect(mapStateToProps)(PickSubjectsModal));
