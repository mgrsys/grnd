import * as React from 'react';
import {
  Theme,
  withStyles,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Slide,
  TextField,
  InputLabel,
  Select,
  MenuItem,
  Avatar
} from '@material-ui/core';
import { Close, CloudUpload, Done, Cancel } from '@material-ui/icons';
import Layout from '@components/Layout';
import SuburbSearch from '@components/SuburbSearch';
import DatePicker from './DatePicker';
import { green, red } from '@material-ui/core/colors';

interface IProps {
  classes: any;
  onCancel: () => any;
  onSave: (formData: IFormData) => any;
  open: boolean;
  initialData: IFormData;
}

interface IState {
  errors: any;
  formData: IFormData;
}
interface IFormData {
  firstName: string;
  lastName: string;
  headline: string;
  grade: number;
  dob: string;
  suburb: any;
  contactNumber: string;
  gender: number;
  profilePicture: string | File | null;
}

const styles = (theme: Theme): any => ({
  root: {

  },
  formInput: {
    margin: "1.0rem 0",
  },
  fileUpload: {
    display: 'flex',
    flexDirection: "row",
    margin: "1.0rem 0",
    alignItems: 'center',
  },
  uploadButton: {
    marginRight: "1.0rem",
    width: 'auto',
  }
});

const Transition = (props: any): React.ReactElement<any> => (
  <Slide direction="up" {...props} />
);

class EditProfileMain extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      errors: {},
      formData: {
        firstName: props.initialData.firstName ? props.initialData.firstName : '',
        lastName: props.initialData.lastName ? props.initialData.lastName : '',
        headline: props.initialData.headline ? props.initialData.headline : '',
        grade: props.initialData.grade ? props.initialData.grade : 0,
        dob: props.initialData.dob ? props.initialData.dob : '',
        suburb: props.initialData.suburb ? props.initialData.suburb.suburb : null,
        contactNumber: props.initialData.contactNumber ? props.initialData.contactNumber : '',
        gender: props.initialData.gender ? props.initialData.gender : -1,
        profilePicture: props.initialData.profilePicture ? props.initialData.profilePicture : null,
      },
    };
  }

  public componentWillReceiveProps(props: IProps): void {
    this.setState({
      errors: {},
      formData: {
        firstName: props.initialData.firstName ? props.initialData.firstName : '',
        lastName: props.initialData.lastName ? props.initialData.lastName : '',
        headline: props.initialData.headline ? props.initialData.headline : '',
        grade: props.initialData.grade ? props.initialData.grade : 0,
        dob: props.initialData.dob ? props.initialData.dob : '',
        suburb: props.initialData.suburb ? props.initialData.suburb.suburb : null,
        contactNumber: props.initialData.contactNumber ? props.initialData.contactNumber : '',
        gender: props.initialData.gender !== null ? props.initialData.gender : -1,
        profilePicture: props.initialData.profilePicture ? props.initialData.profilePicture : null,
      },
    });
  }

  public validateAndSave = (): void => {
    const formData: IFormData = { ...this.state.formData };
    const errors: any = {};

    if (this.state.errors.profilePicture) {
      errors.profilePicture = "Please select a valid profile picture";
    } else if (formData.profilePicture && typeof formData.profilePicture === "string") {
      delete formData.profilePicture;
    } else if (formData.profilePicture) {
      if (formData.profilePicture.size > 2000000) {
        errors.profilePicture = true;
      }

      if (
        !(['image/jpg', 'image/jpeg', 'image/png'].includes(formData.profilePicture.type))
      ) {
        errors.profilePicture = true;
      }
    }

    if (!formData.headline || formData.headline.length === 0) {
      errors.headline = true;
    }

    if (!formData.firstName || formData.firstName.length === 0) {
      errors.firstName = true;
    }

    if (!formData.lastName || formData.lastName.length === 0) {
      errors.lastName = true;
    }

    if (!formData.grade ||
      formData.grade === 0 ||
      (formData.grade !== 9 &&
        formData.grade !== 10 &&
        formData.grade !== 11 &&
        formData.grade !== 12)) {
      errors.grade = true;
    }

    if (typeof formData.suburb === "string") {
      delete formData.suburb;
    }

    else if (!formData.suburb || Object.keys(formData.suburb).length === 0) {
      errors.suburb = true;
    }

    if (formData.gender !== 0 && formData.gender !== 1 && formData.gender !== 2) {
      errors.gender = true;
    }

    if (Object.keys(errors).length > 0) {
      this.setState({
        ...this.state,
        errors,
      });
      return;
    };

    this.props.onSave(formData);
  }

  public handleFieldChange = (field: string, value: any): void => {
    this.setState({
      ...this.state,
      errors: {
        ...this.state.errors,
        [field]: false,
      },
      formData: {
        ...this.state.formData,
        [field]: value,
      },
    });
  }

  public handleFileUpload = (event: any): void => {
    const file: File = event.target.files[0];
    if (file.size > 2000000) {
      this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          profilePicture: "Please choose a photo less than 2mb in size",
        }
      });

      return;
    }
    if (
      file.type !== "image/jpg" &&
      file.type !== "image/jpeg" &&
      file.type !== "image/png"
    ) {
      this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          profilePicture: "Please choose a jpg, or a png",
        }
      });

      return;
    }

    this.setState({
      ...this.state,
      formData: {
        ...this.state.formData,
        profilePicture: file,
      },
      errors: {
        ...this.state.errors,
        profilePicture: false,
      }
    });
  }

  public render() {
    const classes: any = this.props.classes;
    const { formData, errors }: { formData: IFormData, errors: any } = this.state;

    return (
      <Dialog
        fullScreen
        open={this.props.open}
        onClose={this.props.onCancel}
        TransitionComponent={Transition}
      >
        <AppBar position="fixed">
          <Toolbar>
            <IconButton color="inherit" aria-label="Close" onClick={this.props.onCancel}>
              <Close />
            </IconButton>
            <Typography variant="title" color="inherit" style={{ flex: 1 }}>
              Update profile
                    </Typography>
            <Button color="inherit" onClick={this.validateAndSave}>
              save
                    </Button>
          </Toolbar>
        </AppBar>
        <input type="file" name="" id="" ref="hiddenUpload" onChange={this.handleFileUpload} style={{ display: 'none' }} />
        <Layout style={{ paddingTop: "1.0rem" }}>
          <Typography variant="headline" style={{ marginTop: "1.0rem" }}>
            Update profile
          </Typography>
          <form>
            <TextField
              required
              fullWidth
              label="First name"
              error={errors.firstName}
              onChange={(event: any): void =>
                this.handleFieldChange("firstName", event.target.value)
              }
              value={formData.firstName}
              className={classes.formInput}
            />

            <TextField
              required
              fullWidth
              label="Last name"
              error={errors.lastName}
              onChange={(event: any): void =>
                this.handleFieldChange("lastName", event.target.value)
              }
              value={formData.lastName}
              className={classes.formInput}
            />

            <DatePicker
              keyboard
              fullWidth
              label="Date of Birth"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
              clearable
              format="YYYY-MM-DD"
              error={errors.dob}
              value={formData.dob}
              onChange={(value: any): void =>
                this.handleFieldChange("dob", value)
              }
              animateYearScrolling={false}
              className={classes.formInput}
            />

            <InputLabel required htmlFor="gender-simple">Gender</InputLabel>

            <Select
              value={formData.gender}
              fullWidth
              onChange={(event: any): void =>
                this.handleFieldChange("gender", event.target.value)
              }
              name="Gender"
              required
              error={errors.gender}
              className={classes.formInput}
              inputProps={{
                name: 'gender',
                id: 'gender-simple',
              }}
            >
              <MenuItem value={0}>Male</MenuItem>
              <MenuItem value={1}>Female</MenuItem>
              <MenuItem value={2}>Other</MenuItem>
            </Select>

            <TextField
              required
              fullWidth
              label="A bit about yourself"
              error={errors.headline}
              className={classes.formInput}
              onChange={(event: any): void =>
                this.handleFieldChange("headline", event.target.value)
              }
              value={formData.headline}
            />

            <InputLabel required htmlFor="grade-simple">Grade</InputLabel>

            <Select
              fullWidth
              value={formData.grade}
              onChange={(event: any): void =>
                this.handleFieldChange("grade", event.target.value)
              }
              name="Grade"
              required
              error={errors.grade}
              className={classes.formInput}
              inputProps={{
                name: 'grade',
                id: 'grade-simple',
              }}
            >
              <MenuItem value={9}>9</MenuItem>
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={11}>11</MenuItem>
              <MenuItem value={12}>12</MenuItem>
            </Select>

            <TextField
              fullWidth
              className={classes.formInput}
              label="Contact number"
              onChange={(event: any): void =>
                this.handleFieldChange("contactNumber", event.target.value)
              }
              value={formData.contactNumber}
              error={errors.contactNumber}
            />

            <SuburbSearch
              error={errors.suburb}
              onChange={(value: any): void =>
                this.handleFieldChange("suburb", value)
              }
              initialValue={formData.suburb}
            />
            <Typography variant="caption" style={{ marginTop: '1.0rem' }}>
              Profile picture
                        </Typography>
            <div className={classes.fileUpload}>
              <Button
                className={classes.uploadButton}
                onClick={() => this.refs.hiddenUpload.click()}
                variant="raised"
                color="primary"
              >
                <CloudUpload style={{ marginRight: "0.5rem" }} />
                Upload
                            </Button>
              {errors.profilePicture ? (
                <div style={{ display: "flex", flexDirection: "row", alignItems: 'center' }}>
                  <Avatar style={{
                    backgroundColor: red[500],
                    marginRight: '1.0rem',
                    width: 30,
                    height: 30,
                  }}>
                    <Cancel />
                  </Avatar>
                  {errors.profilePicture}
                </div>
              ) :
                formData.profilePicture ? (
                  typeof formData.profilePicture === "string" ?
                    formData.profilePicture
                    :
                    <div style={{ display: "flex", flexDirection: "row", alignItems: 'center' }}>
                      <Avatar style={{
                        backgroundColor: green[500],
                        marginRight: '1.0rem',
                        width: 30,
                        height: 30,
                      }}>
                        <Done />
                      </Avatar>
                      Profile picture selected.
                                        </div>

                ) :
                  "Choose a profile picture"
              }
            </div>

          </form>
        </Layout>
      </Dialog>
    )
  }
}

export default withStyles(styles)(EditProfileMain);
