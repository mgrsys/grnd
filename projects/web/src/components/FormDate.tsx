import {
  IconButton,
  InputAdornment,
  MenuItem,
  TextField,
} from '@material-ui/core';
import { DateRange } from '@material-ui/icons';
import { get } from 'lodash';
import * as React from 'react';

import styled from 'styled-components';

// Components
import { IconWrapper } from '@components/FormComponents/styled';

const getDays = (month) => {
  const start = 1;
  let max = 31;
  switch (month) {
    case 'sep':
    case 'apr':
    case 'jun':
    case 'nov':
      max = 30;
      break;
    case 'feb':
      max = 28;
      break;
    default:
      max = 31;
      break;
  }
  const object = {};
  for (let i = start; i <= max; i++) {
    object[i] = i;
  }
  return object;
};

const months = [
  { mm: '01', mo: 'January' },
  { mm: '02', mo: 'February' },
  { mm: '03', mo: 'March' },
  { mm: '04', mo: 'April' },
  { mm: '05', mo: 'May' },
  { mm: '06', mo: 'Jun' },
  { mm: '07', mo: 'Jul' },
  { mm: '08', mo: 'August' },
  { mm: '09', mo: 'September' },
  { mm: '10', mo: 'October' },
  { mm: '11', mo: 'November' },
  { mm: '12', mo: 'December' },
];

function getYears(minYear, maxYear) {
  const years = {};
  for (let i = maxYear; i >= minYear; i--) {
    years[i] = i;
  }
  return years;
}

const FormFlexField = styled(TextField)`
  flex: 1;
  padding-right: 1rem;
`;

const FormDateControls: any = styled.div`
  ${(props: any) => props.disabled && `
    &:before {
      position: absolute;
      height: 100%
      width: 100%;
      z-index: 2;
      background: rgba(255,255,255,0.7);
      content: '';
    }
  `}
  position relative;
  display: flex;
  flex: 1;
  flex-direction: row;
  `;

interface IProps {
  minYear: number;
  maxYear: number;
  disabled: boolean;
  onChange: (e: any) => any;
}

class FormDateComponent extends React.Component<IProps, any> {
  constructor(props) {
    super(props);
    this.state = {
      day: '',
      disabled: props.disabled,
      maxYear: props.maxYear || new Date().getFullYear(),
      minYear: props.minYear || 1990,
      month: '',
      onChange: props.onChange,
      year: '',
      ...props,
    };
  }
  public shouldComponentUpdate(nextProps, nextState) {
    let bool = false;
    if (this.state !== nextState) {
      bool = true;
    }
    if (this.state.disabled !== nextProps.disabled) {
      bool = true;
    }
    return bool;
  }
  public onChange = (field) => (e) => {
    const { onChange } = this.state;
    const value = get(e, 'target.value', e);
    const data: any = {
      ...this.state,
      [field]: value,
    };
    const date = data.year && data.month && data.day
      ? `${data.year}-${data.month}-${data.day}`
      : null;
    this.setState({
      ...this.state,
      [field]: value,
    });
    onChange(date);
  }

  public handleMouseDownPassword = event => {
    event.preventDefault();
  }

  public handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  }

  public render() {
    const { day, month, year } = this.state;
    const { disabled, minYear, maxYear } = this.state;
    const days = getDays(month);
    const years = getYears(minYear, maxYear);
    return (
      <FormDateControls disabled={disabled}>
        <FormFlexField
          select
          label="Day"
          id="day"
          value={day}
          onChange={this.onChange('day')}
          style={{ paddingRight: '1rem' }}
        >
          {Object.keys(days).map((key: any) => {
            const dayLabel: any = days[key];
            return (
              <MenuItem key={key} value={key < 10 ? `0${key}` : key}>
                {dayLabel}
              </MenuItem>
            );
          })}
        </FormFlexField>
        <FormFlexField
          select
          label="Month"
          id="month"
          value={month}
          onChange={this.onChange('month')}
          style={{ paddingRight: '1rem' }}
        >
          {months.map((data) => {
            return (
              <MenuItem key={data.mm} value={data.mm}>
                {data.mo}
              </MenuItem>
            );
          })}
        </FormFlexField>
        <FormFlexField
          select
          label="Year"
          id="year"
          value={year}
          onChange={this.onChange('year')}
          style={{ paddingRight: '1rem' }}
        >
          {Object.keys(years).map((key) => {
            const dobYear = years[key];
            return (
              <MenuItem key={key} value={key}>
                {dobYear}
              </MenuItem>
            );
          })}
        </FormFlexField>
        <FormFlexField
          style={{ flex: 'none', width: 48 }}
          label="&nbsp;"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end" style={{ marginLeft: 0 }}>
                <IconButton
                  aria-label="Toggle password visibility"
                  onClick={this.handleClickShowPassword}
                  onMouseDown={this.handleMouseDownPassword}
                >
                  <IconWrapper>
                    <DateRange />
                  </IconWrapper>
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </FormDateControls>
    );
  }
}

export default FormDateComponent;
