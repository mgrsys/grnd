import * as React from 'react';
import { connect } from 'react-redux';
import {
  createMuiTheme,
  createStyles,
  MuiThemeProvider,
  Theme,
  withStyles,
} from '@material-ui/core/styles';

import {
  Button,

  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,

  Grid,
  Tab,
  Tabs,
} from '@material-ui/core';

import { removeOpportunity, updateExploreView, newApplication, setEditMode } from '@actions/opportunity';

import { grey, indigo } from '@material-ui/core/colors';

import { ArrowBack, Delete, Edit, Save, Cancel, } from '@material-ui/icons';

import { Opportunity } from '@models/opportunity';
import { Dispatch } from 'redux';

import { withRouter } from 'react-router-dom';

import { getUserRole } from '@utils/user';

interface IProps {
  classes: any;
  dispatch: any;
  exploreView: number;
  editMode: boolean;
  opportunity: Opportunity;
  loading: boolean;
  history: any;
  user: any;
}

interface IState {
  value: number;
  deleteModal: boolean;
}

const theme = createMuiTheme({
  palette: {
    primary: { main: indigo.A700 },
    secondary: grey,
  },
});

const styles = (t: Theme): any => createStyles({
  root: {
    display: 'flex',
    flexBasis: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',

    alignItems: 'center',
  },

  fullBar: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '1.0rem',
  },

  backToListing: {
    color: grey[700],
  },
  remove: {
    color: grey[700],
  },

  edit: {
    color: grey[700],
  },

  canSubmitInterest: {
    marginRight: theme.spacing.unit,
  },

});

class ExploreToolbarComponent extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      deleteModal: false,
    };
  }

  public componentWillReceiveProps(props) {

  }

  public handleChange = (event, value): void => {
    const { dispatch } = this.props;
    dispatch(updateExploreView(value));
  }
  public modalOpen = (event, value) => {
    this.setState({
      ...this.state,
      deleteModal: true,
    });
  }
  public modalClose = (event, value): void => {
    this.setState({
      ...this.state,
      deleteModal: false,
    });
  }
  public delete = (): void => {
    this.setState({
      ...this.state,
      deleteModal: false,
    }, async (): Promise<void> => {
      const {
        opportunity,
        dispatch,
      }: { opportunity: Opportunity, dispatch: Dispatch } = this.props;
      try {
        await dispatch(removeOpportunity(opportunity.id));
        this.props.history.push('/');
      } catch (e) {
        //
      }
    });
  }
  public cancelEdit = () => {
    //
  }
  public goBack = () => {
    this.props.history.push('/');
  }
  public edit = () => {
    const { dispatch, editMode, exploreView }: { dispatch: Dispatch, editMode: boolean, exploreView: number, } = this.props;
    if (exploreView == 1) {
      dispatch(updateExploreView(0));
    }
    dispatch(setEditMode(!editMode));
  }
  public cancel = () => {
    //
  }
  public submitInterest = () => {
    const { dispatch, opportunity }: { dispatch: Dispatch, opportunity: Opportunity } = this.props;
    dispatch(newApplication(opportunity.id));
  }

  public render() {
    const {
      classes,
      exploreView,
      user,
      opportunity,
      loading,
      editMode,
    }: {
      classes: any,
      exploreView: number,
      user: any,
      opportunity: Opportunity,
      loading: boolean,
      editMode: boolean,
    } = this.props;

    const canSubmitInterest: boolean =
      (user !== null) ? getUserRole(user) === 'student' : false;

    // ! bruh fix this.
    const hasntSubmittedInterest: boolean =
      getUserRole(user) === 'student' &&
      canSubmitInterest &&
      opportunity !== null &&
      opportunity.applications.length === 0 &&
      (opportunity.applications.filter(
        application =>
          application.student.email === user.username
      ).length === 0);

    const canDelete: boolean = (user !== null) ? getUserRole(user) === 'organisation' : false;
    const canEdit: boolean = canDelete;
    const canViewApplications: boolean = (user !== null) ?
      (getUserRole(user) === 'organisation' || getUserRole(user) === 'school') : false;
    return (
      <MuiThemeProvider theme={theme}>
        <Dialog
          open={this.state.deleteModal}
          onClose={this.modalClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle>{'Are you sure?'}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {'Are you sure you want to remove \
                         this opportunity? This is an irreversable action.'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.modalClose} color="primary" autoFocus>
              Back
                    </Button>
            <Button onClick={this.delete} color="secondary">
              Delete
                    </Button>
          </DialogActions>
        </Dialog>
        <Grid item sm={12} className={classes.fullBar}>
          <Grid item sm={8}>
            <div className={classes.root}>
              <Tabs
                value={exploreView}
                onChange={this.handleChange}
                style={{ opacity: canViewApplications ? 1 : 0 }}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
              >
                <Tab label="Opportunity" />
                <Tab label="Applications" disabled={editMode} />
              </Tabs>
              <div>
                {canSubmitInterest && (
                  <Button
                    color="primary"
                    variant="contained"
                    className={classes.canSubmitInterest}
                    onClick={this.submitInterest}
                    disabled={!hasntSubmittedInterest || loading}
                  >
                    Submit Interest
                            </Button>
                )}
                {
                  canEdit && editMode &&
                  (<Button className={classes.edit} onClick={this.cancelEdit}>
                    <Cancel style={{ marginRight: '0.5rem' }} /> Cancel
                                </Button>)
                }
                {
                  canEdit && editMode &&
                  (<Button className={classes.edit} onClick={this.edit}>
                    <Save style={{ marginRight: '0.5rem' }} /> Save
                                </Button>)
                }
                {
                  canEdit && !editMode &&
                  (<Button className={classes.edit} onClick={this.edit}>
                    <Edit style={{ marginRight: '0.5rem' }} /> Edit
                                </Button>)
                }
                {canDelete && (<Button className={classes.remove} onClick={this.modalOpen}>
                  <Delete style={{ marginRight: '0.5rem' }} /> Remove
                        </Button>)}
                <Button className={classes.backToListing} onClick={this.goBack}>
                  <ArrowBack style={{ marginRight: '0.5rem' }} /> Back
                        </Button>
              </div>
            </div>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    );
  }

}

const mapStateToProps = (state: any) => {
  return {
    exploreView: state.opportunities.exploreView || 0,
    opportunity: state.opportunities.opportunity || null,
    editMode: state.opportunities.editMode || false,
    loading: state.globals.loading || false,
    user: state.user || null,
  };
};

export const ExploreToolbar = withRouter(connect(
  mapStateToProps,
)(withStyles(styles)(ExploreToolbarComponent)));
