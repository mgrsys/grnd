import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import {
  ListItemIcon,
  Menu,
  MenuItem,
  Button,
  Avatar,
} from '@material-ui/core';
import { Face, AccountCircle, ArrowDropDown } from '@material-ui/icons';
import ExitToApp from '@material-ui/icons/ExitToApp';
import { Storage } from 'aws-amplify';

import logo from '@public/images/logo.svg';
import {
  StyledAppBar,
  StyledToolbar,
  StyledBar,
  NavActions,
  LogoStyled,
  StyledAdd,
  StyledAssignment,
  StyledChat,
  StyledNotifications,
  StyledPeople,
  HideOnSmall,
} from './styled';

import { history } from '@utils/history';
import { Link } from '@utils/link';
import { authLogoutAction } from '@actions/global';
import { signOut } from '@utils/api';
import { getUserRole } from '@utils/user';
import { grey } from '@material-ui/core/colors';
import { getProfileAction } from '@actions/profile';

interface IProps {
  noCreateOpportunity?: boolean;
  accessToProfile?: boolean;
  user: any;
  profile: any;
  dispatch: Dispatch;
}
interface IState {
  active: boolean;
  anchorEl: EventTarget | null | undefined | HTMLElement;
  profilePictureUrl: string | undefined;
}

class NavComponent extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      active: true,
      anchorEl: null,
      profilePictureUrl: undefined,
    };
  }

  componentDidMount() {
    // to show avatar if exist
    const {
      accessToProfile,
      profile,
      dispatch,
    } = this.props;

    if (accessToProfile) {
      if (!profile) {
        dispatch(getProfileAction())
          .then(() => {
            setTimeout(() => {
              if (this.props.profile) {
                Storage.get(this.props.profile.profilePicture)
                  .then(result => {
                    this.setState({
                      profilePictureUrl: result.toString(),
                    });
                  });
              }
            }, 0);
          });
      }
    }
  }

  public toggleActiveView() {
    this.setState({
      ...this.state,
      active: !this.state.active,
    });
  }

  public signOut = () => {
    return signOut()
      .then(() => {
        history.push('/signin');
        this.props.dispatch(authLogoutAction());
      });
  }

  public goToProfile = (): void => {
    history.push('/profile');
  }

  public handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  }

  public handleClose = () => {
    this.setState({ anchorEl: null });
  }

  public render() {
    const { anchorEl, profilePictureUrl } = this.state;
    const open = Boolean(anchorEl);
    const {
      noCreateOpportunity,
      user,
      accessToProfile
    } = this.props;

    return (
      <StyledAppBar
        position="static"
        elevation={0}
      >
        <StyledToolbar style={{
          position: 'fixed',
        }}>
          <StyledBar>
            <LogoStyled src={logo} />
            <NavActions style={{ position: 'relative' }}>
              {
                !noCreateOpportunity && (
                  <Button
                    style={{
                      color: 'rgba(0, 0, 0, 0.6)',
                      fontWeight: 400,
                    }}
                    component={Link('/opportunity/new')}
                  >
                    <StyledAdd />
                    <HideOnSmall>CREATE</HideOnSmall>
                  </Button>
                )
              }
              <Button
                style={{
                  color: 'rgba(0, 0, 0, 0.6)',
                  fontWeight: 400,
                }}
                component={Link('/')}
              >
                <StyledAssignment />
                <HideOnSmall>LISTING</HideOnSmall>
              </Button>

              {
                getUserRole(user) === 'school' ? (
                  <Button
                    style={{
                      color: 'rgba(0, 0, 0, 0.6)',
                      fontWeight: 400,
                    }}
                    component={Link('/contacts')}
                  >
                    <StyledPeople />
                    <HideOnSmall>CONTACTS</HideOnSmall>
                  </Button>
                ) : null
              }

              <StyledChat />
              <StyledNotifications />

              <Avatar
                onClick={this.handleMenu}
                style={{
                  marginLeft: '0.5rem',
                  width: '42px',
                  height: '42px',
                }}
                src={profilePictureUrl}
              >
                { profilePictureUrl ? null : (
                  <AccountCircle
                    style={{
                      fontSize: '50px',
                    }}
                  />
                ) }
              </Avatar>
              <ArrowDropDown
                style={{
                  position: 'absolute',
                  top: '35px',
                  right: '9px',
                  fill: grey[400],
                }}
              />
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl || undefined}
                anchorOrigin={{
                  horizontal: 'right',
                  vertical: 'top',
                }}
                transformOrigin={{
                  horizontal: 'right',
                  vertical: 'top',
                }}
                open={open}
                onClose={this.handleClose}
              >
                {accessToProfile && <MenuItem onClick={this.goToProfile}>
                  <ListItemIcon>
                    <Face />
                  </ListItemIcon>
                  Profile
                  </MenuItem>
                }
                <MenuItem onClick={this.signOut}>
                  <ListItemIcon>
                    <ExitToApp />
                  </ListItemIcon>
                  Logout
                </MenuItem>
              </Menu>
            </NavActions>
          </StyledBar>
        </StyledToolbar>
      </StyledAppBar>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  profile: state.profile.data,
});

export default connect(mapStateToProps)(NavComponent);
