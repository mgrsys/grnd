// tslint:disable:ordered-imports
// tslint:disable:object-literal-sort-keys
import { Avatar, Chip, Paper, Typography } from '@material-ui/core';
import {
  blue,
  grey,
  indigo,
  red,
  green,
  purple,
  orange
} from '@material-ui/core/colors';
import {
  createMuiTheme,
  MuiThemeProvider,
  withStyles,
  createStyles,
  Theme,
} from '@material-ui/core/styles';
import {
  LocationOn,
  PeopleOutline,
  AttachMoney,
  School,
  Build,
  Work,
  ViewList,
} from '@material-ui/icons';
import _ from 'lodash';
import * as React from 'react';
import {
  ListItemContent,
  SkillContainer,
} from '@pages/opportunity/components/OpportunityStyledComponents';

interface IProps {
  id: number;
  experience: any;
  setOpen: (id: number) => void;
  showLocation?: boolean;
  open: boolean;
  classes: any;
}
interface IState { }

const theme = createMuiTheme({
  palette: {
    primary: { main: indigo.A700 },
    secondary: grey,
  },
});

const styles = (t: Theme): any => createStyles({
  root: {
    ...t.mixins.gutters(),
    marginBottom: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingTop: theme.spacing.unit * 2,

    width: '100%',
    cursor: 'pointer',
    boxSizing: 'border-box',
  },
  chip: {
    margin: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    flexBasis: '100%',
    justifyContent: 'space-between',
  },
  avatar: {
    marginRight: theme.spacing.unit * 2,
  },
  chipContainer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
  heading: {
    flexBasis: '250px',
  },
  captions: {
    display: 'flex',
    flexBasis: '300px',
    justifyContent: 'space-between',
  },
  caption: {
    display: 'flex',
    alignItems: 'center',
    textTransform: 'capitalize',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    paddingRight: '0.5rem',
  },
  captionIcon: {
    fontSize: '0.75rem',
    color: grey[800],
    marginRight: '0.5rem',
  },
  aboutRole: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: theme.spacing.unit * 2,
  },
  skills: {
    marginTop: theme.spacing.unit * 2,
  },
  actions: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
  },
  helpLink: {
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-end',
  },
});

class ExperienceListItem extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  public setOpen = (event: any): void => {
    // the first check is essential since className here can be SVGString and no `includes` method exists
    if (event.target.className.includes) {
      if (!event.target.className.includes('prevent-toggle-height')) {
        const id: number = this.props.open ? -1 : this.props.id;
        this.props.setOpen(id);
      }
    }
  }

  public render() {
    const { classes } = this.props;
    const { open, experience }: { open: boolean, experience: any } = this.props;

    const location: string = experience.location ?
      experience.location.suburb.toLowerCase() :
      'Unknown';

    const { opportunityTypeId }:
      { opportunityTypeId: number, daysStillOpen: string } = experience;
    let Icon: any = null;
    let OppColor: any = null;
    switch (opportunityTypeId) {
      case 1: { Icon = () => <Work />; OppColor = indigo[700]; } break;
      case 2: { Icon = () => <ViewList />; OppColor = red[700]; } break;
      case 3: { Icon = () => <PeopleOutline />; OppColor = blue[700]; } break;
      case 4: { Icon = () => <AttachMoney />; OppColor = green[700]; } break;
      case 5: { Icon = () => <School />; OppColor = purple[700]; } break;
      case 6: { Icon = () => <Build />; OppColor = orange[700]; }
    }

    const OppAvatar = () => (
      <Avatar className={classes.avatar} style={{
        background: OppColor,
      }}>
        <Icon />
      </Avatar>
    );
    const OppChip = () => (
      <Chip
        label={experience.opportunityType.name}
        className={classes.chip}
        style={{
          background: OppColor,
          color: '#fff',
        }}
      />
    );

    const height: string = open ? 'auto' : '0';

    return (
      <MuiThemeProvider theme={theme}>
        <Paper onClick={this.setOpen} className={classes.root}>
          <div className={classes.header}>
            <OppAvatar />
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="headline">{experience.title}</Typography>
              <Typography variant="subheading">{experience.company}</Typography>
            </div>
            <div style={{ flex: 1, marginLeft: '1.0rem' }}>
              <OppChip />
            </div>
            <LocationOn className={classes.captionIcon} />
            <Typography variant="body1">{location.toUpperCase()}</Typography>
          </div>
          <ListItemContent style={{ height }}>

            <div className={classes.aboutRole}>
              <Typography variant="subheading">About the experience</Typography>
              <Typography variant="body1">{experience.summary}</Typography>
            </div>

            <div className={classes.skills}>
              <Typography variant="subheading">
                Skills I was exposed to
            </Typography>
              <SkillContainer numItems={experience.skills.length}>
                {experience.skills.map(skill => (
                  <Chip key={skill.id} label={skill.name} />
                ))}
              </SkillContainer>
            </div>
          </ListItemContent>
        </Paper>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(ExperienceListItem);
