import * as React from 'react';
import { Theme, withStyles, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, FormControl, Typography, TextField } from '@material-ui/core';
import * as validation from '@utils/validation';

interface IProps {
  classes?: any;
  onCancel: any;
  onSave: any;
  onClose: any;
  open: boolean;

}
interface IState {
  contact: {
    firstName: string;
    lastName: string;
    middleName: string;
    email: string;
    position: string;
    phoneNumber: string;
  },
  errors: any;
}

const styles = (theme: Theme) => ({
  root: {
    width: '960px',
    [theme.breakpoints.down('md')]: {
      width: '80%'
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      margin: theme.spacing.unit
    },
  },
  formControl: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing.unit * 2,
  },
  formInput: {
    marginRight: '1.0rem',
  }
});

class AddContactModal extends React.Component<IProps, IState> {

  constructor(props) {
    super(props);
    this.state = {
      contact: {
        firstName: '',
        lastName: '',
        middleName: '',
        email: '',
        position: '',
        phoneNumber: '',
      },
      errors: {},
    };
  }
  public handleFieldChange = (field: string, value: string): void => {
    this.setState({
      ...this.state,
      contact: {
        ...this.state.contact,
        [field]: value,
      },
      errors: {
        ...this.state.errors,
        [field]: false,
      },
    }, () => console.info(JSON.stringify(this.state.contact, null, 2)));
  }
  public validateAndSave = (): void => {

    const contact: any = { ...this.state.contact };
    const errors: any = {};
    if (!contact.firstName || contact.firstName === '') errors.firstName = true;
    if (!contact.lastName || contact.lastName === '') errors.lastName = true;
    if (!contact.email || contact.email === '') errors.email = true;
    if (validation.validEmail(contact.email)) errors.email = true;
    if (!contact.position || contact.position === '') errors.position = true;
    if (!contact.phoneNumber || contact.phoneNumber === '') errors.phoneNumber = true;
    if (contact.phoneNumber.length !== 10 || isNaN(contact.phoneNumber as unknown as number)) {
      errors.phoneNumber = true;
    }
    if (Object.keys(errors).length === 0) {
      this.props.onSave(contact);
    } else {
      this.setState({
        ...this.state,
        errors,
      })
    }
  }

  public render() {
    const { classes } = this.props;
    const errors: any = this.state.errors;

    return (
      <Dialog
        open={this.props.open}
        aria-labelledby="form-dialog-title"
        onClose={this.props.onClose}
      >
        <DialogTitle id="form-dialog-title">Add an emergency contact</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter the details of your emergency contact.
          </DialogContentText>
          <form autoComplete="off">
            <FormControl className={classes.formControl} error={errors.firstName || errors.lastName}>
              <Typography variant="caption">Name</Typography>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <TextField
                  className={classes.formInput}
                  required
                  id="required"
                  label="First"
                  error={errors.firstName}
                  value={this.state.contact.firstName}
                  onChange={(event) => this.handleFieldChange("firstName", event.target.value)}
                  fullWidth
                  margin="dense"
                />
                <TextField
                  className={classes.formInput}
                  required
                  id="required"
                  error={errors.lastName}
                  label="Last"
                  value={this.state.contact.lastName}
                  onChange={(event) => this.handleFieldChange("lastName", event.target.value)}
                  fullWidth
                  margin="dense"
                />
              </div>
            </FormControl>
            <FormControl className={classes.formControl} error={errors.phoneNumber || errors.email}>
              <Typography variant="caption">Contact</Typography>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <TextField
                  className={classes.formInput}
                  required
                  id="required"
                  label="Phone"
                  error={errors.phoneNumber}
                  value={this.state.contact.phoneNumber}
                  onChange={(event) => this.handleFieldChange("phoneNumber", event.target.value)}
                  fullWidth
                  margin="dense"
                />
                <TextField
                  className={classes.formInput}
                  required
                  id="required"
                  label="Email"
                  error={errors.email}
                  value={this.state.contact.email}
                  onChange={(event) => this.handleFieldChange("email", event.target.value)}
                  fullWidth
                  margin="dense"
                />
              </div>
            </FormControl>
            <FormControl className={classes.formControl} error={errors.position}>
              <Typography variant="caption">Relationship</Typography>
              <TextField
                className={classes.formInput}
                required
                id="required"
                error={errors.position}
                label="Relationship"
                value={this.state.contact.position}
                onChange={(event) => this.handleFieldChange("position", event.target.value)}
                fullWidth
                margin="dense"
              />
            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={this.props.onCancel}>
            Cancel
                    </Button>
          <Button color="primary" onClick={this.validateAndSave}>
            Update
                    </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(AddContactModal);
