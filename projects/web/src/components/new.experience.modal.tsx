import * as React from 'react';
import { connect } from 'react-redux';
import {
  Theme,
  withStyles,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  Select, FormControl,
  InputLabel,
  MenuItem,
  Typography,
  Chip,
  Slide,
  TextField,
  Switch,
  FormControlLabel,
  List,
  ListItem,
  ListItemSecondaryAction,
  Avatar,
  AppBar,
  Toolbar,
  IconButton,
  FormHelperText,
  ListItemText,
} from '@material-ui/core';

import SuburbSearch from './SuburbSearch';
import DatePicker from './DatePicker';
import { grey, red } from '@material-ui/core/colors';
import { newAlert } from '@actions/global';
import { AlertLevel } from './global.model';
import { Dispatch } from 'redux';
import { Cancel, Close } from '@material-ui/icons';
import Layout from './Layout';

interface IProps {
  dispatch: Dispatch;
  data: any;
  classes?: any;
  onChange: any;
  onCancel: any;
  onSave: any;
  onClose: any;
  open: boolean;

}
interface IState {
  errors: any;
  experienceType: any;
  title: string;
  summary: string;
  company: string;
  location: any;
  from: any;
  to?: any;
  current: boolean;
  tasks: string[];
  skills: any[];

}

const styles = (theme: Theme) => ({
  root: {
    width: '960px',
    [theme.breakpoints.down('md')]: {
      width: '80%'
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      margin: theme.spacing.unit
    },
  },
  formControl: {
    margin: '1.0rem 0',
  },
  periodSection: {
    display: 'flex',
    flexDirection: 'row',
    flexBasis: '100%',
    marginTop: theme.spacing.unit * 2,
  },
  selectedSkillsContainer: {
    width: '100%',
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    borderBottom: `1px solid ${grey[800]}`
  },
  skillsContainer: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class NewExperienceModal extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      experienceType: { id: -1, name: '', },
      title: '',
      summary: '',
      company: '',
      location: {},
      from: null,
      to: null,
      current: false,
      tasks: [''],
      skills: [],
    };
  }

  public handleTypeChange = (event: any): void => {
    const types: any[] = this.props.data ? Object.entries(this.props.data.opportunity.type).map(([id, value]) => ({
      id: parseInt(id, 10),
      name: value.name,
    })) : [];

    const type: any = types[event.target.value - 1];

    this.setState({
      ...this.state,
      experienceType: type,
      errors: { ...this.state.errors, type: false, }
    });
  }

  public handleTitleChange = (event: any): void => {
    this.setState({
      ...this.state,
      title: event.target.value,
      errors: { ...this.state.errors, title: false, }
    });
  }

  public handleSummaryChange = (event: any): void => {
    if (event.target.value.length > 200) return;
    this.setState({
      ...this.state,
      summary: event.target.value,
      errors: { ...this.state.errors, summary: false, }
    });
  }

  public handleCompanyChange = (event: any): void => {
    this.setState({
      ...this.state,
      company: event.target.value,
      errors: { ...this.state.errors, company: false, }
    });
  }

  public handleLocationChange = (value: any): void => {
    this.setState({
      ...this.state,
      location: value,
      errors: { ...this.state.errors, location: false, }
    });
  }

  public handleFromChange = (value: any): void => {
    this.setState({
      ...this.state,
      from: value,
      errors: { ...this.state.errors, from: false, }
    });
  }

  public handleToChange = (value: any): void => {
    this.setState({
      ...this.state,
      to: value,
      errors: { ...this.state.errors, to: false, }
    });
  }

  public handleCurrentChange = (event: any): void => {
    this.setState({
      ...this.state,
      current: !this.state.current,
      errors: { ...this.state.errors, to: false, }
    });
  }

  public handleTaskChange = (event: any): void => {
    const newTasks: string[] = [...this.state.tasks];
    newTasks[parseInt(event.target.id, 10)] = event.target.value;
    this.setState({
      ...this.state,
      tasks: newTasks,
      errors: { ...this.state.errors, tasks: false, }
    });
  }

  public addTaskInput = (): void => {
    const newTasks: string[] = [...this.state.tasks];
    newTasks.push('');
    this.setState({
      ...this.state,
      tasks: newTasks,
      errors: { ...this.state.errors, tasks: false, }
    });
  }

  public removeTask = (index: number): void => {
    const newTasks = [...this.state.tasks];
    newTasks.splice(index, 1);
    this.setState({
      ...this.state,
      tasks: newTasks,
    });
  }

  public addSkill = (skill: any): void => {
    let dontAdd: boolean = false;
    this.state.skills.forEach((stateSkill: any): void => {
      // NOTE this is some silly thing i had to put in because includes
      // and indexOf wouldn't work. Why? Don't know, dont care.
      if (stateSkill.id === skill.id && stateSkill.name === stateSkill.name) {
        dontAdd = true;
      }
    });
    if (dontAdd) {
      this.props.dispatch(newAlert({
        level: AlertLevel.WARNING,
        body: "You've already added this skill.",
      }));
      return;
    }
    this.setState({
      ...this.state,
      skills: this.state.skills.concat(skill),
      errors: { ...this.state.errors, skills: false, }
    })
  }

  public removeSkill = (skill: any): void => {
    const indexOf: number = this.state.skills.indexOf(skill);
    const newSkills: any[] = [...this.state.skills];
    newSkills.splice(indexOf, 1);
    this.setState({
      ...this.state,
      skills: newSkills,
    })
  }

  public checkPropsAndSave = () => {
    const newExperience: any = { ...this.state };
    const dispatch: Dispatch = this.props.dispatch;
    const errors: any = {};

    if (!newExperience.experienceType || newExperience.experienceType.id === -1) errors.type = true;
    if (newExperience.title === '' || !newExperience.title) errors.title = true;
    if (newExperience.summary === '' || !newExperience.summary) errors.summary = true;
    if (newExperience.company === '' || !newExperience.company) errors.company = true;
    if (Object.keys(newExperience.location).length === 0 || !newExperience.location) errors.location = true;
    if (!newExperience.from) errors.from = true;
    if (!newExperience.current && !newExperience.to) errors.to = true;
    if (newExperience.from && newExperience.to && newExperience.to.diff(newExperience.from) < 0 && !newExperience.current) {
      errors.from = true;
      errors.to = true;
    }
    let tasksError: boolean = true;
    newExperience.tasks.forEach((task: string): void => {
      if (task !== '') {
        tasksError = false;
      }
    });
    if (tasksError) errors.tasks = true;
    if (newExperience.skills.length === 0) errors.skills = true;
    if (Object.keys(errors).length) {
      this.setState({
        ...this.state,
        errors,
      }, () => {
        dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: "Something may be missing from your experience."
        }));
      });
    } else {
      const experience: any = { ...this.state };
      delete experience.errors;
      this.props.onSave(experience);
    }
  };

  public render() {
    const { classes, data } = this.props;
    const errors: any = { ...this.state.errors };
    const skills: any[] = data ? Object.entries(data.opportunity.skills).map(([id, value]) => ({
      id: parseInt(id, 10),
      name: value.name,
    })) : [];

    const types: any[] = data ? Object.entries(data.opportunity.type).map(([id, value]) => ({
      id: parseInt(id, 10),
      name: value.name,
    })) : [];

    return (
      <Dialog
        open={this.props.open}
        fullScreen
        aria-labelledby="form-dialog-title"
        TransitionComponent={Transition}
        onClose={this.props.onClose}
      >
        <AppBar className={classes.appBar} color="primary">
          <Toolbar style={{ display: 'flex' }}>
            <IconButton color="inherit" onClick={this.props.onCancel} aria-label="Close">
              <Cancel />
            </IconButton>
            <Typography variant="title" color="inherit" style={{ flex: 1 }}>
              Prior experience
                    </Typography>
            <Button color="inherit" onClick={this.checkPropsAndSave}>
              save
                    </Button>
          </Toolbar>
        </AppBar>
        <Layout greyOut>
          <DialogContent>
            <Typography
              variant="headline"
              style={{ paddingTop: "1.0rem" }}
            >
              Add new work experience
                    </Typography>
            <DialogContentText>
              Below, tell us a bit about the work experience you've had in the past.
                    </DialogContentText>
            <form autoComplete="off" style={{ marginTop: '1.0rem' }}>

              <FormControl className={classes.formControl} style={{ width: "100%" }} error={errors.type}>
                <InputLabel htmlFor="age-simple" required>Type</InputLabel>
                <Select
                  fullWidth
                  required
                  value={this.state.experienceType.id}
                  onChange={this.handleTypeChange}
                  inputProps={{
                    name: 'type',
                    id: 'age-type',
                  }}
                >
                  <MenuItem value={-1}>
                    <em>Select type</em>
                  </MenuItem>
                  {types.map((type: any): React.ReactNode => (
                    <MenuItem key={type.id} value={type.id}>
                      {type.name}
                    </MenuItem>
                  ))}
                </Select>
                <FormHelperText>Select from the dropdown what best describes the type of work experience you had</FormHelperText>
                {errors.type && (
                  <FormHelperText>Please select a valid type</FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.formControl} style={{ width: "100%" }} error={errors.title}>
                <TextField
                  required
                  id="required"
                  label="Title"
                  error={errors.title}
                  helperText="What was this experience?"
                  value={this.state.title}
                  onChange={this.handleTitleChange}
                  fullWidth
                  margin="dense"
                />
                {errors.title && (
                  <FormHelperText>Please enter a valid title</FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.formControl} style={{ width: "100%" }} error={errors.summary}>
                <TextField
                  required
                  id="required"
                  error={errors.summary}
                  label="Short summary"
                  helperText={200 - this.state.summary.length + " characters left"}
                  multiline
                  value={this.state.summary}
                  onChange={this.handleSummaryChange}
                  fullWidth
                  margin="dense"
                />
                {errors.summary && (
                  <FormHelperText>Please select a valid summary</FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.formControl} style={{ width: "100%" }} error={errors.company}>
                <TextField
                  required
                  id="required"
                  label="Company"
                  error={errors.company}
                  helperText="Who was this for?"
                  value={this.state.company}
                  onChange={this.handleCompanyChange}
                  fullWidth
                  margin="dense"
                />
                {errors.company && (
                  <FormHelperText>Please select a company</FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.formControl} style={{ width: "100%" }} error={errors.location}>
                <SuburbSearch
                  id="suburb"
                  type={'text'}
                  error={errors.location}
                  required
                  value={this.state.location}
                  onChange={this.handleLocationChange}
                  showIcon={true}
                />
                {errors.location && (
                  <FormHelperText>Please select a valid location</FormHelperText>
                )}
              </FormControl>
              <div className={classes.periodSection}>
                <FormControl className={classes.formControl} style={{ flex: 1, marginRight: '1.0rem' }} error={errors.to || errors.from}>
                  <DatePicker
                    id="date-from"
                    type="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    label="What date did you start?"
                    maxDate={new Date()}
                    value={this.state.from}
                    onChange={this.handleFromChange}
                    error={errors.from}
                    style={{ width: '100%' }}
                    format="YYYY-MM-DD"
                  />
                  <DatePicker
                    id="date-to"
                    type="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    label="What date did you stop working here?"
                    disabled={this.state.current}
                    maxDate={new Date()}
                    error={errors.to}
                    value={this.state.to}
                    onChange={this.handleToChange}
                    style={{ width: '100%', marginTop: '1.0rem' }}
                    format="YYYY-MM-DD"

                  />
                  {(errors.from || errors.to) && (
                    <FormHelperText>Please select a valid date range. The starting date cannot be greater than the ending date</FormHelperText>
                  )}
                </FormControl>
                <FormControl className={classes.formControl}>
                  <Typography variant="caption">Or</Typography>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.current}
                        onChange={this.handleCurrentChange}
                        value="checkedB"
                        color="primary"
                      />
                    }
                    label="I am currently working here"
                  />
                </FormControl>
              </div>
              <FormControl className={classes.formControl} style={{ marginTop: '1.0rem', width: '100%' }} error={errors.tasks}>
                <Typography variant="subheading">Tasks</Typography>
                <Typography variant="caption">Select up to five tasks you completed on a regular basis</Typography>
                <List>
                  {this.state.tasks.map((value: string, index: number): React.ReactNode => (
                    <React.Fragment key={index}>
                      <ListItem>
                        <ListItemText>
                          <TextField
                            placeholder={"Task " + (index + 1)}
                            value={value}
                            fullWidth
                            id={index.toString()}
                            error={errors.tasks}
                            required={index === 0}
                            onChange={this.handleTaskChange}
                          />
                        </ListItemText>
                        <ListItemSecondaryAction>
                          <Button
                            disabled={this.state.tasks.length === 1}
                            onClick={() => this.removeTask(index)}
                          >
                            <Close />
                          </Button>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </React.Fragment>
                  ))}
                </List>
                <Button
                  variant="outlined"
                  disabled={this.state.tasks.length === 5}
                  onClick={this.addTaskInput}
                >
                  Add a task
                            </Button>
                {errors.tasks && (
                  <FormHelperText>Please select at least one task</FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.formControl} style={{ marginTop: '1.0rem', width: '100%' }} error={errors.skills}>
                <Typography variant="subheading">Skills</Typography>
                <div className={classes.selectedSkillsContainer} style={errors.skills ? { borderColor: red[500] } : {}}>
                  {this.state.skills.map((skill: any, index: number): React.ReactNode => (
                    <Chip
                      label={skill.name}
                      key={skill.id}
                      color="primary"
                      avatar={
                        <Avatar>{index + 1}</Avatar>
                      }
                      onDelete={() => this.removeSkill(skill)}
                      style={{ marginRight: '0.5rem', marginBottom: '0.5rem' }}
                    />
                  ))}
                </div>
                <div style={{ display: 'flex', width: '100%' }}>
                  <Typography
                    style={{ flex: 1 }}
                    variant="caption"
                  >
                    Select up to 12 skills you were exposed to
                                </Typography>
                  <Typography variant="caption">
                    {this.state.skills.length !== 12 ?
                      `add ${12 - this.state.skills.length} more` :
                      "You're all done!"
                    }
                  </Typography>
                </div>
                <div className={classes.skillsContainer}>
                  {skills.map((skill: any, index: number): React.ReactNode => (
                    <Chip
                      label={skill.name}
                      key={skill.id}
                      onClick={(
                        this.state.skills.length === 12) ?
                        (() => { }) :
                        (() => this.addSkill(skill)
                        )}
                      style={{
                        marginRight: '0.5rem',
                        marginBottom: '0.5rem',
                        opacity: (this.state.skills.length === 12) ? 0.5 : 1.0,
                      }}
                    />
                  ))}
                </div>
                {errors.skills && (
                  <FormHelperText>Please select at least one skill</FormHelperText>
                )}
              </FormControl>

            </form>

          </DialogContent>
        </Layout>
      </Dialog>
    )
  }

}

const mapStateToProps = (state: any): any => ({
  data: state.data,
});

export default withStyles(styles)(connect(mapStateToProps)(NewExperienceModal));
