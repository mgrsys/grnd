import {
  CircularProgress,
  InputAdornment,
  MenuItem,
  Paper,
  TextField,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import { StoreMallDirectory } from '@material-ui/icons';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import { castArray, debounce, get, slice } from 'lodash';
import * as React from 'react';
import Autosuggest from 'react-autosuggest';
import styled from 'styled-components';

// Utils
import { schoolSearch } from '@utils/api';

const IconWrapper = styled.div`
  width: 48px;
  height: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${grey[500]};
`;

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const suggestionText = `${suggestion.name}`;
  const matches = match(suggestionText, query);
  const parts = parse(suggestionText, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={String(index)} style={{ fontWeight: 500 }}>
              {part.text}
            </span>
          ) : (
              <strong key={String(index)} style={{ fontWeight: 300 }}>
                {part.text}
              </strong>
            );
        })}
      </div>
    </MenuItem>
  );
}

function renderSuggestionsContainer(options) {
  const { containerProps, children } = options;

  return (
    <Paper {...containerProps} style={{ maxHeight: 400, overflow: 'auto' }} square>
      {children}
    </Paper>
  );
}

function getSuggestionValue(suggestion) {
  return `${suggestion.name}`;
}

const styles: any = theme => ({
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  suggestion: {
    display: 'block',
  },
  suggestionsContainerOpen: {
    left: 0,
    marginTop: theme.spacing.unit,
    position: 'absolute',
    right: 0,
    zIndex: 1,
  },
  suggestionsList: {
    listStyleType: 'none',
    margin: 0,
    padding: 0,
  },
});

interface IProps {
  onChange: (e: any) => any;
  classes: object;
  label: string;
}

class SchoolSearchComponent extends React.Component<IProps | any, any> {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      suggestions: [],
      value: '',
    };
  }

  public renderInput = (inputProps) => {
    const { ref, ...other } = inputProps;
    const classes = inputProps.classes || {};
    const label = inputProps.label || 'School';
    const { loading } = this.state;
    return (
      <TextField
        fullWidth
        label={label}
        InputProps={{
          classes: {
            input: classes.input,
          },
          endAdornment: (
            <InputAdornment position="end">
              <IconWrapper>
                {loading ? <CircularProgress size={24} /> : <StoreMallDirectory />}
              </IconWrapper>
            </InputAdornment>
          ),
          inputRef: ref,
          ...other,
        }}
      />
    );
  }

  public handleSuggestionsFetchRequested = debounce(({ value }) => {
    const inputValue = value.trim().toLowerCase();
    if (inputValue.length > 2) {
      this.setState({
        loading: true,
      });
      schoolSearch(inputValue)
        .then((response: any) => {
          return castArray(get(response, 'data', []));
        })
        .then((suggestions) => {
          return this.setState({
            loading: false,
            suggestions: slice(suggestions, 0, 20),
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          if (get(error, 'code') === 404) {
            this.setState({
              suggestions: [],
            });
          } else {
            return error;
          }
        });
    }
  }, 500, { leading: false, trailing: true });

  public handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  }

  public handleChange = (event, { newValue }) => {
    const { onChange } = this.props;
    this.setState({
      value: newValue,
    });
    onChange({
      id: null,
      name: newValue,
    });
  }

  public handleSelect = (event, { suggestion, suggestionValue }) => {
    const { onChange } = this.props;
    const { id } = suggestion;
    onChange({
      id,
      name: suggestionValue,
    });
  }

  public render() {
    const { classes, label } = this.props;
    return (
      <Autosuggest
        theme={{
          container: classes.container,
          suggestion: classes.suggestion,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
        }}
        renderInputComponent={this.renderInput}
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
        renderSuggestionsContainer={renderSuggestionsContainer}
        onSuggestionSelected={this.handleSelect}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={{
          // classes,
          label,
          onChange: this.handleChange,
          value: this.state.value,
        }}
        highlightFirstSuggestion
      />
    );
  }
}

export default withStyles(styles)(SchoolSearchComponent);
