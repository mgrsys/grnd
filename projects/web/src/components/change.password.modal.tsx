import { DialogTitle, Dialog, DialogContentText, DialogContent, TextField, DialogActions, Button, FormControl, FormHelperText } from '@material-ui/core';

import * as React from 'react';
import { validPassword } from '@utils/validation';

interface IProps {
  onClose: () => void;
  onSave: (any) => void;
  open: boolean;
}

interface IState {
  oldPassword: string;
  newPassword: string;
  repeatPassword: string;
  errors: any;
}

class ChangePasswordModal extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      repeatPassword: '',
      errors: {},
    };
  }

  public onSave = (): void => {
    const { oldPassword, newPassword, repeatPassword, } = this.state;
    const errors: any = {};
    if (!oldPassword) {
      errors.oldPassword = "Please enter your old password";
    }
    if (!newPassword) {
      errors.newPassword = "Please enter a new password";
    }
    if (newPassword !== repeatPassword) {
      errors.repeatPassword = "New passwords don't match";
    }
    if (oldPassword === newPassword) {
      errors.newPassword = "You can't choose the same password";
    }
    if (newPassword && validPassword(newPassword)) {
      errors.newPassword = "Must be a valid password, with at least 8 characters, 1 upper.";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({
        ...this.state,
        errors,
      });
    } else {
      this.props.onSave(this.state);
    }
  }

  public updateField = (field: string, newString: string): void => {
    this.setState({
      ...this.state,
      errors: {
        ...this.state.errors,
        [field]: false,
      },
      [field]: newString,
    });
  }

  public render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Change password</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter your old, and new password below.
                    </DialogContentText>
          <FormControl style={{ display: 'flex', flexDirection: 'column' }} error={Object.keys(this.state.errors).length > 0}>
            <TextField
              error={!!this.state.errors.oldPassword}
              value={this.state.oldPassword} onChange={
                (event: any) => this.updateField("oldPassword", event.target.value)
              }
              style={{ flex: 1, marginBottom: '0.5rem' }} fullWidth type="password" label="Old password" />
            <TextField
              error={!!this.state.errors.newPassword}
              value={this.state.newPassword} onChange={
                (event: any) => this.updateField("newPassword", event.target.value)
              }
              style={{ flex: 1, marginBottom: '0.5rem' }} fullWidth type="password" label="New password" />
            <TextField

              error={!!this.state.errors.repeatPassword}
              value={this.state.repeatPassword} onChange={
                (event: any) => this.updateField("repeatPassword", event.target.value)
              }
              style={{ flex: 1, marginBottom: '0.5rem' }} fullWidth type="password" label="Repeat new password" />


            {this.state.errors.newPassword && (
              <FormHelperText>{this.state.errors.newPassword}</FormHelperText>
            )}
            {this.state.errors.repeatPassword && (
              <FormHelperText>Your new passwords don't match.</FormHelperText>
            )}

          </FormControl>

        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose} color="primary">
            Cancel
                    </Button>
          <Button onClick={this.onSave} color="primary">
            Change
                    </Button>
        </DialogActions>
      </Dialog>);
  }
}
export default ChangePasswordModal;
