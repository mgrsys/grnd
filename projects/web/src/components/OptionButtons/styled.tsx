import styled from 'styled-components';

const OptionButtons = styled.div`
  margin: 1rem 0 0 0;
`;

export default OptionButtons;
