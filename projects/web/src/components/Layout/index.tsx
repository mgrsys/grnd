import * as React from 'react';

import { grey } from '@material-ui/core/colors';
import { FilterToolbar } from '@components/FilterToolbar';

// Components
import Nav from '@components/Nav';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './styles';
import { Layout } from './styled';

interface IProps {
  background?: string;
  children?: React.ReactNode;
  greyOut?: boolean;
  nav?: React.ReactNode;
  noCreateOpportunity?: boolean;
  onlyActive?: boolean;
  page: string;
  accessToProfile?: boolean;
  classes: any;
}
interface IState {

}

class LayoutComponent extends React.Component<IProps, IState> {
  public render() {
    const { classes } = this.props;
    const { children, background, nav, noCreateOpportunity, page, accessToProfile } = this.props;

    return (
      <Layout background={background || grey[100]}>
        {nav || <Nav noCreateOpportunity={noCreateOpportunity} accessToProfile={accessToProfile} />}
        <div className={classes.root}>
          {(page === 'opportunityList') && <FilterToolbar onlyActive={this.props.onlyActive} />}
          <div className={classes.contentWrapper}>
            {children}
          </div>
        </div>
      </Layout>
    );
  }
}

export default withStyles(styles)(LayoutComponent);
