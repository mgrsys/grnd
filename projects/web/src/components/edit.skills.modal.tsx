import { Typography, DialogTitle, Dialog, DialogContentText, DialogContent, DialogActions, Button, Grid } from '@material-ui/core';

import * as React from 'react';
import OptionButtons from '@components/OptionButtons';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withTheme } from '@material-ui/core/styles';
import ItemList from '@components/ItemList';
import { Dispatch } from 'redux';
import { newAlert } from '@actions/global';
import { AlertLevel } from './global.model';

interface IProps {
  dispatch: (a: any) => any;
  onSave: () => any;
  onChange: (e: any) => any;
  onClose: () => any;

  open: boolean;
  data: any;
  pickedSkills: Array<any>;
}
interface IState {
  selectedSkillCategory: number;
}

const SkillsDialogueContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const RemoveSkillsHelperBar = styled.div`
  width: 100%;
  display: flex;
  padding: 0px 1rem;
  font-size: 0.8rem;
  font-weight: normal;
  box-sizing: border-box;
  padding-top: 0.5rem;
`;

const SkillCategoryButton: any = withTheme()(styled.div`
  margin: 0.4rem 1rem;
  padding: 0.7rem;
  border-bottom:
    ${(props: any) => props.selected ? `2px ${props.themeColor} solid` : `2px transparent solid`};
  transition: all 100ms linear;
  cursor: pointer;
  color: #666666;
  &:hover {
  color: #333333;
  border-bottom:
  ${(props: any) => props.selected ? `2px ${props.themeColor} solid` : `2px #cccccc solid`};
  }
` as any);

class EditSkillsModalComponent extends React.Component<IProps, IState> {
  public state = {
    selectedSkillCategory: 0,
  };

  public setSelectedSkillCategory = (value) => {
    this.setState({
      ...this.state,
      selectedSkillCategory: value,
    });
  }

  public updateSelected = (values: number[]): void => {
    const { data } = this.props;

    this.props.onChange(
      values.map(
        value => ({
          id: value,
          name: data.opportunity.skills[value].name,
        })
      )
    );
  }

  public removeSkill = (item): void => {
    const newSkills = [...this.props.pickedSkills];
    const index = newSkills.indexOf(item);
    newSkills.splice(index, 1);
    this.props.onChange(newSkills);
  }

  public onSave = (): void => {
    const pickedSkills: any[] = this.props.pickedSkills;
    const dispatch: Dispatch = this.props.dispatch;
    if (pickedSkills.length === 0) {
      dispatch(newAlert({
        level: AlertLevel.WARNING,
        body: "You must select at least one skill",
      }));
      return;
    }
    this.props.onSave();
  }

  public render() {
    const { data, pickedSkills } = this.props;
    const skillCategories: any = data.opportunity ? data.opportunity.skillCategories : {};
    const skillIds = pickedSkills ? pickedSkills.map(skill => skill.id) : [];
    const skillsDisabled = pickedSkills ? pickedSkills.length >= 12 : true;
    const pickedSkillsLength = pickedSkills ? pickedSkills.length : 0;
    const selectedSkillCategory = this.state.selectedSkillCategory;
    const skillOptions =
      Object.keys(skillCategories).length !== 0 &&
        selectedSkillCategory !== 0 ?
        skillCategories[selectedSkillCategory.toString()].skills :
        {};

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update skills</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter the new skills for the opportunity.
          </DialogContentText>

          <ItemList
            items={pickedSkills}
            label="name"
            onClick={this.removeSkill}
          />

          <RemoveSkillsHelperBar>
            <Typography style={{ flex: 1, alignSelf: 'flex-start' }}>
              To remove selected skills, click on them
            </Typography>
            <Typography>
              add {12 - pickedSkillsLength} more
            </Typography>
          </RemoveSkillsHelperBar>

          <Grid container style={{ margin: '2rem 0' }}>
            <Grid item xs={12} sm={4} style={{ borderRight: '1px #ccc solid' }}>
              {Object.keys(skillCategories).map(i => (
                <SkillCategoryButton
                  key={i}
                  selected={i === selectedSkillCategory}
                  onClick={() => this.setSelectedSkillCategory(i)}
                >
                  {skillCategories[i].name}
                </SkillCategoryButton>
              ))}
            </Grid>
            <Grid item xs={12} sm={8} style={{ padding: '0 1rem' }}>
              {(selectedSkillCategory !== 0) &&
                <OptionButtons
                  size="small"
                  color="primary"
                  multiple
                  disabled={skillsDisabled}
                  selected={skillIds}
                  onChange={this.updateSelected}
                  options={skillOptions}
                />
              }
              {
                (selectedSkillCategory === 0) &&
                <SkillsDialogueContainer>
                  <Typography variant="body1" style={{ maxWidth: '60%' }}>
                    Click on the key skill category to access list of skills.
                  </Typography>
                </SkillsDialogueContainer>
              }
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={this.onSave} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state: any) => {

  return {
    data: state.data || null,
  };
};

const EditSkillsModal = connect(
  mapStateToProps,
)(EditSkillsModalComponent);

export default EditSkillsModal;
