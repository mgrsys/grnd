import { CircularProgress } from '@material-ui/core';
import { Auth } from 'aws-amplify';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';

import { setUser as setUserMethod } from '@models/user';

const LoadingContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

interface IProps {
  children: any;
  setUser: (user: any) => any;
}

class UserLoaderComponent extends React.Component<IProps, any> {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      user: {},
      notAuthorised: false,
      ...props,
    };
    this.getUserData();
  }

  private getUserData() {
    const { setUser } = this.state;
    Auth.currentUserInfo()
      .then(user => {
        if (!user) {
          throw Error('Cannot get user info');
        }
        setUser(user);
        this.setState({
          loading: false,
          user,
        });
      })
      .catch(e => {
        console.warn('Unable to get currentUserInfo', e);
        this.setState({
          loading: false,
          user: {},
          notAuthorised: true,
        });
      });
  }

  private renderLoading = () => {
    return (
      <LoadingContainer>
        <CircularProgress size={64} />
      </LoadingContainer>
    );
  }

  public renderRedirection = () => {
    return <Redirect to="/signin" />;
  }

  public render() {
    const { children, loading } = this.state;
    return loading ? this.renderLoading() : (this.state.notAuthorised ? this.renderRedirection() : children);
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUser: user => {
      return dispatch(setUserMethod(user));
    },
  };
};

export const UserLoader = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserLoaderComponent);

export const withUser = (component: any, profile?: boolean): React.ComponentClass => {
  const Component = connect(
    mapStateToProps,
  )(component);
  // tslint:disable-next-line:max-classes-per-file
  return class extends React.Component<any, any> {
    public render() {
      return (
        <UserLoader>
          <Component />
        </UserLoader>
      );
    }
  };
};
