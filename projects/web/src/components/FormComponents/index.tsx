/* tslint:disable:max-classes-per-file */
import * as React from 'react';
import { get, set } from 'lodash';
// Components
import _FormErrors from '@components/FormErrors';
import {
  FormPage
} from './styled';

export const FormErrors = _FormErrors;

export interface IMultipageFormProps {
}

export interface IMultipageFormState {
}

export class MultipageFormPage<
  P extends IMultipageFormProps,
  S extends IMultipageFormState,
  > extends React.Component<P, S> {
  public validatePage() {
    return {};
  }
  public render() {
    return this.props.children;
  }
}

export interface IMultipageProps {
  children?: React.ReactNode;
}

export interface IMultipageState {
  currentPage: number;
  formData: any;
  formErrors: any;
  pageCount?: null | number;
  responseData?: any;
  submitting: boolean;
}

export class MultipageForm<
  P extends IMultipageProps,
  S extends IMultipageState,
  > extends React.Component<P, S> {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      formData: {},
      formErrors: {},
      pageCount: null,
      responseData: {},
      submitting: false,
      ...props,
    } as S;
  }

  public setFieldValue = (field) => (e) => {
    const value = get(e, 'target.value', e);
    const formData = {
      ...this.state.formData as {},
    };

    set(formData, field, value);
    this.setState({ formData });
  }

  public addItemIntoArray = (field) => (e) => {
    const value = get(e, 'target.value', '');
    const formData = {
      ...this.state.formData as {},
    };

    set(formData, field, [...formData[field], value]);
    this.setState({ formData });
  };

  public setArrayFieldValue = (field) => (e) => {
    let value = get(e, 'target.value', e);

    if (field === 'skillsToAdd') { // FIXME best practice from previous developer
      value = value.map(x => x.toLowerCase()
        .split(' ')
        .map(word => word.charAt(0).toUpperCase() + word.substring(1))
        .join(' ')
      );
    }

    const formData = {
      ...this.state.formData as {},
    };

    set(formData, field, [...value]);
    this.setState({ formData });
  }

  public backPage = () => {
    if (this.state.currentPage > 1) {
      this.setState({
        currentPage: this.state.currentPage - 1,
      });
    }
  }

  public nextPage = (stepsToSkip = 0) => {
    const { pageCount } = this.state;
    if (!this.validateForm()) {
      return false;
    }

    if (!pageCount || this.state.currentPage < pageCount) {
      this.setState({
        currentPage: this.state.currentPage + 1 + stepsToSkip,
      });
    }

    return;
  }

  public validateForm = () => true;

  public handleApiError = (e) => {
    const { error } = e; // TODO: make this error handling nicer
    const postError = error === 'User Exists' ? 'email' : 'postError';
    const errorMessage = error ? error : 'Error submitting form';
    const errorObj = typeof errorMessage === 'string'
      ? { [postError]: errorMessage }
      : errorMessage;
    const formErrors = {
      ...this.state.formErrors as {},
      ...errorObj,
    };
    this.setState({
      formErrors,
      submitting: false,
    });
  }

  public renderPages(pages, extras) {
    const pageComponents: any = [];
    const { currentPage, formData, formErrors, responseData } = this.state;
    let i = 0;

    for (const pageKey of Object.keys(pages)) {
      const Page = pages[pageKey];
      pageComponents.push((
        <FormPage show={currentPage === i * 1 + 1} key={i}>
          <Page
            {...extras}
            formData={formData}
            formErrors={formErrors}
            addItem={this.addItemIntoArray}
            onChange={this.setFieldValue}
            onChangeArray={this.setArrayFieldValue}
            responseData={responseData}
          />
        </FormPage>
      ));
      i = i + 1;
    }

    return pageComponents;
  }

  public render() {
    return this.props.children;
  }
}
