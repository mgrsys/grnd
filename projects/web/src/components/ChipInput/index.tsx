import * as React from 'react';
import {
  FormControl,
  withStyles,
  InputAdornment,
  Icon,
  Input
} from '@material-ui/core';

import InputWrapper from './input';

const styles = theme => ({
  formControl: {
    display: 'flex',
    marginTop: theme.spacing.unit * 4
  },
  icon: {
    color: theme.palette.grey[500],
    marginRight: theme.spacing.unit * 2,
    '&:hover': {
      cursor: 'pointer',
      color: theme.palette.primary[500]
    }
  }
});

interface IProps {
  classes: { icon: any, formControl: any };
  placeholder: string;
  name: string;
  onChange: (e: any) => any;
  values: Array<string>;
}

class TextInput extends React.Component<IProps, any> {
  state = {
    value: '',
  };

  _setValue = value => {
    this.setState({ value });
  };

  _handleChange = values => {
    const { onChange } = this.props;
    this.setState({ values });
    onChange(values);
  };

  _textFieldOnChangeValue = e => {
    const value = e.target.value;
    this.setState({ value });
  };

  _onClearChips = () => {
    this._handleChange([]);
  };

  _onChipDelete = index => e => {
    const { values } = this.props;
    e.preventDefault();
    const _values = [...values];
    _values.splice(index, 1);
    this._handleChange(_values);
  };

  render() {
    const { values, name, placeholder, classes } = this.props;
    const { value } = this.state;

    const adorment = values.length > 0 && (
      <InputAdornment position="end">
        <Icon onClick={this._onClearChips} className={classes.icon}>
          clear
        </Icon>
      </InputAdornment>
    );

    return (
      <FormControl
        aria-describedby={`${name}-text-input`}
        className={classes.formControl}
      >
        <Input
          fullWidth
          endAdornment={adorment}
          inputComponent={InputWrapper}
          inputProps={{
            value: value,
            placeholder: placeholder || '',
            values: values,
            onDelete: this._onChipDelete,
            onChange: this._textFieldOnChangeValue,
            onChipChange: this._handleChange,
            setValue: this._setValue
          }}
        />
      </FormControl>
    );
  }
}

export default withStyles(styles)(TextInput);
