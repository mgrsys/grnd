
import * as React from 'react';

import { Button } from '@material-ui/core';
import { grey, indigo } from '@material-ui/core/colors';

const styles = {
  button: {
    borderRadius: 0,
    color: grey[400],
    display: 'flex',
    fontWeight: 400,
    height: '64px',
    textAlign: 'center',
  },
  buttonActive: {
    borderBottom: '2px solid',
    borderRadius: 0,
    color: indigo.A700,
    display: 'flex',
    fontWeight: 400,
    height: '64px',
    textAlign: 'center',
  },
  hideOnMobile: {},
};

declare interface IProps {
  active?: boolean;
  icon?: JSX.Element;
  title?: string;
  component?: any;
  flexEnd?: any;
  style: any;
}

class NavButton extends React.Component<IProps, any> {
  public render() {
    const { active, icon, title, component, flexEnd } = this.props;
    const parentStyle = active ? styles.buttonActive : styles.button;
    let style = { ...parentStyle };

    if (flexEnd) {
      style = Object.assign(style, { justifyContent: 'flex-end' });
    }

    return (
      <Button
        type="link"
        component={component}
        style={style}
      >
        {icon} {title}
      </Button>
    );
  }
}

export default NavButton;
