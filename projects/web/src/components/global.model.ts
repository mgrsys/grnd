// tslint:disable:interface-name
export enum AlertLevel {
  SUCCESS = 'success',
  INFO = 'info',
  WARNING = 'warning',
  ERROR = 'error',
}

export interface AlertInfo {
  level: AlertLevel;
  body: string;
}

export interface Alert extends AlertInfo {
  timestamp: Date;
  action?: React.ReactNode;
}

export interface GlobalsState {
  alertHistory: Alert[];
  mostRecentAlert: Alert | null;
  loading: boolean;
}
