import * as React from 'react';

import { Auth } from 'aws-amplify';

interface IProps {
  checking?: React.ReactNode;
  noAuth: React.ReactNode;
  children: React.ReactNode;
}

class AuthRequired extends React.Component<IProps, any> {
  constructor(props) {
    super(props);
    this.state = {
      checking: '<p>Checking Authentication</p>',
      isAuthenticated: null,
      noAuth: '<p>Authentication Required</p>',
      ...props,
    };
  }

  public componentDidMount() {
    Auth.currentSession()
      .then(() => this.setState({ isAuthenticated: true }))
      .catch(() => this.setState({ isAuthenticated: false }));
  }

  public render() {
    const { checking, children, isAuthenticated, noAuth } = this.state;
    switch (isAuthenticated) {
      case true:
        return children;
      case false:
        return noAuth;
      default:
        return checking;
    }
  }
}

export default AuthRequired;
