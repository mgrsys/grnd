
// tslint:disable-next-line:interface-name
export enum OpportunityStatus {
  APPROVED = 'approved',
  PENDING = 'pending',
}
// tslint:disable-next-line:interface-name
export interface Organisation {
  id: number;
  name: string;
  email: string;
  description: string;
  locationId: number;
  picture: any;
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
}
// tslint:disable-next-line:interface-name
export interface Skill {
  id: number;
  name: string;
}
// tslint:disable-next-line:interface-name
export interface TimePeriod {
  id: number;
  name: string;
}
// tslint:disable-next-line:interface-name
export interface Location {
  id: number;
  address: string;
  country: string;
  postcode: number;
  state: string;
  suburb: string;
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
}
// tslint:disable-next-line:interface-name
export interface Contact {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  position: string;
  phoneNumber: string;
  picture: any;
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
}
// tslint:disable-next-line:interface-name
export interface OpportunityDuration {
  id: number;
  name: string;
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
}
// tslint:disable-next-line:interface-name
export interface OpportunityType {
  id: number;
  name: string;
  tooltip: string;
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
}
// tslint:disable-next-line:interface-name
export interface School {
  id: number;
  name: string;
}
// tslint:disable-next-line:interface-name
export interface Opportunity {
  id: number;
  title: string;
  description: string;
  status: OpportunityStatus;
  extraInformation: string;
  startDate: string;
  endDate: string;
  openDate: string;
  closeDate: string;
  ongoing: boolean;
  locationId: number;
  duration: string | number;
  workingHours: number;
  opportunityTypeId: number;
  opportunityDurationId: number;
  contactId: number;
  organisationId: number;
  tasks: string[];
  createdAt: string;
  deletedAt: string;
  updatedAt: string;
  organisation: Organisation;
  skills: Skill[];
  time_periods: TimePeriod[];
  location: Location;
  contact: Contact;
  opportunityTypes: OpportunityType[];
  opportunityDuration: OpportunityDuration;
  schools: School[];
  applications: any;
  prerequisites: any;
}
// tslint:disable-next-line:interface-name
export interface OpportunityResponse {
  status: number;
  data: Opportunity;
  error: any;
}
