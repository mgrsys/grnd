const initialState = null;

const SET_USER = 'set_user';

export function setUser(user) {
  return {
    type: SET_USER,
    user,
  };
}

function setUserHandler(state, { user }) {
  return user;
}

const actionHandlers = {
  [SET_USER]: setUserHandler,
};

const reducer = (state = initialState, action: any = {}) => {
  let newState = state;
  if (typeof actionHandlers[action.type] === 'function') {
    newState = actionHandlers[action.type](state, action);
  }
  return newState;
};

export default {
  initialState,
  reducer,
};
