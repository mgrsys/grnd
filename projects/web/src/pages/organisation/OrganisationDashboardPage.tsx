import { grey } from '@material-ui/core/colors';
import * as React from 'react';
import styled from 'styled-components';

// Component
import Layout from '@components/Layout';
import OpportunityListNew from '@pages/opportunity/components/OpportunityList';

// Utils
import { CircularProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Opportunity } from '@models/opportunity';
import { fetchOpportunities } from '@actions/opportunity';

const Dashboard = styled.section`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: ${grey[100]};

  @media (max-width: 600px) {
    background: #FFF;
  }
`;

interface IProps {
  dispatch: Dispatch,
  opportunities: Opportunity[],
}
interface IState {
  opportunities: any[];
  loading: boolean;
}

class DashboardComponent extends React.Component<IProps, IState> {
  public constructor(props) {
    super(props);
    this.state = {
      opportunities: [],
      loading: true,
    };
  }

  public componentDidMount() {
    this.props.dispatch(fetchOpportunities())
      .then((results: any) => {
        const opportunities = results.data || [];
        return this.setState({ opportunities, loading: false, });
      }).catch(() => { this.setState({ ...this.state, loading: false }) });
  }

  public render() {
    return (
      <Layout page="opportunityList">
        <Dashboard>
          {!this.props.opportunities ? <CircularProgress size={48} style={{ marginTop: '1.0rem' }} /> :
            <OpportunityListNew
              opportunities={this.props.opportunities}
              showDuration
              showAge
              showExploreButton
              showCreateOpportunity
            />
          }
        </Dashboard>
      </Layout>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  opportunities: state.opportunities.opportunityList,
});

export default connect(
  mapStateToProps,
)(DashboardComponent);
