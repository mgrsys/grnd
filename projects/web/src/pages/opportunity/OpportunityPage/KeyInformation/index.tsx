import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment-timezone';
import { get } from 'lodash';
import {
  Chip,
  Avatar,
  Typography,
  Button,
} from '@material-ui/core';
import {
  LocationOn,
  Schedule,
  AvTimer,
  Update,
  Today,
  Alarm,
} from '@material-ui/icons';

import { TimePeriod } from '@models/opportunity';
import EditLocationModal from '../../components/modals/edit.location.modal';
import EditTimeModal from '../../components/modals/edit.time.modal';
import EditDurationModal from '../../components/modals/edit.duration.modal';
import EditWorkingHoursModal from '../../components/modals/edit.workingHours.modal';
import EditStartDateModal from '../../components/modals/edit.startDate.modal';
import EditCloseDateModal from '../../components/modals/edit.closeDate.modal';

class KeyInformationComponent extends React.Component<any, any> {
  state = {
    editLocationModal: false,
    editTimeModal: false,
    editDurationModal: false,
    editSkillsModal: false,
    editWorkingHoursModal: false,
    editStartDateModal: false,
    editCloseDateModal: false,
  };

  public editLocationModalOpen = () => {
    this.setState({ editLocationModal: true });
  };

  public editLocationModalClose = (withUpdate) => () => {
    this.setState({ editLocationModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public editTimeModalOpen = () => {
    this.setState({ editTimeModal: true });
  };

  public editTimeModalClose = (withUpdate) => () => {
    this.setState({ editTimeModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public editDurationModalOpen = () => {
    this.setState({ editDurationModal: true });
  };

  public editDurationModalClose = (withUpdate) => () => {
    this.setState({ editDurationModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public editWorkingHoursModalOpen = () => {
    this.setState({ editWorkingHoursModal: true });
  };

  public editWorkingHoursModalClose = (withUpdate) => () => {
    this.setState({ editWorkingHoursModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public editStartDateModalOpen = () => {
    this.setState({ editStartDateModal: true });
  };

  public editStartDateModalClose = (withUpdate) => () => {
    this.setState({ editStartDateModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public editCloseDateModalOpen = () => {
    this.setState({ editCloseDateModal: true });
  };

  public editCloseDateModalClose = (withUpdate) => () => {
    this.setState({ editCloseDateModal: false }, () => {
      if (!withUpdate) {
        this.props.undo();
      }
    });
  };

  public updateEditOpportunity = (field, value) => {
    const { onChange } = this.props;
    onChange(field, value);
  };

  render() {
    const {
      classes,
      editMode,
      editOpportunity,
      opportunity,
      data,
    } = this.props;
    const {
      editLocationModal,
      editTimeModal,
      editDurationModal,
      editWorkingHoursModal,
      editStartDateModal,
      editCloseDateModal,
    } = this.state;

    const durations = get(data, 'opportunity.durations', {});

    return editMode ? (
      <div className={classes.keyInformation}>
        <EditLocationModal
          open={editLocationModal}
          onClose={this.editLocationModalClose}
          opportunity={editOpportunity}
          onChange={(value) => {
            if (value && Object.keys(value).length) {
              this.updateEditOpportunity('location', value)
            }
          }}
        />
        <EditTimeModal
          open={editTimeModal}
          onClose={this.editTimeModalClose}
          opportunity={editOpportunity}
          onChange={(values) => this.updateEditOpportunity('time_periods', values)}
        />
        <EditDurationModal
          open={editDurationModal}
          onClose={this.editDurationModalClose}
          opportunity={editOpportunity}
          onChange={(key, value) => this.updateEditOpportunity(key, value)}
        />
        <EditWorkingHoursModal
          open={editWorkingHoursModal}
          onClose={this.editWorkingHoursModalClose}
          opportunity={editOpportunity}
          onChange={(value) => this.updateEditOpportunity('workingHours', value)}
        />
        <EditStartDateModal
          open={editStartDateModal}
          onClose={this.editStartDateModalClose}
          opportunity={editOpportunity}
          onChange={(value) => this.updateEditOpportunity('startDate', value)}
        />
        <EditCloseDateModal
          open={editCloseDateModal}
          onClose={this.editCloseDateModalClose}
          opportunity={editOpportunity}
          onChange={(value) => this.updateEditOpportunity('closeDate', value)}
        />

        {
          editOpportunity.location ? (
            <Typography>
              <LocationOn className={classes.editIcon} />
              <Typography className={classes.editValue}>
                {editOpportunity.location.suburb || editOpportunity.location.location}
              </Typography>
              <Button
                className={classes.updateButton}
                onClick={this.editLocationModalOpen}
              >UPDATE</Button>
            </Typography>
          ) : null
        }

        {
          editOpportunity.time_periods.length ? (
            <Typography>
              <Schedule className={classes.editIcon} />
              <Typography
                className={classes.editValue}
                style={{
                  display: 'inline',
                }}
              >
                {editOpportunity.time_periods.map((period: TimePeriod): string => period.name).join(', ')}
              </Typography>
              <Button
                className={classes.updateButton}
                onClick={this.editTimeModalOpen}
              >UPDATE</Button>
            </Typography>
          ) : null
        }

        <Typography>
          <AvTimer className={classes.editIcon} />
          <Typography className={classes.editValue}>
            {`${editOpportunity.duration} ${get(durations[editOpportunity.opportunityDurationId], 'name', 'Day/s')}`}
          </Typography>
          <Button
            className={classes.updateButton}
            onClick={this.editDurationModalOpen}
          >UPDATE</Button>
        </Typography>

        <Typography>
          <Update className={classes.editIcon} />
          <Typography className={classes.editValue}>
            {`${editOpportunity.workingHours} Hours`}
          </Typography>
          <Button
            className={classes.updateButton}
            onClick={this.editWorkingHoursModalOpen}
          >UPDATE</Button>
        </Typography>

        <Typography>
          <Today className={classes.editIcon} />
          <Typography className={classes.editValue}>
            {moment(editOpportunity.startDate).format('Do MMMM YYYY')}
          </Typography>
          <Button
            className={classes.updateButton}
            onClick={this.editStartDateModalOpen}
          >UPDATE</Button>
        </Typography>

        <Typography
          component="p"
          className={classes.editValue}
        >
          <Alarm className={classes.editIcon} />

          <Typography
            component="span"
            style={{
              display: 'inline-block',
              verticalAlign: 'middle',
              marginRight: '0.1rem',
              maxWidth: '160px',
            }}
          >
            {`Application closes ${moment(editOpportunity.closeDate).fromNow()}`}
          </Typography>

          <Button
            className={classes.updateButton}
            style={{
              display: 'inline-block',
            }}
            onClick={this.editCloseDateModalOpen}
          >UPDATE</Button>
        </Typography>
      </div>
    ) : (
      <div className={classes.keyInformation}>
        {
          opportunity.location ? (
            <Chip
              avatar={
                <Avatar className={classes.chipAvatar}>
                  <LocationOn />
                </Avatar>
              }
              clickable={false}
              label={opportunity.location.suburb}
              className={classes.chip}
            />
          ) : null
        }

        {
          opportunity.time_periods.length ? (
            <Chip
              avatar={
                <Avatar className={classes.chipAvatar}>
                  <Schedule />
                </Avatar>
              }
              clickable={false}
              label={opportunity.time_periods.map((period: TimePeriod): string => period.name).join(', ')}
              className={classes.chip}
            />
          ) : null
        }

        {
          opportunity.opportunityDuration ? (
            <Chip
              avatar={
                <Avatar className={classes.chipAvatar}>
                  <AvTimer />
                </Avatar>
              }
              clickable={false}
              label={`${opportunity.duration} ${opportunity.opportunityDuration.name}`}
              className={classes.chip}
            />
          ) : null
        }

        <Chip
          avatar={
            <Avatar className={classes.chipAvatar}>
              <Update />
            </Avatar>
          }
          clickable={false}
          label={`${opportunity.workingHours} Hours`}
          className={classes.chip}
        />

        <Chip
          avatar={
            <Avatar className={classes.chipAvatar}>
              <Today />
            </Avatar>
          }
          clickable={false}
          label={moment(opportunity.startDate).format('Do MMMM YYYY')}
          className={classes.chip}
        />

        <Typography
          component="p"
          style={{
            marginTop: '8px'
          }}
        >
          <Alarm style={{ verticalAlign: 'middle' }} />

          <Typography
            component="span"
            style={{
              display: 'inline-block',
              verticalAlign: 'middle',
              marginLeft: '8px'
            }}
          >
            {`Application closes ${moment(opportunity.closeDate).fromNow()}`}
          </Typography>
        </Typography>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return { data: state.data || null };
};

const KeyInformation = connect(
  mapStateToProps,
)(KeyInformationComponent);

export default KeyInformation;
