import * as React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as moment from 'moment-timezone';
import * as _ from 'lodash';

import {
  Grid,
  Button,
  Paper,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import {
  MuiThemeProvider,
  withStyles,
} from '@material-ui/core/styles';
import { Opportunity } from '@models/opportunity';
import { DataLoader } from '@components/DataLoader';
import Layout from '@components/Layout';
import { getUserRole } from '@utils/user';
import {
  removeOpportunity,
  fetchOpportunities,
  setOpportunity,
  newApplication,
  sendUpdateOpportunity,
} from '@actions/opportunity';
import { newAlert } from '@actions/global';
import { AlertLevel } from '@components/global.model';
import { getProfileAction } from '@actions/profile';
import { theme, styles } from './styles';
import KeyInformation from './KeyInformation';
import ActionButtons from './ActionButtons';
import Organisation from './Organisation';
import Details from './Details';
import RootBar from './RootBar';
import OpportunityApplications from '../OpportunityApplications';
import DeleteModal from './DeleteModal';
import FieldEditable from '@components/field.editable';
import LocalAlert from './LocalAlert';
import { getOpportunityGradient } from './RootBar';

moment.tz('Australia/Brisbane');

interface IProps {
  classes: any;
  dispatch: Dispatch;
  loading: boolean;
  opportunity: Opportunity;
  editMode: boolean;
  view: number;
  user: any;
  history: any;
  data: any;
  profile: any;
  opportunities: any;
  match: any;
}

interface IState {
  alerted: boolean;
  editOpportunity: Opportunity | null;
  previouslySavedOpportunity: Opportunity | null;
  editMode: boolean;
  deleteModalShown: boolean;
  exploreApplications: boolean;
}

const getBackground = (opportunityTypes) => {
  const segmentPercentage = 100.0 / opportunityTypes.length;
  let i = 0;

  let finalGradient = 'linear-gradient(to right, ';
  for (const opportunityType of opportunityTypes) {
    const stops = getOpportunityGradient(opportunityType.id);
    const stopPercentage = segmentPercentage / (stops.length - 1);
    let j = 0;

    for (const stop of stops) {
      finalGradient += `${stop} ${i * segmentPercentage + stopPercentage * j}%, `;
      j += 1;
    }

    i += 1;
  }

  // remove last ', ' characters
  finalGradient = finalGradient.slice(0, finalGradient.length - 2);
  return finalGradient + ')';
};

class OpportunityPageComponent extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      alerted: false,
      editOpportunity: null,
      previouslySavedOpportunity: null,
      editMode: false,
      deleteModalShown: false,
      exploreApplications: false,
    };
  }

  public async componentWillReceiveProps(props: IProps): Promise<void> {
    if (props.opportunities) {
      const opportunity: Opportunity = props.opportunities[props.match.params.id];
      props.dispatch(setOpportunity(opportunity))
    }
  }

  public componentDidMount(): void {
    const { dispatch } = this.props;
    if (getUserRole(this.props.user) === "student" && !this.props.profile) {
      dispatch(getProfileAction());
    }

    if (!this.props.opportunities) {
      this.props.dispatch(fetchOpportunities());
    } else {
      try {
        const opportunity: Opportunity = this.props.opportunities[this.props.match.params.id];
        this.props.dispatch(setOpportunity(opportunity));
      } catch (e) {
        this.props.dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: "Uh oh! Couldn't find that opportunity.",
        }));
      }
    }
  }

  public canViewApplications = (role: string): boolean => {
    return role === 'organisation' || role === 'school';
  }

  public goBack = () => {
    const { exploreApplications } = this.state;

    if (exploreApplications) {
      this.setState({ exploreApplications: false });
    } else {
      this.props.history.push('/');
    }
  }

  public reviewApplications = () => {
    this.setState({ exploreApplications: true });
  };

  public submitInterest = () => {
    const { dispatch, opportunity, profile }: { dispatch: Dispatch, opportunity: Opportunity, profile: any } = this.props;
    const profileComplete: boolean = profile ? profile.completeness.overall : false;

    if (profileComplete) {
      dispatch(newApplication(opportunity.id));
      return;
    }

    dispatch(
      newAlert({
        level: AlertLevel.ERROR,
        body: "Your profile isn't quite complete.",
      }, (
        <span>
          <Link to="/profile">
            <Button color="secondary">Complete now</Button>
          </Link>
        </span>
      ))
    );
  }

  public canSubmitInterest = () => {
    const {
      user,
      opportunity
    } = this.props;

    if (opportunity && getUserRole(user) === 'student') {
      const { applications } = opportunity;

      return (
        applications.length === 0 ||
        applications.filter(
          application =>
            application.student.email === user.username
        ).length === 0
      );
    }

    return false;
  };

  public canDelete = () => {
    const {
      user,
      opportunity,
    } = this.props;

    return opportunity && ['school', 'organisation'].includes(getUserRole(user));
  };

  public canEdit = () => {
    const {
      user,
      opportunity,
    } = this.props;

    return opportunity && ['school', 'organisation'].includes(getUserRole(user));
  };

  public showDeleteModal = () => {
    this.setState({ deleteModalShown: true });
  }

  public closeDeleteModal = () => {
    this.setState({ deleteModalShown: false });
  }

  /**
   * Deletes opportunity.
   */
  public deleteOpportunity = async () => {
    const {
      opportunity,
      dispatch,
    }: { opportunity: Opportunity, dispatch: Dispatch } = this.props;

    try {
      await dispatch(removeOpportunity(opportunity.id));
      this.props.history.push('/');
    } catch (e) {}
  };

  /**
   * Toggles "on" edit mode with deep copy of opportunity object to have ability to revert
   * all changes.
   */
  public toggleEditMode = () => {
    const {
      opportunity,
    } = this.props;

    const editOpportunity = _.cloneDeep(opportunity);

    this.setState({
      editOpportunity,
      previouslySavedOpportunity: editOpportunity,
      editMode: true,
    });
  };

  /**
   * Shows yellow alert "non implemented yet"
   */
  public toggleAlert = () => {
    this.setState({ alerted: true });
  };

  /**
   * Hides yellow alert "non implemented yet"
   */
  public closeAlert = () => {
    this.setState({ alerted: false });
  };

  /**
   * Closes edit mode without any sync with server.
   */
  public cancelEditMode = () => {
    this.setState({
      editOpportunity: null,
      previouslySavedOpportunity: null,
      editMode: false,
    });
  };

  /**
   * Revert local changes through modal component in edit mode.
   */
  public undo = () => {
    this.setState({
      editOpportunity: this.state.previouslySavedOpportunity,
    });
  };

  /**
   * Saves changes when user clicks on "Save" button.
   */
  public saveChanges = () => {
    const {
      editOpportunity
    } = this.state;

    if (editOpportunity) {
      this.props.dispatch(
        sendUpdateOpportunity(
          editOpportunity,
        )
      ).then(() => {
        this.setState({
          editOpportunity: null,
          editMode: false,
        });
      });
    }
  };

  /**
   * The server expects to have skillIds and prerequisitesIds as array of numbers only,
   * skills and prerequisites array with names is just for displaying purposes.
   *
   * Tasks is stored in database as blob (array of string).
   */
  public handleChange = (field, value) => {
    const { editOpportunity } = this.state;
    let previouslySavedOpportunity = _.cloneDeep(editOpportunity);

    if (editOpportunity) {
      if (field === 'skillIds') {
        const {
          data,
        } = this.props;

        const skills = _.get(data, 'opportunity.skillCategories[1].skills', {});
        if (Object.keys(skills).length) {
          // save ids to successfully PUT
          editOpportunity[field] = value;

          field = 'skills';
          value = value.map((id) => ({ id, ...skills[id] }));
        }

        // changes should be in packet, not single
        previouslySavedOpportunity = _.cloneDeep(this.state.previouslySavedOpportunity);
      }

      if (field === 'prerequisitesIds') {
        const {
          data,
        } = this.props;

        const prerequisites = _.get(data, 'opportunity.prerequisites', {});
        if (Object.keys(prerequisites).length) {
          // save ids to successfully PUT
          editOpportunity[field] = value;

          field = 'prerequisites';
          value = value.map((id) => ({ id, ...prerequisites[id] }));
        }

        // changes should be in packet, not single
        previouslySavedOpportunity = _.cloneDeep(this.state.previouslySavedOpportunity);
      }

      const changed = _.set(editOpportunity, field, value);

      this.setState({
        editOpportunity: { ...changed },
        previouslySavedOpportunity,
      });
    }
  }

  render() {
    const {
      loading,
      opportunity,
      classes,
      user,
      history,
    } = this.props;
    const {
      alerted,
      editOpportunity,
      editMode,
      deleteModalShown,
      exploreApplications
    } = this.state;

    const opportunityTypes = opportunity === null ? null : opportunity.opportunityTypes;
    const profiles = opportunity === null ? [] : opportunity.profiles;
    const applications = opportunity === null ? [] : opportunity.applications;

    return (
      <MuiThemeProvider theme={theme}>
        <Layout
          page="exploreOpportunity"
          noCreateOpportunity={!this.canViewApplications(getUserRole(user))}
        >
          <LocalAlert
            open={alerted}
            onClose={this.closeAlert}
          />
          <DataLoader required>
            {
              (opportunity === null || loading === true) ? (
                <CircularProgress
                  size={48}
                  style={{
                    marginTop: '36px',
                    left: '45%',
                    position: 'relative',
                  }}
                />
              ) : (
                <div>
                  <DeleteModal
                    open={deleteModalShown}
                    onClose={this.closeDeleteModal}
                    onChange={this.deleteOpportunity}
                  />

                  <RootBar
                    history={history}
                    interactive={!exploreApplications}
                    opportunityTypes={opportunityTypes}
                    userRole={getUserRole(user)}
                    goBack={this.goBack}
                    reviewApplications={this.reviewApplications}
                    canSubmitInterest={this.canSubmitInterest()}
                    editMode={editMode}
                    cancelEditMode={this.cancelEditMode}
                    deleteOpportunity={this.showDeleteModal}
                    saveChanges={this.saveChanges}
                    submitInterest={this.submitInterest}
                  />
                  {
                    exploreApplications ? (
                      <OpportunityApplications
                        profiles={profiles}
                        applications={applications}
                      />
                    ) : (
                      <Grid container spacing={24}>
                        <Grid item md={5}>
                          <Paper className={classes.paper}>
                            {
                              editMode ? (
                                <FieldEditable
                                  edit
                                  value={editOpportunity && editOpportunity.title}
                                  size={0}
                                  onChange={({ target: { value }}) => this.handleChange('title', value)}
                                />
                              ) : (
                                <Typography variant="headline" component="h3">
                                  {opportunity.title}
                                </Typography>
                              )
                            }

                            {
                              editMode ? (
                                <FieldEditable
                                  edit
                                  value={editOpportunity && editOpportunity.description}
                                  size={2}
                                  onChange={({ target: { value }}) => this.handleChange('description', value)}
                                />
                              ) : (
                                <Typography
                                  variant="caption"
                                  component="p"
                                  style={{
                                    fontSize: '16px',
                                  }}
                                >
                                  {opportunity.description}
                                </Typography>
                              )
                            }

                            <KeyInformation
                              classes={classes}
                              editMode={editMode}
                              editOpportunity={editOpportunity}
                              opportunity={opportunity}
                              onChange={this.handleChange}
                              undo={this.undo}
                            />

                            {
                              editMode ? null : (
                                <ActionButtons
                                  classes={classes}
                                  canEdit={this.canEdit()}
                                  canDelete={this.canDelete()}
                                  onAlert={this.toggleAlert}
                                  onEdit={this.toggleEditMode}
                                  onDelete={this.showDeleteModal}
                                />
                              )
                            }

                            {
                              (opportunity.organisation && !editMode) ? (
                                <Organisation
                                  classes={classes}
                                  createdAt={opportunity.createdAt}
                                  organisation={opportunity.organisation}
                                />
                              ) : null
                            }

                            {
                              editMode ? null : (
                                <div
                                  className={classes.paperAppendix}
                                  style={{
                                    background: getBackground(opportunity.opportunityTypes),
                                  }}
                                ></div>
                              )
                            }
                          </Paper>
                        </Grid>
                        <Grid item md={7} className={classes.lastGridItem}>
                          <Paper className={classes.paper}>
                            <Details
                              classes={classes}
                              editMode={editMode}
                              editOpportunity={editOpportunity}
                              opportunity={opportunity}
                              onChange={this.handleChange}
                              undo={this.undo}
                            />
                          </Paper>
                        </Grid>
                      </Grid>
                    )
                  }
                </div>
              )
            }
          </DataLoader>
        </Layout>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    loading: state.globals.loading || null,
    opportunity: state.opportunities.opportunity || null,
    opportunities: state.opportunities.opportunityList,
    view: state.opportunities.exploreView || 0,
    user: state.user,
    data: state.data,
    profile: state.profile.data,
  };
};

const OpportunityPage = withRouter(
  connect(
    mapStateToProps,
  )(OpportunityPageComponent),
);

export default withStyles(styles)(OpportunityPage);
