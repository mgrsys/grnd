import * as React from 'react';
import {
  Typography,
  Grid,
  Divider,
  Avatar,
} from '@material-ui/core';
import * as moment from 'moment-timezone';

const Organisation = ({ classes, createdAt, organisation }) => (
  <div className={classes.organisationBlock}>
    <Divider className={classes.divider} />
    <Grid container spacing={24}>
      <Grid item xs={3}>
        <Avatar
          style={{
            border: '1px solid #FFD300',
            width: '58px',
            height: '58px',
          }}
        />
      </Grid>
      <Grid item xs={9}>
        <Typography
          variant="headline"
          component="h3"
          className={classes.organisationName}
        >
          {organisation.name}
        </Typography>
        <Typography
          variant="body1"
          component="p"
          className={classes.organisationDescription}
        >
          {organisation.description}
        </Typography>
        <Typography
          variant="caption"
          component="p"
          className={classes.opportunityPosted}
        >
          {`Posted ${moment(createdAt).fromNow()}`}
        </Typography>
      </Grid>
    </Grid>
  </div>
);

export default Organisation;
