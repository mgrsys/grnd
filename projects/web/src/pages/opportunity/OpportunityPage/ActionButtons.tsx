import * as React from 'react';
import {
  Button,
} from '@material-ui/core';
import {
  Share,
  Favorite,
  CloudDownload,
  Edit,
  Delete,
} from '@material-ui/icons';

const ActionButtons = ({ classes, canEdit, canDelete, onAlert, onEdit, onDelete }) => (
  <div className={classes.actionButtons}>
    <Button
      variant="fab"
      className={classes.actionButton}
      onClick={onAlert}
    >
      <Share />
    </Button>

    <Button
      variant="fab"
      className={classes.actionButton}
      onClick={onAlert}
    >
      <Favorite />
    </Button>

    <Button
      variant="fab"
      className={classes.actionButton}
      onClick={onAlert}
    >
      <CloudDownload />
    </Button>

    {
      canEdit ? (
        <Button
          variant="fab"
          className={classes.actionButton}
          onClick={onEdit}
        >
          <Edit />
        </Button>
      ) : null
    }

    {
      canDelete ? (
        <Button
          variant="fab"
          className={classes.actionButton}
          onClick={onDelete}
        >
          <Delete />
        </Button>
      ) : null
    }
  </div>
);

export default ActionButtons;
