
import { grey } from '@material-ui/core/colors';
import styled, { css } from 'styled-components';

export interface IThemeProps {
    experience: boolean;
    structured: boolean;
    shadow: boolean;
    paid: boolean;
    traineeship: boolean;
    apprenticeship: boolean;
}
export interface ISKillsProps {
  numItems: number;
}

export const ListItemContent = styled.div`
  transition: height 0.2s;
  display: flex;
  flex-direction: column;
  padding: 0 calc(40px + 1.0rem);
  marginTop: 1.0rem;
  overflow: hidden;
  color: ${grey[800]};
`;

export const ListItemInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  margin-right: 0.5rem;
  white-space: nowrap;
`;

export const SkillContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-self: center;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-top: 0.5rem;
    ${(props: ISKillsProps) => (props.numItems <= 2) && css`
      justify-content: flex-start;
      div {
        margin-right: 1.0rem;
      }
    `};
`;
