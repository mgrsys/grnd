// FIXME think about Nav, how to use it instead of one more implementation
import * as React from 'react';
import { connect } from 'react-redux';
import { Storage } from 'aws-amplify';

import { history } from '@utils/history';
import { authLogoutAction } from '@actions/global';
import { signOut } from '@utils/api';
import { getProfileAction } from '@actions/profile';
import { Avatar, Button } from '@material-ui/core';
import {
  AccountCircle,
  Add,
  Assignment,
  People,
  Chat,
  NotificationsNone,
  ExitToApp,
} from '@material-ui/icons';
import { getUserRole } from '@utils/user';
import { Link } from '@utils/link';
import logo from '@public/images/logo.svg';
class Drawer extends React.Component<any, any>{
  constructor(props) {
    super(props);

    this.state = {
      active: true,
      anchorEl: null,
      profilePictureUrl: undefined,
    };
  }

  componentDidMount() {
    // to show avatar if exist
    const {
      user,
      profile,
      dispatch,
    } = this.props;

    if (getUserRole(user) === 'student') {
      if (!profile) {
        dispatch(getProfileAction())
          .then(() => {
            setTimeout(() => {
              if (this.props.profile) {
                Storage.get(this.props.profile.profilePicture)
                  .then(result => {
                    this.setState({
                      profilePictureUrl: result.toString(),
                    });
                  });
              }
            }, 0);
          });
      } else {
        Storage.get(this.props.profile.profilePicture)
          .then(result => {
            this.setState({
              profilePictureUrl: result.toString(),
            });
          });
      }
    }
  }

  public signOut = () => {
    return signOut()
      .then(() => {
        history.push('/signin');
        this.props.dispatch(authLogoutAction());
      });
  };

  public goToProfile = (): void => {
    history.push('/profile');
  };

  render() {
    const {
      classes,
      profile = {},
      user,
    } = this.props;
    const {
      profilePictureUrl,
    } = this.state;

    const userRole = getUserRole(user);

    return (
      <React.Fragment>
        <div
          style={{
            height: '172px',
            background: '#FFD300',
          }}
        >
          <Avatar
            onClick={() => {
              if ('student' === userRole) {
                this.goToProfile();
              }
            }}
            style={{
              position: 'absolute',
              top: '45px',
              left: '24px',
              width: '64px',
              height: '64px',
            }}
            src={profilePictureUrl}
          >
            { profilePictureUrl ? null : (
              <AccountCircle
                style={{
                  fontSize: '76px',
                }}
              />
            ) }
          </Avatar>
          {
            (profile && profile.student) ? (
              <span
                style={{
                  position: 'absolute',
                  top: '125px',
                  left: '24px',
                  fontSize: '18px',
                  fontFamily: 'Raleway, sans-serif',
                  color: 'rgba(0, 0, 0, 0.6)',
                }}
              >{`${profile.student.firstName || ''} ${profile.student.lastName || ''}`}</span>
            ) : null
          }
        </div>
        <div>
          {
            ['organisation', 'school'].includes(userRole) ? (
              <React.Fragment>
                <Button
                  variant="flat"
                  fullWidth
                  className={classes.button}
                  component={Link('/opportunity/new')}
                >
                  <Add className={classes.drawerIcon} />
                  <span className={classes.drawerText}>CREATE</span>
                </Button>
                <Button
                  variant="flat"
                  fullWidth
                  className={classes.button}
                  component={Link('/')}
                >
                  <Assignment className={classes.drawerIcon} />
                  <span className={classes.drawerText}>LISTING</span>
                </Button>
              </React.Fragment>
            ) : null
          }

          {
            ['school'].includes(userRole) ? (
              <Button
                variant="flat"
                fullWidth
                className={classes.button}
                component={Link('/contacts')}
              >
                <People className={classes.drawerIcon} />
                <span className={classes.drawerText}>CONTACTS</span>
              </Button>
            ) : null
          }

          <Button
            variant="flat"
            fullWidth
            className={classes.button}
          >
            <Chat className={classes.drawerIcon} />
            <span className={classes.drawerText}>MESSAGE</span>
          </Button>

          <Button
            variant="flat"
            fullWidth
            className={classes.button}
          >
            <NotificationsNone className={classes.drawerIcon} />
            <span className={classes.drawerText}>NOTIFICATIONS</span>
          </Button>

          <Button
            variant="flat"
            fullWidth
            className={classes.button}
            onClick={this.signOut}
          >
            <ExitToApp className={classes.drawerIcon} />
            <span className={classes.drawerText}>SIGN OUT</span>
          </Button>
        </div>

        <img className={classes.logo} src={logo} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  profile: state.profile.data,
});

export default connect(mapStateToProps)(Drawer);
