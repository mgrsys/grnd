import * as React from 'react';
import { get } from 'lodash';
import {
  Grid,
  FormControl,
  FormHelperText,
  Select,
  MenuItem,
} from '@material-ui/core';

import {
  FormControlContainer,
  FormLabel,
} from '@components/FormComponents/styled';
import { DataLoader } from '@components/DataLoader';

interface IProps {
  formErrors: any;
  formData: any;
  onChange: (e: any) => any;
  data: any;
}

class WorkingSchedule extends React.Component<IProps, any> {
  render() {
    const {
      onChange,
      formErrors,
    } = this.props;

    const data = this.props.data || {};
    const formData = this.props.formData || {
      duration: '1',
      opportunityDurationId: 1,
      workingHours: 1,
    };

    const {
      workingHours,
    } = formData;

    const durations = get(data, 'opportunity.durations', {});
    const durationOptions = Object.keys(durations).map((id) => ({
      id: parseInt(id, 10), name: durations[id].name
    }));

    return (
      <Grid container spacing={24}>
        <Grid item xs={5}>
          <FormControlContainer>
            <FormControl fullWidth error={!!formErrors.opportunityDurationId}>
              <FormLabel>Duration</FormLabel>
              <FormHelperText>How long will they attend the opportunity</FormHelperText>
              <DataLoader required>
                <Grid container spacing={8}>
                  <Grid item xs={8}>
                    <Select
                      style={{marginTop: '0.5rem'}}
                      id="duration"
                      value={formData.duration}
                      onChange={onChange('duration')}
                      fullWidth
                    >
                      {
                        [...Array(31).keys()].map(day => (
                          <MenuItem key={day} value={String(day + 1)}>{day+1}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item xs={4}>
                    <Select
                      id="opportunityDurationId"
                      value={formData.opportunityDurationId}
                      style={{marginTop: '0.5rem'}}
                      onChange={onChange('opportunityDurationId')}
                      fullWidth
                    >
                      {
                        durationOptions.map(({ id, name }) => (
                          <MenuItem key={id} value={id}>{name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                </Grid>
            </DataLoader>
            </FormControl>
          </FormControlContainer>
        </Grid>
        <Grid item xs={6}>
          <FormControlContainer>
            <FormControl fullWidth error={!!formErrors.workingHours}>
              <FormLabel>Working hours</FormLabel>
              <FormHelperText>How many hours per week do you expect</FormHelperText>
              <DataLoader required>
                <Select
                  style={{marginTop: '0.5rem'}}
                  id="other-duration"
                  value={workingHours}
                  onChange={onChange('workingHours')}
                  fullWidth
                >
                  {
                    [...Array(40).keys()].map(hour => (
                      <MenuItem key={hour} value={hour + 1}>{hour+1}</MenuItem>
                    ))
                  }
                </Select>
              </DataLoader>
            </FormControl>
          </FormControlContainer>
        </Grid>
      </Grid>
    );
  }
}

export default WorkingSchedule;
