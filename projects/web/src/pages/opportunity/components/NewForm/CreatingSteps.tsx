import * as React from 'react';
import {
  Step,
  StepLabel,
  Stepper,
  StepContent,
  Grid,
  Paper,
  MobileStepper,
} from '@material-ui/core';
import withWidth from '@material-ui/core/withWidth';

interface IProps {
  currentPage: number;
  width: string;
}

const style = {
  boxShadow: '0 9px 18px rgba(0, 0, 0, 0.18)',
  paddingTop: '56px'
};

const STEPS = [
  { label: 'Key details', text: 'This section is for the most important things young people need to know about the opportunity' },
  { label: 'School', text: 'Select school/s you would like to list the opportunity with' },
  { label: 'Contact', text: 'Please add details of the person-in-change working directly with the applicants and school' },
  { label: 'Preview', text: 'Please review and confirm the opportunity listing, before it goes live...' },
];

const Content = ({ currentPage, width }) => ['lg'].includes(width) ? (
  <Grid
    container
    alignItems="stretch"
    style={{
      position: 'fixed',
      height: '100%',
      top: 0,
      left: 0,
      zIndex: 0,
      width: '256px'
    }}
  >
    <Paper
      square
      style={style}
    >
      <Stepper
        orientation="vertical"
        activeStep={currentPage - 1}
      >
        {
          STEPS.map(({ label, text }) => {
            return (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
                <StepContent
                  style={{
                    fontSize: '12px',
                    color: 'rgba(0, 0, 0, 0.54)'
                  }}
                >
                  {text}
                </StepContent>
              </Step>
            );
          })
        }
      </Stepper>
    </Paper>
  </Grid>
) : (
  <MobileStepper
    variant="dots"
    steps={STEPS.length}
    position="bottom"
    activeStep={currentPage - 1}
    nextButton={<span></span>}
    backButton={<span></span>}
  />
);

class CreatingSteps extends React.Component<IProps, any> {
  render() {
    const { currentPage, width } = this.props;

    return (
      <Content
        currentPage={currentPage}
        width={width}
      />
    );
  }
}

export default withWidth()(CreatingSteps);
