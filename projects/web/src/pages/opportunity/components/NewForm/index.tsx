import { get } from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import {
  Button,
  Grid,
} from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// Components
import {
  FormErrors,
  IMultipageProps,
  IMultipageState,
  MultipageForm,
} from '@components/FormComponents';
import {
  FormPageContainer,
  FormActions
} from '@components/FormComponents/styled';
import ProgressButton from '@components/ProgressButton';

// Utils
import {
  createOpportunity,
  getOrganisation,
  updateOpportunity,
  createSkill,
  schoolByUser,
} from '@utils/api';
import { history } from '@utils/history';
import { isEmptyArray } from '@utils/validation';

// Pages
import KeyDetails from './KeyDetails';
import School from './School';
import Contact from './Contact';
import Preview from './Preview';
import CreatingSteps from './CreatingSteps';
import { fetchOpportunities } from '@actions/opportunity';
import { getUserRole } from '@utils/user';

const pages = [KeyDetails, School, Contact, Preview];

const NewOpportunityForm = styled.div`
  padding-bottom: 3rem;
`;

const requiredFields = {
  1: {
    description: 'Description',
    location: 'Location',
    opportunityDurationId: 'Opportunity duration',
    opportunityTypeIds: 'Opportunity Type',
    prerequisitesIds: 'Prerequisites',
    timePeriodIds: 'When',
    title: 'Title',
    workingHours: 'Working Hours',
    duration: 'Opportunity duration',
  },
  2: {
    schoolIds: 'School',
  },
  3: {
    'contact.email': 'Contact Email',
    'contact.name': 'Contact Name',
    'contact.phoneNumber': 'Contact Phone Number',
    'contact.position': 'Contact Position',
  },
  4: {},
};

const errorMessages = {
  'contact.email': 'Email is required',
  'contact.name': 'Contact Name is required',
  'contact.phoneNumber': 'Contact Phone Number is required',
  'contact.position': 'Contact Position is required',
  description: 'Short description required',
  location: 'Location is required',
  opportunityDurationId: 'Duration is required',
  duration: 'Duration is required',
  opportunityTypeIds: 'At least one Opportunity Type is required',
  prerequisitesIds: 'Please submit another prerequisite',
  schoolIds: 'At least one school is required',
  skillIds: 'At least one skill is required',
  tasks: 'At least one task is required',
  timePeriodIds: 'Time period is required',
  title: 'Title is required',
  workingHours: 'Working Hours is required',
};

const theme = createMuiTheme({
  palette: {
    primary: { main: '#2626F7' },
    secondary: red,
  },
});

interface IProps extends IMultipageProps {
  opportunityId: string;
  dispatch: (e: any) => any;
  user: any;
}

interface IState extends IMultipageState {
  organisation: any;
  opportunityId: string | null;
  responseData: any;
  formErrors: any;
  submitting: boolean;
}

class NewOpportunityFormComponent extends MultipageForm<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      formData: {
        startDate: Date.now(),
        closeDate: Date.now(),
        contact: {
          email: '',
          name: '',
          phoneNumber: '',
          position: '',
        },
        contactId: null,
        description: '',
        extraInformation: '',
        location: {},
        ongoing: false,
        opportunityDurationId: 1,
        duration: '1',
        otherPrerequisite: '',
        opportunityTypeIds: [],
        prerequisitesIds: [],
        schoolIds: [],
        skillIds: [],
        tasks: [''],
        timePeriodIds: [],
        title: '',
        workingHours: 1,
        skillsToAdd: [],
      },
      formErrors: {},
      opportunityId: null,
      organisation: {},
      pageCount: pages.length,
      responseData: {},
      submitting: false,
      ...props,
    };
  }

  public componentDidMount() {
    const { opportunityId, user } = this.props;

    if (opportunityId && !this.state.opportunityId) {
      history.replace('/opportunity/new');
    }

    if (getUserRole(user) === 'organisation') {
      getOrganisation('me')
        .then(({ data: organisation }) => {
          return this.setState({ organisation });
        });
    } else if (getUserRole(user) === 'school') {
      schoolByUser()
        .then(({ data }) => {
          const { formData } = this.state;

          if (data.length) {
            return this.setState({
              organisation: {
                schools: data
              },
              formData: {
                ...formData,
                schoolIds: [data[0].id]
              }
            });
          }
        });
    }
  }

  public requiredFields = (fields) => {
    const { formData } = this.state;
    const required = {};

    for (const field of Object.keys(fields)) {
      const fieldData = get(formData, field);

      if (field === "location" && !fieldData.location) {
        required[field] = errorMessages[field];
      }

      if (field === "prerequisitesIds" && fieldData.includes(6) && formData.otherPrerequisite.length === 0) {
        required[field] = errorMessages[field];
      }

      if (!fieldData) {
        required[field] = errorMessages[field];
      } else if (fieldData.constructor === Array) {
        if (isEmptyArray(fieldData) && field !== "prerequisitesIds") {
          required[field] = errorMessages[field];
        }
      }
    }

    return required;
  }

  public validateForm = () => {
    const { currentPage } = this.state;
    const formErrors = this.requiredFields(requiredFields[currentPage]);

    this.setState({ formErrors });
    if (Object.keys(formErrors).length > 0) {
      return false;
    }

    return true;
  }

  public getFormActionsPage1 = () => {
    const { submitting } = this.state;
    return (
      <React.Fragment>
        <Button size="large" href="/">
          Back
        </Button>
        <ProgressButton
          size="large"
          variant="raised"
          color="primary"
          disabled={submitting}
          loading={submitting}
          onClick={this.createOpportunity}
        >
          Next
        </ProgressButton>
      </React.Fragment>
    );
  }

  public getFormActionsDefault = () => {
    const { currentPage } = this.state;
    return (
      <React.Fragment>
        <Button size="large" onClick={this.backPage} disabled={currentPage === 1}>
          Back
        </Button>
        <Button
          size="large"
          variant="raised"
          color="primary"
          onClick={this.nextPage}
        >
          Next
        </Button>
      </React.Fragment>
    );
  }

  public getFormActionsPage4 = () => {
    const { submitting } = this.state;
    return (
      <React.Fragment>
        <Button size="large" href="/">
          Cancel
        </Button>
        <ProgressButton
          size="large"
          variant="raised"
          color="primary"
          disabled={submitting}
          loading={submitting}
          onClick={this.updateOpportunity}
        >
          Next
        </ProgressButton>
      </React.Fragment>
    );
  }

  public getFormActionsThankyouPage = () => {
    const { dispatch } = this.props;
    const { opportunityId } = this.state;

    return (
      <React.Fragment>
        <div>&nbsp;</div>
        <Button
          size="large"
          variant="raised"
          color="primary"
          onClick={() => {
            dispatch(fetchOpportunities()).then(() => {
              history.replace(`/opportunity/${opportunityId}`)
            })
          }}
        >
          View Listing
        </Button>
      </React.Fragment>
    );
  }

  public getFormActions = () => {
    const { currentPage } = this.state;
    switch (currentPage) {
      case 1:
        return this.getFormActionsPage1();
      case 3:
        return this.getFormActionsPage4();
      case 4:
        return this.getFormActionsThankyouPage();
      default:
        return this.getFormActionsDefault();
    }
  };

  public updateSkills = () => {
    const { formData } = this.state;

    return Promise.resolve()
      .then(() => {
        const {
          skillsToAdd = []
        } = formData;

        if (skillsToAdd.length) {
          return Promise.all(
            skillsToAdd.map(skill =>
              createSkill(skill)
            )
          );
        }

        return [];
      })
      .then((response: any) => {
        for (const { data: { id: skillId } } of response) {
          formData.skillIds.push(skillId);
          formData.skillsToAdd = [];
        }

        this.setState({ formData });
      });
  };

  public createOpportunity = () => {
    const { user } = this.props;
    const { opportunityId, formData } = this.state;
    if (!this.validateForm()) {
      return false;
    }

    if (opportunityId) {
      return this.updateOpportunity();
    }

    const stepsToSkip = getUserRole(user) === 'school' ? 1 : 0;

    Promise.resolve()
      .then(() => {
        this.setState({
          submitting: true,
        });
        return createOpportunity(formData);
      })
      .then((response: any) => {
        const newOpportunityId = response.data.id;

        return this.setState({
          formErrors: {},
          opportunityId: newOpportunityId,
          responseData: response.data,
          submitting: false,
        }, () => {
          this.nextPage(stepsToSkip);
        });
      })
      .catch((e) => {
        return this.handleApiError(e);
      });

    return;
  }

  public updateOpportunity = () => {
    const { formData } = this.state;

    return Promise.resolve()
      .then(() => this.updateSkills())
      .then(() => {
        this.setState({
          submitting: true,
        });

        return updateOpportunity(this.state.opportunityId, formData);
      })
      .then((response: any) => {
        return this.setState({
          formErrors: {},
          responseData: response.data,
          submitting: false,
        }, () => {
          return this.nextPage();
        });
      })
      .catch((e) => {
        return this.handleApiError(e);
      });
  }

  public render() {
    const {
      formErrors,
      currentPage,
      organisation,
    } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <CreatingSteps currentPage={currentPage} />
        <NewOpportunityForm>
          <Grid container justify="center">
            <Grid item xs={10}>
              <FormPageContainer>
                {this.renderPages(pages, { organisation })}
              </FormPageContainer>
              <FormErrors formErrors={formErrors} />
              <FormActions
                style={{ marginTop: '4rem', justifyContent: 'flex-end' }}
              >
                {this.getFormActions()}
              </FormActions>
            </Grid>
          </Grid>
        </NewOpportunityForm>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.user || null,
  };
};

export default connect(mapStateToProps)(NewOpportunityFormComponent);
