import * as React from 'react';
import { get, filter } from 'lodash';
import {
  FormControl,
  FormHelperText,
} from '@material-ui/core';
import { withTheme } from '@material-ui/core/styles';
import {
  FormControlContainer,
  FormLabel,
} from '@components/FormComponents/styled';
import styled from 'styled-components';
import ChipInput from '@components/ChipInput';
import OptionButtons from '@components/OptionButtons';
// Containers
import { DataLoader } from '@components/DataLoader';

const themeColor = 'theme.palette.primary[600]';

export const SkillsDialogueContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

/* tslint:disable:max-line-length */
export const SkillCategoryButton: any = withTheme()(styled.div`
  margin: 0.4rem 1rem;
  padding: 0.7rem;
  border-bottom:
    ${(props: any) => props.selected ? `2px ${get(props, themeColor)} solid` : `2px transparent solid`};
  transition: all 100ms linear;
  cursor: pointer;
  color: #666666;
  &:hover {
  color: #333333;
  border-bottom:
  ${(props: any) => props.selected ? `2px ${get(props, themeColor)} solid` : `2px #cccccc solid`};
  }
` as any);
/* tslint: enable */

interface IProps {
  abilityToAdd: boolean;
  formErrors: any;
  onChangeArray: (e: any) => any;
  data: any;
  formData: any;
}

/**
 * Before major new opportunity page update there was a few different skill categories.
 * Client decided to put everything into single one.
 */
const SKILL_CATEGORY = 1;

class SkillsList extends React.Component<IProps, any, any> {
  public removeSkill = (skillId) => {
    const { onChangeArray } = this.props;
    const formData = this.props.formData || {};
    const { skillIds } = formData;
    const newSkills = filter(skillIds, o => o !== parseInt(skillId, 10));
    onChangeArray('skillIds')(newSkills);
  }

  render() {
    const {
      abilityToAdd = true,
      formErrors,
      onChangeArray
    } = this.props;

    const data = this.props.data || {};
    const formData = this.props.formData || {};

    const { skillIds, skillsToAdd = [] } = formData;

    const skillCategories = get(data, 'opportunity.skillCategories', {});
    const skillsDisabled = skillIds.length > 11;
    const skillOptions = get(skillCategories, `${SKILL_CATEGORY}.skills`, {});

    return (
      <React.Fragment>
        <FormControlContainer>
          <FormControl fullWidth error={!!formErrors.skillIds}>
            <FormLabel>Select key skills/interest they will be exposed to</FormLabel>
            <FormHelperText>
              Add up to 12 (ideally) skills you may find relevant of this opportunity
            </FormHelperText>
          </FormControl>
          <DataLoader required>
            <OptionButtons
              size="small"
              multiple
              disabled={skillsDisabled}
              selected={skillIds}
              onChange={onChangeArray('skillIds')}
              options={skillOptions}
            />
          </DataLoader>
        </FormControlContainer>
        {
          abilityToAdd ? (
            <FormControlContainer>
              <ChipInput
                name="skillsToAdd"
                placeholder="Type to add different skill/s"
                values={skillsToAdd}
                onChange={onChangeArray('skillsToAdd')}
              />
              <FormHelperText style={{ marginBottom: '1rem' }}>
                Now what you're looking for
              </FormHelperText>
            </FormControlContainer>
          ) : null
        }
      </React.Fragment>
    );
  }
}

export default SkillsList;
