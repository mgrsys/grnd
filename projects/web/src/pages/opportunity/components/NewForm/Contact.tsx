import {
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  Typography,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
} from '@material-ui/core';
import {
  AccountCircle,
  Clear
} from '@material-ui/icons';
import { find, get } from 'lodash';
import * as React from 'react';

// Components
import {
  IMultipageFormProps,
  IMultipageFormState,
  MultipageFormPage,
} from '@components/FormComponents';
import { FormControlContainer } from '@components/FormComponents/styled';
import { DataLoader } from '@components/DataLoader';

interface IProps extends IMultipageFormProps {
  data: any;
  formData: any;
  formErrors: any;
  onChange: (e: any) => any;
  onChangeArray: (e: any) => any;
  organisation: any;
}

interface IState extends IMultipageFormState {
  selectedDate?: any;
  selectedSkillCategory: string | number;
}

const styles = {
  selectedContact: {
    position: 'absolute',
    right: '16px'
  }
};

class Contact extends MultipageFormPage<IProps, IState> {
  public onChangeContact = (id) => {
    const { onChange, organisation } = this.props;
    const contacts = get(organisation, 'contacts', []);
    const contact = find(contacts, o => o.id === id);

    if (contact) {
      onChange('contact.name')(`${contact.firstName} ${contact.lastName}`);
      onChange('contact.position')(contact.firstName);
      onChange('contact.phoneNumber')(contact.phoneNumber);
      onChange('contact.email')(contact.email);
    } else {
      onChange('contact.name')('');
      onChange('contact.position')('');
      onChange('contact.phoneNumber')('');
      onChange('contact.email')('');
    }
    onChange('contactId')(id);
  }

  public render() {
    const { formErrors, onChange, organisation } = this.props;
    const formData = this.props.formData || {
      contact: {
        email: '',
        name: '',
        phoneNumber: '',
        position: '',
      },
    };
    const { contact, contactId } = formData;
    const { name, position, phoneNumber, email } = contact;
    const allContacts = get(organisation, 'contacts', []).map(o => {
      return {
        name: `${o.firstName} ${o.lastName}`,
        value: o.id,
      };
    });

    return (
      <Grid container spacing={32}>
        <Grid item xs={7}>
          <Typography variant="title" gutterBottom>Contact details</Typography>
          <Typography variant="body1" gutterBottom>
            Add details of the person-in-charge working directly with applicants and school
          </Typography>

          <FormControlContainer>
            <FormControl fullWidth error={get(formErrors, 'contact.name')}>
              <InputLabel htmlFor="name">Full Name</InputLabel>
              <Input
                id="name"
                type={'text'}
                value={name}
                onChange={onChange('contact.name')}
                autoComplete="off"
                disabled={!!contactId}
              />
              <FormHelperText>Enter name to search from previous contact</FormHelperText>
            </FormControl>
          </FormControlContainer>

          <FormControlContainer>
            <FormControl fullWidth error={get(formErrors, 'contact.position')}>
              <InputLabel htmlFor="position">Position</InputLabel>
              <Input
                id="position"
                type={'text'}
                value={position}
                onChange={onChange('contact.position')}
                autoComplete="off"
                disabled={!!contactId}
              />
            </FormControl>
          </FormControlContainer>

          <FormControlContainer>
            <FormControl fullWidth error={get(formErrors, 'contact.phoneNumber')}>
              <InputLabel htmlFor="phoneNumber">Contact number</InputLabel>
              <Input
                id="phoneNumber"
                type={'text'}
                value={phoneNumber}
                onChange={onChange('contact.phoneNumber')}
                autoComplete="off"
                disabled={!!contactId}
              />
            </FormControl>
          </FormControlContainer>

          <FormControlContainer>
            <FormControl fullWidth error={get(formErrors, 'contact.email')}>
              <InputLabel htmlFor="email">Email</InputLabel>
              <Input
                id="email"
                type={'text'}
                value={email}
                onChange={onChange('contact.email')}
                autoComplete="off"
                disabled={!!contactId}
              />
            </FormControl>
          </FormControlContainer>
        </Grid>
        <Grid item xs={5} style={{ marginTop: '36px' }}>
          <DataLoader required>
            <Paper>
              {
                allContacts && allContacts.length ? (
                  <List component="nav">
                    {
                      allContacts.map(({ value, name }) => {
                        const isSelected = contactId === value;

                        return (
                          <ListItem
                            key={value}
                            button
                            onClick={() => this.onChangeContact(value)}
                          >
                            <ListItemIcon>
                              <React.Fragment>
                                <AccountCircle />
                                {
                                  isSelected ? (
                                    <Clear
                                      style={styles.selectedContact}
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        this.onChangeContact(null)
                                      }}
                                    />
                                  ) : null
                                }
                              </React.Fragment>
                            </ListItemIcon>
                            <ListItemText primary={name} />
                          </ListItem>
                        );
                      })
                    }
                  </List>
                ) : null
              }
            </Paper>
          </DataLoader>
        </Grid>
      </Grid>
    );
  }
}

export default Contact;
