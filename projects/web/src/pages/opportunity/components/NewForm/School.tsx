import {
  FormControl,
  FormHelperText,
  Typography,
} from '@material-ui/core';
import { get } from 'lodash';
import * as React from 'react';

import styled from 'styled-components';

// Components
import {
  IMultipageFormProps,
  IMultipageFormState,
  MultipageFormPage,
} from '@components/FormComponents';
import {
  FormControlContainer,
  FormLabel,
} from '@components/FormComponents/styled';
import OptionButtons from '@components/OptionButtons';

const NewOpportunityFormPage3 = styled.div``;

interface IProps extends IMultipageFormProps {
  data: any;
  formData: any;
  formErrors: any;
  onChange: (e: any) => any;
  onChangeArray: (e: any) => any;
  organisation: any;
}

interface IState extends IMultipageFormState {
  selectedDate?: any;
  selectedSkillCategory: string | number;
}

class School extends MultipageFormPage<IProps, IState> {
  public render() {
    const { formData, formErrors, onChange, organisation } = this.props;
    const { schoolIds } = formData;
    const allSchools = get(organisation, 'schools', []).map(o => {
      return {
        name: o.name,
        value: o.id,
      };
    });

    return (
      <NewOpportunityFormPage3>
        <Typography variant="title" gutterBottom>School listing</Typography>
        <Typography variant="body1" gutterBottom>
          Select the school/s you would like to list the opportunity with.
        </Typography>

        <FormControlContainer error={!!formErrors.when}>
          <FormControl fullWidth>
            <FormLabel>Connected schools</FormLabel>
            <FormHelperText>Select one or more schools you are connected with.</FormHelperText>
            <OptionButtons
              selected={schoolIds}
              options={allSchools}
              xs={6}
              multiple
              onChange={onChange('schoolIds')}
            />
          </FormControl>
        </FormControlContainer>
      </NewOpportunityFormPage3>
    );
  }
}

export default School;
