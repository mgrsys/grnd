import * as React from 'react';
import {
  Grid,
  FormControl,
  FormHelperText,
} from '@material-ui/core';
import DatePicker from '@components/DatePicker';

import {
  FormControlContainer,
  FormLabel,
} from '@components/FormComponents/styled';

interface IProps {
  formErrors: any;
  formData: any;
  onChange: (e: any) => any;
}

class WorkingDates extends React.Component<IProps, any>{
  public onChangeDate = (field) => (date) => {
    const { onChange } = this.props;
    this.setState({ selectedDate: date });
    onChange(field)(date.format('YYYY-MM-DD'));
  }

  render() {
    const { formErrors } = this.props;

    const formData = this.props.formData || {};
    const { startDate, closeDate } = formData;

    return (
      <Grid container spacing={24}>
        <Grid item md={6}>
          <FormControlContainer>
            <FormControl fullWidth error={!!formErrors.startDate}>
              <FormLabel htmlFor="start">Start date</FormLabel>
              <FormHelperText>Please select date between today and three months</FormHelperText>
              <DatePicker
                id="start"
                type="date"
                InputLabelProps={{ shrink: true, }}
                required
                disablePast
                value={startDate}
                onChange={this.onChangeDate('startDate')}
                style={{ width: '100%' }}
                format="YYYY-MM-DD"
              />
            </FormControl>
          </FormControlContainer>
        </Grid>
        <Grid item md={6}>
          <FormControlContainer>
            <FormControl fullWidth error={!!formErrors.closeDate}>
              <FormLabel htmlFor="close">Close date</FormLabel>
              <FormHelperText>Please select date between start and three months</FormHelperText>
              <DatePicker
                id="close"
                type="date"
                InputLabelProps={{ shrink: true, }}
                required
                disablePast
                value={closeDate}
                minDate={startDate}
                onChange={this.onChangeDate('closeDate')}
                style={{ width: '100%' }}
                format="YYYY-MM-DD"
              />
            </FormControl>
          </FormControlContainer>
        </Grid>
      </Grid>
    )
  }
}

export default WorkingDates;
