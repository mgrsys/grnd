import { Grid, Paper, Typography } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { get } from 'lodash';
import * as React from 'react';

import styled from 'styled-components';

// Components
import OpportunityType from './OpportunityType';

const OpportunityItemWrapper = styled.div`
  margin: 2rem 0;
`;

const OpportunityTypeWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const OpportunityDetail = styled(Paper)`
  margin: 2rem 0;
  padding: 2rem;
`;

const SectionTitle = styled(Typography)`
  && {
    color: ${grey[400]};
    font-weight: 500;
    font-size: 1.2rem;
  }
`;

const Description = styled.div`
  margin-bottom: 1.5rem;
`;

const ListItem = styled(Grid)`
  border-bottom: 1px ${grey[300]} solid;
  padding-bottom: 0.8rem;
  margin-bottom: 0.8rem;
  &:last-child {
    margin-bottom: 0;
    padding-bottom: 0;
    border-bottom: none;
  }
`;

const ListTitle = styled(Typography)`
  && {
    color: ${grey[900]};
    font-weight: 500;
    font-size: 1rem;
  }
`;

interface IProps {
  opportunity: any;
}

class OpportunityItemComponent extends React.Component<IProps, any> {
  public render() {
    const opportunity = this.props.opportunity || {
      contact: {},
    };

    const { contact, location } = opportunity;
    let closeDate = 'Unknown';
    if (opportunity.ongoing) {
      closeDate = 'Ongoing';
    } else if (opportunity.closeDate) {
      closeDate = opportunity.closeDate.split('T')[0];
    }
    const keyDetailsList = [{
      title: 'Where',
      value: location ? `${location.suburb}, ${location.state}` : `Unknown`,
    }, {
      title: 'When',
      value: opportunity.time_periods ? opportunity.time_periods.map(({ name }) => name) : 'N/A',
    }, {
      title: 'Duration',
      value: `${opportunity.duration} ${get(opportunity, 'opportunityDuration.name', 'N/A')}`,
    }, {
      title: 'Closing',
      value: closeDate,
    }];
    return (
      <OpportunityItemWrapper>
        <OpportunityDetail elevation={1}>

          <Grid container>
            <Grid item xs={7}>
              <Typography variant="title" gutterBottom>{opportunity.title}</Typography>

              <SectionTitle gutterBottom>About the role</SectionTitle>
              <Description>
                <Typography>{opportunity.description}</Typography>
              </Description>

              {keyDetailsList.map(detail => (
                <ListItem key={detail.title} container>
                  <Grid item xs={5}>
                    <ListTitle>{detail.title}</ListTitle>
                  </Grid>
                  <Grid item xs={7}>
                    <Typography>{detail.value}</Typography>
                  </Grid>
                </ListItem>
              ))}

            </Grid>
            <Grid item xs={5}>
              <OpportunityTypeWrapper>
                <OpportunityType
                  id={opportunity.opportunityTypeId}
                  name={get(opportunity, 'opportunityType.name', 'N/A')}
                  size={128}
                />
              </OpportunityTypeWrapper>
            </Grid>
          </Grid>

        </OpportunityDetail>

        <OpportunityDetail elevation={1}>
          <SectionTitle gutterBottom>What will I be doing?</SectionTitle>
          <ul>
            {opportunity.tasks && opportunity.tasks.map((task, i) => String(task).length > 0 && (
              <li key={i}>
                <Typography gutterBottom>{task}</Typography>
              </li>
            ))}
          </ul>

        </OpportunityDetail>

        <OpportunityDetail elevation={1}>
          <SectionTitle gutterBottom>Skills I will be exposed to</SectionTitle>
          <ul>
            {opportunity.skills && opportunity.skills.map((skill, i) => (
              <li key={i}>
                <Typography gutterBottom>{skill.name}</Typography>
              </li>
            ))}
          </ul>
        </OpportunityDetail>

        {contact && (
          <OpportunityDetail elevation={1}>
            <SectionTitle gutterBottom>People</SectionTitle>
            <React.Fragment>
              <Typography variant="title">{contact.firstName} {contact.lastName}</Typography>
              <Typography variant="subheading">{contact.position}</Typography>
            </React.Fragment>
          </OpportunityDetail>
        )}

      </OpportunityItemWrapper>
    );
  }
}

export default OpportunityItemComponent;
