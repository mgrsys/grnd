import { amber, blue, green, grey, indigo, purple, teal } from '@material-ui/core/colors';
import * as React from 'react';

import styled from 'styled-components';

const OpportunityType: any = styled.div`
  width: ${(props: any) => props.size}px;
  height: ${(props: any) => props.size}px;
  border-radius: ${(props: any) => props.size / 2}px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${(props: any) => getOpportunityTypeColor(props.id)};
  text-align: center;
  font-size: 12px;
  color: white;
  overflow: hidden;
`;

function getOpportunityTypeColor(id) {
  switch (id) {
    case 1: return teal[600];
    case 2: return amber[600];
    case 3: return purple[600];
    case 4: return blue[600];
    case 5: return green[600];
    case 6: return indigo[600];
    default: return grey[600];
  }
}

interface IProps {
  id: number;
  name: string;
  size?: number;
}

class OpportunityTypeComponent extends React.Component<IProps, any> {
  public render() {
    const { id, name } = this.props;
    const size = this.props.size || 72;
    return (
      <OpportunityType id={id} size={size}>
        {name}
      </OpportunityType>
    );
  }
}

export default OpportunityTypeComponent;
