import * as React from 'react';
import {
  DialogTitle,
  Dialog,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';
import DatePicker from '@components/DatePicker';

class EditStartDateModal extends React.Component<any, any> {
  public onChangeDate = (date) => {
    const { onChange } = this.props;
    onChange(date.format('YYYY-MM-DD'));
  }

  render() {
    const {
      open,
      opportunity,
      onClose,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={onClose(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update start date</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter new start date of the opportunity.
          </DialogContentText>

          <DatePicker
            id="start"
            type="date"
            InputLabelProps={{ shrink: true, }}
            required
            value={opportunity.startDate}
            onChange={this.onChangeDate}
            style={{ width: '100%' }}
            format="YYYY-MM-DD"
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={onClose(true)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default EditStartDateModal;
