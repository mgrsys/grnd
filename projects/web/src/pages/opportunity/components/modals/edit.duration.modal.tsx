import * as React from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import {
  DialogTitle,
  Dialog,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
  Grid,
  Select,
  MenuItem,
} from '@material-ui/core';

interface IProps {
  open: boolean;
  onClose: (e: any) => any;
  onChange: (key: any, value: any) => any;
  data: any;
  opportunity: any;
}
interface IState {

}

class EditDurationModalComponent extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.opportunity ? props.opportunityDurationId : 1,
    };
  }

  public render() {
    const { data, opportunity, onChange } = this.props;
    const opportunityDurationId: number = parseInt(opportunity ? opportunity.opportunityDurationId : 1, 10);

    const durations = get(data, 'opportunity.durations', {});
    const durationOptions = Object.keys(durations).map((id) => ({
      id: parseInt(id, 10), name: durations[id].name
    }));

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update duration</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter the new duration for the opportunity.
          </DialogContentText>

          <Grid container spacing={8}>
            <Grid item xs={8}>
              <Select
                style={{marginTop: '0.5rem'}}
                id="duration"
                value={opportunity.duration}
                onChange={({ target: { value } }) => onChange('duration', value)}
                fullWidth
              >
                {
                  [...Array(31).keys()].map(day => (
                    <MenuItem key={day} value={String(day + 1)}>{day+1}</MenuItem>
                  ))
                }
              </Select>
            </Grid>
            <Grid item xs={4}>
              <Select
                id="opportunityDurationId"
                value={opportunityDurationId}
                style={{marginTop: '0.5rem'}}
                onChange={({ target: { value } }) => onChange('opportunityDurationId', value)}
                fullWidth
              >
                {
                  durationOptions.map(({ id, name }) => (
                    <MenuItem key={id} value={id}>{name}</MenuItem>
                  ))
                }
              </Select>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={this.props.onClose(true)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data || null,
  };
};

const EditDurationModal = connect(
  mapStateToProps,
)(EditDurationModalComponent);

export default EditDurationModal;
