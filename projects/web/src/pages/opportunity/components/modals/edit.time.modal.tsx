import * as React from 'react';
import { connect } from 'react-redux';
import { DialogTitle, Dialog, DialogContentText, DialogContent, DialogActions, Button } from '@material-ui/core';

import OptionButtons from '@components/OptionButtons';

interface IProps {
  open: boolean;
  onClose: (e: any) => any;
  onChange: (e: any) => any;
  data: any;
  opportunity: any;
}

class EditTimeModalComponent extends React.Component<IProps, any> {
  public updateSelected = (values): void => {
    const { timePeriod } = this.props.data.opportunity;

    this.props.onChange(
      values.map(timePeriodId => ({
        id: parseInt(timePeriodId, 10),
        name: timePeriod[timePeriodId].name,
      }))
    );
  }

  public render() {
    const {
      data,
      opportunity,
      open,
      onClose,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={onClose(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update time period</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter the new time periods for the opportunity.
          </DialogContentText>

          <OptionButtons
            selected={opportunity ? opportunity.time_periods.map(time => time.id) : []}
            options={data.opportunity ? data.opportunity.timePeriod : {}}
            onChange={this.updateSelected}
            multiple
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={onClose(true)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data || null,
  };
};

const EditTimeModal = connect(
  mapStateToProps,
)(EditTimeModalComponent);

export default EditTimeModal;
