import * as React from 'react';
import { connect } from 'react-redux';
import {
  DialogTitle,
  Dialog,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';
import SkillsList from '../NewForm/SkillsList';

class EditSkillsModalComponent extends React.Component<any, any> {
  render() {
    const {
      data,
      skills,
      open,
      onClose,
      onChange,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={onClose(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update skills</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, choose new skills.
          </DialogContentText>

          <SkillsList
            abilityToAdd={false}
            formErrors={{}}
            data={data}
            formData={{
              skillIds: skills.map(({ id }) => id)
            }}
            onChangeArray={onChange}
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={onClose(true)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => ({
  data: state.data,
});

export default connect(mapStateToProps)(EditSkillsModalComponent);
