import * as React from 'react';
import {
  DialogTitle,
  Dialog,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';
import DatePicker from '@components/DatePicker';

class EditCloseDateModal extends React.Component<any, any> {
  public onChangeDate = (date) => {
    const { onChange } = this.props;
    onChange(date.format('YYYY-MM-DD'));
  }

  render() {
    const {
      open,
      opportunity,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={this.props.onClose(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Update close date</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Below, enter new close date of the opportunity.
          </DialogContentText>

          <DatePicker
            id="start"
            type="date"
            InputLabelProps={{ shrink: true, }}
            required
            value={opportunity.closeDate}
            onChange={this.onChangeDate}
            style={{ width: '100%' }}
            format="YYYY-MM-DD"
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={this.props.onClose(true)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default EditCloseDateModal;
