import * as React from 'react';
import {
  Grid,
  Paper,
  Typography,
  Avatar,
} from '@material-ui/core';
import {
  Book, School,
} from '@material-ui/icons';
import {
  withStyles,
  MuiThemeProvider,
} from '@material-ui/core/styles';
import { theme, styles } from './styles';

import Search from './Search';

class OpportunityApplications extends React.Component<any, any> {
  state = {
    search: '',
  };

  public search = ({ target: { value } }) => {
    this.setState({ search: value });
    // TODO use then
  };

  render() {
    const {
      classes,
      applications
    } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        {
          applications.length ? (
            <Grid
              container
              spacing={24}
              className={classes.grid}
            >
              <Grid item md={8}>
                {
                  applications.map((application) => {
                    const {
                      id,
                      student: {
                        firstName,
                        lastName,
                        schoolYear,
                        schools = [],
                        location,
                      }
                    } = application;

                    return (
                      <Paper
                        key={id}
                        elevation={1}
                        className={classes.paper}
                      >
                        <Grid container spacing={16}>
                          <Grid item md={2}>
                            <Avatar
                              style={{
                                border: '1px solid #FFD300',
                                width: '58px',
                                height: '58px',
                              }}
                            />
                          </Grid>
                          <Grid item md={10}>
                            <Typography
                              variant="subheading"
                              component="p"
                              className={classes.studentName}
                            >
                              {`${firstName} ${lastName}`}
                            </Typography>
                            {
                              location ? (
                                <Typography
                                  variant="caption"
                                  component="p"
                                  className={classes.location}
                                >
                                  {location.suburb}
                                </Typography>
                              ) : null
                            }
                          </Grid>
                        </Grid>
                        <div>
                          <Typography
                            variant="caption"
                            component="span"
                            style={{
                              display: 'inline-block',
                              marginTop: '16px',
                              marginRight: '24px',
                              fontSize: '12px',
                            }}
                          >
                            <Book style={{ verticalAlign: 'middle' }} />

                            <Typography
                              component="span"
                              style={{
                                display: 'inline-block',
                                verticalAlign: 'middle',
                                marginLeft: '8px'
                              }}
                            >
                              {`Grade ${schoolYear}`}
                            </Typography>
                          </Typography>

                          <Typography
                            variant="caption"
                            component="span"
                            style={{
                              display: 'inline-block',
                              marginTop: '16px',
                              marginRight: '24px',
                              fontSize: '12px',
                            }}
                          >
                            <School style={{ verticalAlign: 'middle' }} />

                            {
                              schools.slice(0, 1).map(({ id, name }) => (
                                <Typography
                                  key={id}
                                  component="span"
                                  style={{
                                    display: 'inline-block',
                                    verticalAlign: 'middle',
                                    marginLeft: '8px'
                                  }}
                                >
                                  {name}
                                </Typography>
                              ))
                            }
                          </Typography>
                        </div>
                      </Paper>
                    );
                  })
                }
              </Grid>
              <Grid item md={4}>
                <Search
                  classes={classes}
                  search={this.search}
                />
              </Grid>
            </Grid>
          ) : (
            <Typography
              variant="subheading"
              className={classes.noApplications}
            >
              No applications yet.
            </Typography>
          )
        }
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(OpportunityApplications);
