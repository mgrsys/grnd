import {
  createMuiTheme,
  createStyles,
  Theme,
} from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

export const theme = createMuiTheme({
  palette: {
    primary: { main: '#2626F7' },
    secondary: red,
  },
});

export const styles = (t: Theme): any => createStyles({
  noApplications: {
    padding: '12px',
  },
  search: {
    [theme.breakpoints.only('xs')]: {
      display: 'none',
    }
  },
  grid: {
    [theme.breakpoints.only('xs')]: {
      margin: '-10px -12px',
    },
  },
  paper: {
    position: 'relative',
    padding: '24px',
    [theme.breakpoints.only('xs')]: {
      borderRadius: '0',
      boxShadow: 'none',
      padding: '0 24px 12px',
      marginTop: '8px',
      width: '100%',
      borderBottom: '1px solid rgba(0, 0, 0, 0.18)',
    }
  },
  studentName: {
    marginTop: '4px',
    [theme.breakpoints.only('xs')]: {
      marginTop: '8px',
    }
  },
  location: {
    marginTop: 0,
    color: 'rgba(0, 0, 0, 0.6)',
    fontSize: '16px',
  }
});
