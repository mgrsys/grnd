import * as React from 'react';
import {
  Paper,
  Typography,
  TextField,
  InputAdornment,
} from '@material-ui/core';

class Search extends React.Component<any, any> {
  render() {
    const {
      classes,
      search,
    } = this.props;

    return (
      <div className={classes.search}>
        <Paper className={classes.paper} elevation={1}>
          <Typography variant="subheading" component="p">
            Advance search
          </Typography>
          <Typography
            variant="body2"
            component="p"
            style={{
              marginTop: '24px',
              marginBottom: '8px',
            }}
          >
            Keyword search
          </Typography>
          <TextField
            onChange={search}
            placeholder="e.g. work experience"
            style={{
              background: 'rgba(0, 0, 0, 0.076)',
              width: '100%',
            }}
            InputProps={{
              startAdornment: <InputAdornment position="start">&nbsp;</InputAdornment>,
            }}
          />
        </Paper>
      </div>
    );
  }
}

export default Search;
