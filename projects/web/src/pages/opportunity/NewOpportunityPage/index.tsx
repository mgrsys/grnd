import { Paper } from '@material-ui/core';
import { Clear } from '@material-ui/icons';
import { get } from 'lodash';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { Auth } from 'aws-amplify';
// Components
import Layout from '@components/Layout';
import NewOpportunityForm from '../components/NewForm';
import { CustomNav, CustomNavTite, CloseButton } from './styled';

const CustomNavComponent = (
  <CustomNav>
    <CustomNavTite>Create an opportunity</CustomNavTite>
    <CloseButton to="/">
      <Clear />
    </CloseButton>
  </CustomNav>
);

interface IProps {
  match: any;
}
interface IState {
  notAuthorized: boolean;
}

class NewOpportunityPage extends React.Component<IProps, IState> {
  state = {
    notAuthorized: false
  };

  public componentDidMount() {
    Auth.currentUserInfo()
    .then(user => {
      if (!user) {
        throw Error('Cannot get user info');
      };
    })
    .catch(e => {
      console.warn('Unable to get currentUserInfo', e);
      this.setState({
        ...this.state,
        notAuthorized: true,
      });
    });
  }

  public render() {
    const opportunityId = get(this.props, 'match.params.id', null);

    return (this.state.notAuthorized ? (<Redirect to="/signin" />) :
      (<Layout nav={CustomNavComponent}>
        <Paper elevation={0}>
          <NewOpportunityForm opportunityId={opportunityId} />
        </Paper>
      </Layout>)
    );
  }
}

export default NewOpportunityPage;
