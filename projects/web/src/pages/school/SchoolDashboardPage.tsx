import { grey } from '@material-ui/core/colors';
import * as React from 'react';
import styled from 'styled-components';

import Layout from '@components/Layout';
import OpportunityList from '@pages/opportunity/components/OpportunityList';

// Utils
import { fetchOpportunities } from '@actions/opportunity';
import { connect } from 'react-redux';

const Dashboard = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${grey[100]};

  @media (max-width: 600px) {
    background: #FFF;
  }
`;

class DashboardComponent extends React.Component<any, any> {
  public state = {
    opportunities: [],
  };

  public componentDidMount() {
    if (!this.props.opportunities) {
      this.props.dispatch(fetchOpportunities());
    }
  }

  public render() {
    const { opportunities } = this.props;

    return (
      <Layout page="opportunityList">
        <Dashboard>
          <OpportunityList
            opportunities={opportunities || {}}
            showDuration
            showAge
            showExploreButton
          />
        </Dashboard>
      </Layout>
    );
  }
}

const mapStateToProps = (state: any) => ({
  opportunities: state.opportunities.opportunityList,
});


export default connect(mapStateToProps)(DashboardComponent);
