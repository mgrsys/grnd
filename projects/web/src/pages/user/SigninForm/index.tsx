import { Auth } from 'aws-amplify';
import { isEmpty } from 'lodash';
import * as React from 'react';
import styled from 'styled-components';

// Material Ui
import { Button, Typography } from '@material-ui/core';
import { indigo, red, grey } from '@material-ui/core/colors';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// Components
import {
  FormErrors,
} from '@components/FormComponents';
import {
  FormPage,
  FormPageContainer,
  FormPaper,
  FormActions,
} from '@components/FormComponents/styled';
import ProgressButton from '@components/ProgressButton';
import Page1 from './Page1';
import Page2 from './Page2';

// Utils
import { history } from '@utils/history';

const SigninFormContainer = styled.div`
  width: 60%;
  max-width: 600px;
  transition: width 100ms linear;
  @media(max-width: 768px) {
    margin-top: 2rem;
    width: 90%;
  }
`;

const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: red,
  },
});

class SigninFormComponent extends React.Component<any, any> {
  public state = {
    currentPage: 1,
    forgotPassword: false,
    forgotPasswordReset: false,
    formData: {
      code: '',
      email: '',
      password: '',
      newPassword: '',
    },
    helperText: '',
    formErrors: {},
    submitting: false,
    error: false,
  };
  public setFieldValue = (field) => (e) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [field]: e.target ? e.target.value : e,
      },
    });
  }
  public loginButtonClick = () => {
    const { email, password } = this.state.formData;
    const formErrors: any = {};
    if (!email) {
      formErrors.email = 'Email is required';
    }
    if (!password) {
      formErrors.password = 'Password is required';
    }
    if (isEmpty(formErrors)) {
      this.setState({
        submitting: true,
      }, this.loginAction);
    } else {
      this.setState({ formErrors });
    }
  }
  public verifyButtonClick = () => {
    this.setState({
      submitting: true,
    }, this.verifyAction);
  }
  public loginAction = () => {
    const { email, password } = this.state.formData;
    Auth.signIn(email, password)
      .then(user => {
        return this.setState({
          submitting: false,
        }, this.gotoProfilePage);
      }).catch(e => {
        switch (e.code) {
          case 'UserNotConfirmedException':
            return this.setState({
              currentPage: 2,
              submitting: false,
            });
          case 'NotAuthorizedException':
          case 'UserNotFoundException':
            return this.setState({
              formErrors: {
                confirm: 'Invalid username or password',
              },
              submitting: false,
            });
          default:
            console.error('Unknown error code for attempted auth', e);
            return this.setState({
              formErrors: {
                confirm: 'An unknown error occured',
              },
              submitting: false,
            });
        }
      });
  }
  public verifyAction = () => {
    const { code, email } = this.state.formData;
    Auth.confirmSignUp(email, code)
      .then(data => {
        return this.loginAction();
      }).catch(e => {
        this.setState({
          formErrors: {
            ...this.state.formErrors,
            confirm: 'An unknown error occured',
          },
          submitting: false,
        });
      });
  }

  public forgotPasswordClick = () => {
    const { email } = this.state.formData;
    this.setState({ submitting: true, error: false, }, () => {
      Auth.forgotPassword(email)
      .then((response: any) => {

        return this.setState({ ...this.state, submitting: false, helperText: 'Password reset instructions have been emailed to ' + email, }, () => {

            this.setState({
              ...this.state,
              currentPage: 2,
              forgotPasswordReset: true,
            });

        });
      })
      .catch(e => {
        switch(e.code) {
          case "LimitExceededException":
            return this.setState({
              submitting: false,
              error: true,
              helperText: "You've tried to reset too many times. Try again later.",
            });
          case "UserNotFoundException":
            return this.setState({
              submitting: false,
              error: true,
              helperText: 'This email address has not been registered with Grandshake',
            });
          case "InvalidParameterException":
            return this.setState({
              submitting: false,
              error: true,
              helperText: 'This user has still not been confirmed. Please check your inbox.',
            });
          default:
            return this.setState({
              submitting: false,
              error: true,
              helperText: 'An unknown error occurred. Please try again later.',
            });
        }

      });
    });
  }
  public changePassword = () => {
    const { code, email, newPassword } = this.state.formData;
    if(!code || !email || !newPassword) {
      this.setState({
        ...this.state,
        error: true,
        helperText: 'Please enter a new password.'
      })
    }
    Auth.forgotPasswordSubmit(email, code, newPassword)
    .then(data => this.setState({
      ...this.state,
      error: false,
      currentPage: 1,
      forgotPassword: false,
      formData: {
        ...this.state.formData,
        password: '',
        newPassword: '',
        code: '',
      },
      forgotPasswordReset: false,
      helperText: 'Successfully reset password. Login above.'
    }))
    .catch(e => {
      switch(e.code) {
        case "CodeMismatchException":
          return this.setState({
            ...this.state,
            error: true,
            helperText: 'Incorrect code. Please check your email again for the code.'
          })
        case "InvalidParameterException":
          return this.setState({
            ...this.state,
            error: true,
            helperText: 'Invalid password (Length min 8, an uppercase)'
          })

        case "InvalidPasswordException":
          return this.setState({
            ...this.state,
            error: true,
            helperText: 'Invalid password (Length min 8, an uppercase)'
          })
      }
    });

  }

  public gotoProfilePage() {
    history.push('/');
  }
  public toggleForgotPassword = () => {
    const forgotPassword = !this.state.forgotPassword;
    this.setState({
      ...this.state,
      forgotPassword,
      forgotPasswordReset: false,
      currentPage: this.state.forgotPasswordReset ? 1 : this.state.currentPage,
      error: false,
      helperText: '',
    });
  }
  public backPage = () => {
    this.setState({
      currentPage: this.state.currentPage - 1,
    });
  }
public render() {
    const { currentPage, formData, formErrors, forgotPassword, submitting, forgotPasswordReset, } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <SigninFormContainer>

          <FormPaper elevation={0}>

            <FormPageContainer style={{flexDirection: 'column', display: 'flex'}}>
              <FormPage show={currentPage === 1} >
                <Page1
                  formData={formData}
                  formErrors={formErrors}
                  forgotPassword={forgotPassword}
                  toggleForgotPassword={this.toggleForgotPassword}
                  login={this.loginButtonClick}
                  onChange={this.setFieldValue}
                />
              </FormPage>
              <FormPage show={currentPage === 2} >
                <Page2
                  formData={formData}
                  formErrors={formErrors}
                  onChange={this.setFieldValue}
                  forgotPasswordReset={this.state.forgotPasswordReset}
                />
              </FormPage>
              {this.state.helperText.length > 0 && (<Typography variant="caption" style={{
                marginBottom: '1.5rem',
                marginTop: '-1.5rem',
                color: this.state.error ? red[500] : grey[800],
              }}>
                {this.state.helperText}
              </Typography>)}
            </FormPageContainer>

            <FormErrors formErrors={formErrors} />

            <FormActions>
              {(forgotPassword && !forgotPasswordReset) && (
                <React.Fragment>
                  <Button size="large" onClick={this.toggleForgotPassword}>
                    Sign In
                  </Button>
                  <ProgressButton
                    size="large"
                    variant="raised"
                    color="primary"
                    disabled={submitting}
                    loading={submitting}
                    onClick={this.forgotPasswordClick}
                  >
                    Forgot Password
                  </ProgressButton>
                </React.Fragment>
              )}
              {(forgotPassword && forgotPasswordReset) && (
                <React.Fragment>
                  <Button size="large" onClick={this.toggleForgotPassword}>
                    Sign In
                  </Button>
                  <ProgressButton
                    size="large"
                    variant="raised"
                    color="primary"
                    disabled={submitting}
                    loading={submitting}
                    onClick={this.changePassword}
                  >
                    Change password
                  </ProgressButton>
                </React.Fragment>
              )}
              {!forgotPassword && (currentPage === 1) && (
                <React.Fragment>
                  <Button size="large" onClick={() => history.push('/student/signup')}>
                    Sign Up
                  </Button>
                  <ProgressButton
                    size="large"
                    variant="raised"
                    color="primary"
                    disabled={submitting}
                    loading={submitting}
                    onClick={this.loginButtonClick}
                  >
                    Sign In
                  </ProgressButton>
                </React.Fragment>
              )}
              {!forgotPassword && currentPage === 2 && (
                <React.Fragment>
                  <Button size="large" onClick={this.backPage}>
                    Sign Up
                  </Button>
                  <ProgressButton
                    size="large"
                    variant="raised"
                    color="primary"
                    disabled={submitting}
                    loading={submitting}
                    onClick={this.verifyButtonClick}
                  >
                    Continue
                  </ProgressButton>
                </React.Fragment>
              )}
            </FormActions>
          </FormPaper>

        </SigninFormContainer>
      </MuiThemeProvider>
    );
  }
}

export default SigninFormComponent;
