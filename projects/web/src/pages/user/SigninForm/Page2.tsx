import * as React from 'react';

import {
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
  Typography,
} from '@material-ui/core';
import { Dialpad } from '@material-ui/icons';
import styled from 'styled-components';

// Components
import {
  FormControlContainer,
  IconWrapper
} from '@components/FormComponents/styled';

const SigninFormPage2 = styled.section``;

interface IProps {
  formData: any;
  formErrors: any;
  forgotPasswordReset?: boolean;
  onChange: (e: any) => any;
}

class SigninFormPage2Component extends React.Component<IProps, any> {

  constructor(props) {
    super(props);
    this.state = {
      formData: {
        code: '',
        ...props.formData || {},
      },
    };
  }

  public render() {
    const { onChange, forgotPasswordReset, } = this.props;
    const { code, newPassword, } = this.props.formData;
    return (
      <SigninFormPage2>

        <Typography variant="title" gutterBottom>Validate your account</Typography>
        <Typography variant="display1">Enter the code from your email</Typography>

        <FormControlContainer>
          <FormControl fullWidth>
            <InputLabel htmlFor="code">Validation Code</InputLabel>
            <Input
              id="code"
              type={'text'}
              value={code}
              onChange={onChange('code')}
              autoComplete="off"
              endAdornment={
                <InputAdornment position="end">
                  <IconWrapper>
                    <Dialpad />
                  </IconWrapper>
                </InputAdornment>
              }
            />
            </FormControl>
            {forgotPasswordReset && (
              <FormControl fullWidth style={{marginTop: '0.5rem'}}>
                <InputLabel htmlFor="code">New password</InputLabel>
                <Input
                  id="newPassword"
                  type={'password'}
                  value={newPassword}
                  onChange={onChange('newPassword')}
                  autoComplete="off"

                />
              </FormControl>
            )}
        </FormControlContainer>

      </SigninFormPage2>
    );
  }

}

export default SigninFormPage2Component;
