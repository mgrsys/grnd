import * as React from 'react';
import Layout from '@components/Layout';
import {
  MuiThemeProvider,
  Theme,
  withStyles,
  createMuiTheme,
  Avatar,
  Typography,
  Chip,
  Button,
  List,
  ListItem,
  ListItemText,
  CircularProgress,
  LinearProgress,
  ListItemSecondaryAction,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  CardHeader,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { Edit, School, Face, Email, Call, Add, Work, ViewList, PeopleOutline, AttachMoney, Build, KeyboardArrowLeft, Phone, Timer } from '@material-ui/icons';
import {
  blue,
  indigo,
  red,
  green,
  purple,
  orange
} from '@material-ui/core/colors';

import { getProfileAction, updateProfileAction, addExperienceToProfile, addContactToProfile, editMainAction } from '@actions/profile';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import EditSkillsModal from '@components/edit.skills.modal'
import { DataLoader } from '@components/DataLoader';
import PickSubjectsModal from '@components/pick.subjects.modal';
import NewExperienceModal from '@components/new.experience.modal';
import AddContactModal from '@components/add.contact.modal';
import { newAlert } from '@actions/global';
import { withRouter } from 'react-router';
import EditProfileMain from '@components/edit.profile.main';
import { AlertLevel } from '@components/global.model';

import { Storage } from 'aws-amplify';
import { Base64 } from 'js-base64';
import ChangePasswordModal from '@components/change.password.modal';
interface IProps {
  classes: any;
  dispatch: Dispatch;
  user: any;
  loading: boolean;
  profile: any;
  subjects: any;
  history: any;
}

interface IState {
  loading: boolean;
  profilePictureUrl: string;
  editProfile: any; //  SOON TO BE PROFILE MODEL
  editMain: boolean;
  editSubjects: boolean;
  editSkillsHave: boolean;
  editSkillsWant: boolean;
  editEmergencyContacts: boolean;
  editPassword: boolean;
  editExperience: boolean;
  editNewExperience: boolean;
  actionInProgress: boolean;
  showFinishedModal: boolean;
  openExperience: number;
}

const theme: Theme = createMuiTheme({
  palette: {
    primary: { main: indigo.A700 },
    secondary: red,
  },
});

const styles = (theme: Theme): any => ({
  root: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },
  profilePicture: {
    width: 96,
    height: 96,
  },
  sectionTwoChips: {
    marginTop: theme.spacing.unit,
  },
  profileChip: {
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  formControl: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  card: {
    width: 320,
    [theme.breakpoints.down('sm')]: {
      marginBottom: '1.0rem',
      width: '100%',
    },
    display: 'flex',
    flexDirection: 'column',
    marginRight: theme.spacing.unit * 2,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    backgroundPosition: '0 0',
  },
  cardGroupOne: {
    display: 'flex', flexDirection: 'row', width: '100%',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  nonEditable: {
    opacity: 0.5,
  }
});

const opportunityTypeArray: any = {
  1: {
    icon: <Work />, color: indigo[700],
  },
  2: {
    icon: <ViewList />, color: red[700],
  },
  3: {
    icon: <PeopleOutline />, color: blue[700],
  },
  4: {
    icon: <AttachMoney />, color: green[700],
  },
  5: {
    icon: <School />, color: purple[700],
  },
  6: {
    icon: <Build />, color: orange[700],
  },
};

const progressPhrases: string[] = [
  "Tell us a little bit about yourself.",
  "Add some skills that you possess.",
  "Add some skills that you want to gain from future opportunities.",
  "Add some subjects that you're studying. Optionally, you can add some previous work experience you've had.",
  "Finally, add at least one emergency contact.",
];

class ProfilePage extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true,
      profilePictureUrl: '',
      editProfile: null,
      editMain: false,
      editSubjects: false,
      editSkillsHave: false,
      editSkillsWant: false,
      editPassword: false,
      editExperience: false,
      editNewExperience: false,
      editEmergencyContacts: false,
      actionInProgress: false,
      openExperience: -1,
      showFinishedModal: false,
    };
  }

  public componentDidMount(): void {
    const { dispatch } = this.props;

    if (!this.props.profile) {
      this.setState({
        ...this.state,
        loading: true,
      });

      dispatch(getProfileAction());
    } else {
      this.setState({
        ...this.state,
        loading: false,
        editProfile: this.props.profile,
      });

      if (this.props.profile.profilePicture) {
        Storage.get(this.props.profile.profilePicture)
          .then(result => {
            this.setState({
              ...this.state,
              profilePictureUrl: result.toString(),
            });
          })
          .catch(err => {
            console.error(err);
            dispatch(newAlert({
              level: AlertLevel.WARNING,
              body: 'Uh oh! There was an issue retrieving your profile picture'
            }));
          });
      }
    }
  }

  public componentWillReceiveProps(props: IProps): void {
    if (props.profile) {
      this.setState({
        ...this.state,
        loading: false,
        editProfile: props.profile,
      });

      if (props.profile.profilePicture) {
        Storage.get(props.profile.profilePicture)
          .then(result => {
            this.setState({
              ...this.state,
              profilePictureUrl: result.toString(),
            });
          })
          .catch(err => {
            console.error(err);
            this.props.dispatch(newAlert({
              level: AlertLevel.WARNING,
              body: 'Uh oh! There was an issue retrieving your profile picture'
            }));
          });
      }
    }
  }

  public scrollTop = (): void => {
    window.scrollTo(0, 0);
  }

  public editMain = (): void => {
    this.setState({
      ...this.state,
      editMain: true,
    });
  }

  public cancel = (): void => {
    const profile: any = this.props.profile;
    this.setState({
      ...this.state,
      editMain: false,
      editSkillsHave: false,
      editPassword: false,
      editSkillsWant: false,
      editSubjects: false,
      editExperience: false,
      editNewExperience: false,
      editEmergencyContacts: false,
      editProfile: profile,
    });
  }

  public saveProfile = (): void => {
    const { dispatch, profile }: { dispatch: Dispatch, profile: any } = this.props;
    const newProfile: any = this.state.editProfile;
    //  set state here to set editMain to be false
    this.setState({
      ...this.state,
      editMain: false,
      editSkillsHave: false,
      editSkillsWant: false,
      editSubjects: false,
      editEmergencyContacts: false,
      actionInProgress: true,
    }, async () => {
      try {
        //  if it resolves, then the profile will be different as
        //  it gets updated in the action, and props will deal with it
        await dispatch(updateProfileAction(newProfile));
      } catch (e) {
        //  if it rejects, revert it to the initial state.
        this.setState({
          ...this.state,
          editProfile: profile,
        });
      } finally {
        this.setState({
          ...this.state,
          actionInProgress: false,
        })
      }
    });
  }

  public changePassword = async (passwords: any): Promise<void> => {
    this.setState({
      ...this.state,
      editPassword: false,
      actionInProgress: true,
    });

    try {
      this.props.dispatch(newAlert({
        level: AlertLevel.SUCCESS,
        body: "Successfully updated password. Please relog in now.",
      }));
    } catch (e) {
      if (e.code === "LimitExceededException") {
        this.props.dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: "Too many attempts. Please try again later.",
        }));
      }
      else if (e.code === "NotAuthorizedException") {
        this.props.dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: "Incorrect old password.",
        }));
        this.setState({
          ...this.state,
          editPassword: true,
        });
      } else {
        this.props.dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: "Uh oh! Something went wrong when updating your password.",
        }));
      }
    } finally {
      this.setState({
        ...this.state,
        actionInProgress: false,
      });
    }
  }

  public editSection = (section: string): void => {
    this.setState({
      ...this.state,
      [section]: true,
    });
  }

  public saveNewExperience = (experience: any): void => {
    this.setState({
      ...this.state,
      editNewExperience: false,
      actionInProgress: true,
    }, async (): Promise<void> => {
      const dispatch: Dispatch = this.props.dispatch;
      try {
        await dispatch(addExperienceToProfile(this.state.editProfile.id, experience));
      } catch (e) {
        this.setState({
          ...this.state,
          editProfile: this.props.profile,
        });
      } finally {
        this.setState({
          ...this.state,
          actionInProgress: false,
        });
      }
    });
  };

  public saveNewContact = (contact: any): void => {
    const firstContact: boolean = !this.state.editProfile.completeness.emergencyContacts;
    this.setState({
      ...this.state,
      editEmergencyContacts: false,
      actionInProgress: true,
    }, async (): Promise<void> => {
      const dispatch: Dispatch = this.props.dispatch;
      try {
        await dispatch(addContactToProfile(this.state.editProfile.id, contact));
        if (firstContact) this.setState({
          ...this.state,
          showFinishedModal: true,
        })
      } catch (e) {
        this.setState({
          ...this.state,
          editProfile: this.props.profile,
        });
      } finally {
        this.setState({
          ...this.state,
          actionInProgress: false,
        });
      }
    });
  };

  public updateProfile = (key: string, value: any): void => {
    const newProfile: any = Object.assign({}, this.state.editProfile);
    newProfile[key] = value;
    this.setState({
      ...this.state,
      editProfile: newProfile,
    });
  }

  public handleSubjectsChange = (values: string[]): void => {
    const subjects: any = this.props.subjects;
    const selectedSubjects = Object.entries(subjects).filter(([id, title]): any => {
      return values.includes(id);
    });
    const newProfile = Object.assign({}, this.state.editProfile);
    newProfile.subjects = selectedSubjects.map(([id, title]) => ({ id, title }));
    this.setState({
      ...this.state,
      editProfile: newProfile,
    });
  }

  public setOpenExperience = (id: any): void => {
    this.setState({
      ...this.state,
      openExperience: id,
    });
  }

  public dismissFinishedModal = (): void => {
    this.setState({
      ...this.state,
      showFinishedModal: false,
    });
  }

  public updateMain = (formData: any): void => {
    this.setState({
      ...this.state,
      editMain: false,
      actionInProgress: true,
    }, async (): Promise<void> => {
      const dispatch: Dispatch = this.props.dispatch;
      try {
        if (formData.profilePicture) {
          const fileName: string = Base64.encode(formData.profilePicture.name + "-" + this.state.editProfile.id) + "." + formData.profilePicture.type.split("/")[1];
          await Storage.put(fileName, formData.profilePicture, {
            contentType: formData.profilePicture.type,
          });

          dispatch(newAlert({
            level: AlertLevel.SUCCESS,
            body: "Updated profile picture",
          }));
          formData.newPictureFileName = fileName;
        }

        await dispatch(editMainAction(this.state.editProfile.id, formData));
      } catch (e) {
        dispatch(newAlert({
          level: AlertLevel.WARNING,
          body: 'Uh oh! There was an issue uploading your profile picture.',
        }));

        this.setState({
          ...this.state,
          editProfile: this.props.profile,
        });
      } finally {
        this.setState({
          ...this.state,
          actionInProgress: false,
        });
      }
    });
  }

  public render(): React.ReactNode {
    const { classes } = this.props;
    const { editProfile, loading, } = this.state;

    const name: string = editProfile ? editProfile.student.firstName + ' ' + editProfile.student.lastName : '';
    const headline: string = editProfile ? editProfile.headline : '';
    const grade: string = editProfile ? "Grade " + editProfile.student.schoolYear : '';
    const school: string = editProfile ? editProfile.student.schools[0].name : '';
    const subjects: any[] = editProfile ? editProfile.subjects : [];
    const skillsIHave: any[] = editProfile ? editProfile.skillsIHave : [];
    const skillsIWant: any[] = editProfile ? editProfile.skillsIWant : [];
    const emergencyContacts: any[] = editProfile ? editProfile.emergencyContacts : [];
    const userExperience: any[] = editProfile ? this.state.editProfile.userExperience : [];
    const profilePicture: any = editProfile ? editProfile.profilePicture : null;
    const phone = editProfile ? editProfile.student.contactNumber : null;
    const dob = editProfile ? editProfile.student.dob : null;
    const location = editProfile ? editProfile.student.location : null;
    const initialMainData: any = {
      firstName: editProfile ? editProfile.student.firstName : '',
      lastName: editProfile ? editProfile.student.lastName : '',
      headline,
      grade: editProfile ? parseInt(editProfile.student.schoolYear, 10) : 0,
      dob,
      suburb: location,
      contactNumber: phone,
      gender: editProfile ? editProfile.student.gender : -1,
      profilePicture,
    };

    const completeness: any = editProfile ? editProfile.completeness : {
      about: false,
      subjects: false,
      skillsIHave: false,
      skillsIWant: false,
      emergencyContacts: false,
      overall: false,
    };

    const percentageComplete: number = completeness ? (
      (completeness.about ? 20 : 0) +
      (completeness.subjects ? 20 : 0) +
      (completeness.skillsIHave ? 20 : 0) +
      (completeness.skillsIWant ? 20 : 0) +
      (completeness.emergencyContacts ? 20 : 0)
    ) : 0;

    let currentStep: number = -1;
    let currentButton: any = null;

    switch (true) {
      case !completeness.about:
        currentStep = 0;
        currentButton = <Button onClick={this.editMain}>Add basic info</Button>
        break;
      case !completeness.skillsIHave:
        currentStep = 1;
        currentButton = (
          <Button onClick={(): void => this.editSection("editSkillsHave")}>
            Add some skills
          </Button>
        );
        break;
      case !completeness.skillsIWant:
        currentStep = 2;
        currentButton = (
          <Button onClick={(): void => this.editSection("editSkillsWant")}>
            Add some skills
          </Button>
        );
        break;
      case !completeness.subjects:
        currentStep = 3;
        currentButton = (
          <Button onClick={(): void => this.editSection("editSubjects")}>
            Add subjects
          </Button>
        );
        break;
      case !completeness.emergencyContacts:
        currentStep = 4;
        currentButton = (
          <Button onClick={(): void => this.editSection("editEmergencyContacts")}>
            Add contact
          </Button>
        );
        break;
      default:
        currentStep = 5;
        currentButton = null;
    };

    return (
      <MuiThemeProvider theme={theme}>
        <Layout noCreateOpportunity={true}>
          <DataLoader required>
            <div className={classes.root}>
              {loading === true ? (<CircularProgress size={48} />)
                :
                (<React.Fragment>
                  {/*! TODO change the edit.skills.modal to not use editOpportunity*/}
                  <EditSkillsModal
                    open={this.state.editSkillsHave}
                    pickedSkills={skillsIHave}
                    onChange={(values: any[]) => this.updateProfile("skillsIHave", values)}
                    onClose={this.cancel}
                    onSave={this.saveProfile}
                  />
                  <EditSkillsModal
                    open={this.state.editSkillsWant}
                    pickedSkills={skillsIWant}
                    onChange={(values: any[]) => this.updateProfile("skillsIWant", values)}
                    onClose={this.cancel}
                    onSave={this.saveProfile}
                  />
                  <PickSubjectsModal
                    selectedSubjects={subjects}
                    onChange={this.handleSubjectsChange}
                    onCancel={this.cancel}
                    onClose={this.cancel}
                    onSave={this.saveProfile}
                    open={this.state.editSubjects}
                  />
                  <AddContactModal
                    open={this.state.editEmergencyContacts}
                    onCancel={this.cancel}
                    onClose={this.cancel}
                    onSave={this.saveNewContact}
                  />
                  <NewExperienceModal
                    open={this.state.editNewExperience}
                    onCancel={this.cancel}
                    onClose={this.cancel}
                    onSave={this.saveNewExperience}

                  />
                  <EditProfileMain
                    open={this.state.editMain}
                    onCancel={this.cancel}
                    onSave={this.updateMain}
                    initialData={initialMainData}
                  />
                  <ChangePasswordModal
                    open={this.state.editPassword}
                    onSave={this.changePassword}
                    onClose={this.cancel}
                  />
                  <Dialog
                    open={this.state.showFinishedModal}
                    onClose={this.dismissFinishedModal}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">{"Congratulations! You're all done."}</DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        You've finished up your profile. Now you can begin applying for opportunities.
                        You'll be able to apply for any of the opportunities that appear on the listings
                        page.
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button color="primary" autoFocus onClick={this.dismissFinishedModal}>
                        Okay!
                      </Button>
                    </DialogActions>
                  </Dialog>
                  <div style={{ display: "flex", flexDirection: "row", width: "100%", alignItems: "center", marginTop: "1.0rem" }}>
                    <div style={{ justifyContent: 'flex-start', display: 'flex', flexDirection: 'column', width: '100%', marginBottom: '1.0rem' }}>
                      <Typography variant="display1">Profile</Typography>
                      <Typography variant="subheading">This is your profile. You can update your information here.</Typography>
                    </div>
                    <Button onClick={() => this.props.history.push('/')}>
                      <KeyboardArrowLeft style={{ marginRight: "0.5rem" }} />
                      Back
                    </Button>
                  </div>
                  {(currentStep < 5) &&
                    <Card style={{ flex: 1, marginBottom: '1.0rem', width: '100%' }} raised={true}>
                      <LinearProgress variant="determinate" value={percentageComplete} />
                      <CardHeader
                        title="Complete your profile"
                        subheader="Before you can apply for any opportunities, we need to know a little bit more about you."
                        style={{ paddingBottom: '0.5rem' }}
                      >
                      </CardHeader>
                      <CardContent style={{ paddingTop: 0, paddingBottom: 0 }}>
                        <Typography variant="caption" style={{ fontWeight: 500 }}>Next up: {progressPhrases[currentStep]}</Typography>
                      </CardContent>
                      <CardActions>
                        {currentButton}
                      </CardActions>
                    </Card>
                  }

                  <div className={classes.cardGroupOne}>
                    <Card className={classes.card} raised={true}>
                      <CardHeader
                        avatar={
                          <Avatar className={classes.avatar}>
                            {name.split(" ").map(s => s.substring(0, 1)).join("").toUpperCase()}
                          </Avatar>
                        }
                        action={
                          <IconButton onClick={this.editMain}>
                            <Edit />
                          </IconButton>
                        }
                        title={name}
                        subheader="Joined this year"
                      />
                      {!this.state.profilePictureUrl.includes("/public/null?") ?
                        <CardMedia
                          className={classes.media}
                          image={this.state.profilePictureUrl} />
                        : (
                          <CardContent
                            className={classes.media}
                          >

                            <Typography>No profile picture added yet.</Typography>
                          </CardContent>
                        )}
                      <CardContent style={{ flex: 1 }}>

                        <Typography>{headline ? headline : "No headline added yet."}</Typography>

                      </CardContent>
                      <CardActions className={classes.actions} disableActionSpacing style={{ paddingLeft: '24px', display: 'inline' }}>
                        <Chip className={classes.profileChip} label={grade} avatar={
                          <Avatar>
                            <Face />
                          </Avatar>
                        } />

                        <Chip className={classes.profileChip} label={school} avatar={
                          <Avatar>
                            <School />
                          </Avatar>
                        } />

                        {phone &&
                          (<Chip className={classes.profileChip} label={phone} avatar={
                            <Avatar>
                              <Phone />
                            </Avatar>
                          } />)
                        }
                        {dob &&
                          (<Chip className={classes.profileChip} label={dob} avatar={
                            <Avatar>
                              <Timer />
                            </Avatar>
                          } />)
                        }
                      </CardActions>

                    </Card>
                    <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
                      <Card style={{ marginBottom: '1.0rem' }} raised={true} className={!completeness.about ? classes.nonEditable : null}>
                        <CardContent>
                          <CardHeader
                            style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                            action={

                              <IconButton onClick={(): void => this.editSection("editSkillsHave")} disabled={!completeness.about}>
                                <Edit />
                              </IconButton>
                            }
                            title={"Skills I have"}
                            subheader="Skills I have acquired from school or other activites"
                          />

                          <div className={classes.sectionTwoChips}>
                            {skillsIHave.map((skill: any, index: number): React.ReactNode => (
                              <Chip key={skill.id}

                                className={classes.profileChip} label={skill.name} />
                            ))}
                            {skillsIHave.length === 0 && (
                              <Typography variant="subheading">No skills added yet.</Typography>
                            )}
                          </div>
                        </CardContent>
                      </Card>
                      <Card style={{ flex: 1 }} raised={true} className={!completeness.skillsIHave ? classes.nonEditable : null}>
                        <CardContent>

                          <CardHeader
                            style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                            action={
                              <IconButton onClick={(): void => this.editSection("editSkillsWant")} disabled={!completeness.skillsIHave}>
                                <Edit />
                              </IconButton>
                            }
                            title={"Skills I want"}
                            subheader="Skills I am looking to acquire in future"
                          />
                          <div className={classes.sectionTwoChips}>
                            {skillsIWant.map((skill: any, index: number): React.ReactNode => (
                              <Chip key={skill.id}

                                className={classes.profileChip} label={skill.name} />
                            ))}
                            {skillsIWant.length === 0 && (
                              <Typography variant="subheading">No skills added yet.</Typography>
                            )}
                          </div>
                        </CardContent>
                      </Card>
                    </div>
                  </div>

                  <Card
                    style={{ flex: 1, marginTop: '1.0rem', width: '100%', maxHeight: '320px' }}
                    raised={true}
                    className={!completeness.skillsIWant ? classes.nonEditable : null}
                  >
                    <CardContent>
                      <CardHeader
                        style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                        action={
                          <IconButton onClick={(): void => this.editSection("editNewExperience")} disabled={!completeness.skillsIWant}>
                            <Add />
                          </IconButton>
                        }
                        title={"Prior experience"}
                        subheader="My previous work experience"
                      />
                      <List dense={true}>
                        {userExperience.map((experience: any): React.ReactNode => (

                          <ListItem key={experience.id} style={{ paddingLeft: 0 }}>
                            <Avatar style={{ width: 32, height: 32, }} color={opportunityTypeArray[experience.opportunityType.id].color}>
                              {opportunityTypeArray[experience.opportunityType.id].icon}
                            </Avatar>
                            <ListItemText primary={experience.title} secondary={experience.company} />
                            <ListItemSecondaryAction>
                              <Chip
                                //color={opportunityTypeArray[experience.opportunityType.id].color}
                                avatar={

                                  <Avatar>
                                    {opportunityTypeArray[experience.opportunityType.id].icon}
                                  </Avatar>
                                }
                                label={experience.opportunityType.name} />
                            </ListItemSecondaryAction>
                          </ListItem>

                        ))}

                      </List>
                      {userExperience.length === 0 && <Typography variant="subheading">No prior experience added yet.</Typography>}
                    </CardContent>
                  </Card>

                  <Card
                    style={{ flex: 1, marginTop: '1.0rem', width: '100%', maxHeight: '320px' }}
                    raised={true}
                    className={!completeness.skillsIWant ? classes.nonEditable : null}
                  >
                    <CardContent>

                      <CardHeader
                        style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                        action={
                          <IconButton onClick={(): void => this.editSection("editSubjects")} disabled={!completeness.skillsIWant}>
                            <Edit />
                          </IconButton>
                        }
                        title={"Subjects"}
                        subheader="Subjects I am currently enrolled in or have studied previously"
                      />
                      <List style={{ maxHeight: '172px', overflow: 'scroll' }} dense={true}>
                        {subjects.map((subject: any, index: number): React.ReactNode => (
                          <React.Fragment key={subject.id}>
                            <ListItem style={{ paddingLeft: 0 }}>
                              <Avatar style={{ width: 32, height: 32 }}>
                                {index + 1}
                              </Avatar>
                              <ListItemText primary={subject.title} />
                            </ListItem>
                          </React.Fragment>
                        ))}
                        {subjects.length === 0 && <Typography variant="subheading">No subjects added yet.</Typography>}
                      </List>
                    </CardContent>
                  </Card>

                  <Card
                    style={{ flex: 1, marginTop: '1.0rem', width: '100%', marginBottom: '1.0rem' }}
                    raised={true}
                    className={!completeness.subjects ? classes.nonEditable : null}
                  >
                    <CardContent>
                      <CardHeader
                        style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                        action={
                          <IconButton onClick={(): void => this.editSection("editEmergencyContacts")} disabled={!completeness.subjects}>
                            <Add />
                          </IconButton>
                        }
                        title={"Emergency contacts"}
                        subheader="My contacts in case of an emergency"
                      />
                      {emergencyContacts.length > 0 &&
                        (<List dense={true}>
                          {emergencyContacts.map((contact: any): React.ReactNode => (
                            <React.Fragment key={contact.id}>
                              <ListItem style={{ paddingLeft: 0 }}>
                                <Avatar style={{ width: 32, height: 32 }}>
                                  <Face />
                                </Avatar>
                                <ListItemText primary={contact.firstName + ' ' + contact.lastName} secondary={contact.position} />
                                <ListItemSecondaryAction>
                                  <Chip
                                    avatar={
                                      <Avatar>
                                        <Email />
                                      </Avatar>

                                    }
                                    label={contact.phoneNumber} style={{ marginRight: '0.5rem' }} />
                                  <Chip
                                    avatar={

                                      <Avatar>
                                        <Call />
                                      </Avatar>
                                    }
                                    label={contact.email} />
                                </ListItemSecondaryAction>

                              </ListItem>
                            </React.Fragment>
                          ))}
                        </List>)}
                      {emergencyContacts.length === 0 && (
                        <Typography variant="subheading">No contacts added yet.</Typography>
                      )}
                    </CardContent>
                  </Card>

                  <Card
                    style={{ flex: 1, marginTop: '1.0rem', width: '100%', marginBottom: '1.0rem' }}
                    raised={true}
                  >
                    <CardContent>
                      <CardHeader
                        style={{ padding: 0, paddingBottom: '0.5em', paddingRight: '0.5rem' }}
                        title={"Account"}
                        subheader="Change your password, or deactivate your account."
                      />
                      <Button variant="raised" style={{ marginRight: '0.5rem' }} color="primary" onClick={(): void => this.editSection("editPassword")}>Change password</Button>
                      <Button variant="raised" color="secondary" disabled>Deactivate account</Button>
                    </CardContent>
                  </Card>

                  {this.state.actionInProgress &&
                    <CircularProgress
                      style={{
                        position: 'fixed',
                        bottom: '1.0rem',
                        left: '1.0rem',
                      }}
                      size={48}
                      color="secondary" />
                  }

                </React.Fragment>)}
            </div>
          </DataLoader>
        </Layout>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state: any): any => ({
  user: state.user,
  profile: state.profile.data || null,
  loading: state.globals.loading,
  subjects: state.data.subjects || {},
});

export default withRouter((withStyles(styles)(connect(mapStateToProps)(ProfilePage))) as any);
