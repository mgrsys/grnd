import * as React from 'react';

import { Redirect } from 'react-router-dom';

// Components
import AuthRequired from '@components/AuthRequired';
import OrganisationDashboardPage from '@pages/organisation/OrganisationDashboardPage';
import SchoolDashboardPage from '@pages/school/SchoolDashboardPage';
import StudentDashboardPage from '@pages/student/StudentDashboardPage';
// Utils
import { getUserRole } from '@utils/user';

class DashboardPage extends React.Component<any, any> {
  public renderDashboard = () => {
    const { user } = this.props;
    let dashboard: any = null;
    switch (getUserRole(user)) {
      case 'student': dashboard = <StudentDashboardPage />; break;
      case 'organisation': dashboard = <OrganisationDashboardPage />; break;
      case 'school': dashboard = <SchoolDashboardPage />; break;
      default:
        console.info('User role unknown, redirecting to login', user, getUserRole(user));
      break;
    }
    if (!dashboard) return this.renderRedirect();

    return (
      <AuthRequired noAuth={<Redirect to="/signin" />}>
        {dashboard}
      </AuthRequired>
    );
  }
  public renderRedirect = () => {
    return <Redirect to="/signin" />;
  }
  public render() {
    return this.renderDashboard();
  }
}

export default DashboardPage;
