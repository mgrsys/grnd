import { grey } from '@material-ui/core/colors';
import * as React from 'react';
import styled from 'styled-components';

// Component
import Layout from '@components/Layout';
import OpportunityListNew from '@pages/opportunity/components/OpportunityList';

// Utils
import { Snackbar, Button, CircularProgress } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { getProfileAction } from '@actions/profile';
import { fetchOpportunities } from '@actions/opportunity';
import { Opportunity } from '@models/opportunity';

const Dashboard: any = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${grey[100]};
`;

interface IProps {
  opportunities: Opportunity[];
  profile: any;
  dispatch: Dispatch;
}

class DashboardComponent extends React.Component<IProps, any> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {
    if (!this.props.opportunities) {
      this.props.dispatch(fetchOpportunities());
    }

    if (!this.props.profile) {
      this.props.dispatch(getProfileAction());
    }
  }

  public render() {
    const { profile } = this.props;
    const profileComplete: boolean = profile ? profile.completeness.overall : false;

    return (
      <Layout noCreateOpportunity accessToProfile page="opportunityList" onlyActive>
        <Dashboard>
          {!this.props.opportunities || !this.props.profile ? <CircularProgress size={48} style={{ marginTop: '1.0rem' }} /> :
            (<OpportunityListNew
              opportunities={this.props.opportunities}
              showDuration showAge showExploreButton showApplyButton onlyActive />)
          }
        </Dashboard>
        {this.props.profile && !profileComplete &&
          <Snackbar
            open={true}
            message={<span>Your profile isn't quite complete. <Link to="profile"><Button color="secondary">Complete now</Button></Link></span>}
            anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          />
        }
      </Layout>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  profile: state.profile.data,
  opportunities: state.opportunities.opportunityList,
});

export default connect(mapStateToProps)(DashboardComponent);
