import { Button } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { Auth } from 'aws-amplify';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';

// Component
import AuthRequired from '@components/AuthRequired';

// Utils
import { history } from '@utils/history';

const ProfilePage = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${grey[100]};
`;

class ProfilePageComponent extends React.Component<any, any> {
  public signOut() {
    Auth.signOut()
      .then(() => {
        return history.push('/signin');
      });
  }
public render() {
    return (
      <ProfilePage>
        <AuthRequired noAuth={<Redirect to="/signin" />}>
          Sign in succesfull
        </AuthRequired>
        <Button color="primary" variant="raised" onClick={this.signOut}>Sign Out</Button>
      </ProfilePage>
    );
  }
}

export default ProfilePageComponent;
