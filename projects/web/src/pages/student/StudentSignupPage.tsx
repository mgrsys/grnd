import { amber } from '@material-ui/core/colors';
import * as React from 'react';
import styled from 'styled-components';

// Components
import SignupForm from './SignupForm/index';

const SignupPage = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${amber[400]};
`;

class SignupPageComponent extends React.Component<any, any> {
public render() {
    return (
      <SignupPage>
        <SignupForm />
      </SignupPage>
    );
  }
}

export default SignupPageComponent;
