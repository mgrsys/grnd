import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Button,
  Paper,
  Typography,
} from '@material-ui/core';
import {
  Add,
  ChevronLeft,
} from '@material-ui/icons';
import {
  MuiThemeProvider,
  createMuiTheme,
} from '@material-ui/core/styles';
import {
  grey
} from '@material-ui/core/colors';

import {
  createContact,
} from '@actions/contacts';
import Layout from '@components/Layout';
import { AlertLevel } from '@components/global.model';
import { getUserRole } from '@utils/user';
import { newAlert } from '@actions/global';
import NewContact from './NewContact';

export const theme = createMuiTheme({
  palette: {
    primary: { main: '#2626F7' },
  },
});

class ContactsPage extends React.Component<any, any> {
  state = {
    newContactModalShown: false,
  };

  // prevent to see the page for everyone who is not a school user
  componentWillMount() {
    const { user } = this.props;

    if (getUserRole(user) !== 'school') {
      this.props.history.push('/');
    }
  }

  goBack = () => {
    this.props.history.push('/');
  };

  showModal = () => {
    this.setState({ newContactModalShown: true });
  };

  hideModal = () => {
    this.setState({ newContactModalShown: false });
  };

  addContact = (data) => () => {
    this.props.dispatch(createContact(data))
      .then(() => {
        this.hideModal();

        this.props.dispatch(newAlert({
          level: AlertLevel.SUCCESS,
          body: 'New organisation user has successfully created',
        }));
      })
      .catch(() => {
        this.hideModal();

        this.props.dispatch(newAlert({
          level: AlertLevel.ERROR,
          body: 'An error occured while creating new organisation user',
        }));
      });
  };

  render() {
    const {
      newContactModalShown
    } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Layout
          page="exploreContacts"
        >
          <NewContact
            open={newContactModalShown}
            onClose={this.hideModal}
            onChange={this.addContact}
          />

          { /* TODO so, move this to separate component as it's already described as RootBar component */ }
          { /* But client said that this functionality is temporary and in future we will have brand new dashboard for the purposes */ }
          <div
            style={{
              display: 'flex',
              flexBasis: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '16px',
              marginBottom: '8px',
            }}
          >
            <div>
              <Button
                style={{
                  color: grey[500],
                  fontWeight: 400,
                  letterSpacing: '1.2px',
                }}
                onClick={this.goBack}
              >
                <ChevronLeft style={{ marginRight: '0.5rem' }} /> Go Back
              </Button>
              <Typography
                variant="headline"
                component="p"
                style={{
                  display: 'inline-block',
                  fontWeight: 400,
                  verticalAlign: 'middle',
                  fontSize: '20px',
                  fontFamily: 'Raleway, sans-serif',
                  marginLeft: '12px',
                }}
              >
                Contacts
              </Typography>
            </div>

            <div>
              <Button
                style={{
                  color: grey[500],
                  fontWeight: 400,
                  letterSpacing: '1.2px',
                }}
                onClick={this.showModal}
              >
                <Add
                  style={{
                    marginRight: '0.5rem',
                    color: '#000DFF',
                    opacity: 0.87
                  }}
                />
                ADD NEW
              </Button>
            </div>
          </div>

          <Paper style={{ padding: '24px' }}>
            <Typography variant="body2">
              Industry partners
            </Typography>
          </Paper>
        </Layout>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.user,
  };
};

export default withRouter(
  connect(
    mapStateToProps,
  )(ContactsPage)
);
