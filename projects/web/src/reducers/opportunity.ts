import {
  UPDATE_EXPLORE_VIEW,
  UPDATE_FILTERS,
  SET_OPPORTUNITY,
  UPDATE_EDIT_MODE,
  SET_OPPORTUNITIES,
} from '@actions/opportunity';

const opportunities = (state = {}, action) => {
  switch (action.type) {
    case UPDATE_FILTERS:
      return {
        ...state,
        filterOptions: {
          active: action.filterOptions.active,
          closed: action.filterOptions.closed,
          opportunityTypes: action.filterOptions.opportunityTypes,
          searchString: action.filterOptions.searchString,
        },
      };
    case UPDATE_EXPLORE_VIEW:
      return {
        ...state,
        exploreView: action.exploreView,
      };
    case SET_OPPORTUNITY:
      return {
        ...state,
        opportunity: action.opportunity,
      };
    case UPDATE_EDIT_MODE:
      return {
        ...state,
        editMode: action.editMode,
      };
    case SET_OPPORTUNITIES: {
      return {
        ...state,
        opportunityList: action.opportunities,
      };
    }
    default:
      return state;
  }
};

export default {
  opportunities,
};
