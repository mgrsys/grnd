import {
  SET_PROFILE,
} from '@actions/profile';

import {
  ProfileState,
} from '../models/profile';

const defaultState: ProfileState = {
};

const profile = (state = defaultState, action: any): ProfileState => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        data: action.profile,
      };
    default:
      return state;
  }
};

export default {
  profile,
};
