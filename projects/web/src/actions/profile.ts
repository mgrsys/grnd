import { Dispatch } from 'redux';

import { AlertLevel } from '@components/global.model';
import { setLoading, newAlert } from '@actions/global';
import { getProfile, putProfile, postExperience, postContact, editProfileMain } from '@utils/api';

export const SET_PROFILE = 'SET_PROFILE';

export const setProfile = (profile: any): any => ({
  type: SET_PROFILE,
  profile,
});

export const getProfileAction = (): any => async (dispatch: Dispatch): Promise<any> => {
  dispatch(setLoading(true));
  try {
    const profile: any = await getProfile();

    if (profile.code !== 200) {
      throw profile.error;
    }

    dispatch(setProfile(profile.data));
  } catch (e) {
    console.error(e);
    dispatch(newAlert({
      level: AlertLevel.ERROR,
      body: 'Uh oh! Error retrieving profile.',
    }));
  } finally {
    dispatch(setLoading(false));
  }
};

export const updateProfileAction = (profile: any): any => (dispatch: Dispatch): Promise<void> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const profileResponse: any = await putProfile(profile, profile.id);
      if (profileResponse.code !== 200) {
        throw profileResponse.error;
      }
      dispatch(setProfile(profileResponse.data));
      dispatch(newAlert({
        level: AlertLevel.SUCCESS,
        body: 'Successfully updated profile.',
      }));
      resolve();
    } catch (e) {
      dispatch(newAlert({
        level: AlertLevel.ERROR,
        body: 'Uh oh! Error updating profile.',
      }));
      reject();
    }
  });
};

export const addExperienceToProfile = (id: number, experience: any): any => (dispatch: Dispatch): Promise<void> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const profileWithExperienceResponse: any = await postExperience(experience, id);
      if (profileWithExperienceResponse.code !== 200) {
        throw profileWithExperienceResponse.error;
      }
      dispatch(setProfile(profileWithExperienceResponse.data));
      dispatch(newAlert({
        level: AlertLevel.SUCCESS,
        body: 'Successfully added new work experience.',
      }));
      resolve();
    } catch (e) {
      dispatch(newAlert({
        level: AlertLevel.ERROR,
        body: 'Uh oh! Error adding new work experience.',
      }));
      reject();
    }
  });
};

export const addContactToProfile = (id: number, contact: any): any => (dispatch: Dispatch): Promise<void> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const profileWithContactResponse: any = await postContact(contact, id);
      if (profileWithContactResponse.code !== 200) {
        throw profileWithContactResponse.error;
      }
      dispatch(setProfile(profileWithContactResponse.data));
      dispatch(newAlert({
        level: AlertLevel.SUCCESS,
        body: 'Successfully added new emergency contact.',
      }));
      resolve();
    } catch (e) {
      dispatch(newAlert({
        level: AlertLevel.ERROR,
        body: 'Uh oh! Error adding new emergency contact.',
      }));
      reject();
    }
  });
};

export const editMainAction = (id: number, formData: any): any => (dispatch: Dispatch): Promise<void> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    try {
      const updatedProfile: any = await editProfileMain(formData, id);
      if (updatedProfile.code !== 200) {
        throw updatedProfile.error;
      }
      dispatch(setProfile(updatedProfile.data));
      dispatch(newAlert({
        level: AlertLevel.SUCCESS,
        body: 'Successfully updated profile.',
      }));
      resolve();
    } catch (e) {
      dispatch(newAlert({
        level: AlertLevel.ERROR,
        body: 'Uh oh! Error updated profile.',
      }));
      reject();
    }
  });
};
