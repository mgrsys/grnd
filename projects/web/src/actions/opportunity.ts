import { deleteOpportunity, getOpportunity, updateOpportunity, postApplication, getOpportunities } from '@utils/api';
import { Dispatch } from 'redux';

import { newAlert, setLoading } from '@actions/global';
import { AlertLevel } from '@components/global.model';
import { OpportunityResponse, Opportunity } from '@models/opportunity';

export const UPDATE_FILTERS: string = 'UPDATE_FILTERS';
export const UPDATE_EXPLORE_VIEW: string = 'UPDATE_EXPLORE_VIEW';
export const SET_OPPORTUNITY: string = 'SET_OPPORTUNITY';
export const UPDATE_EDIT_MODE: string = 'UPDATE_EDIT_MODE';
export const SET_OPPORTUNITIES: string = 'SET_OPPORTUNITIES';

export const updateFilters = (filterOptions: any): any => {
  return {
    filterOptions,
    type: UPDATE_FILTERS,
  };
};

export const updateExploreView = (exploreView: number): any => {
  return {
    exploreView,
    type: UPDATE_EXPLORE_VIEW,
  };
};

export const setOpportunity = (opportunity: Opportunity): any => {
  return {
    opportunity,
    type: SET_OPPORTUNITY,
  }
};

export const setOpportunities = (opportunities: any): any => {
  return {
    opportunities: opportunities,
    type: SET_OPPORTUNITIES,
  };
};

export const fetchOpportunity = (id: number): any => async (dispatch: Dispatch): Promise<void> => {
  try {
    dispatch(setLoading(true));
    const opportunity: OpportunityResponse = await getOpportunity(id);
    if (opportunity.error === {}) throw opportunity.error;
    dispatch(setOpportunity(opportunity.data));
    dispatch(setLoading(false));
  } catch (e) {
    console.error(e);
    dispatch(newAlert({
      level: AlertLevel.ERROR,
      body: 'An error occured when fetching the opportunity',
    }))
  }
};

export const removeOpportunity = (id: number): any => (dispatch: Dispatch): Promise<void> => {
  return new Promise(async (resolve: Function, reject: Function): Promise<void> => {
    dispatch(setLoading(true));

    try {
      await deleteOpportunity(id);
    } catch (e) {
      console.error(e);
      dispatch(newAlert({
        level: AlertLevel.ERROR,
        body: "An error occured when deleting the opportunity",
      }));
      dispatch(setLoading(false));
      reject();
    }
    dispatch(fetchOpportunities());
    dispatch(setLoading(false));
    dispatch(newAlert({
      level: AlertLevel.SUCCESS,
      body: "Successfully deleted the opportunity",
    }))
    resolve();
  })
};

export const sendUpdateOpportunity = (opportunity: Opportunity): any => async (dispatch: Dispatch): Promise<void> => {
  try {
    dispatch(setLoading(true));

    // prepare data
    opportunity.skillIds = opportunity.skills.map((skill) => skill.id);
    delete opportunity.skills;

    opportunity.timePeriodIds = opportunity.time_periods.map((period) => period.id);

    await updateOpportunity(opportunity.id, opportunity);
    dispatch(newAlert({
      level: AlertLevel.SUCCESS,
      body: "Successfully updated the opportunity",
    }))
    dispatch(fetchOpportunities());
  } catch (e) {
    dispatch(newAlert({
      level: AlertLevel.ERROR,
      body: "An error occured when deleting the opportunity",
    }));
    dispatch(setLoading(false));
  }
}

export const newApplication = (id: number): any => async (dispatch: Dispatch): Promise<void> => {
  try {
    dispatch(setLoading(true));
    await postApplication(id);
  } catch (e) {
    console.error(e);
    dispatch(newAlert({
      level: AlertLevel.ERROR,
      body: "An error occured when applying for this opportunity",
    }))
    dispatch(setLoading(false));
    return;
  }

  dispatch(fetchOpportunities());
  dispatch(newAlert({
    level: AlertLevel.SUCCESS,
    body: "Successfully applied for this opportunity",
  }));
};

export const setEditMode = (editMode: boolean): any => {
  return {
    editMode,
    type: UPDATE_EDIT_MODE,
  };
};

export const fetchOpportunities = (): any => async (dispatch: Dispatch): Promise<any> => {
  try {
    dispatch(setLoading(true));
    const response: any = await getOpportunities();

    const opportunities: any = {};
    response.data.forEach((opp: Opportunity): any => opportunities[opp.id] = opp);

    dispatch(setOpportunities(opportunities));
    dispatch(setLoading(false));
  } catch (e) {
    console.error(e);
    dispatch(newAlert({
      level: AlertLevel.ERROR,
      body: "An error occured when fetching all opportunities",
    }))
    dispatch(setLoading(false));
    return;
  }
};
